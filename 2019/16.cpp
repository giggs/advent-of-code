#include "my_functions.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <numeric>

// Very inefficient method. 
// Probably look up Fast Fourier Transform for something better



void Sixteen()
{
	constexpr int OriginalSize = 4;
	int OriginalPattern[OriginalSize] = { 0, 1, 0, -1 };

	std::vector<std::vector<int>> Patterns;

	std::ifstream File;
	File.open("16.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 16";
		return;
	}
	
	std::vector<int> Input;
	char c;
	while ((c = File.get()) != '\n')
	{
		Input.push_back(c-'0');

	}
	File.close();
	std::vector<int> P2Input;

	for (int i = 0; i < 10000; i++)
	{
		P2Input.insert(P2Input.end(), Input.begin(), Input.end());
	}

	int Offset = 0;
	for (int i = 0; i < 7; i++)
	{
		Offset *= 10;
		Offset += Input[i];
	}

	std::vector<int> TrimmedP2Input(P2Input.begin() + Offset, P2Input.end());

	// Creating patterns for part1

	for (int i = 1; i < Input.size()+1; i++)
	{
		std::vector<int> Pattern;
		while (Pattern.size() < Input.size() + 1)
		{
			for (int k = 0; k < OriginalSize; k++)
			{
				for (int j = 0; j < i; j++)
				{
					Pattern.push_back(OriginalPattern[k]);
				}
			}
		}
		Patterns.push_back(Pattern);
	}

	for (int Phase = 0; Phase < 100; Phase++)
	{
		std::vector<int> New;
		for (int i = 0; i < Input.size(); i++)
		{
			int NewDigit = 0;
			for (int j = 0; j < Input.size(); j++)
			{
				NewDigit += Input[j] * Patterns[i][j + 1];
			}
			NewDigit %= 10;
			New.push_back(NewDigit < 0 ? -NewDigit : NewDigit);
		}
		Input = move(New);
	}

	for (int i = 0; i < 8; i++)
	{
		std::cout << Input[i];
	}
	std::cout << " ";

	// Notice that the offset leads into the back half of the input
	// The back half digits are always the sum of the remaining digits % 10
	// See on the example page, 8th digit is constant. 7th is 7th+8th etc...

	std::reverse(TrimmedP2Input.begin(), TrimmedP2Input.end());

	for (int Phase = 0; Phase < 100; Phase++)
	{
		int Sum = 0;
		for (auto i = 0; i < TrimmedP2Input.size(); i++)
		{
			Sum = (Sum + TrimmedP2Input[i])%10;
			TrimmedP2Input[i] = Sum;
		}
	}

	for (auto i = TrimmedP2Input.size()-1; i > TrimmedP2Input.size()-9; i--)
	{
		std::cout << TrimmedP2Input[i];
	}
	std::cout << " ";
}
