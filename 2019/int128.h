#ifndef INT128_H
#define INT128_H

#include <stdint.h>
#include <string>
#include <iostream>

struct Int128
{
    Int128();
    Int128(int a);
    Int128(unsigned int a);
    Int128(int64_t a);
    Int128(uint64_t a);
    Int128(int64_t a, uint64_t b);
    Int128(const Int128& a);
    Int128 operator  - () const;
    Int128 operator  = (const Int128& a);
    Int128 operator += (const Int128& a);
    Int128 operator  - (const Int128& a) const;
    Int128 operator -= (const Int128& a);
    Int128 operator *= (const Int128& a);
    Int128 operator  / (const Int128& a) const;
    Int128 operator /= (const Int128& a);
    Int128 operator  % (const Int128& a) const;
    Int128 operator %= (const Int128& a);
    int sign() const;
    int64_t  Hi;
    uint64_t Lo;
};

Int128 operator  + (const Int128& l, const Int128& r);
Int128 operator  * (const Int128& l, const Int128& r);
bool   operator == (const Int128& l, const Int128& r);
bool   operator != (const Int128& l, const Int128& r);
int cmp128(const Int128& a, const Int128& b);
std::ostream& operator << (std::ostream& ost, const Int128& v);


#endif 

