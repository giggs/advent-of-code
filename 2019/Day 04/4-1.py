min = 124075
max = 580769

passwords = []

for number in range(min,max):
    split_number = [int(x) for x in list(str(number))]
    decreases = False
    has_double = False
    for i in range(0,5):
        if split_number[i+1] < split_number[i]:
            decreases = True
        if split_number[i] == split_number[i+1]:
            has_double = True
    if decreases == False and has_double == True:
        passwords.append(number)

print(len(passwords))
    