min = 124075
max = 580769

passwords = []

for number in range(min,max):
    split_number = [int(x) for x in list(str(number))]
    decreases = False
    has_double = False
    
    i = 0
    while i < 5:
        if split_number[i+1] < split_number[i]:
            decreases = True
            break
        i += 1

    i = 0
    if decreases == False:
        while i < 5:
            if split_number[i] == split_number[i+1]:
                if i == 4 and split_number[i] != split_number [i-1]:
                    has_double = True
                elif i in range(1,4) and split_number[i+2] != split_number[i+1] and split_number[i] != split_number [i-1]:
                    has_double = True
                elif i == 0 and split_number[i+2] != split_number[i+1]:
                    has_double = True
            i += 1     

    if decreases == False and has_double == True:
        passwords.append(number)

print(len(passwords))