#include <map>
#include "my_functions.h"
#include "intcode.h"

std::map<Point2<int>, bool> PaintHull(std::vector<int64_t> & Instructions, bool InitialColor = 0)
{
	std::map<Point2<int>, bool> Panels;
	std::vector<Point2<int>> TurnLeft = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
	std::vector<Point2<int>> TurnRight = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };

	PaintingRobot Robot;
	Robot.Instructions = Instructions;
	Robot.Input.push_back(InitialColor);
	Point2<int> RobotDirection = { 1, 0 };
	Point2<int> CurrentPanel = { 0,0 };
	int64_t Result = 1;

	do
	{
		Result = Robot.RunProgram();
		Panels.insert_or_assign(CurrentPanel, Robot.Output.back());
		Result = Robot.RunProgram();
		std::vector<Point2<int>> Turn = (Robot.Output.back() == 0) ? TurnLeft : TurnRight;
		for (size_t i = 0; i < Turn.size(); i++)
		{
			if (Turn[i] == RobotDirection)
			{
				if (i == 3)
				{
					RobotDirection = Turn[0];
				}
				else
				{
					RobotDirection = Turn[i + 1];
				}
				break;
			}

		}
		CurrentPanel = CurrentPanel + RobotDirection;
		auto Search = Panels.find(CurrentPanel);
		if (Search == Panels.end())
		{
			Robot.Input.push_front(0);
		}
		else
		{
			Robot.Input.push_front(Search->second);
		}
	} while (Result);

	return Panels;
}

void Eleven()
{
	std::ifstream File;
	File.open("11.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 11";
		return;
	}
	
	std::vector<int64_t> Program;
	ParseProgram(File, Program);
	File.close();
	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Need to do better than that 
	
	std::map<Point2<int>, bool> Panels = PaintHull(Program);
	std::cout << Panels.size() << "\n";
	Panels = PaintHull(Program, 1);
	int Min_y = INT_MAX;
	int Max_y = INT_MIN;
	int Min_x = INT_MAX;
	int Max_x = INT_MIN;
	for (auto It = Panels.begin(); It != Panels.end(); It++)
	{
		Point2<int> Temp = It->first;
		Min_y = Temp.y < Min_y ? Temp.y : Min_y;
		Min_x = Temp.x < Min_x ? Temp.x : Min_x;
		Max_y = Temp.y > Max_y ? Temp.y : Max_y;
		Max_x = Temp.x > Max_x ? Temp.x : Max_x;
	}

	for (int x = Max_x; x >= Min_x; x--)
	{
		for (int y = Min_y; y < Max_y-1; y++)
		{
			auto Search = Panels.find({ x,y });
			if ((Search == Panels.end()) || Search->second == 0)
			{
				std::cout << " ";
			}
			else
			{
				std::cout << "#";
			}
		}
		std::cout << '\n';
	}
}