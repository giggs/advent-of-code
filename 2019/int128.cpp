// Modified from pgl10's which had the most correct implementation I've seen
// https://codes-sources.commentcamarche.net/source/100764-int128-une-bibliotheque-pour-entiers-sur-128-bits

#include "int128.h"

constexpr uint64_t ALLBITS = 0xFFFFFFFFFFFFFFFF;
constexpr uint64_t HIGHBIT = 0x8000000000000000;

Int128::Int128() : Hi(0), Lo(0) {}
Int128::Int128(int a) : Hi(-(a<0)), Lo(a) {}
Int128::Int128(unsigned int a) : Hi(0), Lo(a) {}
Int128::Int128(int64_t a) : Hi(-(a<0)), Lo(a) {}
Int128::Int128(uint64_t a) : Hi(0), Lo(a) {}
Int128::Int128(int64_t a, uint64_t b) : Hi(a), Lo(b) {}
Int128::Int128(const Int128& a) : Hi(a.Hi), Lo(a.Lo) {}

Int128 Int128::operator - () const 
{
    Int128 Temp;
    Temp.Lo = (Lo ^ ALLBITS) + 1;
    Temp.Hi = (Hi ^ ALLBITS) + (Lo==0);
    return Temp;
}

Int128 Int128::operator = (const Int128& a) 
{ 
    Hi=a.Hi; 
    Lo=a.Lo; 
    return *this; 
}

Int128 Int128::operator += (const Int128& a) 
{
    uint64_t Temp = Lo;
    Lo += a.Lo;
    Hi += a.Hi + (Lo < Temp);
    return *this;
}

Int128 Int128::operator - (const Int128& a) const 
{
    Int128 d;
    d.Lo = Lo - a.Lo;
    d.Hi = Hi - a.Hi - (d.Lo > Lo);
    return d;
}

Int128 Int128::operator -= (const Int128& a) 
{
    uint64_t Temp = Lo;
    Lo -= a.Lo;
    Hi -= a.Hi + (Lo > Temp);
    return *this;
}

Int128 Int128::operator *= (const Int128& a) 
{
    Int128 Temp;
    Temp.Hi = Hi;
    Temp.Lo = Lo;
    Temp = Temp * a;
    Lo = Temp.Lo;
    Hi = Temp.Hi;
    return *this;
}

void shr128(int64_t* hi, uint64_t* lo) 
{
    *lo = (*lo >> 1) | (*hi << 63);
    *hi = *hi >> 1;
}

Int128 Int128::operator / (const Int128& a)  const 
{
    Int128 d, n, q, d2, p2;
    int s;
    d = a;
    if ((d.Hi == 0) && (d.Lo == 0))
    {
        std::cout << "Division by 0" << std::endl;
        exit(EXIT_FAILURE);
    }
    n.Hi = Hi;
    n.Lo = Lo;
    s = 1;
    if(n.sign() < 0) 
    {
        n = -n;
        s = -s;
    }
    if(d.sign() < 0) 
    {
        d = -d;
        s = -s;
    }
    if (cmp128(n, d) < 0)
    {
        return Int128(0);
    }
    if ((d.Hi == 0) && (d.Lo == 1))
    {
        return *this;
    }
    q = 0;
    p2 = 1;
    d2 = d;
    n = n - d2;
    while(!(cmp128(n, d2) < 0)) 
    {
        n = n - d2;
        d2 = d2 + d2;
        p2 = p2 + p2;
    }
    n = n + d2;
    while((p2.Hi != 0) || p2.Lo != 0) 
    {
        if(!(cmp128(n, d2) < 0)) 
        {
            n = n - d2;
            q = q + p2;
        }
        shr128(&p2.Hi, &p2.Lo);
        shr128(&d2.Hi, &d2.Lo);
    }
    if (s < 0)
    {
        q = -q;
    }
    return q;
}

Int128 Int128::operator /= (const Int128& a) 
{
    Int128 Temp = *this;
    Temp = Temp / a;
    Lo = Temp.Lo;
    Hi = Temp.Hi;
    return *this;
}

Int128 Int128::operator % (const Int128& a) const 
{
    Int128 q, Temp;
    Temp.Hi = Hi;
    Temp.Lo = Lo;
    q = Temp / a;
    return  Temp - (q * a);
}

Int128 Int128::operator %= (const Int128& a) 
{
    Int128 Temp = *this;
    Temp = Temp % a;
    Lo = Temp.Lo;
    Hi = Temp.Hi;
    return *this;
}

int Int128::sign() const 
{
    if ((Hi == 0) && (Lo == 0))
    {
        return 0;
    }
    if ((Hi & HIGHBIT) == 0)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

Int128 operator + (const Int128& l, const Int128& r) 
{
    Int128 s;
    s.Lo = l.Lo + r.Lo;  
    s.Hi = l.Hi + r.Hi + (s.Lo < r.Lo);
    return s;
}

void mulul64(uint64_t u, uint64_t v, uint64_t* whi, uint64_t* wlo) 
{
//  From the Hacker's Delight collection : Montgomery multiplication. 
//  Multiply unsigned long 64-bit routine, i.e., 64 * 64 => 128.
//  u and v are multiplied and the 128-bit product is placed in (*whi, *wlo).
//  It is Knuth's Algorithm M from [Knu2] section 4.3.1.
    uint64_t u0, u1, v0, v1, k, Temp;
    uint64_t w0, w1, w2;
    u1 = u >> 32; u0 = u & 0xFFFFFFFF;
    v1 = v >> 32; v0 = v & 0xFFFFFFFF;
    Temp = u0*v0;
    w0 = Temp & 0xFFFFFFFF;
    k = Temp >> 32;
    Temp = u1*v0 + k;
    w1 = Temp & 0xFFFFFFFF;
    w2 = Temp >> 32;
    Temp = u0*v1 + w1;
    k = Temp >> 32;
    *wlo = (Temp << 32) + w0;
    *whi = u1*v1 + w2 + k;
    return;
}

Int128 operator * (const Int128& l, const Int128& r) 
{
    Int128 Temp, u, v;
    int su, sv;
    u = l; su = 1;
    if(u.sign() < 0) 
    {
        u = -l; 
        su = -1;
    }
    v = r; sv = 1;
    if(v.sign() < 0) 
    {
        v = -r; 
        sv = -1;
    }
    uint64_t ulo, vlo, thi, tlo;
    ulo = u.Lo;
    vlo = v.Lo;
    mulul64(ulo, vlo, &thi, &tlo);
    Temp.Lo = tlo;
    Temp.Hi = thi + (u.Hi * vlo) + (v.Hi * ulo);
    if (su == sv)
    {
        return Temp;
    }
    else
    {
        return -Temp;
    }
}

bool operator == (const Int128& l, const Int128& r) 
{
    return (l.Lo == r.Lo) && (l.Hi == r.Hi);
}

bool operator != (const Int128& l, const Int128& r) 
{
    return (l.Lo != r.Lo) || (l.Hi != r.Hi);
}

int cmp128(const Int128& a, const Int128& b) 
{
    return (a - b).sign();
}

std::ostream& operator << (std::ostream& ost, const Int128& v) 
{
   Int128 Temp = v;
   if (Temp.Hi && HIGHBIT)
   {
       ost << '-';
       Temp = -Temp;
   }
   if (Temp.Hi != 0)
   {
       ost << Temp.Hi;
   }
   ost << Temp.Lo;
   return ost;
}