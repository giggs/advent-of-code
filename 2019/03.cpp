#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include "my_functions.h"

// NOTE(giggs) Could probably be faster if we deal with segments instead of points,
// the vectors here are pretty big (150K each)
// Indeed, this solution here does this https://github.com/henrikglass/AoC-2019/blob/master/sol3.cpp

struct Instruction
{
	int dx;
	int dy;
	int Steps;
};

std::vector<Point2<int>> Wire1Coordinates;
std::vector<Point2<int>> Wire2Coordinates;

void PlaceWire(std::vector<Point2<int>> & WireCoordinates, Point2<int> & Wire, Instruction & Instruction)
{
	while (Instruction.Steps > 0)
	{
		Wire.x += Instruction.dx;
		Wire.y += Instruction.dy;
		WireCoordinates.push_back(Wire);
		//WireMap.insert({ Wire, ++Steps });
		Instruction.Steps--;
	}
}

void ParseWire(std::ifstream& Input, std::vector<Point2<int>> & WireCoordinates)
{
	std::string temp;
	char c;
	Instruction Current;
	Point2<int> Wire = {};
	while ((c = (char)Input.get()) != '\n')
	{
		switch (c)
		{
		case ',':
		{
			Current.Steps = stoi(temp);
			temp = "";
			PlaceWire(WireCoordinates, Wire, Current);
		} break;

		case 'U':
		{
			Current.dy = 1;
			Current.dx = 0;
		} break;

		case 'D':
		{
			Current.dy = -1;
			Current.dx = 0;
		} break;

		case 'R':
		{
			Current.dy = 0;
			Current.dx = 1;
		} break;

		case 'L':
		{
			Current.dy = 0;
			Current.dx = -1;
		} break;

		default: // number
		{
			temp += c;
		}
		}
	}

}

void Three()
{
	std::ifstream File;
	File.open("03.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening File for day 03";
	}
	
	ParseWire(File, Wire1Coordinates);
	ParseWire(File, Wire2Coordinates);
	File.close();
	std::vector<Point2<int>>Wire1Copy = Wire1Coordinates;
	std::vector<Point2<int>>Wire2Copy = Wire2Coordinates;
	std::vector<Point2<int>> WireInteresctions;
	std::sort(Wire1Coordinates.begin(), Wire1Coordinates.end());
	std::sort(Wire2Coordinates.begin(), Wire2Coordinates.end());
	std::set_intersection(Wire1Coordinates.begin(), Wire1Coordinates.end(),
					  	  Wire2Coordinates.begin(), Wire2Coordinates.end(),
						  std::back_inserter(WireInteresctions));

	int P1 = INT_MAX;
	for (Point2<int> Intersection : WireInteresctions)
	{
		int temp = ManhattanDistanceToOrigin(Intersection);
		P1 = temp < P1 ? temp : P1;
	}

	uint64_t P2 = INT_MAX;
	
	for (Point2<int> Intersection : WireInteresctions)
	{
		uint64_t Steps = 2; // The central port is not indexed, each step count is off by 1.
		auto Index = std::find(Wire1Copy.begin(), Wire1Copy.end(), Intersection);
		Steps += Index - Wire1Copy.begin();
		Index = std::find(Wire2Copy.begin(), Wire2Copy.end(), Intersection);
		Steps += Index - Wire2Copy.begin();
		P2 = Steps < P2 ? Steps : P2;
	}
	

	std::cout << P1 << " " << P2 << " ";


}

