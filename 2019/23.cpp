#include "my_functions.h"
#include "intcode.h"

void TwentyThree()
{
	constexpr int BOT_NUMBER = 50;
	
	std::ifstream File;
	File.open("23.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 23";
		return;
	}

	std::vector<int64_t> Program;
	ParseProgram(File, Program);

	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	File.close();

	StandardBot BotNetwork[BOT_NUMBER] = {};
	for (int i = 0; i < BOT_NUMBER; i++)
	{
		BotNetwork[i].Instructions = Program;
		BotNetwork[i].Input.push_back(i);
	}

	Point2<int64_t> NAT = {};
	int64_t Last = 0;
	bool P1 = true;
	bool Running = true;
	while (Running)
	{
		for (StandardBot& Bot : BotNetwork)
		{
			Bot.RunProgram();
			if (Bot.Output.size() > 0)
			{
				size_t Index = Bot.Output.size() - 3;
				int64_t Address = Bot.Output[Index];
				int64_t X = Bot.Output[Index + 1];
				int64_t Y = Bot.Output[Index + 2];
				Bot.Output.pop_back();
				Bot.Output.pop_back();
				Bot.Output.pop_back();
				if (Address == 255)
				{
					if (P1)
					{
						std::cout << Y << " ";
						P1 = false;
					}
					NAT.x = X;
					NAT.y = Y;
				}
				else
				{
					BotNetwork[Address].Input.push_back(X);
					BotNetwork[Address].Input.push_back(Y);
				}
			}
		}
		bool Idle = true;
		for (StandardBot& Bot : BotNetwork)
		{
			if (Bot.Input.size() > 0)
			{
				Idle = false;
				break;
			}
		}
		if (Idle)
		{
			if (NAT.y != 0)
			{
				if (Last == NAT.y)
				{
					std::cout << NAT.y << " ";
					return;
				}
				Last = NAT.y;
				BotNetwork[0].Input.push_back(NAT.x);
				BotNetwork[0].Input.push_back(NAT.y);
				NAT.x = 0;
				NAT.y = 0;
			}
		}
	}
}