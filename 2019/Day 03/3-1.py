with open('input.txt') as f:
    instructions = f.read().splitlines()

def get_tuples(liste):
    liste = liste.split(',') 
    liste = [(x[0],int(x[1:])) for x in liste]
    return liste

def get_coordinates(liste):
    abscisse = 0
    ordonnée = 0
    coordinates = []
    for (x,y) in liste:
        if x == 'R':  
            while y > 0:  
                abscisse += 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        elif x == 'L':
            while y > 0:  
                abscisse -= 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        elif x == 'U':
            while y > 0:  
                ordonnée += 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        elif x == 'D':
            while y > 0:  
                ordonnée -= 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        
    return coordinates

def get_intersections(liste1,liste2):
    crosses = list(set(liste1).intersection(liste2))
    return crosses

def get_distances(liste):
    distances = []
    for (x,y) in liste:
        distance = abs(x) + abs(y)
        distances.append(distance)
    return distances

wire1 = instructions[0]
wire2 = instructions[1]
wire1 = get_tuples(wire1)
wire2 = get_tuples(wire2)
coordinates_wire1 = get_coordinates(wire1)
coordinates_wire2 = get_coordinates(wire2)
intersections = get_intersections(coordinates_wire1,coordinates_wire2)
distances = get_distances(intersections)

print(min(distances))