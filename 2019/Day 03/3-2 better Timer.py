import cProfile

with open('input.txt') as f:
    instructions = f.read().splitlines()

def get_tuples(liste):
    liste = liste.split(',') 
    liste = [(x[0],int(x[1:])) for x in liste]
    return liste

def get_coordinates(liste):
    abscisse = 0
    ordonnée = 0
    coordinates = []
    for (x,y) in liste:
        if x == 'R':  
            while y > 0:  
                abscisse += 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        elif x == 'L':
            while y > 0:  
                abscisse -= 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        elif x == 'U':
            while y > 0:  
                ordonnée += 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        elif x == 'D':
            while y > 0:  
                ordonnée -= 1
                y -= 1
                coordinates.append((abscisse,ordonnée))
        
    return coordinates

def get_intersections(liste1,liste2):
    crosses = list(set(liste1).intersection(liste2))
    return crosses

def get_timings(liste1,liste2,liste3):
    timings = []
    for (x,y) in liste1:
        steps = 0
        steps += liste2.index((x,y))
        steps += liste3.index((x,y))
        timings.append(steps)

    return timings

def run(liste):
    wire1 = liste[0]
    wire2 = liste[1]
    wire1 = get_tuples(wire1)
    wire2 = get_tuples(wire2)
    coordinates_wire1 = get_coordinates(wire1)
    coordinates_wire2 = get_coordinates(wire2)
    intersections = get_intersections(coordinates_wire1,coordinates_wire2)
    timings = get_timings(intersections,coordinates_wire1,coordinates_wire2)

    print(2+min(timings))

cProfile.run('run(instructions)')