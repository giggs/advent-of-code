import cProfile

with open('input.txt') as f:
    instructions = f.read().splitlines()

def get_tuples(liste):
    liste = liste.split(',') 
    liste = [(x[0],int(x[1:])) for x in liste]
    return liste

def get_coordinates(liste):
    abscisse = 0
    ordonnée = 0
    steps = 0
    coordinates = []
    for (x,y) in liste:
        if x == 'R':  
            while y > 0:  
                abscisse += 1
                y -= 1
                steps += 1
                coordinates.append((abscisse,ordonnée,steps))
        elif x == 'L':
            while y > 0:  
                abscisse -= 1
                y -= 1
                steps += 1
                coordinates.append((abscisse,ordonnée,steps))
        elif x == 'U':
            while y > 0:  
                ordonnée += 1
                y -= 1
                steps += 1
                coordinates.append((abscisse,ordonnée,steps))
        elif x == 'D':
            while y > 0:  
                ordonnée -= 1
                y -= 1
                steps += 1
                coordinates.append((abscisse,ordonnée,steps))
        
    return coordinates

def strip_timings(liste):
    stripped_coordinates = [(x,y) for (x,y,z) in liste]
    return stripped_coordinates

def get_intersections(liste1,liste2):
    crosses = list(set(liste1).intersection(liste2))
    return crosses

def get_timings(liste1,liste2,liste3):
    timings = []   
    for (x,y) in liste1:
        steps = 0
        for (a,b,c) in liste2:
            if x == a and y == b:
                steps += c
                break
        for (a,b,c) in liste3:
            if x == a and y == b:
                steps += c
                timings.append(steps)
                break  
    return timings

def run(liste):
    wire1 = instructions[0]
    wire2 = instructions[1]
    wire1 = get_tuples(wire1)
    wire2 = get_tuples(wire2)
    timed_coordinates_wire1 = get_coordinates(wire1)
    timed_coordinates_wire2 = get_coordinates(wire2)
    coordinates_wire1 = strip_timings(timed_coordinates_wire1)
    coordinates_wire2 = strip_timings(timed_coordinates_wire2)
    intersections = get_intersections(coordinates_wire1,coordinates_wire2)
    timings = get_timings(intersections,timed_coordinates_wire1,timed_coordinates_wire2)
    print(min(timings))

cProfile.run('run(instructions)')