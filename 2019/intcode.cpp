#include "my_functions.h"
#include "intcode.h"

void ParseProgram(std::ifstream& Input, std::vector<int> & Program)
{
	std::string Temp;
	char c;
	while ((c = (char)Input.get()) != '\n')
	{
		if (c == ',')
		{
			Program.push_back(stoi(Temp));
			Temp = "";
		}
		else
		{
			Temp += c;
		}
	}
	Program.push_back(stoi(Temp));
}

void ParseProgram(std::ifstream& Input, std::vector<int64_t>& Program)
{
	std::string Temp;
	char c;
	while ((c = (char)Input.get()) != '\n')
	{
		if (c == ',')
		{
			Program.push_back(stoll(Temp));
			Temp = "";
		}
		else
		{
			Temp += c;
		}
	}
	Program.push_back(stoll(Temp));
}

int Amplifier::RunProgram(bool FeedbackLoop)
{
	//NOTE(giggs) : at the end, check if In really needs to be a vector. 
	// Maybe an int will be enough
	int Instruction = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		int Opcode = Instruction % 100;
		int Param1 = (Instruction / 100) % 10;
		int Param2 = (Instruction / 1000) % 10;
		//int Param3 = (Instruction / 10000);
		switch (Opcode)
		{
			case 1: // ADD
			{
				int a = Instructions[Index + 1];
				a = Param1 == 0 ? Instructions[a] : a;
				int b = Instructions[Index + 2];
				b = Param2 == 0 ? Instructions[b] : b;
				int Target = Instructions[Index + 3];
				Instructions[Target] = a + b;
				Index += 4;
			} break;
			case 2: // MUL
			{
				int a = Instructions[Index + 1];
				a = Param1 == 0 ? Instructions[a] : a;
				int b = Instructions[Index + 2];
				b = Param2 == 0 ? Instructions[b] : b;
				int Target = Instructions[Index + 3];
				Instructions[Target] = a * b;
				Index += 4;
			} break;
			case 3: // IN
			{
				int Target = Instructions[Index + 1];
				Instructions[Target] = Input.front();
				Input.pop_front();
				Index += 2;
			} break;
			case 4: // OUT
			{
				int Target = Instructions[Index + 1];
				Target = Param1 == 0 ? Instructions[Target] : Target;
				Output.push_back(Target);
				Index += 2;
				if (FeedbackLoop)
				{
					return -1;
				}
			} break;
			case 5: // JNZ
			{
				int a = Instructions[Index + 1];
				a = Param1 == 0 ? Instructions[a] : a;
				int b = Instructions[Index + 2];
				b = Param2 == 0 ? Instructions[b] : b;
				if (a != 0)
				{
					Index = b;
				}
				else
				{
					Index += 3;
				}
			} break;
			case 6: // JEZ
			{
				int a = Instructions[Index + 1];
				a = Param1 == 0 ? Instructions[a] : a;
				int b = Instructions[Index + 2];
				b = Param2 == 0 ? Instructions[b] : b;
				if (a == 0)
				{
					Index = b;
				}
				else
				{
					Index += 3;
				}
			} break;
			case 7: // TLT
			{
				int a = Instructions[Index + 1];
				a = Param1 == 0 ? Instructions[a] : a;
				int b = Instructions[Index + 2];
				b = Param2 == 0 ? Instructions[b] : b;
				int Target = Instructions[Index + 3];
				Instructions[Target] = a < b ? 1 : 0;
				Index += 4;
			} break;
			case 8: // TEQ
			{
				int a = Instructions[Index + 1];
				a = Param1 == 0 ? Instructions[a] : a;
				int b = Instructions[Index + 2];
				b = Param2 == 0 ? Instructions[b] : b;
				int Target = Instructions[Index + 3];
				Instructions[Target] = a == b ? 1 : 0;
				Index += 4;
			} break;
		}

	}
	return 0;
}

int RunProgram(std::vector<int> & Instructions)
{
	unsigned int Index = 0;

	while (Instructions[Index] != 99)
	{
		switch (Instructions[Index])
		{
			case 1:
			{
				int a = Instructions[Index + 1];
				int b = Instructions[Index + 2];
				int Target = Instructions[Index + 3];
				Instructions[Target] = Instructions[a] + Instructions[b];
				Index += 4;
			} break;
			case 2:
			{
				int a = Instructions[Index + 1];
				int b = Instructions[Index + 2];
				int Target = Instructions[Index + 3];
				Instructions[Target] = Instructions[a] * Instructions[b];
				Index += 4;
			} break;
		}
		
	}
	return Instructions[0];
}



uint64_t PaintingRobot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param)
{
	switch (Param)
	{
	case 0: // Position mode
	{
		return Program[Program[Position]];
	} break;
	case 1: // Immediate mode
	{
		return Program[Position];
	} break;
	case 2: // Relative mode
	{
		return Program[Program[Position] + Relative];
	} break;
	default:
	{
		assert(false);
		return 0;
	}break;
	}
}

uint64_t PaintingRobot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param, bool Writing)
{
	switch (Param)
	{
	case 0: [[fallthrough]];
	case 1:
	{
		return Program[Position];
	} break;
	case 2:
	{
		return Program[Position] + Relative;
	} break;

	default:
	{
		assert(false);
		return Writing; // This is not good... There has to be a better way
	}
	}
}

int64_t PaintingRobot::RunProgram()
{
	uint64_t Instruction = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		int64_t Opcode = Instruction % 100;
		int64_t Param1 = (Instruction / 100) % 10;
		int64_t Param2 = (Instruction / 1000) % 10;
		int64_t Param3 = (Instruction / 10000);
		switch (Opcode)
		{
		case 1: // ADD
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a + b;
			Index += 4;
		} break;
		case 2: // MUL
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a * b;
			Index += 4;
		} break;
		case 3: // IN
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1, true);
			Instructions[Target] = Input.front();
			Input.pop_front();
			Index += 2;
		} break;
		case 4: // OUT
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Output.push_back(Target);
			Index += 2;
			return 1;
		} break;
		case 5: // JNZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a != 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 6: // JEZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a == 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 7: // TLT
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a < b ? 1 : 0;
			Index += 4;
		} break;
		case 8: // TEQ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a == b ? 1 : 0;
			Index += 4;
		} break;
		case 9:
		{
			RelativeBase += ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
		} break;
		}
	}
	return 0;
}


uint64_t ArkanoidBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param)
{
	switch (Param)
	{
	case 0: // Position mode
	{
		return Program[Program[Position]];
	} break;
	case 1: // Immediate mode
	{
		return Program[Position];
	} break;
	case 2: // Relative mode
	{
		return Program[Program[Position] + Relative];
	} break;
	default:
	{
		assert(false);
		return 0;
	}break;
	}
}

uint64_t ArkanoidBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param, bool Writing)
{
	switch (Param)
	{
	case 0: [[fallthrough]];
	case 1:
	{
		return Program[Position];
	} break;
	case 2:
	{
		return Program[Position] + Relative;
	} break;

	default:
	{
		assert(false);
		return Writing; // This is not good... There has to be a better way
	}
	}
}

int64_t ArkanoidBot::RunProgram()
{
	uint64_t Instruction = 0;
	uint64_t OutputCounter = 0;
	uint64_t BallX = 0;
	uint64_t PaddleX = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		int64_t Opcode = Instruction % 100;
		int64_t Param1 = (Instruction / 100) % 10;
		int64_t Param2 = (Instruction / 1000) % 10;
		int64_t Param3 = (Instruction / 10000);
		switch (Opcode)
		{
		case 1: // ADD
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a + b;
			Index += 4;
		} break;
		case 2: // MUL
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a * b;
			Index += 4;
		} break;
		case 3: // IN
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1, true);
			Instructions[Target] = Input.front();
			Input.pop_front();
			Index += 2;
		} break;
		case 4: // OUT
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Output.push_back(Target);
			OutputCounter++;
			if (OutputCounter == 3)
			{
				if (Target == 4)
				{
					BallX = Output[Output.size() - 3];
					if (BallX == PaddleX)
					{
						Input.push_front(0);
					}
					else if (BallX > PaddleX)
					{
						Input.push_front(1);
					}
					else
					{
						Input.push_front(-1);
					}
				}
				else if (Target == 3)
				{
					PaddleX = Output[Output.size() - 3];
				}
				OutputCounter = 0;
			}

			Index += 2;
		} break;
		case 5: // JNZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a != 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 6: // JEZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a == 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 7: // TLT
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a < b ? 1 : 0;
			Index += 4;
		} break;
		case 8: // TEQ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a == b ? 1 : 0;
			Index += 4;
		} break;
		case 9:
		{
			RelativeBase += ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
		} break;
		}
	}
	return 0;
}

uint64_t CleaningBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param)
{
	switch (Param)
	{
	case 0: // Position mode
	{
		return Program[Program[Position]];
	} break;
	case 1: // Immediate mode
	{
		return Program[Position];
	} break;
	case 2: // Relative mode
	{
		return Program[Program[Position] + Relative];
	} break;
	default:
	{
		assert(false);
		return 0;
	}break;
	}
}

uint64_t CleaningBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param, bool Writing)
{
	switch (Param)
	{
	case 0: [[fallthrough]];
	case 1:
	{
		return Program[Position];
	} break;
	case 2:
	{
		return Program[Position] + Relative;
	} break;

	default:
	{
		assert(false);
		return Writing; // This is not good... There has to be a better way
	}
	}
}

int64_t CleaningBot::RunProgram()
{
	uint64_t Instruction = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		int64_t Opcode = Instruction % 100;
		int64_t Param1 = (Instruction / 100) % 10;
		int64_t Param2 = (Instruction / 1000) % 10;
		int64_t Param3 = (Instruction / 10000);
		switch (Opcode)
		{
		case 1: // ADD
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a + b;
			Index += 4;
		} break;
		case 2: // MUL
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a * b;
			Index += 4;
		} break;
		case 3: // IN
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1, true);
			Instructions[Target] = Input.front();
			Input.pop_front();
			Index += 2;
		} break;
		case 4: // OUT
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
			return Target;
		} break;
		case 5: // JNZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a != 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 6: // JEZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a == 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 7: // TLT
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a < b ? 1 : 0;
			Index += 4;
		} break;
		case 8: // TEQ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a == b ? 1 : 0;
			Index += 4;
		} break;
		case 9:
		{
			RelativeBase += ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
		} break;
		}
	}
	return -1;
}


uint64_t StandardBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param)
{
	switch (Param)
	{
	case 0: // Position mode
	{
		return Program[Program[Position]];
	} break;
	case 1: // Immediate mode
	{
		return Program[Position];
	} break;
	case 2: // Relative mode
	{
		return Program[Program[Position] + Relative];
	} break;
	default:
	{
		assert(false);
		return 0;
	}break;
	}
}

uint64_t StandardBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param, bool Writing)
{
	switch (Param)
	{
	case 0: [[fallthrough]];
	case 1:
	{
		return Program[Position];
	} break;
	case 2:
	{
		return Program[Position] + Relative;
	} break;

	default:
	{
		assert(false);
		return Writing; // This is not good... There has to be a better way
	}
	}
}

int64_t StandardBot::RunProgram()
{
	uint64_t Instruction = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		int64_t Opcode = Instruction % 100;
		int64_t Param1 = (Instruction / 100) % 10;
		int64_t Param2 = (Instruction / 1000) % 10;
		int64_t Param3 = (Instruction / 10000);
		switch (Opcode)
		{
		case 1: // ADD
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a + b;
			Index += 4;
		} break;
		case 2: // MUL
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a * b;
			Index += 4;
		} break;
		case 3: // IN
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1, true);
			if (Input.size() > 0)
			{
				Instructions[Target] = Input.front();
				Input.pop_front();
			}
			else
			{
				Instructions[Target] = -1;
			}
			Index += 2;
			return 1;
		} break;
		case 4: // OUT
		{
			
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
			Output.push_back(Target);
			if (Output.size() % 3 == 0)
			{
				return 0;
			}
		} break;
		case 5: // JNZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a != 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 6: // JEZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a == 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 7: // TLT
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a < b ? 1 : 0;
			Index += 4;
		} break;
		case 8: // TEQ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a == b ? 1 : 0;
			Index += 4;
		} break;
		case 9:
		{
			RelativeBase += ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
		} break;
		}
	}
	return -1;
}



uint64_t FinalBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param)
{
	switch (Param)
	{
	case 0: // Position mode
	{
		return Program[Program[Position]];
	} break;
	case 1: // Immediate mode
	{
		return Program[Position];
	} break;
	case 2: // Relative mode
	{
		return Program[Program[Position] + Relative];
	} break;
	default:
	{
		assert(false);
		return 0;
	}break;
	}
}

uint64_t FinalBot::ModeSwitch(const std::vector<int64_t>& Program, const uint64_t& Position, const int64_t& Relative, const int64_t& Param, bool Writing)
{
	switch (Param)
	{
	case 0: [[fallthrough]];
	case 1:
	{
		return Program[Position];
	} break;
	case 2:
	{
		return Program[Position] + Relative;
	} break;

	default:
	{
		assert(false);
		return Writing; // This is not good... There has to be a better way
	}
	}
}

int64_t FinalBot::RunProgram()
{
	uint64_t Instruction = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		int64_t Opcode = Instruction % 100;
		int64_t Param1 = (Instruction / 100) % 10;
		int64_t Param2 = (Instruction / 1000) % 10;
		int64_t Param3 = (Instruction / 10000);
		switch (Opcode)
		{
		case 1: // ADD
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a + b;
			Index += 4;
		} break;
		case 2: // MUL
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a * b;
			Index += 4;
		} break;
		case 3: // IN
		{
			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1, true);
			if (Input.size() > 0)
			{
				Instructions[Target] = Input.front();
				Input.pop_front();
			}
			else
			{
				Instructions[Target] = -1;
			}
			Index += 2;
			return 1;
		} break;
		case 4: // OUT
		{

			int64_t Target = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
			Output.push_back(Target);

		} break;
		case 5: // JNZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a != 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 6: // JEZ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			if (a == 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 7: // TLT
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a < b ? 1 : 0;
			Index += 4;
		} break;
		case 8: // TEQ
		{
			int64_t a = ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			int64_t b = ModeSwitch(Instructions, Index + 2, RelativeBase, Param2);
			int64_t Target = ModeSwitch(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a == b ? 1 : 0;
			Index += 4;
		} break;
		case 9:
		{
			RelativeBase += ModeSwitch(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
		} break;
		}
	}
	return -1;
}

