#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <map>
#include <vector>
#include <sstream>
#include "my_functions.h"

struct Chemical
{
	uint64_t Quantity;
	std::string Name;

	bool operator<(const Chemical& B) const
	{
		return Name < B.Name;
	}

};

std::map<Chemical, std::vector<Chemical>> Reactions;
std::vector<Chemical> Order;
std::set<Chemical> Visited;
std::map<uint64_t, Chemical> OrderedIngredients;

void DFS(Chemical Node)
{
	Visited.insert(Node);
	if (Node.Name == "ORE")
	{
		return;
	}
	auto Search = Reactions.find(Node);
	for (Chemical Ingredient : Search->second)
	{
		auto SearchVisited = Visited.find(Ingredient);
		if (SearchVisited == Visited.end())
		{
			DFS(Ingredient);
		}
	}
	Order.push_back(Node);
}

uint64_t OreRequired(uint64_t FuelProduced)
{
	Visited = {};
	Visited.insert({ FuelProduced, "FUEL" });
	uint64_t Ore = 0;

	while (Visited.size() > 0)
	{
		Chemical Current;
		for (auto It = OrderedIngredients.begin(); It != OrderedIngredients.end(); It++)
		{
			auto SearchVisited = Visited.find(It->second);
			if (SearchVisited != Visited.end())
			{
				Current = *SearchVisited;
				Visited.erase(Current);
				break;
			}
		}
		auto Search = Reactions.find(Current);
		uint64_t Produced = Search->first.Quantity;
		std::vector<Chemical> Ingredients = Search->second;
		uint64_t Multiplier = Current.Quantity / Produced;
		if (Current.Quantity % Produced != 0)
		{
			Multiplier = Current.Quantity / Produced + 1;
		}
		for (Chemical Ingredient : Ingredients)
		{
			if (Ingredient.Name == "ORE")
			{
				Ore += Multiplier * Ingredient.Quantity;
			}
			else
			{
				auto SearchVisited = Visited.find(Ingredient);
				if (SearchVisited == Visited.end())
				{
					Visited.insert({ Ingredient.Quantity * Multiplier, Ingredient.Name });
				}
				else
				{
					Chemical Temp = *SearchVisited;
					Visited.erase(Temp);
					Visited.insert({ Temp.Quantity + Ingredient.Quantity * Multiplier, Ingredient.Name });
				}
			}
		}
	}
	return Ore;
}

void Fourteen()
{
	std::ifstream File;
	File.open("14.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 14";
		return;
	}

	std::string Line;
	getline(File, Line);
	

	for (; Line != ""; getline(File, Line))
	{
		std::istringstream SLine(Line);
		Chemical Current;
		std::vector<Chemical> LeftSide;
		bool Comma = false;
		do
		{
			SLine >> Current.Quantity;
			SLine >> Current.Name;
			Comma = (Current.Name.back() == ',');
			if (Comma)
			{
				Current.Name.pop_back();
			}
			LeftSide.push_back(Current);
			
		} while (Comma);
		SLine >> Current.Name;
		SLine >> Current.Quantity;
		SLine >> Current.Name;
		
		Reactions.insert({ Current, LeftSide });
	}

	auto Search = Reactions.begin();
	while (Search->first.Name != "FUEL")
	{
		Search++;
	}
	DFS(Search->first);

	std::reverse(Order.begin(), Order.end());

	for (uint64_t Index = 0; Index < Order.size(); Index++)
	{
		OrderedIngredients.insert({ Index, Order[Index] });
	}

	uint64_t P1 = OreRequired(1);

	std::cout << P1 << " ";

	uint64_t Low = 2;
	uint64_t High = INT_MAX;
	uint64_t Target = 1000000000000;

	while (Low != High)
	{
		uint64_t Mid = (Low + High) / 2;
		uint64_t Ore = OreRequired(Mid);
		if (Ore == Target)
		{
			std::cout << Mid << " ";
			return;
		}
		else if (Ore < Target)
		{
			Low = Mid+1;
		}
		else
		{
			High = Mid-1;
		}
	}
	std::cout << Low-1 << " ";
}