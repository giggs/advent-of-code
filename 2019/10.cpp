#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <algorithm>
#include <vector>
#include <numeric>
#include "my_functions.h"
constexpr double Pi = 3.14159365359f;

struct AtanOfPoint
{
	double Atan;
	Point2<int> Slope;
	
	bool operator<(const AtanOfPoint& B) const
	{
		return B.Atan < Atan;
	}

	bool operator==(const AtanOfPoint& B) const
	{
		return Atan == B.Atan && Slope == B.Slope;
	}
};

void Ten()
{
	std::ifstream File;
	File.open("10.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 10";
		return ;
	}

	std::string Line;
	getline(File, Line);
	std::vector<Point2<int>> Asteroids;

	for (int y = 0; Line != ""; y++)
	{
		for (int x = 0; x < Line.size(); x++)
		{
			if (Line[x] == '#')
			{
				Asteroids.push_back({ x,y });
			}
		}
		getline(File, Line);
	}
	File.close();
	size_t P1 = 0;
	Point2<int> BestAsteroid = {};
	std::vector<Point2<int>> MostSlopes;

	for (Point2<int> Asteroid : Asteroids)
	{
		std::vector<Point2<int>> Slopes;
		for (Point2<int> OtherAsteroid : Asteroids)
		{
			if (OtherAsteroid == Asteroid)
			{
				continue;
			}
			int dx = OtherAsteroid.x - Asteroid.x;
			int dy = OtherAsteroid.y - Asteroid.y;
			int GCD = std::gcd(dy, dx);
			//Since the y axis is going down, we need to invert dy
			Slopes.push_back({ dx / GCD, -dy / GCD });
		}
		std::sort(Slopes.begin(), Slopes.end());
		Slopes.erase(unique(Slopes.begin(), Slopes.end()), Slopes.end());
		if (Slopes.size() > P1)
		{
			P1 = Slopes.size();
			BestAsteroid = Asteroid;
			MostSlopes = Slopes;
		}
	}
	
	std::vector<AtanOfPoint> AsteroidRing;
	for (Point2<int> Slope : MostSlopes)
	{
		double key = atan2(Slope.y, Slope.x);
		// Compute the angle between y axis and slopes. 
		// The values between Pi / 2 and Pi are the last one we want, not the first. 
		// So substract one full circle and they'll be sorted last.
		AsteroidRing.push_back({ key > (Pi / 2.0) ? (key - 2 * Pi) : key, Slope });
	}

	std::set<Point2<int>> SortedAsteroids(Asteroids.begin(), Asteroids.end());
	std::sort(AsteroidRing.begin(), AsteroidRing.end());
	Point2<int> Lucky200th = AsteroidRing[199].Slope;
	auto Search = SortedAsteroids.begin();
	do
	{
		BestAsteroid.x += Lucky200th.x;
		BestAsteroid.y -= Lucky200th.y;
		Search = SortedAsteroids.find(BestAsteroid);
	} while (Search == SortedAsteroids.end());

	std::cout << P1 << " " << BestAsteroid.x * 100 + BestAsteroid.y << " ";
}