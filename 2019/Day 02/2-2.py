with open('input.txt') as f:
    opcode = f.read().split(',')

opcode = [int(x) for x in opcode]

noun = 0
verb = -1
output = 19690720
working_opcode = opcode.copy()

while working_opcode[0] != output:
    if verb == 99:
        noun += 1
    verb += 1 
    verb = verb % 100
    working_opcode = opcode.copy()
    working_opcode[1] = noun
    working_opcode[2] = verb
    i = 0
    while working_opcode[i] != 99:
        a = working_opcode[i+1]
        b = working_opcode[i+2]
        target = working_opcode[i+3]
        if working_opcode[i] == 1:
            working_opcode[target] = working_opcode[a] + working_opcode[b]
        elif working_opcode[i] == 2: 
            working_opcode[target] = working_opcode[a] * working_opcode[b]
        i += 4
  
print(100*noun+verb)