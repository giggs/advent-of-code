with open('input.txt') as f:
    Opcode = f.read().split(',')

Opcode = [int(x) for x in Opcode]

Opcode[1] = 12
Opcode[2] = 2
i = 0

while Opcode[i] != 99:
    a = Opcode[i+1]
    b = Opcode[i+2]
    target = Opcode[i+3]
    if Opcode[i] == 1:
        Opcode[target] = Opcode[a] + Opcode[b]
    elif Opcode[i] == 2: 
        Opcode[target] = Opcode[a] * Opcode[b]
    i += 4

print(Opcode[0])