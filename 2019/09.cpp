#include "my_functions.h"
#include "intcode.h"

void Nine()
{
	std::ifstream File;
	File.open("09.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 09";
		return;
	}

	std::vector<int64_t>Program;
	ParseProgram(File, Program);
	File.close();
	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Need to do better than that 
	std::vector<int64_t> Program_copy = Program;
	std::deque<int64_t> Input = { 1 };
	std::vector<int64_t> Output;

	RunProgram<int64_t>(Program, Input, Output);
	std::cout << Output.back() << " ";
	Input.push_front(2);
	RunProgram<int64_t>(Program_copy, Input, Output);
	std::cout << Output.back() << " ";
	
}