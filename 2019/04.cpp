#include <iostream>
#include <string>
#include "my_functions.h"


uint32_t LowerBound = 124075;
uint32_t UpperBound = 580769;

void Four()
{
	uint32_t P1 = 0;
	uint32_t P2 = 0;

	for (uint32_t i = LowerBound; i <= UpperBound; i++)
	{
		bool Decrease = false;
		std::string Temp = std::to_string(i);
		for (int j = 1; j < 6; j++)
		{
			if (Temp[j] < Temp[j-1]) // Decreases
			{
				Decrease = true;
				for (int k = j; k < 6; k++)
				{
					Temp[k] = Temp[j-1];
				}
				i = stoi(Temp);
				i--;
				break;
			}

		}
		if (Decrease)
		{
			continue;
		}
			
		bool Strict_double = false;
		bool Double = false;
		uint32_t Index = 0;
		while (Index < 5)
		{
			uint32_t Counter = 1;
			while ((Index + Counter < 6) && Temp[Index] == Temp[Index+Counter])
			{
				Counter++;
			}
			if (Counter > 1)
			{
				Double = true;
			}
			if (Counter == 2)
			{
				Strict_double = true;
			}
			Index += Counter;
		}
		P1 += Double;
		P2 += Strict_double;
	}

	std::cout << P1 << " " << P2 << " ";

}