with open('workfile.txt') as f:
    input = f.read().splitlines()

input = [int(x) for x in input]

answer = 0

def fuelmass(n):
    return n // 3 - 2

for module in input:
    mass = fuelmass(module)
    answer += mass
    while mass > 8:
        mass = fuelmass(mass)
        answer += mass

print(answer)