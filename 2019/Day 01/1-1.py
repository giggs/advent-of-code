with open('workfile.txt') as f:
    input = f.read().splitlines()

input = [int(x) for x in input]

def fuelmass(n):
    return n // 3 - 2

fuel = 0

for module in input:
    mass = fuelmass(module)
    fuel += mass

print(fuel)