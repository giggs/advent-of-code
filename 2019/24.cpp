#include "my_functions.h"
#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <cctype>
#include <map>


// Convolutions would speed this up
// Also swapping pointers between each minute instead of copying 
// Use actual array instead of sets
// Sets were used because I expected a large grid in part2


void Reproduce(std::set<Point2<int>>& Bugs, std::set<Point2<int>>& NewBugs)
{
	constexpr int DIRECTION_COUNT = 4;
	Point2<int> Directions[DIRECTION_COUNT] = { {0,1}, {0,-1}, {-1,0}, {1,0} };

	constexpr int GRID_SIZE = 5;
	for (int y = 0; y < GRID_SIZE; y++)
	{
		for (int x = 0; x < GRID_SIZE; x++)
		{
			Point2<int> Current = { x,y };
			int Neighbours = 0;
			for (auto Dir : Directions)
			{
				Point2<int> New = Current + Dir;
				auto SearchBugs = Bugs.find(New);
				if (SearchBugs != Bugs.end())
				{
					Neighbours++;
				}
			}
			auto SearchBugs = Bugs.find(Current);
			if (SearchBugs == Bugs.end())
			{
				if (Neighbours == 1 || Neighbours == 2)
				{
					NewBugs.insert(Current);
				}
			}
			else
			{
				if (Neighbours == 1)
				{
					NewBugs.insert(Current);
				}
			}
		}
	}
}

void RecursivelyReproduce(std::set<Point2<int>>* Bugs, std::set<Point2<int>>& NewBugs, int LevelIndex)
{
	constexpr int DIRECTION_COUNT = 4;
	Point2<int> Directions[DIRECTION_COUNT] = { {0,1}, {0,-1}, {-1,0}, {1,0} };

	constexpr int GRID_SIZE = 5;
	for (int y = 0; y < GRID_SIZE; y++)
	{
		for (int x = 0; x < GRID_SIZE; x++)
		{
			if (x == 2 && y == 2) //Don't count center tile
			{
				continue;
			}
			Point2<int> Current = { x,y };
			int Neighbours = 0;
			int Adjacent = 0;
			for (auto Dir : Directions)
			{
				if ((y == 0) && (Dir.y == -1)) // ABCDE
				{
					Adjacent++;
					auto SearchBugs = Bugs[LevelIndex - 1].find({ 2,1 }); //8
					if (SearchBugs != Bugs[LevelIndex - 1].end())
					{
						Neighbours++;
					}
				}
				else if ((y == (GRID_SIZE - 1)) && (Dir.y == 1)) //UVWXY
				{
					Adjacent++;
					auto SearchBugs = Bugs[LevelIndex - 1].find({ 2,3 }); //18
					if (SearchBugs != Bugs[LevelIndex - 1].end())
					{
						Neighbours++;
					}
				}
				else if ((x == 0) && (Dir.x == -1)) //AFKPU
				{
					Adjacent++;
					auto SearchBugs = Bugs[LevelIndex - 1].find({ 1,2 }); //12
					if (SearchBugs != Bugs[LevelIndex - 1].end())
					{
						Neighbours++;
					}
				}
				else if ((x == (GRID_SIZE-1)) && (Dir.x == 1)) //EJOTY
				{
					Adjacent++;
					auto SearchBugs = Bugs[LevelIndex - 1].find({ 3,2 }); //12
					if (SearchBugs != Bugs[LevelIndex - 1].end())
					{
						Neighbours++;
					}
				}
				else if (x == 2 && y == 1 && Dir.y == 1) //8
				{
					for (int i = 0; i < GRID_SIZE; i++)
					{
						Adjacent++;
						auto SearchBugs = Bugs[LevelIndex + 1].find({ i,0 }); //ABCDE
						if (SearchBugs != Bugs[LevelIndex + 1].end())
						{
							Neighbours++;
						}
					}
				}
				else if (x == 2 && y == 3 && Dir.y == -1) //18
				{
					for (int i = 0; i < GRID_SIZE; i++)
					{
						Adjacent++;
						auto SearchBugs = Bugs[LevelIndex + 1].find({ i,(GRID_SIZE-1) }); //UVWXY
						if (SearchBugs != Bugs[LevelIndex + 1].end())
						{
							Neighbours++;
						}
					}
				}
				else if (x == 1 && y == 2 && Dir.x == 1) //12
				{
					Adjacent++;
					for (int i = 0; i < GRID_SIZE; i++)
					{
						auto SearchBugs = Bugs[LevelIndex + 1].find({ 0,i }); //AFKPU
						if (SearchBugs != Bugs[LevelIndex + 1].end())
						{
							Neighbours++;
						}
					}
				}
				else if (x == 3 && y == 2 && Dir.x == -1) //14
				{
					Adjacent++;
					for (int i = 0; i < GRID_SIZE; i++)
					{
						auto SearchBugs = Bugs[LevelIndex + 1].find({ (GRID_SIZE-1),i }); //EJOTY
						if (SearchBugs != Bugs[LevelIndex + 1].end())
						{
							Neighbours++;
						}
					}
				}
				else
				{
					Adjacent++;
					Point2<int> New = Current + Dir;
					auto SearchBugs = Bugs[LevelIndex].find(New);
					if (SearchBugs != Bugs[LevelIndex].end())
					{
						Neighbours++;
					}
				}
			}
			auto SearchBugs = Bugs[LevelIndex].find(Current);
			if (SearchBugs == Bugs[LevelIndex].end())
			{
				if (Neighbours == 1 || Neighbours == 2)
				{
					NewBugs.insert(Current);
				}
			}
			else
			{
				if (Neighbours == 1)
				{
					NewBugs.insert(Current);
				}
			}
		}
	}
}

void TwentyFour()
{
	std::ifstream File;
	File.open("24.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 24";
		return;
	}

	int y = 0;

	std::set<Point2<int>> Bugs;
	std::set<std::set<Point2<int>>> Layouts;

	std::string Line;

	getline(File, Line);

	for (; Line != ""; getline(File, Line))
	{
		int x = 0;
		for (char c : Line)
		{
			if (c == '#')
			{
				Bugs.insert({ x, y });
			}
			x++;
		}
		y++;
	}

	std::set<Point2<int>> P2Bugs[301];
	P2Bugs[150] = Bugs;

	while (true)
	{
		std::set<Point2<int>> NewBugs;
		Reproduce(Bugs, NewBugs);
		auto SearchLayouts = Layouts.find(NewBugs);
		if (SearchLayouts == Layouts.end())
		{
			Layouts.insert(NewBugs);
			Bugs = NewBugs;
		}
		else
		{
			Bugs = NewBugs;
			break;
		}
	}
	int P1 = 0;
	for (auto Bug : Bugs)
	{
		P1 += 1 << (Bug.y * 5 + Bug.x);
	}

	std::cout << P1 << " ";

	for (int i = 0; i < 200; i++)
	{
		std::set<Point2<int>> NewP2Bugs[301];
		for (int j = 150 - ((i / 2) + 1); j <= 150 + (i / 2) + 1; j++)
		{
			RecursivelyReproduce(P2Bugs, NewP2Bugs[j], j);
		}
		for (int j = 150 - ((i / 2) + 1); j <= 150 + (i / 2) + 1; j++)
		{
			P2Bugs[j] = NewP2Bugs[j];
		}
	}
	size_t P2 = 0;
	for (auto& Level : P2Bugs)
	{
		P2 += Level.size();
	}

	std::cout << P2 << " ";
}
