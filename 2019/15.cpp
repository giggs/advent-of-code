#include "my_functions.h"
#include "intcode.h"
#include <set>



void Fifteen()
{
	std::set<Point2<int>> Walls;
	constexpr int DirectionCount = 4;
	Point2<int> Directions[DirectionCount] = { {0,1}, {0,-1}, {-1,0}, {1,0} };
	
	std::ifstream File;
	File.open("15.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 15";
		return;
	}

	std::vector<int64_t> Program;
	ParseProgram(File, Program);
	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	File.close();
	std::vector<CleaningBot> Queue;
	CleaningBot Start = {};
	Start.Instructions = Program;
	Queue.push_back(Start);
	std::set<Point2<int>> Seen;
	Seen.insert(Start.Coordinates);
	uint64_t P1 = 0;

	Point2<int> Oxygen = {};

	while (Queue.size() > 0)
	{
		CleaningBot Current = Queue.back();
		Queue.pop_back();
		for (int i = 1; i < 5; i++)
		{
			CleaningBot New = Current;
			New.Input.push_front(i);
			int64_t Result = New.RunProgram();
			New.Coordinates = New.Coordinates + Directions[i - 1];
			New.Steps++;
			if (Result == 0)
			{
				Walls.insert(New.Coordinates);
				continue;
			}
			else if (Result == 1)
			{
				auto Search = Seen.find(New.Coordinates);
				if (Search == Seen.end())
				{
					Seen.insert(New.Coordinates);
					Queue.push_back(New);
				}
			}
			else if (P1 == 0)
			{
					P1 = New.Steps;
					Oxygen = New.Coordinates;
			}
		}
	}

	std::cout << P1 << " ";
	uint64_t P2 = 0;
	Seen = {Oxygen};
	std::vector<Point2<int>> Frontier = {Oxygen};
	std::vector<Point2<int>> NewFrontier;

	while (Frontier.size())
	{
		for (Point2<int> Current : Frontier)
		{
			for (Point2<int> Dir : Directions)
			{
				Point2<int> New = Current + Dir;
				auto Search = Walls.find(New);
				if (Search != Walls.end())
				{
					continue;
				}
				Search = Seen.find(New);
				if (Search != Seen.end())
				{
					continue;
				}
				NewFrontier.push_back(New);
				Seen.insert(New);
			}
		}
		Frontier = NewFrontier;
		NewFrontier = { };
		P2++;
	}
	std::cout << P2-1 << " ";
}