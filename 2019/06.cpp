#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include "my_functions.h"

// NOTE(giggs): Building an actual tree might be faster?

static int P1 = 0;

std::map<std::string, std::vector<std::string>> Mappings;
std::map<std::string, std::string> ReverseMappings;

void ParseMappings(std::string Node, int Depth)
{
	P1 += Depth;
	auto Search = Mappings.find(Node);
	if (Search != Mappings.end())
	{
		for (std::string ChildNode : Search->second)
		{
			ParseMappings(ChildNode, Depth + 1);
		}
	}
}

void ReversePath(std::string Node, std::vector<std::string>& Path)
{
	//Don't include YOU or SAN, they don't count as an orbit change.
	
	while (Node != "COM")
	{
		auto Search = ReverseMappings.find(Node);
		Node = Search->second;
		Path.push_back(Node);
	}
}

void Six()
{
	std::ifstream File;
	File.open("06.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 06";
	}

	std::string Line;
	getline(File, Line);

	while (Line != "")
	{
		std::string Center = Line.substr(0, 3);
		Line = Line.substr(4, Line.size());
		auto Search = Mappings.find(Center);
		std::vector<std::string> Temp;
		if (Search != Mappings.end())
		{
			Temp = Search->second;
			Mappings.erase(Center);
		}
		Temp.push_back(Line);
		Mappings.insert({ Center, Temp });
		ReverseMappings.insert({ Line, Center });
		getline(File, Line);
	}
	File.close();
	ParseMappings("COM", 0);
	std::vector<std::string> YOUPath;
	ReversePath("YOU", YOUPath);
	std::vector<std::string> SANPath;
	ReversePath("SAN", SANPath);

	while (SANPath.back() == YOUPath.back())
	{
		YOUPath.pop_back();
		SANPath.pop_back();
	}

	std::cout << P1 << " " << YOUPath.size() + SANPath.size() << " ";
	

}