#include "my_functions.h"
#include "intcode.h"
#include <set>

void Seventeen()
{
	constexpr int DirectionCount = 4;
	Point2<int> Directions[DirectionCount] = { {0,1}, {0,-1}, {-1,0}, {1,0} };

	std::ifstream File;
	File.open("17.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 17";
		return;
	}
	std::vector<int64_t> Program;
	ParseProgram(File, Program);

	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	File.close();
	std::vector<int64_t> Program_copy = Program;
	Program_copy[0] = 2;
	std::deque<int64_t> Input;
	std::vector<int64_t> Output;

	RunProgram(Program, Input, Output);

	std::set<Point2<int>> Tracks;
	int y = 0;
	int x = 0;
	for (auto It = Output.begin(); It != Output.end(); It++)
	{
		if (*It == static_cast<int>('\n'))
		{
			x = 0;
			y++;
			continue;
		}
		else if (*It == static_cast<int>('#'))
		{
			Tracks.insert({ x,y });
		}
		x++;
	}
	
	int P1 = 0;

	for (auto It = Tracks.begin(); It != Tracks.end(); It++)
	{
		int Count = 0;
		for (Point2 Dir : Directions)
		{
			auto Search = Tracks.find(*It + Dir);
			if (Search != Tracks.end())
			{
				Count++;
			}
		}
		if (Count == 4)
		{
			P1 += It->x * It->y;
		}
	}

	std::cout << P1 << " ";

	// L,4,R,8,L,6,L,10,			A
	// L,6,R,8,R,10,L,6,L,6,		B
	// L,4,R,8,L,6,L,10,			A
	// L,6,R,8,R,10,L,6,L,6,		B
	// L,4,L,4,L,10					C
	// L,4,L,4,L,10,				C
	// L,6,R,8,R,10,L,6,L,6,		B
	// L,4,R,8,L,6,L,10,			A
	// L,6,R,8,R,10,L,6,L,6,		B
	// L,4,L,4,L,10					C

	for (char c : "A,B,A,B,C,C,B,A,B,C\nL,4,R,8,L,6,L,10\nL,6,R,8,R,10,L,6,L,6\nL,4,L,4,L,10\nn\n")
	{
		Input.push_back(static_cast<int>(c));
	}
	RunProgram(Program_copy, Input, Output);
	std::cout << Output.back() << " ";

	// If you were to solve this programmatically, here's an idea.
	// Create the path string by following the tracks, saving each turn 
	// and forward instructions.
	// Start at the beginning of the path string and add characters one by one
	// while a regex match finds more than one occurence.
	// Save the longest string < 19 charactersthat gets more than 1 match
	// as function A, replace it in the path string by A.
	// Repeat for B and C. Wouldn't quite work here, but it's a start

}
