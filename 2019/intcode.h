#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <assert.h>
#include <deque>
#include "my_functions.h"

struct FinalBot
{
	std::vector<int64_t> Instructions;
	std::deque<int64_t> Input;
	std::vector<int64_t> Output;
	size_t Index = 0;
	int64_t RelativeBase = 0;

	int64_t RunProgram();
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param);
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param, bool Writing);
};

struct StandardBot
{
	std::vector<int64_t> Instructions;
	std::deque<int64_t> Input;
	std::vector<int64_t> Output;
	size_t Index = 0;
	int64_t RelativeBase = 0;

	int64_t RunProgram();
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param);
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param, bool Writing);
};

struct Amplifier
{
	std::vector<int> Instructions;
	std::deque<int> Input;
	std::vector<int> Output;
	unsigned Index = 0;

	int RunProgram(bool FeedbackLoop = false);

};

struct PaintingRobot
{
	std::vector<int64_t> Instructions;
	std::deque<int64_t> Input;
	std::vector<int64_t> Output;
	size_t Index = 0;
	int64_t RelativeBase = 0;

	int64_t RunProgram();
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param);
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param, bool Writing);

};

struct ArkanoidBot
{
	std::vector<int64_t> Instructions;
	std::deque<int64_t> Input;
	std::vector<int64_t> Output;
	size_t Index = 0;
	int64_t RelativeBase = 0;

	int64_t RunProgram();
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param);
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param, bool Writing);
};

struct CleaningBot
{
	std::vector<int64_t> Instructions;
	std::deque<int64_t> Input;
	size_t Index = 0;
	int64_t RelativeBase = 0;
	Point2<int> Coordinates = {0,0};
	int64_t Steps = 0;

	int64_t RunProgram();
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param);
	uint64_t ModeSwitch(const std::vector<int64_t>& Instructions, const uint64_t& Index, const int64_t& RelativeBase, const int64_t& Param, bool Writing);
};

void ParseProgram(std::ifstream& Input, std::vector<int> & Program);
void ParseProgram(std::ifstream& Input, std::vector<int64_t> & Program);

int RunProgram(std::vector<int> & Instructions);


template <typename T>
T ModeSwitch(const std::vector<T>& Instructions, const T& Index, const T& RelativeBase, const T& Param)
{
	switch (Param)
	{
	case 0: // Position mode
	{
		return Instructions[Instructions[Index]];
	} break;
	case 1: // Immediate mode
	{
		return Instructions[Index];
	} break;
	case 2: // Relative mode
	{
		return Instructions[Instructions[Index]+RelativeBase];
	} break;
	default:
	{
		assert(false);
		return 0;
	}break;
	}
}

template <typename T>
T ModeSwitch(const std::vector<T>& Instructions, const T& Index, const T& RelativeBase, const T& Param, bool Writing)
{
	switch (Param)
	{
		case 0: [[fallthrough]];
		case 1: 
		{
			return Instructions[Index];
		} break;
		case 2:
		{
			return Instructions[Index] + RelativeBase;
		} break;
		
		default:
		{
			assert(false);
			return Writing; // This is not good... There has to be a better way
		}
	}
}

template <typename T>
T RunProgram(std::vector<T>& Instructions, std::deque<T>& Input, std::vector<T>& Output)
{
	T Index = 0;
	T Instruction = 0;
	T RelativeBase = 0;

	while ((Instruction = Instructions[Index]) != 99)
	{
		T Opcode = Instruction % 100;
		T Param1 = (Instruction / 100) % 10;
		T Param2 = (Instruction / 1000) % 10;
		T Param3 = (Instruction / 10000);
		switch (Opcode)
		{
		case 1: // ADD
		{
			T a = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			T b = ModeSwitch<T>(Instructions, Index + 2, RelativeBase, Param2);
			T Target = ModeSwitch<T>(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a + b;
			Index += 4;
		} break;
		case 2: // MUL
		{
			T a = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			T b = ModeSwitch<T>(Instructions, Index + 2, RelativeBase, Param2);
			T Target = ModeSwitch<T>(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a * b;
			Index += 4;
		} break;
		case 3: // IN
		{
			T Target = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1, true);
			Instructions[Target] = Input.front();
			Input.pop_front();
			Index += 2;
		} break;
		case 4: // OUT
		{
			T Target = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			Output.push_back(Target);
			Index += 2;
		} break;
		case 5: // JNZ
		{
			T a = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			T b = ModeSwitch<T>(Instructions, Index + 2, RelativeBase, Param2);
			if (a != 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 6: // JEZ
		{
			T a = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			T b = ModeSwitch<T>(Instructions, Index + 2, RelativeBase, Param2);
			if (a == 0)
			{
				Index = b;
			}
			else
			{
				Index += 3;
			}
		} break;
		case 7: // TLT
		{
			T a = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			T b = ModeSwitch<T>(Instructions, Index + 2, RelativeBase, Param2);
			T Target = ModeSwitch<T>(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a < b ? 1 : 0;
			Index += 4;
		} break;
		case 8: // TEQ
		{
			T a = ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			T b = ModeSwitch<T>(Instructions, Index + 2, RelativeBase, Param2);
			T Target = ModeSwitch<T>(Instructions, Index + 3, RelativeBase, Param3, true);
			Instructions[Target] = a == b ? 1 : 0;
			Index += 4;
		} break;
		case 9:
		{
			RelativeBase += ModeSwitch<T>(Instructions, Index + 1, RelativeBase, Param1);
			Index += 2;
		} break;
		}

	}
	return Instructions[0];
}
