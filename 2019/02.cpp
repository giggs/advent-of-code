#include <iostream>
#include <fstream>
#include <vector>
#include "my_functions.h"
#include "intcode.h"


void Two()
{
	std::ifstream File;
	File.open("02.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening File for day 02";
		return;
	}

	std::vector<int> Program;

	ParseProgram(File, Program);
	File.close();
	std::vector<int> ProgramCopy;
	for (int i : Program)
	{
		ProgramCopy.push_back(i);
	}
	ProgramCopy[1] = 12;
	ProgramCopy[2] = 2;

	int P1 = RunProgram(ProgramCopy);

	int Noun = 0;
	int Verb = -1;
	do 
	{
		ProgramCopy.clear();
		Verb++;
		if (Verb == 100)
		{
			++Noun;
			Verb = 0;
		}

		for (int i : Program)
		{
			ProgramCopy.push_back(i);
		}
		ProgramCopy[1] = Noun;
		ProgramCopy[2] = Verb;

	} while (RunProgram(ProgramCopy) != 19690720);

	std::cout << P1 << " " << 100*Noun+Verb << " ";
}