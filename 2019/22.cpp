#include "my_functions.h"
#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <cctype>
#include <map>
#include <deque>
#include <algorithm>
#include <sstream>
#include <vector>
#include <cstdint>
#include "int128.h"

Int128 Modpow(Int128 a, Int128 b, Int128 mod)
{
	if (b == 0)
	{
		return 1;
	}
	else if (b % 2 == 0)
	{
		return Modpow((a * a) % mod, b / 2, mod);
	}
	else
	{
		return (a * Modpow(a, b - 1, mod)) % mod;
	}
}

Int128 Modinv(Int128 a, Int128 mod)
{
	return Modpow(a, mod - 2, mod);
}

void TwentyTwo()
{
	uint64_t P1_DECK_SIZE = 10007;
	std::ifstream File;
	File.open("22.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 22";
		return;
	}
	
	std::deque<int> Deck;
	for (int i = 0; i < P1_DECK_SIZE; i++)
	{
		Deck.push_back(i);
	}

	std::string Line;
	std::vector<std::string> ReversedProgram;
	getline(File, Line);

	for (; Line != ""; getline(File, Line))
	{
		ReversedProgram.push_back(Line);
		std::stringstream SLine(Line);
		std::string Temp;
		int Offset;
		SLine >> Temp;
		if (Temp == "cut")
		{
			SLine >> Offset;
			if (Offset > 0)
			{
				for (int i = 0; i < ABS(Offset); i++)
				{
					Deck.push_back(Deck.front());
					Deck.pop_front();
				}
			}
			else
			{
				for (int i = 0; i < ABS(Offset); i++)
				{
					Deck.push_front(Deck.back());
					Deck.pop_back();
				}
			}
			continue;
		}
		SLine >> Temp;
		if (Temp == "with")
		{
			SLine >> Temp;
			SLine >> Offset;
			std::deque<int> NewDeck(P1_DECK_SIZE, 0);
			int NewPos = 0;
			for (auto It = Deck.begin(); It != Deck.end(); It++)
			{
				NewDeck[NewPos] = *It;
				NewPos += Offset;
				NewPos %= P1_DECK_SIZE;
			}
			Deck = NewDeck;
		}
		else
		{
			std::deque<int> NewDeck(P1_DECK_SIZE, 0);
			auto Fill = NewDeck.begin();
			for (auto It = Deck.rbegin(); It != Deck.rend(); It++)
			{
				*Fill++ = *It;
			}
			Deck = NewDeck;
		}

	}
	int P1 = 0;
	
	for (auto It = Deck.begin(); It != Deck.end(); It++)
	{
		if (*It == 2019)
		{
			std::cout << P1 << " ";
			break;
		}
		P1++;
	}

						 
	Int128 DECK_SIZE = 119315717514047;
	Int128 SHUFFLES = 101741582076661;
	
	Int128 a = 1;
	Int128 b = 0;

	for (auto It = ReversedProgram.rbegin(); It != ReversedProgram.rend(); It++)
	{
		Line = *It;
		if (Line == "deal into new stack")
		{
			a = DECK_SIZE-a;
			b = -b + DECK_SIZE - 1;
		}
		else if (Line[0] == 'c')
		{
			std::stringstream SLine(Line);
			std::string Temp;
			Int128 Offset;
			int o;
			SLine >> Temp;
			SLine >> o;
			if (o < 0)
			{
				Offset = DECK_SIZE - ABS(o);
			}
			else
			{
				Offset = o;
			}
			b = (b + Offset) % DECK_SIZE;
		}
		else
		{
			std::stringstream SLine(Line);
			std::string Temp;
			int Offset;
			SLine >> Temp >> Temp >> Temp;
			SLine >> Offset;
			Int128 N = Modinv(Offset, DECK_SIZE);
			a *= N;
			b *= N;
		}
		a %= DECK_SIZE;
		b %= DECK_SIZE;
	}

	Int128 P2 = (Modpow(a, SHUFFLES, DECK_SIZE) * 2020)%DECK_SIZE;
	Int128 P2B = (b * Modinv(a - 1, DECK_SIZE)) % DECK_SIZE;
	P2 += P2B* (Modpow(a, SHUFFLES, DECK_SIZE) - 1);
	P2 %= DECK_SIZE;

	std::cout << P2 << " ";
	


}

