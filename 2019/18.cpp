#include "my_functions.h"
#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <cctype>
#include <map>
#include <vector>
#include <queue>
#include <bitset>


// Bruteforcing is slow! 
// A better solution is to compute all shortests paths from start to each keys
// And from each key to each key, according to which keys you've collected
// O(n^3)
// Maybe there's even better, I haven't checked the subreddit.

struct MazeState
{
	Point2<int> Pos;
	int Steps = 0;
	uint64_t Keys = 0;

	bool operator<(const MazeState& B) const
	{
		if (Pos != B.Pos)
		{
			return Pos < B.Pos;
		}
		else
		{
			return Keys < B.Keys;
		}
	}
	bool operator==(const MazeState& B) const
	{
		return Pos.x == B.Pos.x && Pos.y == B.Pos.y && Keys == B.Keys;
	}
};

void Eighteen()
{
	constexpr int DirectionCount = 4;
	Point2<int> Directions[DirectionCount] = { {0,1}, {0,-1}, {-1,0}, {1,0} };



	std::ifstream File;
	File.open("18.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 18";
		return;
	}
	

	std::string Line;

	getline(File, Line);



	std::vector<std::vector<char>> Map;
	std::vector<char> Row;

	Point2<int> StartPosition = {};



	for (; Line != ""; getline(File,Line))
	{	
		for (int i = 0; i < Line.size(); i++)
		{
			Row.push_back(Line[i]);
			if (Row[i] == '@')
			{
				StartPosition = { i, static_cast<int>(Map.size()) };
			}
		}
		Map.push_back(Row);
		Row.clear();
	}

	MazeState Start = {};
	Start.Pos = StartPosition;

	std::set<MazeState> Seen;
	std::bitset<26> KeysObtained;
	std::queue<MazeState> Queue;
	Queue.push(Start);

	while (!Queue.empty())
	{
		MazeState Current = Queue.front();
		Queue.pop();
		KeysObtained = Current.Keys;

		if (KeysObtained.all())
		{
			std::cout << Current.Steps << " ";
			break;
		}
		for (auto Dir : Directions)
		{
			MazeState New = Current;
			New.Pos = New.Pos + Dir;
			if (New.Pos.x < 0 || New.Pos.y < 0 || New.Pos.x == Map[0].size() || New.Pos.y == Map.size())
			{
				continue;
			}
			else if (Map[New.Pos.y][New.Pos.x] == '#')
			{
				continue;
			}
			else if (Seen.find(New) != Seen.end())
			{
				continue;
			}
			else if (isupper(Map[New.Pos.y][New.Pos.x])&&
				!KeysObtained[Map[New.Pos.y][New.Pos.x] - 'A'])
			{
				continue;
			}
			if (islower(Map[New.Pos.y][New.Pos.x]))
			{
				KeysObtained[Map[New.Pos.y][New.Pos.x] - 'a'] = true;
			}
			New.Steps++;
			New.Keys = KeysObtained.to_ullong();
			Queue.push(New);
			Seen.insert(New);
			KeysObtained = Current.Keys;
		}
	}


	for (auto Dir : Directions)
	{
		Point2<int> New = StartPosition + Dir;
		Map[New.y][New.x] = '#';
	}



	Point2<int> Diagonals[DirectionCount] = { {-1,-1}, {1,-1}, {-1,1}, {1,1} };
	MazeState P2Starts[4] = {};
	std::bitset<26> P2KeysObtained[4];

	for (int i = 0; i < DirectionCount; i++)
	{
		MazeState New = Start;
		New.Pos = New.Pos + Diagonals[i];
		P2Starts[i] = New;
	}

	for (int y = 0; y < Map.size(); y++)
	{
		for (int x = 0; x < Map[0].size(); x++)
		{
			if (islower(Map[y][x])) 
			{
				// consider keys in other quadrants collected by default, shouldn't work for all inputs ?
				P2KeysObtained[0][Map[y][x] - 'a'] = !((x <= P2Starts[0].Pos.x) && (y <= P2Starts[0].Pos.y));
				P2KeysObtained[1][Map[y][x] - 'a'] = !((x >= P2Starts[1].Pos.x) && (y <= P2Starts[1].Pos.y));
				P2KeysObtained[2][Map[y][x] - 'a'] = !((x <= P2Starts[2].Pos.x) && (y >= P2Starts[2].Pos.y));
				P2KeysObtained[3][Map[y][x] - 'a'] = !((x >= P2Starts[3].Pos.x) && (y >= P2Starts[3].Pos.y));
			}
		}
	}
		
	int P2 = 0;

	for (int i = 0; i < 4; i++)
	{
		MazeState Start = P2Starts[i];
		Start.Keys = P2KeysObtained[i].to_ullong();
		std::set<MazeState> Seen;
		std::bitset<26> KeysObtained;
		std::queue<MazeState> Queue;
		Queue.push(Start);

		while (!Queue.empty())
		{
			MazeState Current = Queue.front();
			Queue.pop();
			KeysObtained = Current.Keys;

			if (KeysObtained.all())
			{
				P2 += Current.Steps;
				break;
			}
			for (auto Dir : Directions)
			{
				MazeState New = Current;
				New.Pos = New.Pos + Dir;
				if (New.Pos.x < 0 || New.Pos.y < 0 || New.Pos.x == Map[0].size() || New.Pos.y == Map.size())
				{
					continue;
				}
				else if (Map[New.Pos.y][New.Pos.x] == '#')
				{
					continue;
				}
				else if (Seen.find(New) != Seen.end())
				{
					continue;
				}
				else if (isupper(Map[New.Pos.y][New.Pos.x]) &&
					!KeysObtained[Map[New.Pos.y][New.Pos.x] - 'A'])
				{
					continue;
				}
				if (islower(Map[New.Pos.y][New.Pos.x]))
				{
					KeysObtained[Map[New.Pos.y][New.Pos.x] - 'a'] = true;
				}
				New.Steps++;
				New.Keys = KeysObtained.to_ullong();
				Queue.push(New);
				Seen.insert(New);
				KeysObtained = Current.Keys;
			}
		}
	}
	std::cout << P2 << " ";
}