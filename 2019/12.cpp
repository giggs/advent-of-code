#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <numeric>
#include "my_functions.h"
constexpr int MoonCount = 4;
constexpr int Steps = 1000;

// It might be faster to just continue the main loop and check the coordinates each time?

struct Moon
{
	int PosX;
	int PosY;
	int PosZ;
	int VelX;
	int VelY;
	int VelZ;

	void ApplyVelocity()
	{
		PosX += VelX;
		PosY += VelY;
		PosZ += VelZ;
	}

	void ApplyGravity(Moon& Other)
	{
		if (PosX < Other.PosX)
		{
			VelX++;
			Other.VelX--;
		}
		else if (Other.PosX < PosX)
		{
			VelX--;
			Other.VelX++;
		}
		if (PosY < Other.PosY)
		{
			VelY++;
			Other.VelY--;
		}
		else if (Other.PosY < PosY)
		{
			VelY--;
			Other.VelY++;
		}
		if (PosZ < Other.PosZ)
		{
			VelZ++;
			Other.VelZ--;
		}
		else if (Other.PosZ < PosZ)
		{
			VelZ--;
			Other.VelZ++;
		}
	}

	int ComputeEnergy()
	{
		return (ABS(PosX) + ABS(PosY) + ABS(PosZ)) * (ABS(VelX) + ABS(VelY) + ABS(VelZ));
	}
};

struct ReduxMoon
{
	int Pos;
	int Vel; 
	
	void ApplyVelocity()
	{
		Pos += Vel;
	}
	void ApplyGravity(ReduxMoon& Other)
	{
		if (Pos < Other.Pos)
		{
			Vel++;
			Other.Vel--;
		}
		else if (Other.Pos < Pos)
		{
			Vel--;
			Other.Vel++;
		}
	}

	bool operator!=(const ReduxMoon& B)
	{
		return Pos != B.Pos || Vel != B.Vel;
	}

};

uint64_t FindPeriod(const ReduxMoon Originals[])
{
	ReduxMoon ReduxMoons[MoonCount] = {};
	for (int i = 0; i < MoonCount; i++)
	{
		ReduxMoons[i] = Originals[i];
	}
	uint64_t Counter = 0;
	do
	{
		for (int i = 0; i < MoonCount - 1; i++)
		{
			for (int j = i + 1; j < MoonCount; j++)
			{
				ReduxMoons[i].ApplyGravity(ReduxMoons[j]);
			}
		}
		for (int i = 0; i < MoonCount; i++)
		{
			ReduxMoons[i].ApplyVelocity();
		}
		Counter++;
	} while ((ReduxMoons[0] != Originals[0]) || (ReduxMoons[1] != Originals[1])
		|| (ReduxMoons[2] != Originals[2]) || (ReduxMoons[3] != Originals[3]));

	return Counter;
}

void Twelve()
{
	std::ifstream File;
	File.open("12.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 12";
		return;
	}

	Moon Moons[MoonCount] = {};

	std::string Line;
	for (int i = 0; i < MoonCount; i++)
	{
		getline(File, Line);
		std::stringstream ss(Line);
		char _;
		ss >> _ >> _ >> _ >> Moons[i].PosX
			>> _ >> _ >> _ >> Moons[i].PosY
			>> _ >> _ >> _ >> Moons[i].PosZ;
	}

	ReduxMoon OriginalMoonsX[MoonCount] = {};
	ReduxMoon OriginalMoonsY[MoonCount] = {};
	ReduxMoon OriginalMoonsZ[MoonCount] = {};
	for (int i = 0; i < MoonCount; i++)
	{
		OriginalMoonsX[i].Pos = Moons[i].PosX;
		OriginalMoonsX[i].Vel = Moons[i].VelX;
		OriginalMoonsY[i].Pos = Moons[i].PosY;
		OriginalMoonsY[i].Vel = Moons[i].VelY;
		OriginalMoonsZ[i].Pos = Moons[i].PosZ;
		OriginalMoonsZ[i].Vel = Moons[i].VelZ;
	}

	for (int S = 1; S <= Steps; S++)
	{
		for (int i = 0; i < MoonCount - 1; i++)
		{
			for (int j = i + 1; j < MoonCount; j++)
			{
				Moons[i].ApplyGravity(Moons[j]);
			}
		}
		for (int i = 0; i < MoonCount; i++)
		{
			Moons[i].ApplyVelocity();
		}
	}

	int P1 = 0;
	for (int i = 0; i < MoonCount; i++)
	{
		P1 += Moons[i].ComputeEnergy();
	}

	std::cout << P1 << " ";

	uint64_t PeriodX = FindPeriod(OriginalMoonsX);
	uint64_t PeriodY = FindPeriod(OriginalMoonsY);
	uint64_t PeriodZ = FindPeriod(OriginalMoonsZ);

	std::cout << std::lcm((std::lcm(PeriodX, PeriodY)),PeriodZ) << " ";

}

// 231614 too too low