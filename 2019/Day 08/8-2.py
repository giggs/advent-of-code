with open('input.txt') as f:
    image = list(f.read())

layers = [image[i:i+150] for i in range(0,len(image), 150)]
decoded_image = []

while len(decoded_image) < 150:
    decoded_image.append(0)

i = 0
while i < 150:
    n = 0 
    while n < len(layers):
        if layers[n][i] != '2':
            decoded_image[i] = layers[n][i]
            break
        else:
            n += 1
    i += 1

for i in range(5,len(decoded_image)*2, 6):
    decoded_image.insert(i,' ')
for i in range(30,len(decoded_image), 31):
    decoded_image.insert(i,'\n')

print(''.join(decoded_image))