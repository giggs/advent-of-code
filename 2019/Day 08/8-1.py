with open('input.txt') as f:
    image = list(f.read())

image = [int(x) for x in image]
layers = [image[i:i+150] for i in range(0,len(image), 150)]
zeros = [n.count(0) for n in layers]
target_layer = zeros.index(min(zeros))
answer = layers[target_layer].count(1) * layers[target_layer].count(2)

print(answer)