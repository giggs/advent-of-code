#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "my_functions.h"

constexpr int LayerWidth = 25;
constexpr int LayerHeight = 6;
constexpr int LayerSize = LayerHeight * LayerWidth;

struct Layer
{
	unsigned ZeroCount = 0;
	unsigned OneCount = 0;
	unsigned TwoCount = 0;
};

void Eight()
{
	std::ifstream File;
	File.open("08.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 08";
		return;
	}

	std::vector<char> Pixels((std::istreambuf_iterator<char>(File)), {});
	File.close();
	std::vector<Layer> Layers;
	unsigned MinZeroCount = INT_MAX;
	unsigned MinZeroCountIndex = 0;
	unsigned LayerIndex = 0;
	Layer Current;
	char FinalLayer[LayerSize] = {};

	for (unsigned PixelIndex = 0; PixelIndex != Pixels.size(); PixelIndex++)
	{
		switch (Pixels[PixelIndex])
		{
			case '0':
			{
				Current.ZeroCount++;
				if (FinalLayer[PixelIndex % LayerSize] == 0)
				{
					FinalLayer[PixelIndex % LayerSize] = '0';
				}
			} break;
			case '1':
			{
				Current.OneCount++;
				if (FinalLayer[PixelIndex % LayerSize] == 0)
				{
					FinalLayer[PixelIndex % LayerSize] = '1';
				}
			} break;
			case '2':
			{
				Current.TwoCount++;
			} break;
		}
		if (PixelIndex != 0 && PixelIndex % LayerSize == 0)
		{
			Layers.push_back(Current);
			if (Current.ZeroCount < MinZeroCount)
			{
				MinZeroCount = Current.ZeroCount;
				MinZeroCountIndex = LayerIndex;
			}
			Current = {};
			LayerIndex++;
		}
	}

	std::cout << Layers[MinZeroCountIndex].OneCount * Layers[MinZeroCountIndex].TwoCount << '\n';
	std::string Output;

	for (unsigned Row = 0; Row < LayerHeight; Row++)
	{
		for (unsigned Column = 0; Column < LayerWidth; Column++)
		{
			FinalLayer[Row*LayerWidth + Column] == '0' ? Output += " " : Output += "#";	
		}
		Output += '\n';
	}
	std::cout << Output;
}