#ifndef MY_FUNCTIONS
#define MY_FUNCTIONS
#define ABS(A) ((A) > 0 ? (A) : -(A))

template <typename T>
struct Point2
{
	T x;
	T y;

	bool operator<(const Point2& B) const
	{
		if (y != B.y)
		{
			return y < B.y;
		}
		else
		{
			return x < B.x;
		}
	}

	bool operator==(const Point2& B) const
	{
		return x == B.x && y == B.y;
	}

	bool operator!=(const Point2& B) const
	{
		return x != B.x || y != B.y;
	}

	Point2 operator+(const Point2& B) const
	{
		Point2 Temp;
		Temp.x = x + B.x;
		Temp.y = y + B.y;
		return Temp;
	}

};

int ManhattanDistanceToOrigin(Point2<int> A);

void One();
void Two();
void Three();
void Four();
void Five();
void Six();
void Seven();
void Eight();
void Nine();
void Ten();
void Eleven();
void Twelve();
void Thirteen();
void Fourteen();
void Fifteen();
void Sixteen();
void Seventeen();
void Eighteen();
void Nineteen();
void Twenty();
void TwentyOne();
void TwentyTwo();
void TwentyThree();
void TwentyFour();
void TwentyFive();

#endif // !MY_FUNCTIONS

