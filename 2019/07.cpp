#include <algorithm>
#include "my_functions.h"
#include "intcode.h"

void Seven()
{
	std::ifstream File;
	File.open("07.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 07";
	}

	std::vector<int> Program;
	ParseProgram(File, Program);
	File.close();
	std::vector<int> Phases = { 0,1,2,3,4 };
	std::deque<int> Input;
	std::vector<int> Output;
	int P1 = 0;

	do
	{
		int InputSignal = 0;
		for (int Amplifier = 0; Amplifier < 5; Amplifier++)
		{
			std::vector<int> Program_copy = Program;
			Input.push_back(Phases[Amplifier]);
			Input.push_back(InputSignal);
			RunProgram<int>(Program, Input, Output);
			InputSignal = Output.back();
		}
		P1 = InputSignal > P1 ? InputSignal : P1;

	} while (std::next_permutation(Phases.begin(), Phases.end()));
	
	std::cout << P1 << " ";


	Phases = { 5,6,7,8,9 };
	int P2 = 0;
	do
	{
		Amplifier AmplifierLoop[5];

		for (int i = 0; i < 5; i++)
		{
			AmplifierLoop[i].Instructions = Program;
			AmplifierLoop[i].Input.push_back(Phases[i]);
			if (i == 0)
			{
				AmplifierLoop[i].Input.push_back(0);
			}
		}

		int AmplifierIndex = 0;

		while (true)
		{
			Amplifier& Current = AmplifierLoop[AmplifierIndex];
			int Result = Current.RunProgram(true);
			if (Result == 0)
			{
				int ThrusterSignal = AmplifierLoop[4].Output.back();
				P2 = ThrusterSignal > P2 ? ThrusterSignal : P2;
				break;
			}
			int NextInput = Current.Output.back();
			AmplifierIndex++;
			AmplifierIndex = AmplifierIndex > 4 ? 0 : AmplifierIndex;
			AmplifierLoop[AmplifierIndex].Input.push_back(NextInput);
		}
		
	} while (std::next_permutation(Phases.begin(), Phases.end()));

	std::cout << P2 << " ";
}
