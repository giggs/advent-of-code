#include "my_functions.h"
#include "intcode.h"


void Five()
{
	std::ifstream File;
	File.open("05.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 05";
		return;
	}

	std::vector<int> Program;

	ParseProgram(File, Program);
	File.close();
	std::vector<int> Program_copy = Program;
	std::deque<int> Input = { 1 };
	std::vector<int> Output;

	int Result;
	Result = RunProgram<int>(Program, Input, Output);

	std::cout << Output.back() << " ";

	Input.push_front(5);

	Result = RunProgram<int>(Program_copy, Input, Output);

	std::cout << Output.back() << " ";

}