#include "my_functions.h"
#include "intcode.h"
#include <set>

// Very slow, probably a better idea to model the tractor beam edges rather than bruteforce
// but I just want to get 400 stars

void Nineteen()
{
	std::ifstream File;
	File.open("19.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 19";
		return;
	}
	
	std::vector<int64_t> Program;
	ParseProgram(File, Program);

	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	File.close();

	std::deque<int64_t> Input;
	std::vector<int64_t> Output;
	
	int64_t P1 = 0;

	for (int y = 0; y < 50; y++)
	{
		for (int x = 0; x < 50; x++)
		{
			std::vector<int64_t> Program_copy = Program;
			Input.push_back(x);
			Input.push_back(y);
			RunProgram(Program_copy, Input, Output);
			P1 += Output.back();
		}
	}
	std::cout << P1 << " ";

	std::set<Point2<int>> TractorBeam;

	int First = 0;
	int y = 51;
	bool SpaceShipFits = false;
	while (!SpaceShipFits)
	{
		int NewFirst = 0;
		bool FoundFirstX = false;
		bool EndOfBeam = false;
		for (int x = First; !FoundFirstX || !EndOfBeam; x++)
		{
			std::vector<int64_t> Program_copy = Program;
			Input.push_back(x);
			Input.push_back(y);
			RunProgram(Program_copy, Input, Output);
			if (Output.back())
			{
				if (!FoundFirstX)
				{
					FoundFirstX = true;
					NewFirst = x;
				}
				TractorBeam.insert({ x,y });
				auto Search = TractorBeam.find({ x, y - 99 });
				if (Search != TractorBeam.end())
				{
					Search = TractorBeam.find({ x + 99, y - 99 });
					if (Search != TractorBeam.end())
					{
						std::cout << x * 10000 + y-99 << " ";
						return;
					}
				}
			}
			else
			{
				if (FoundFirstX)
				{
					EndOfBeam = true;
				}
			}
		}
		First = NewFirst;
		y++;
	}
}