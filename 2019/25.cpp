#include "my_functions.h"
#include "intcode.h"
#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <cctype>
#include <map>

void TwentyFive()
{

	std::ifstream File;
	File.open("25.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 25";
		return;
	}
	
	std::vector<int64_t> Program;
	ParseProgram(File, Program);

	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	File.close();

	FinalBot Droid = {};
	Droid.Instructions = Program;

	while (true)
	{
		Droid.RunProgram();

		if (Droid.Output.size() > 0)
		{
			for (auto& c : Droid.Output)

			{
				std::cout << static_cast<char>(c);
			}

			std::vector<char> Commands;
			std::string Temp;
			
			std::getline(std::cin, Temp);

			for (auto& c : Temp)
			{
				Droid.Input.push_back(c);
			}
	
			Droid.Input.push_back('\n');

			Droid.Output.clear();
		}
	}
	// Items to take to the pressure plate are: mutex, loom, wreath and sand
	// Avoid molten lava (duh), photon, dark matter, electromagnet
}
