#include "my_functions.h"
#include "intcode.h"
#include <set>

void TwentyOne()
{

	std::ifstream File;
	File.open("21.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 21";
		return;
	}

	std::vector<int64_t> Program;
	ParseProgram(File, Program);

	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	File.close();

	std::vector<int64_t> ProgramCopy = Program;

	// Jump if any of the first 3 tiles has a hole but the 4th hasn't.

	std::string ASCIIProgram = "OR A J\nAND B J\nAND C J\nNOT J J\nAND D J\nWALK\n";

	std::deque<int64_t> Input;
	std::vector<int64_t> Output;

	for (char c : ASCIIProgram)
	{
		Input.push_back(c);
	}

	RunProgram(Program, Input, Output);

	std::cout << Output.back() << " ";

	// Same, but don't jump if you're forced to jump in a whole after landing

	ASCIIProgram = "OR A J\nAND B J\nAND C J\nNOT J J\nAND D J\nOR E T\nOR H T\nAND T J\nRUN\n";
	Input = {};

	for (char c : ASCIIProgram)
	{
		Input.push_back(c);
	}

	RunProgram(ProgramCopy, Input, Output);

	std::cout << Output.back() << " ";

}
