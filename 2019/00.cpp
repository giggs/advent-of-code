#include <time.h>
#include <iostream>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "my_functions.h"


int64_t GlobalPerfCountFrequency;

inline double Win32TimeElapsed(LARGE_INTEGER Start, LARGE_INTEGER End)
{
	double Result = ((double)(End.QuadPart - Start.QuadPart) / (double)GlobalPerfCountFrequency);
	return Result;
}


int main()
{
	double Total = 0.0f;
	LARGE_INTEGER Begin = {};
	LARGE_INTEGER End = {};
	double DeltaT = 0.0f;
	LARGE_INTEGER PerfCountFrequencyResult;
	QueryPerformanceFrequency(&PerfCountFrequencyResult);
	GlobalPerfCountFrequency = PerfCountFrequencyResult.QuadPart;

	QueryPerformanceCounter(&Begin);
	One();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Two();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Three();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';	

	QueryPerformanceCounter(&Begin);
	Four();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Five();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Six();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Seven();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Eight();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Nine();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Ten();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Eleven();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Twelve();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Thirteen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Fourteen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Fifteen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Sixteen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Seventeen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Eighteen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Nineteen();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	Twenty();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	TwentyOne();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	TwentyTwo();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	TwentyThree();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	TwentyFour();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';
	
	QueryPerformanceCounter(&Begin);
	TwentyFive();
	QueryPerformanceCounter(&End);
	DeltaT = Win32TimeElapsed(Begin, End);
	Total += DeltaT;
	std::cout << DeltaT << '\n';

	std::cout << "Total time: " << Total;

}