#include <map>
#include "my_functions.h"
#include "intcode.h"

void Thirteen()
{
	std::ifstream File;
	File.open("13.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 13";
		return;
	}

	std::vector<int64_t> Program;
	ParseProgram(File, Program);
	for (int i = 0; i < 10000; i++)
	{
		Program.push_back(0);
	} // Input padding, need to do better than that 
	
	std::vector<int64_t> Program_copy = Program;
	std::map<Point2<int64_t>, int64_t> GameScreen;
	std::deque<int64_t> Input;
	std::vector<int64_t> Output;

	RunProgram<int64_t>(Program, Input, Output);
	int P1 = 0;
	for (size_t Index = 0; Index < Output.size(); Index += 3)
	{
		GameScreen.insert_or_assign({ Output[Index], Output[Index + 1] }, Output[Index + 2]);
		if (Output[Index + 2] == 2)
		{
			P1++;
		}
	}

	std::cout << P1 << " ";

	Program_copy[0] = 2;
	ArkanoidBot Bot = {};
	Bot.Instructions = Program_copy;
	Bot.RunProgram();

	std::cout << Bot.Output.back() << " ";
}

/* This is the code to that prints out the gamescreen

int y = 0;
	for (auto It = GameScreen.begin(); It != GameScreen.end(); It++)
	{
		if (It->first.x == -1)
		{
			std::cout << "-1 input";
		}
		if (It->first.y > y)
		{
			y++;
			std::cout << '\n';
		}
		switch (It->second)
		{
			case 0:
			{
				std::cout << " ";
			} break;
			case 1:
			{
				std::cout << "|";
			}break;
			case 2:
			{
				std::cout << "#";
			}break;
			case 3:
			{
				std::cout << "_";
			}break;
			case 4:
			{
				std::cout << "O";
			}break;
			default:
			{
				std::cout << "Invalid tile ID";
			}
		}
	}*/
