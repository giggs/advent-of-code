#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "my_functions.h"

inline int ComputeFuel(int Mass)
{
	return Mass / 3 - 2;
}

void One()
{
	std::ifstream File;
	File.open("01.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening File for day 01";
		return;
	}
	std::vector<int> Modules;
	std::string Line;
	getline(File, Line);

	int P1 = 0;
	int P2 = 0;

	for (; Line != ""; getline(File, Line))
	{
		int Mass = stoi(Line);
		Mass = ComputeFuel(Mass);
		P1 += Mass;
		while (Mass > 0)
		{
			P2 += Mass;
			Mass = ComputeFuel(Mass);
		}

	}
	File.close();
	std::cout << P1 << " " << P2 << " ";
}