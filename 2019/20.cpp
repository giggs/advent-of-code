#include "my_functions.h"
#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <cctype>
#include <map>
#include <vector>
#include <queue>


struct Point2Steps
{
	Point2<int> Pos;
	int Steps;

	bool operator<(const Point2Steps& B) const
	{
		return Pos < B.Pos;
	}
};

struct Point3
{
	int x;
	int y;
	int z;

	bool operator< (const Point3& B)const
	{
		if (z != B.z)
		{
			return z < B.z;
		}
		else if (y != B.y)
		{
			return y < B.y;
		}
		else
		{
			return x < B.x;
		}
	}

	bool operator== (const Point3& B)const
	{
		return x == B.x && y == B.y && z == B.z;
	}

};

struct Point3Steps
{
	Point3 Pos;
	int Steps;
};

void Twenty()
{
	std::map<std::string, std::vector<Point2<int>>> Warps;

	std::ifstream File;
	File.open("20.txt", std::ifstream::in);
	if (!File.good())
	{
		std::cout << "Error opening file for day 20";
		return;
	}

	int MaxX = 0;
	std::string Line;
	std::vector<std::vector<char>> Map;

	while (std::getline(File, Line))
	{
		std::vector<char> Row;
		if (Line == "")
		{
			break;
		}
		for (auto c : Line)
		{
			Row.push_back(static_cast<char>(c));
		}
		if (Line.size() > MaxX)
		{
			MaxX = Line.size();
		}
		Map.push_back(Row);
	}
	int MaxY = Map.size();
	
	for (int x = 2; x < MaxX - 2; x++)
	{
		if (std::isalpha(Map[1][x]))
		{
			std::string Temp;
			Temp += Map[0][x]; 
			Temp += Map[1][x];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{x,2}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ x,2 });
				Warps.erase(Temp);
				Warps.insert({Temp, TempV});
			}
		}
	}

	for (int x = 2; x < MaxX - 2; x++)
	{
		if (std::isalpha(Map[MaxY-2][x]))
		{
			std::string Temp;
			Temp += Map[MaxY - 2][x]; 
			Temp += Map[MaxY - 2+1][x]; 
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{x,MaxY-2-1}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ x,MaxY-2-1 });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}

	for (int y = 2; y < MaxY - 2; y++)
	{
		if (std::isalpha(Map[y][0]))
		{
			std::string Temp;
			Temp += Map[y][0];
			Temp += Map[y][1];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{2,y}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ 2,y });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}

	for (int y = 2; y < MaxY - 2; y++)
	{
		if (std::isalpha(Map[y][131]))
		{
			std::string Temp;
			Temp += Map[y][131];
			Temp += Map[y][132];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{130,y}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ 130,y });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}



	for (int y = 2; y < MaxY - 2; y++)
	{
		if (std::isalpha(Map[y][37]))
		{
			std::string Temp;
			Temp += Map[y][37];
			Temp += Map[y][38];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{36,y}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ 36,y });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}

	for (int x = 2; x < MaxX - 2; x++)
	{
		if (std::isalpha(Map[93][x]))
		{
			std::string Temp;
			Temp += Map[92][x];
			Temp += Map[93][x];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{x,94}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ x,94 });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}

	for (int x = 2; x < MaxX - 2; x++)
	{
		if (std::isalpha(Map[37][x]))
		{
			std::string Temp;
			Temp += Map[37][x];
			Temp += Map[37 + 1][x];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{x,37 - 1}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ x,37 - 1 });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}

	for (int y = 2; y < MaxY - 2; y++)
	{
		if (std::isalpha(Map[y][94]))
		{
			std::string Temp;
			Temp += Map[y][94];
			Temp += Map[y][95];
			auto SearchWarps = Warps.find(Temp);
			if (SearchWarps == Warps.end())
			{
				Warps.insert({ Temp, {{96,y}} });
			}
			else
			{
				std::vector<Point2<int>> TempV = SearchWarps->second;
				TempV.push_back({ 96,y });
				Warps.erase(Temp);
				Warps.insert({ Temp, TempV });
			}
		}
	}


	std::map<Point2<int>, Point2<int>> GoBack;
	std::map<Point2<int>, Point2<int>> Recurse;

	Point2<int> Start;
	Point2<int> End;

	for (auto It = Warps.begin(); It != Warps.end(); It++)
	{
		if (It->first == "AA")
		{
			Start = It->second[0];
			continue;
		}
		else if (It->first == "ZZ")
		{
			End = It->second[0];
		}
		else
		{
			GoBack.insert({ It->second[0], It->second[1] });
			Recurse.insert({ It->second[1], It->second[0] });
		}
	}

	std::queue<Point2Steps> Queue;
	Queue.push({ Start, 0 });
	std::set<Point2<int>> Seen;
	Seen.insert(Start);

	constexpr int DirectionCount = 4;
	Point2<int> Directions[DirectionCount] = { {0,1}, {0,-1}, {-1,0}, {1,0} };
	bool Portaling = true;
	while (Portaling)
	{
		Point2Steps Current = Queue.front();
		Queue.pop();

		for (Point2<int> Dir : Directions)
		{
			Point2Steps New = Current;
			New.Pos = New.Pos + Dir;
			New.Steps++;
			if (New.Pos == End)
			{
				std::cout << New.Steps << " ";
				Portaling = false;
				break;
			}
			if (Map[New.Pos.y][New.Pos.x] != '.')
			{
				continue;
			}
			else if (Seen.find(New.Pos) != Seen.end())
			{
				continue;
			}
			
			Seen.insert(New.Pos);
			auto Search = GoBack.find(New.Pos);
			if (Search != GoBack.end())
			{
				New.Pos = Search->second;
				New.Steps++;
				Seen.insert(New.Pos);
				Queue.push(New);
				continue;
			}
			Search = Recurse.find(New.Pos);
			if (Search != Recurse.end())
			{
				New.Pos = Search->second;
				New.Steps++;
				Seen.insert(New.Pos);
				Queue.push(New);
				continue;
			}
			Queue.push(New);
		}
	}
	
	Point3 Start3D = { Start.x, Start.y, 0 };
	Point3 End3D = { End.x, End.y, 0 };
	std::queue < Point3Steps> Queue3D;
	Queue3D.push({ Start3D, 0 });
	std::set<Point3> Seen3D;
	Seen3D.insert(Start3D);
	bool Recursing = true;
	while (Recursing)
	{
		Point3Steps Current = Queue3D.front();
		Queue3D.pop();

		for (Point2<int> Dir : Directions)
		{
			Point3Steps New = Current;
			New.Pos.x += Dir.x;
			New.Pos.y += Dir.y;
			New.Steps++;
			if (New.Pos == End3D)
			{
				std::cout << New.Steps << " ";
				Recursing = false;
				break;
			}
			if (Map[New.Pos.y][New.Pos.x] != '.')
			{
				continue;
			}
			else if (Seen3D.find(New.Pos) != Seen3D.end())
			{
				continue;
			}

			Seen3D.insert(New.Pos);
			auto Search = GoBack.find({ New.Pos.x, New.Pos.y });
			if (Search != GoBack.end())
			{
				if (New.Pos.z == 0)
				{
					continue;
				}
				New.Pos.x = Search->second.x;
				New.Pos.y = Search->second.y;
				New.Pos.z -= 1;
				New.Steps++;
				Seen3D.insert(New.Pos);
				Queue3D.push(New);
				continue;
			}
			Search = Recurse.find({ New.Pos.x, New.Pos.y });
			if (Search != Recurse.end())
			{
				New.Pos.x = Search->second.x;
				New.Pos.y = Search->second.y;
				New.Steps++;
				New.Pos.z += 1;
				Seen3D.insert(New.Pos);
				Queue3D.push(New);
				continue;
			}
			Queue3D.push(New);
		}
	}
}
