from collections import defaultdict

data = open('input.txt').read().splitlines()
values = defaultdict(lambda: 0)

def bitwise (s1,s2,keyword):
    if keyword == 'LSHIFT':
        return values[s1] << int(s2)
    elif keyword == 'RSHIFT':
        return values[s1] >> int(s2)
    elif keyword == 'AND':
        if s1.isnumeric():
            return 1 & values[s2]
        else:
            return values[s1] & values[s2]
    elif keyword == 'OR':
        return values[s1] | values[s2]

def build_circuit(data):
    for _ in range(25):
        for line in data:
            instruction, destination = line.split(' -> ')
            instruction = instruction.split(' ')
            if len(instruction) == 1:
                if instruction[0].isnumeric(): 
                    values[destination] = int(instruction[0])
                else: 
                    values[destination] = values[instruction[0]]
            elif len(instruction) == 2:
                values[destination] = ~ values[instruction[1]]
            else:
                values[destination] = bitwise(instruction[0],instruction[2],instruction[1])

build_circuit(data)
print(values['a'])

data = open('input2.txt').read().splitlines()
values = defaultdict(lambda: 0)

build_circuit(data)
print(values['a'])