instructions = open('input.txt').read().replace(',','').splitlines()

def run_program(a,b,pointer):
    while pointer in range(len(instructions)):
        instruction = instructions[pointer].split(' ')
        if len(instruction) == 2:
            if instruction[0] == 'hlf':
                if instruction[1] == 'a':
                    a = a // 2
                else:
                    b = b // 2
            elif instruction[0] == 'tpl':
                if instruction[1] == 'a':
                    a *= 3
                else:
                    b *= 3
            elif instruction[0] == 'inc':
                if instruction[1] == 'a':
                    a += 1
                else:
                    b += 1
            elif instruction[0] == 'jmp':
                pointer += int(instruction[1]) - 1
        else:
            if instruction[0] == 'jio':
                if instruction[1] == 'a' and a == 1:
                    pointer += int(instruction[2]) - 1
                elif instruction[1] == 'b' and b == 1:
                    pointer += int(instructions[2]) - 1
            else:
                if instruction[1] == 'a' and a % 2 == 0:
                    pointer += int(instruction[2]) - 1
                elif instruction[1] == 'b' and b % 2 == 0:
                    pointer += int(instruction[2]) - 1
        pointer += 1
    print(b)

run_program(0,0,0)
run_program(1,0,0)