# I couldn't think of a way to code an algorithm to find the strategy.
# I also thought it'd be funnier to actually play the game, so this is just game logic.
# Play it yourself to find the optimal strategy!

def fight_boss(difficulty):
    boss_hp = 51
    boss_damage = 9
    my_mana = 500
    my_hp = 50
    shield = 0
    poison = 0
    recharge = 0
    used_mana = 0

    while True:
        if difficulty == 'hard':
            my_hp -= 1
        if my_hp < 1 or my_mana < 53:
            print("you lose")
            break
        armor = 0
        if recharge > 0:
            my_mana += 101
            recharge -= 1
        if poison > 0:
            boss_hp -= 3
            poison -= 1
        if shield > 0:
            armor = 7
            shield -= 1
        if boss_hp < 1:
            print("you win")
            break
        print(my_hp,my_mana,boss_hp,shield,poison,recharge)
        command = input("1 = magic missile, 2 = Drain, 3 = Shield, 4 = Poison, 5 = Recharge\n")
        if command == '1':
            my_mana -= 53
            used_mana += 53
            boss_hp -= 4
        elif command == '2':
            my_mana -= 73
            used_mana += 73
            boss_hp -= 2
            my_hp += 2
        elif command == '3':
            my_mana -= 113
            used_mana += 113
            shield = 6
        elif command == '4':
            my_mana -= 173
            used_mana += 173
            poison = 6
        else:
            my_mana -= 229
            used_mana += 229
            recharge = 5
        armor = 0
        if recharge > 0:
            my_mana += 101
            recharge -= 1
        if poison > 0:
            boss_hp -= 3
            poison -= 1
        if shield > 0:
            armor = 7
            shield -= 1
        if boss_hp < 1:
            print('you win')
            break
        my_hp -= (boss_damage - armor)

    print(my_hp,my_mana,boss_hp,shield,poison,recharge)
    print(used_mana)

fight_boss("easy") # 4,5,3,4,1,1,1,1
fight_boss("hard") # 4,5,3,4,5,2,4,1