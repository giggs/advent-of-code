import re
import itertools

ingredients = open('input.txt').read()
part1 = ingredients.replace(':',',').splitlines()

sugar = [int(x) for x in re.findall('-?\d', part1[0])]
sprinkles = [int(x) for x in re.findall('-?\d', part1[1])]
candy = [int(x) for x in re.findall('-?\d', part1[2])]
chocolate = [int(x) for x in re.findall('-?\d', part1[3])]

recipes = [x for x in itertools.combinations_with_replacement(['sugar','sprinkles','candy','chocolate'],100)]
scores = []
scores_for_500 = []

for recipe in recipes:
    score = [0,0,0,0,0]
    for ing in recipe:
        if ing == 'sugar':
            for i in range(5):
                score[i] += sugar[i]
        elif ing == 'sprinkles':
            for i in range(5):
                score[i] += sprinkles[i]
        elif ing == 'candy':
            for i in range(5):
                score[i] += candy[i]
        else:
            for i in range(5):
                score[i] += chocolate[i]
    for i in range(4):
        if score[i] < 0:
            score[i] = 0
    scores.append(score[0]*score[1]*score[2]*score[3])
    if score[4] == 500:
        scores_for_500.append(score[0]*score[1]*score[2]*score[3])

print(max(scores))
print(max(scores_for_500))