stats = open('input.txt').read().splitlines()

boss_hp = int(stats[0].split(': ')[1])
boss_damage = int(stats[1].split(': ')[1])
boss_armor = int(stats[2].split(': ')[1])
my_hp = 100
my_damage = 0
my_armor = 0
cheap_attack = {4:8, 5:10, 6:25, 7:40, 8:65, 9:90, 10:124, 11:174}
cheap_armor = {0:0, 1:13, 2:31, 3:51, 4:71, 5:93, 6:115, 7:142, 8:182}
expensive_attack = {4:8, 5:33, 6:58, 7:108, 8:110, 9:125, 10:140, 11:174}
expensive_armor = {0:0, 1:20, 2:40, 3:80, 4:93, 5:111, 6:33, 7:155, 8:182}
winning_stats = []
losing_stats = []
winning_spendings = []
losing_spendings = []

def fight_boss(my_hp,my_damage,my_armor,boss_hp,boss_damage,boss_armor):
    while my_hp > 0 and boss_hp > 0:
        boss_hp -= (my_damage - boss_armor) if my_damage > boss_armor else 1
        if boss_hp > 0:
            my_hp -= (boss_damage - my_armor) if boss_damage > my_armor else 1
    if my_hp > 0:
        return(True,my_damage,my_armor)
    else:
        return(False,my_damage,my_armor)

for x in range(4,12):
    for y in range(0,9):
        result = fight_boss(100,x,y,boss_hp,boss_damage,boss_armor)
        if result[0]:
            winning_stats.append((result[1],result[2]))
        else:
            losing_stats.append((result[1],result[2]))

for (x,y) in winning_stats:
    winning_spendings.append(cheap_attack[x]+cheap_armor[y])
print(min(winning_spendings))

for (x,y) in losing_stats:
    losing_spendings.append(expensive_attack[x]+expensive_armor[y])
print(max(losing_spendings))