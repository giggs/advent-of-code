input = open('input.txt').read().splitlines()
ticker_tape = open('ticker tape.txt').read().splitlines()
checklist = {}
for line in ticker_tape:
    key,value = line.split(': ')
    checklist[key] = int(value)

def find_Sue(data,part):
    possible_Sues = []
    for line in data:
        line = line.replace(',',':').split(': ')
        checks = 0
        for i in range(1,len(line),2):
            if part == 'part2' and (line[i] == 'cats' or line[i] == 'trees'):
                if int(line[i+1]) > checklist[line[i]]:
                    checks += 1
            elif part == 'part2' and (line[i] == 'pomeranians' or line[i] == 'goldfish'):
                if int(line[i+1]) < checklist[line[i]]:
                    checks += 1
            elif checklist[line[i]] == int(line[i+1]):
                checks += 1
        if checks == 3:
            possible_Sues.append(line[0])
    print(possible_Sues)

find_Sue(input,'part1')
find_Sue(input,'part2')