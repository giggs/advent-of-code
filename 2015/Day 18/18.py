grid = [list(line) for line in open('input.txt').read().splitlines()]

row = len(grid)
column = len(grid[0])
on_lights = set()

for x in range(row):
    for y in range(column):
        if grid[x][y] == '#':
            on_lights.add((x,y))

part2 = on_lights.copy()

def animate(data,r,c,part,steps):
    for _ in range(steps):
        corners = [(0,0), (0,c-1), (r-1,0), (r-1,c-1)]
        new_lights = set(corners) if part == 'part2' else set()
        for x in range(r):
            for y in range(c):
                neighbours = 0
                for dx in [-1,0,1]:
                    for dy in [-1,0,1]:
                        if dx != 0 or dy != 0:
                            if (x+dx,y+dy) in data:
                                neighbours += 1
                if neighbours == 3:
                    new_lights.add((x,y))
                elif neighbours == 2 and (x,y) in data:
                    new_lights.add((x,y))
        data = new_lights.copy()
    print(len(data))

animate(on_lights,row,column,'part1',100)
animate(on_lights,row,column,'part2',100)