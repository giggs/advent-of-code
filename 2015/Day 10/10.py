input = [int(x) for x in '1113122113']
    
for _ in range(50):
    temp = []
    i = 0
    while i < len(input):
        count = 1
        c = input[i]
        if i < len(input)-1:
            while input[i+count] == c:
                count += 1
        i += count
        temp.extend([count, c])
    input = temp.copy()

print(len(input))