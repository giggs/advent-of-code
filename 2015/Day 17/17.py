import itertools

containers = [int(x) for x in open('input.txt').read().splitlines()]
good_combinations = []
lengths = []

for k in range(len(containers)): # range can be narrowed down to 4-10 for speed
    combinations = [x for x in itertools.combinations(containers,k)]
    for combination in combinations:
        if sum(combination) == 150:
            good_combinations.append(combination)
            lengths.append(len(combination))

print(len(good_combinations))
print(lengths.count(min(lengths)))