with open('input.txt') as f:
    input = f.read()

input = input.replace('\n','')

locations = [(0,0)]
locations_sr = [(0,0)]

def navigate(start,step,data,houses):
    x = 0
    y = 0
    for i in range(start,len(data),step):
        if data[i] == '<':
            x -= 1
        elif data[i] == '>':
            x += 1
        elif data[i] == '^':
            y += 1
        else: 
            y -= 1
        houses.append((x,y))
    return houses

print(len(set(navigate(0,1,input,locations))))
locations_sr = navigate(0,2,input,locations_sr)
print(len(set(navigate(1,2,input,locations_sr))))