import string

alphabet = string.ascii_lowercase
forbidden_letters = 'iol'
input = list('hxbxwxba')

def increment(data,alphabet):
    i = -1
    while i > - len(data):
        letter = data[i]
        if letter != 'z':
            data[i] = alphabet[alphabet.index(letter)+1]
            return data
        else:
            data[i] = 'a'
            i -= 1

def search_next(password,alphabet,forbidden_letters):
    search = True
    while search:
        password = increment(password,alphabet)
        forbidden = False
        straight = False
        pairs = 0
        for i in range(len(password)):
            if password[i] in forbidden_letters:
                forbidden = True
            if i < len(password) - 2 and ''.join(password[i:i+3]) in alphabet:
                straight = True
            if i == len(password)-1:
                continue
            else:
                if password[i] == password[i+1]:
                    if i == 0:
                        pairs += 1
                    elif password[i-1] != password[i]:
                        pairs += 1
        if pairs >= 2 and straight and not forbidden:
            search = False
    print(''.join(password)) 
    return password

next = search_next(input,alphabet,forbidden_letters)
nextnext = search_next(next,alphabet,forbidden_letters)