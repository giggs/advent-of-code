input = [x for x in open('input.txt').read().splitlines()]

sq_feet = 0
ribbon = 0

for dimensions in input:
    n = dimensions.split('x')
    n = sorted(int(x) for x in n)
    sq_feet += (3*n[0]*n[1] + 2*n[1]*n[2] + 2*n[0]*n[2])
    ribbon += (2*n[0] + 2*n[1] + n[0]*n[1]*n[2])

print(sq_feet, ribbon)