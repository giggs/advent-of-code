input = 29000000

def OEIS_A326122(n):
    divisors = set()    
    for i in range(1, int((n ** 0.5)) + 1):
        if n % i == 0:
            divisors.add(i)
            divisors.add(n//i)
    return 10*(sum(divisors))

def part_2(n):
    divisors = set()
    for i in range(1, int((n**0.5)) + 1):
        if n % i == 0:
            if 50*i >= n:
                divisors.add(i)
            if (n//i)*50 >= n:
                divisors.add(n//i)
    return 11*(sum(divisors))

n = 1
while OEIS_A326122(n) <= input:
    n += 1
print(n)

n = 1
while part_2(n) <= input:
    n += 1
print(n)