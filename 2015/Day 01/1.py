input = [x for x in open('input.txt').read()]

print(input.count('(')-input.count(')'))

i = 0
floor = 0
while floor > -1:
    if input[i] == '(':
        floor += 1
    else:
        floor -= 1
    i += 1

print(i)