from collections import defaultdict

reindeers = open('input.txt').read().splitlines()

# def distance(speed,duration,rest,limit):
#     distance = 0
#     time = 0
#     while time < limit:
#         if time + duration < limit:
#             distance += speed*duration
#             time += duration + rest
#         else:
#             distance += speed*(limit-time)
#             time += duration
#     return distance

distances = []
stats = {}
scores = {}
distances_over_time = defaultdict(lambda: [0])

for reindeer in reindeers:
    reindeer = reindeer.split()
    name, speed, duration, rest = reindeer[0], int(reindeer[3]), int(reindeer[6]), int(reindeer[-2])
    # distances.append(distance(speed,duration,rest,2503))
    scores[name] = 0
    distance = 0
    t = 0
    r = 0
    move = duration
    while t < 2503:
        if r == 0:
            distance += speed
            move -= 1
            if move == 0:
                r = rest
        else:
            r -= 1
            if r == 0:
                move = duration
        temp = distances_over_time[name]
        temp.append(distance)
        distances_over_time[name] = temp
        t += 1

lead = 0
for i in range(1,2504):
    for key, value in distances_over_time.items():
        if value[i] > lead:
            lead = value[i]
    for key, value in distances_over_time.items():
        if value[i] == lead:
            scores[key] = scores[key] + 1
    if i == 2503:
        print(lead)
          
print(max(scores.values()))