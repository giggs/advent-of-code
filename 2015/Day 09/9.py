import itertools

input = open('input.txt').read().splitlines()

distances = {}
locations = set()

for line in input:
    trip, distance = line.split(' = ')
    distances[trip] = int(distance)
    loc_a, loc_b = trip.split(' to ')
    locations.add(loc_a)
    locations.add(loc_b)
    trip = trip.split(' ')
    rev_trip = ' '.join(trip[::-1])
    distances[rev_trip] = int(distance)
    
permutations = [p for p in itertools.permutations(locations)]
trips = []

for trip in permutations:
    distance = 0
    for i in range(len(trip)-1):
        step = ' to '.join(trip[i:i+2])
        distance += distances[step]
    trips.append(distance)

print(min(trips))
print(max(trips))