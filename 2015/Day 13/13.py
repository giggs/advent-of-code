import itertools
from collections import defaultdict

data = open('input.txt').read().splitlines()
happiness = defaultdict(lambda: 0)
names = set()

for line in data:
    line = line.replace('.','').split(' ')
    names.add(line[0])
    pairing = ''.join([line[0], line[-1]])
    value = int(line[3])
    if line[2] == "lose":
        value *= -1
    happiness[pairing] = value

permutations = [c for c in itertools.permutations(names)]
no_repeats = set()
for p in permutations:
    if p[::-1] not in no_repeats:
        no_repeats.add(p)

def count_happiness(guests,part):
    seating_arrangements = {}
    for p in guests:
        happiness_change = 0
        seats = []
        if part == 'part2':
            seats.append('Me')
        for name in p:
            seats.append(name)
        for i in range(len(seats)):
            a = ''.join([seats[i],seats[(i+1)%len(seats)]])
            b = ''.join([seats[(i+1)%len(seats)],seats[i]])
            happiness_change += happiness[a]
            happiness_change += happiness[b]
        seats =''.join(seats)    
        seating_arrangements[seats] = happiness_change
    print(max(seating_arrangements.values()))

count_happiness(no_repeats,'part1')
count_happiness(no_repeats,'part2')