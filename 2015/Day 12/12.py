import re

json = open('input.txt').read()
print(sum([int(x) for x in re.findall("-?\d+", json)]))

part2 = list(json)

for i in range(len(part2)):
    if part2[i] == ']':
        for j in reversed(range(i)):
            if part2[j] == '[':
                test = ''.join(part2[j:i+1])
                test = test.replace("red","rod").replace('[','(').replace(']',')')
                test = list(test)
                for k in range(j,i+1):
                    part2[k] = test.pop(0)
                break
    elif part2[i] == '}':
        for j in reversed(range(i)):
            if part2[j] == '{':
                test = ''.join(part2[j:i+1])
                if "red" in test:
                    for k in range(j,i+1):
                        part2[k] = '#'
                else:
                    part2[j] = '('
                    part2[i] = ')'
                break

print(sum([int(x) for x in re.findall("-?\d+", "".join(part2))]))