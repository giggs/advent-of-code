import itertools

def get_quantum_entanglement(parts):
    weights = [int(x) for x in open('input.txt').read().splitlines()]
    combinations = []
    i = 1

    while len(combinations) == 0:
        combinations = [x for x in itertools.combinations(weights,i) if sum(x) == sum(weights)//parts]
        i += 1
    
    quantum_entanglement = 1
    for j in combinations[0]:
        quantum_entanglement *= j
    print(quantum_entanglement)

get_quantum_entanglement(3)
get_quantum_entanglement(4)