input = [string for string in open('input.txt').read().splitlines()]

vowels = 'aeiou'
naughty = ['ab','cd','pq','xy']
nice = []

for string in input:
    three_v = False
    double = False
    forbidden = False
    v = 0
    for substring in naughty:
        if substring in string:
            forbidden = True
    for letter in list(string):
        if letter in vowels:
            v += 1
    if v > 2:
        three_v = True
    for i in range(len(string)-1):
        if string[i] == string[i+1]:
            double = True
    if three_v and double and not forbidden:
        nice.append(string)

print(len(nice))

nice_2 = []

for string in input:
    twice = False
    repeat = False
    for i in range(len(string)-2):
        if string[i:i+2] in string[i+2:]:
            twice = True
    for i in range(len(string)-2):
        if string[i] == string[i+2]:
            repeat = True
    if twice and repeat:
        nice_2.append(string)

print(len(nice_2))