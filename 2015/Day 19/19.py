import re
import random

transformations, starting_molecule = open('input.txt').read().split('\n\n')
transformations = transformations.splitlines()
molecules = set()

for t in transformations:
    pattern, replacement = t.split(' => ')
    n = starting_molecule.count(pattern)
    if n > 0:
        new = re.sub(pattern,replacement,starting_molecule,1)
        molecules.add(new)
        if n > 1:
            for i in range(1,n):
                new = re.sub(rf'^(.*?({pattern}.*?){{{i}}}){pattern}',rf'\1{replacement}',starting_molecule)
                molecules.add(new)

print(len(molecules))

working_molecule = starting_molecule
changes = 0

while working_molecule != "e":
    old_changes = changes
    for t in transformations:
        replacement, pattern = t.split(" => ")
        changes += working_molecule.count(pattern)
        working_molecule = working_molecule.replace(pattern,replacement)
    if changes == old_changes:
        random.shuffle(transformations)
        working_molecule = starting_molecule
        changes = 0

print(changes)