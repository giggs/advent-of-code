with open('input.txt') as f:
    input = f.read()

input = input.replace('through ','')
input = input.replace('turn on','turnon')
input = input.replace('turn off','turnoff')
input = input.replace('\n',' ')
input = input.split(' ')

grid = []

for x in range(1000):
    row = []
    for y in range(1000):
        row.append(0)
    grid.append(row)

for i in range(0, len(input), 3):
    start = [int(x) for x in input[i+1].split(',')]
    end = [int(x) for x in input[i+2].split(',')]
    for x in range(start[0],end[0]+1):
        for y in range(start[1],end[1]+1):
            if input[i] == 'turnon':
                grid[x][y] += 1
            elif input[i] == 'toggle':
                grid[x][y] += 2
            else:
                grid[x][y] -= 1
                if grid[x][y] < 0:
                    grid[x][y] = 0

print(sum(sum(row) for row in grid))