row = 6
column = 6
value = 27995004

while row != 3010 or column != 3019:
    value *= 252533
    value %= 33554393
    if row > 1:
        column += 1
        row -= 1
    else:
        row = column + 1
        column = 1

print(value)