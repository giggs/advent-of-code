import re

# Saw the idea to use combined modulo/LCM on reddit, it's much faster.
# Actually parsing instead of using eval should also be much faster.

def monkey_business(rounds = 20):
    
    monkeys = open('11.txt').read().strip().split('\n\n')
    tests = []
    for i, monkey in enumerate(monkeys):
        _, items, op, test, if_true, if_false = monkey.splitlines()
        items = list(map(int,list(re.findall('\d+', items))))
        _, op = op.split('=')
        if rounds == 20:
            op = f'({op}) // 3'
        tests.append(int(re.findall('\d+', test)[0]))
        if_true = int(if_true[-1])
        if_false = int(if_false[-1])
        monkeys[i] = [items, op, test, if_true, if_false, 0]

    worry_reducer = 1
    for t in tests:
        worry_reducer *= t

    for _ in range(rounds):
        for i, monkey in enumerate(monkeys):
            items = monkey.pop(0) 
            op, test, if_true, if_false, count = monkey
            while items:
                item = items.pop(0)
                compute = op.replace('old', str(item))
                item = eval(compute) % worry_reducer
                if item % tests[i] == 0:
                    monkeys[if_true][0].append(item)
                else:
                    monkeys[if_false][0].append(item)
                count += 1
            monkeys[i] = [[], op, test, if_true, if_false, count]

    activity = [monkey[-1] for monkey in monkeys]
    activity.sort()
    return(activity[-1]*activity[-2])

print(monkey_business(20), monkey_business(10000))