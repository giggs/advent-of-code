import re
from functools import lru_cache

def monkey_business(rounds = 20):
    
    @lru_cache
    def reduce_worry(op,idx,obj):
        res = op.replace('old',str(obj))
        res = eval(res) % tests[idx]
        return res

    monkeys = open('11.txt').read().strip().split('\n\n')
    tests = []
    for i, monkey in enumerate(monkeys):
        _, items, op, test, if_true, if_false = monkey.splitlines()
        items = list(map(int,list(re.findall('\d+', items))))
        _, op = op.split('=')
        if rounds == 20:
            op = f'({op}) // 3'
        tests.append(int(re.findall('\d+', test)[0]))
        if_true = int(if_true[-1])
        if_false = int(if_false[-1])
        monkeys[i] = [items, op, test, if_true, if_false, 0]

    if rounds != 20:
        for i, monkey in enumerate(monkeys):
            items = monkey[0]
            new_items = []
            for item in items:
                new_items.append([item%tests[i] for i in range(len(monkeys))])
            monkeys[i][0] = new_items

    for _ in range(rounds):
        for i, monkey in enumerate(monkeys):
            items = monkey.pop(0) 
            op, test, if_true, if_false, count = monkey
            while items:
                item = items.pop(0)
                if rounds != 20:
                    new_item = []
                    for j,it in enumerate(item):
                        new_item.append(reduce_worry(op,j,it))
                    if new_item[i] % tests[i] == 0:
                        monkeys[if_true][0].append(new_item)
                    else:
                        monkeys[if_false][0].append(new_item)
                else:
                    compute = op.replace('old', str(item))
                    item = eval(compute)
                    if item % tests[i] == 0:
                        monkeys[if_true][0].append(item)
                    else:
                        monkeys[if_false][0].append(item)
                count += 1
            monkeys[i] = [[], op, test, if_true, if_false, count]

    activity = [monkey[-1] for monkey in monkeys]
    activity.sort()
    return(activity[-1]*activity[-2])

print(monkey_business(20), monkey_business(10000))

# first solution I got. 12s with the cached function, 39s without.