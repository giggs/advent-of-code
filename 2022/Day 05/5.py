input = open('5.txt').read().split('\n\n')
h_stacks = input[0].splitlines()
instructions = input[1].strip().splitlines()

def rearrange(over_9000):
    stack = [[h_stacks[i][j] for i in range(len(h_stacks)-1) if h_stacks[i][j] != ' '] for j in range(1, len(h_stacks[0]), 4)]
    for instruction in instructions:
        _, n, _, s, _, d = instruction.split(' ')
        n, s, d = int(n), int(s)-1, int(d)-1
        stack[d] = stack[s][:n] + stack[d] if over_9000 else stack[s][:n][::-1] + stack[d]
        stack[s] = stack[s][n:]
    return ''.join([stack[i][0] for i in range(len(stack))])

print(rearrange(False), rearrange(True))   