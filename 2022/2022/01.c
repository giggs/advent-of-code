#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int one()
{
	FILE* input = fopen("01.txt", "r");
	int c;
	char calories[6];
	int max3 = 0;
	int max2 = 0;
	int max1 = 0;
	int elf = 0;
	int i = 0;

	while ((c = fgetc(input)) != EOF)
	{
		if (c == '\n')
		{
			calories[i] = '\0';
			if (i == 0)
			{
				if (elf > max3)
				{
					max3 = elf;
					if (max3 > max2)
					{
						max3 = max2;
						max2 = elf;
						if (max2 > max1)
						{
							max2 = max1;
							max1 = elf;
						}
					}
				}
				elf = 0;
			}
			else
			{
				int snack = atoi(calories);
				calories[0] = '\0';
				elf = elf + snack;
				i = 0;
			}
		}
		else
		{
			calories[i++] = c;
		}
	}
	printf("%d %d", max1, max1 + max2 + max3);
	return 0;
}