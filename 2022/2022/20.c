#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

typedef struct node* Nodeptr;

typedef struct node {
	Nodeptr previous;
	Nodeptr next;
	long long value;
} Node;

long long decryption_key = 811589153;

Node mixed_1[5050];
Node mixed_2[5050];

void mix(Nodeptr list, long long key, int j)
{
	for (int a = 0; a < j; a++)
	{
		Nodeptr current = &list[a];
		Nodeptr previous;
		Nodeptr next;
		long long n = current->value;
		n %= j - 1;
		current->previous->next = current->next;
		current->next->previous = current->previous;
		previous = current->previous;
		if (n < 0)
		{
			while (n++ != 0)
			{
				previous = previous->previous;
			}
		}
		else if (n > 0)
		{
			while (n-- != 0)
			{
				previous = previous->next;
			}
		}
		next = previous->next;
		previous->next = current;
		current->previous = previous;
		next->previous = current;
		current->next = next;
	}
}

long long score(Nodeptr list)
{
	Nodeptr current = &list[0];
	long long total = 0;

	while (current->value != 0)
	{
		current = current->next;
	}

	for (int i = 1; i < 3001; i++)
	{
		current = current->next;
		if (i % 1000 == 0)
		{
			total += current->value;
		}
	}
	return total;
}

int twenty() 
{
	FILE* input = fopen("20.txt", "r");

	int c;
	int j = 0;
	while ((c = fgetc(input)) != EOF)
	{
		Node mixed_n;
		if (c == '\n')
			continue;
		char n[10];
		int i = 0;
		do {
			n[i++] = c;
		} while ((c = fgetc(input)) != '\n');
		n[i] = '\0';
		mixed_n.value = atoi(n);
		mixed_n.next = &mixed_1[j + 1];
		if (j > 0)
		{
			mixed_n.previous = &mixed_1[j - 1];
		}
		mixed_1[j] = mixed_n;
		mixed_n.value *= decryption_key;
		mixed_n.next = &mixed_2[j + 1];
		if (j > 0)
		{
			mixed_n.previous = &mixed_2[j - 1];
		}
		mixed_2[j++] = mixed_n;
	}
	mixed_1[0].previous = &mixed_1[j-1];
	mixed_1[j-1].next = &mixed_1[0];
	mixed_2[0].previous = &mixed_2[j - 1];
	mixed_2[j - 1].next = &mixed_2[0];

	mix(mixed_1, 1, j);

	for (int b = 0; b < 10; b++)
	{
		mix(mixed_2, decryption_key, j);
	}

	long long p1 = score(mixed_1);
	long long p2 = score(mixed_2);

	printf("%lld %lld",p1,p2);

	return 0;
}