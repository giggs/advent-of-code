guide = open('2.txt').read().strip().splitlines()

score1 = 0
score2 = 0
won = ('A Y', 'B Z', 'C X')
draw = ('A X', 'B Y', 'C Z')
xyz_points = '0XYZ'
abc_points = '0ABC'

for line in guide:
    opponent, you = line.split(' ')
    score1 += xyz_points.index(you)
    if line in won:
        score1 += 6
    elif line in draw:
        score1 += 3
    if you == 'Y': #draw
        score2 += abc_points.index(opponent)+3
    elif you == 'X': #loss
        round = abc_points.index(opponent)-1
        score2 += 3 if round == 0 else round
    else:
        round = abc_points.index(opponent)+1
        score2 += 7 if round == 4 else round+6
    
print(score1, score2)