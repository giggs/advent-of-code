sequence = open('6.txt').read().strip()
i = 4

while len(set(sequence[i-4:i])) != 4:
    i += 1
print(i)

while len(set(sequence[i-14:i])) != 14:
    i += 1
print(i)

# one-liner
# print([[i for i in range(n,len(sequence)+1) if len(set(sequence[i-n:i])) == n][0] for n in [4, 14]])