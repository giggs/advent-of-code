import re
import portion as P

# First solution, goes through all possible positions for part 2 in under 10 minutes. Not good

data = open('15.txt').read().strip().splitlines()
limit = 2000000
limit_2 = 4000000
multiplier = 4000000
part1 = set()
part2 = 0
beacons = set()

for line in data:
    sx, sy, bx, by = [int(x) for x in re.findall('-?\d+', line)]
    if by == limit:
        beacons.add(bx)
    if sy == limit:
        beacons.add(sx)
    distance = abs(sx-bx) + abs(sy-by)
    if (limit in range(sy, sy+distance+1)) or (limit in range(sy-distance, sy)):
        part1 |= set(range(sx - distance + abs(limit-sy), sx + distance - abs(limit-sy) + 1))            

part1 -= beacons

for y in reversed(range(limit_2+1)):
    impossible = P.empty()
    for line in data:
        sx, sy, bx, by = [int(x) for x in re.findall('-?\d+', line)]
        distance = abs(sx-bx) + abs(sy-by)
        if (y in range(sy, sy+distance+1)) or (y in range(sy-distance, sy)):
            impossible |= P.closed(max(0,sx - distance + abs(y-sy)), min(limit_2+1,sx + distance - abs(y-sy) + 1))
    if impossible != P.closed(0, limit_2+1):
        part2 = P.to_data(impossible)[0][2] * multiplier + y
        break

print(len(part1), part2)