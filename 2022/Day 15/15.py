import re

data = open('15.txt').read().strip().splitlines()
limit = 2000000
multiplier = 4000000
part1 = set()
beacons = set()
sensors = {}

def manhattan(a,b):
    return abs(a[0]-b[0]) + abs(a[1]-b[1])

def search(): #only checking squares on the perimeter of a scanner
    for sensor, distance in sensors.items():
        sx, sy = sensor
        for x in range(max(0,sx-distance-1), min(multiplier,sx+distance+2)):
            for y in [sy+(distance+1-abs(x-sx)), sy-(distance+1-abs(x-sx))]:
                if y in range(multiplier+1):
                    for s,d in sensors.items():
                        if manhattan((x,y),s) <= d:
                            break
                    else:
                        return x * multiplier + y

for line in data:
    sx, sy, bx, by = [int(x) for x in re.findall('-?\d+', line)]
    if by == limit:
        beacons.add(bx)
    distance = manhattan((sx,sy),(bx,by))
    sensors[(sx,sy)] = distance
    if (limit in range(sy, sy+distance+1)) or (limit in range(sy-distance, sy)):
        part1 |= set(range(sx - distance + abs(limit-sy), sx + distance - abs(limit-sy) + 1))            

print(len(part1-beacons), search())