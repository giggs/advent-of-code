from collections import defaultdict

steps = open('9.txt').read().strip().splitlines()
rope = [(0,0)]*10
visited_1 = set()
visited_9 = set()
dirs = {'L': (-1,0), 'U': (0,1), 'D': (0,-1), 'R':  (1,0)}
knot_move = defaultdict(int)
knot_move[-2], knot_move[2] = 1, -1

for step in steps:
    dir, l = step.split(' ')
    for _ in range(int(l)):
        rope[0] = (rope[0][0] + dirs[dir][0], rope[0][1] + dirs[dir][1])
        for i in range(1,len(rope)):
            cur, prev = rope[i], rope[i-1]
            if abs(prev[0]-cur[0]) < 2 and abs(prev[1]-cur[1]) < 2: 
                break
            else:
                rope[i] = (prev[0] + knot_move[prev[0]-cur[0]], prev[1] + knot_move[prev[1]-cur[1]])
        visited_1.add(rope[1])
        visited_9.add(rope[9])

print(len(visited_1), len(visited_9))

# alternate method with python built-in functions for element-wise operations in tuple
# 3 to 4 times slower but would be more readable for longer tuples
# a lambda function for knot_move is slower but again, saves time for multiple values
# a Userdict is a good alternative for caching lambda function results

# knot_move = lambda x: int(-x/2)
# for step in steps:
#     dir, l = step.split(' ')
#     for _ in range(int(l)):
#         rope[0] = tuple(map(sum, zip(rope[0], dirs[dir])))
#         for i in range(1,len(rope)):
#             diff = tuple(x-y for x,y in zip(rope[i-1],rope[i]))
#             if max(map(abs,diff)) < 2: 
#                 break
#             else:
#                 rope[i] = tuple(map(sum, zip(rope[i-1], tuple(map(knot_move,diff)))))
#         visited_1.add(rope[1])
#         visited_9.add(rope[9])