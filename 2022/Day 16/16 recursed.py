# inspired by reddit strategies

import re, functools

data = open('16.txt').read().strip().splitlines()
flows = {}
edges = {}
distances = {}

for line in data:
    valves = re.findall('[A-Z][A-Z]', line)
    valve = valves.pop(0)
    flow = re.findall('\d+', line)[0]
    if int(flow) != 0:
        flows[valve] = int(flow)
    edges[valve] = valves

@functools.cache
def travel(start, end):
    frontier = [[start]]
    while frontier:
        path = frontier.pop(0)
        latest = path[-1]
        for next in edges[latest]:
            if next not in path:
                path2 = path + [next]
                if next == end:
                    return len(path2)
                else:
                    frontier.append(path2)
    return "Fail"

for v in edges.keys():
    for vv in edges.keys():
        if v != vv:
            distances[v,vv] = travel(v,vv)

@functools.cache
def release_pressure(time, valve = 'AA', closed = frozenset([v for v in flows]), elephant=False):
    released = [release_pressure(26, 'AA', closed) if elephant else 0]
    # After each valve you open, compute the best the elephant can do with the remaining valves.
    # You'll ending up keeping the best choice for you,
    # which might be to do nothing and let the elephant handle it
    # if the number of valves is small enough (the example is one such case)
    for v in closed:
        if (t:= time - distances[valve,v]) > 0:
            pressure = flows[v] * t + release_pressure(t, v, closed-frozenset([v]), elephant) 
            released.append(pressure)
    return max(released)

print(release_pressure(30), release_pressure(26, elephant = True))