program = open('10.txt').read().strip().replace('addx', 'noop\n').splitlines()
strengths = 0
pixels = []
X = 1

for i, inst in enumerate(program, 1):
    pixels.append('\u2588' if (i-1) % 40 in [X-1, X, X+1] else ' ')
    if i % 40 == 20:
        strengths += X*i
    if inst != 'noop':
        X += int(inst)
        
print(strengths)
for i in range(6): print(''.join(pixels[i*40:(i+1)*40]))