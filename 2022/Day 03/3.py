rucksacks = open('3.txt').read().strip().splitlines()
part1 = 0
part2 = 0

f = lambda x:(ord(x) - ord('A') + 27 
           if x.isupper() 
         else ord(x) - ord('a') + 1)

for i,sack in enumerate(rucksacks):
    half = len(sack)//2
    first, second = sack[:half], sack[half:]
    assert len(first) == len(second)
    extra = (set(first) & set(second)).pop()
    part1 += f(extra)
    if i % 3 == 2:
        badge = (set(rucksacks[i]) & set(rucksacks[i-1]) & set(rucksacks[i-2])).pop()
        part2 += f(badge)

print(part1, part2)
