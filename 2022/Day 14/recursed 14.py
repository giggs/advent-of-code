# Read about using recursion to speed up part 2 and implemented it. Over 30x faster!

import re

rock_paths = open('14.txt').read().strip().splitlines()
blocked = set()
max_depth = 0
p1 = 0

for path in rock_paths:
    lines = list(map(int,re.findall('\d+',path)))
    for a in range(0, len(lines)-3, 2):
        x,xx,y,yy = sorted([lines[a], lines[a+2]]) + sorted([lines[a+1], lines[a+3]])
        max_depth = max(max_depth, yy)
        for i in range(x,xx+1):
            for j in range(y,yy+1):
                blocked.add((i,j))

rocks = len(blocked)

def drop_from(x,y):
    global p1
    if (x,y) in blocked: 
        return
    if y == max_depth + 1:
        if not p1: p1 = len(blocked)-rocks
        blocked.add((x,y))
        return
    for dx in [0,-1,1]:
        drop_from(x+dx,y+1)
    blocked.add((x,y))

drop_from(500,0)
print(p1, len(blocked)-rocks)