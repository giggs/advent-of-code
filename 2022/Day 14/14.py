import re

rock_paths = open('14.txt').read().strip().splitlines()
blocked = set()
max_depth = 0
sand = -1
p1 = 0

for path in rock_paths:
    lines = list(map(int,re.findall('\d+',path)))
    for a in range(0, len(lines)-3, 2):
        x,xx,y,yy = sorted([lines[a], lines[a+2]]) + sorted([lines[a+1], lines[a+3]])
        max_depth = max(max_depth, yy)
        for i in range(x,xx+1):
            for j in range(y,yy+1):
                blocked.add((i,j))

while (500,0) not in blocked:
    sand += 1
    x,y = 500,0
    while True:
        if y == max_depth and not p1:
            p1 = sand
            break
        if y == max_depth+1:
            blocked.add((x,y))
            break
        for dest in [(x,y+1), (x-1,y+1), (x+1, y+1)]:
            if dest not in blocked:
                x,y = dest
                break
        else:
            blocked.add((x,y))
            break

print(p1, sand)