# My original solution. Didn't consider the mod method for some reason

SNAFU = {'2': 2, '1': 1, '0': 0, '-': -1, '=': -2}

def SNAFU_to_dec(number):
    return sum(SNAFU[x]*5**i for i,x in enumerate(str(number)[::-1]))

def dec_to_SNAFU(number):
    order = '210-='
    res = ['2']
    while SNAFU_to_dec(''.join(res)) < number:
        res.append('2')
    for i in range(len(res)):
        j = 0
        while SNAFU_to_dec(''.join(res)) > number and j < 4:
            j+=1
            res[i] = order[j] 
        if SNAFU_to_dec(''.join(res)) < number:
            res[i] = order[j-1]
        if SNAFU_to_dec(''.join(res)) == number:
            return ''.join(res)

print(dec_to_SNAFU(sum(SNAFU_to_dec(n) for n in open('25.txt').read().strip().splitlines())))