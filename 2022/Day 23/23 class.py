# I wanted to use a class here at first. I liked my other solution better though

directions = [((-1j, 1-1j, -1-1j), -1j), # N
              ((1j, 1+1j, -1+1j), 1j), # S
              ((-1, -1+1j, -1-1j), -1), # W
              ((1, 1+1j, 1-1j), 1)] # E
elves = []
occupied = set()
neighbours = set([d for n,dir in directions for d in n])
moves = 1
rounds = 0

class Elf():
    def __init__(self, position, candidate = None):
        self.position = position
        self.candidate = candidate

for y, row in enumerate(open('23.txt').read().strip().splitlines()):
    for x, col in enumerate(row):
        if col == '#':
            elves.append(Elf(x+y*1j))
            occupied.add(x+y*1j)

while moves != 0:
    if rounds == 10:
        x = [z.real for z in occupied]
        y = [z.imag for z in occupied]
        p1 = (max(x) - min(x) + 1) * (max(y) - min(y) + 1) - len(occupied)
    propositions = set()
    invalid = set()
    for elf in elves:
        if any([(elf+n) in occupied for n in neighbours]): 
            for adj, dir in directions:
                if all([(elf.position+p) not in occupied for p in adj]):
                    elf.candidate = elf.position + dir
                    if elf.candidate in propositions:
                        invalid.add(elf.candidate)
                    else:
                        propositions.add(elf.candidate)
                    break
    moves = 0
    for elf in elves:
        if elf.candidate is not None and elf.candidate not in invalid:
            occupied.remove(elf.position)
            elf.position = elf.candidate
            occupied.add(elf.position)
            moves += 1
        elf.candidate = None
    directions.append(directions.pop(0))
    rounds += 1

print(int(p1), rounds)