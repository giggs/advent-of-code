# other solution using stuff I found on the subreddit and want to remember. 
# json.loads() was mentioned by several users
# compare function using match statements from u/4HbQ
# reminder that map can take several iterables if the function wants more than 1

import json

signal = open('13.txt').read().strip().split('\n\n')
ordered_pairs = 0
packets = []

def cmp(l, r):
    match l, r:
        case int(), int():  return (l>r) - (l<r)
        case int(), list(): return cmp([l], r)
        case list(), int(): return cmp(l, [r])
        case list(), list():
            for z in map(cmp, l, r):
                if z: return z
            return cmp(len(l), len(r))  

for i, packet_pairs in enumerate(signal,1):
    left, right = [json.loads(packet) for packet in packet_pairs.splitlines()]
    packets.extend([left,right])
    if cmp(left,right) == -1:
        ordered_pairs += i
    
position_1 = 1 + sum(1 for packet in packets if cmp(packet, [[2]]) == -1)
position_2 = 2 + sum(1 for packet in packets if cmp(packet, [[6]]) == -1)

print(ordered_pairs, position_1 * position_2)