signal = open('13.txt').read().strip().split('\n\n')
ordered_pairs = 0
packets = []

def compare(left, right):
    for i, l_value in enumerate(left):
        if len(right) < i+1: # right side is smaller
            return False
        r_value = right[i]
        if isinstance(l_value,int) & isinstance(r_value, int):
            if l_value != r_value:
                return l_value < r_value
        elif isinstance(l_value, list) & isinstance(r_value, list):
            if (recurse := compare(l_value, r_value))!= None:
                return recurse 
        else:
            if isinstance(l_value, int):
                l_value = [l_value]
            else:
                r_value = [r_value]
            if (recurse := compare(l_value, r_value))!= None:
                return recurse      
    return True if len(left) < len(right) else None        

for i, packet_pairs in enumerate(signal,1):
    left, right = [eval(packet) for packet in packet_pairs.splitlines()]
    packets.extend([left,right])
    if compare(left,right):
        ordered_pairs += i
    
position_1 = 1 + sum(1 for packet in packets if compare(packet, [[2]]))
position_2 = 2 + sum(1 for packet in packets if compare(packet, [[6]]))

print(ordered_pairs, position_1 * position_2)