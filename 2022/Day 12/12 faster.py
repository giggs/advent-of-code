height_map = open('12.txt').read().strip().splitlines()

grid = []
start = 0
end = 0
lowest = []

# Realized part2 is actually the BFS from the end point with a different goal rather than from all the lowest points. Almost 100x faster

for y, row in enumerate(height_map):
    line = []
    for x, height in enumerate(row):
        if height == 'a':
            lowest.append((x,y))
        if height == 'S':
            start = (x,y)
            line.append(0)
        elif height == 'E':
            end = (x,y)
            line.append(25)
        else:
            line.append(ord(height)-ord('a')) 
    grid.append(line)

def shortest_path_search(start, successors, goal):
    explored = set()
    frontier = [[start]]
    while frontier:
        path = frontier.pop(0)
        latest = path[-1]
        for pos in successors(latest, goal):
            if pos not in explored:
                explored.add(pos)
                path2 = path + [pos]
                if goal(pos):
                    return len(path2)-1
                else:
                    frontier.append(path2)
    return 1000

def p1(pos):
    return pos == end

def p2(pos):
    return grid[pos[1]][pos[0]] == 0

def successors(pos, goal):
    x,y = pos
    height = grid[y][x]
    s = []
    for diff in [(1,0), (-1,0), (0, 1), (0,-1)]:
        dx, dy = diff
        nx = x + dx
        ny = y + dy
        if (nx in range(len(grid[0]))) & (ny in range(len(grid))):
            if goal.__name__ == 'p1':
                if (grid[ny][nx] -1) <= height:
                    s.append((nx,ny))
            else:
                if (height - grid[ny][nx]) <= 1:
                    s.append((nx,ny))
    return s

print(shortest_path_search(start, successors, p1), shortest_path_search(end, successors, p2))