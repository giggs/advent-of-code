class Node:
    def __init__(self, value, prev = None, next = None):
        self.value = value
        self.prev = prev
        self.next = next

def create_list(key = 1):
    numbers = [Node(key*int(n)) for n in open("20.txt").read().strip().splitlines()]
    for i in range(len(numbers)-1):
        numbers[i].next = numbers[i+1]
        numbers[i+1].prev = numbers[i]
    numbers[0].prev = numbers[-1]
    numbers[-1].next = numbers[0]
    return numbers

def mix(numbers, rounds = 1):
    for _ in range(rounds):
        for node in numbers:
            node.prev.next = node.next
            node.next.prev = node.prev
            prev = node.prev
            steps = node.value % (len(numbers)-1)
            for _ in range(steps): # mod is always the sign of denominator in python
                prev = prev.next
            next = prev.next
            prev.next = node
            node.prev = prev
            next.prev = node
            node.next = next
    return numbers

def score(numbers):
    ans = 0
    for node in numbers:
        if node.value == 0:
            for _ in range(1,3001):
                node = node.next
                if _ % 1000 == 0:
                    ans += node.value
            return ans

print(score(mix(create_list())), score(mix(create_list(811589153), 10)))