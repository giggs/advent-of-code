assignments = open('4.txt').read().strip().splitlines()
part1 = 0
part2 = 0

for pair in assignments:
    first, second = pair.split(',')
    a,b = map(int,first.split('-'))
    c,d = map(int,second.split('-'))
    section1 = set(range(a, b+1))
    section2 = set(range(c, d+1))
    if section1 & section2:
        part2 += 1
        if (section1 <= section2) | (section2 <= section1): #checking for subset
            part1 += 1

print(part1, part2)