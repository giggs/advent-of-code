elves = open('1.txt').read().strip().split('\n\n')

for i,elf in enumerate(elves):
    calories = sum([int(x) for x in elf.splitlines()])
    elves[i] = calories

elves.sort()

print(elves[-1], sum(elves[-3:]))