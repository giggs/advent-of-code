import re

# I spent a lot of time trying to limit the search space here...
# I probably should have used numpy arrays to make operations easier

blueprints = []
p1 = 0
p2 = 1

for blueprint in open('19.txt').read().strip().splitlines():
    _, ore, c, ob1, ob2, g1, g2 = map(int, re.findall('\d+', blueprint))
    blueprint = []
    blueprint.append((ore, 0, 0, 0)) # ore robot
    blueprint.append((c, 0, 0, 0)) # clay robot 
    blueprint.append((ob1, ob2, 0, 0)) # obsidian robot
    blueprint.append((g1, 0, g2, 0)) # geode robot
    blueprints.append(tuple(blueprint))

def value(state):
    # Weighted extracted resources was the best culling heuristic I found
    robots, resources, extracted, costs = state
    return -(extracted[3]*10000 + extracted[2]*100 + extracted[1]*10)

def BFS_prune(blueprint, successors, time):
    frontier = [((1, 0, 0 ,0), (0, 0, 0, 0), (0, 0, 0, 0), blueprints[blueprint])]
    limits = [max([blueprints[blueprint][j][i] for j in range(4)]) for i in range(4)]
    for minute in range(time):
        new_frontier = []
        while frontier:
            current = frontier.pop(0)
            for next in successors(current):
                robots, resources, extracted, costs = next
                if any([robots[i] > limits[i] for i in range(3)]): #never build more robots than max resource cost
                    continue
                new_frontier.append(next)
        frontier = sorted(new_frontier, key=value)[:1000]
    return frontier[0][1][-1]

def successors(state):
    s = set()
    robots, resources, extracted, costs = state
    extracted = tuple([extracted[i] + robots[i] for i in range(4)])
    for i in reversed(range(4)): # build robots!
        if all([resources[j] >= costs[i][j] for j in range(4)]):
            ror = list(robots)
            ror[i] += 1
            rer = [resources[j] - costs[i][j] for j in range(4)]
            rer = [rer[i] + robots[i] for i in range(4)]
            s.add((tuple(ror), tuple(rer), extracted, costs))
    resources = [robots[i] + resources[i] for i in range(4)]
    s.add((robots, tuple(resources), extracted, costs)) # maybe don't build ? (see example 1 minute 14)
    return s

for i in range(1,len(blueprints)+1):
    p1 += i*BFS_prune(i-1, successors, 24)
    if i < 4 : p2 *= BFS_prune(i-1, successors, 32)

print(p1,p2)