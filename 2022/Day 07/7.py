filesystem = open('7.txt').read().strip().splitlines()
i = 0
limit = len(filesystem)
directories = {}

def parse(dirname):
    global i
    subdirs = {}
    dir_size = 0
    while i < limit-1:
        i += 1
        inst = filesystem[i]
        if inst == '$ ls' or inst[:3] == 'dir':
            continue
        elif inst == '$ cd ..':
            break
        elif inst[:4] == '$ cd':
            _, _, subdir = inst.split(' ')
            subdir = dirname + subdir
            subdir_size = parse(subdir)
            subdirs[subdir] = subdir_size
            directories[subdir] = subdir_size
        else:
            size, _ = inst.split(' ')
            dir_size += int(size)
    return dir_size + sum(x for x in subdirs.values())

directories['/'] = parse('/')
values = sorted(x for x in directories.values())
free = 70000000 - directories['/']
for x in values:
    if (free + x) >= 30000000:
        print(sum(y for y in directories.values() if y <= 100000), x); break