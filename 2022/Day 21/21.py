monkeys = dict()

for line in open("21.txt").read().strip().splitlines():
    line = line.replace('/','//').split(': ')
    monkeys[line[0]] = line[1]

def compute(monkey):
    if len(monkey := monkey.split(' ')) == 1:
        return int(monkey[0])
    else:
        return eval(f'{compute(monkeys[monkey[0]])}{monkey[1]}{compute(monkeys[monkey[2]])}')

def solve(lower, upper):
    monkeys['root2'] = monkeys['root'].replace('+','-').replace('*','-').replace('/','-')
    low = lower
    high = upper
    half = (high + low)//2
    monkeys['humn'] = str(half)
    while (res := compute(monkeys['root2'])) != 0:
        if res < 0:
            low = half
        elif res > 0:
            high = half
        if high-low == 1:
            low = upper
            high = lower
        half = (high + low)//2
        monkeys['humn'] = str(half)
    return half

print(compute(monkeys['root']),solve(0, 1_000_000_000_000_000_000))