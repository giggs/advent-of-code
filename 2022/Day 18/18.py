cubes = set()
p1 = 0
min_cord = 1000
max_cord = -1000
def neighbours(x,y,z): 
    return [(x+dx, y+dy, z+dz) for (dx,dy,dz) in 
            [(1,0,0),(-1,0,0),(0,1,0),(0,-1,0),(0,0,1),(0,0,-1)]]

for cube in open('18.txt').read().strip().splitlines():
    x,y,z = [int(x) for x in cube.split(',')]
    min_cord = min(min_cord, x, y, z)
    max_cord = max(max_cord, x, y, z)
    cubes.add((x,y,z))
    p1 += 6 - sum((n in cubes for n in neighbours(x,y,z)))*2

min_cord -= 3
max_cord += 2
p2 = 0
frontier = [(min_cord+1, min_cord+1, min_cord+1)]
seen = set()

while frontier:
    air = frontier.pop(0)    
    for n in neighbours(*air):
        if n not in seen:
            if n in cubes:
                p2 += 1
            elif all((min_cord <= i <= max_cord) for i in n):
                seen.add(n)
                frontier.append(n)

print(p1, p2)        