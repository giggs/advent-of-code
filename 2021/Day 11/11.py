data = open('input.txt').read().splitlines()
height,width = len(data[0]),len(data)
octopi = {}

for i in range(width):
    for j in range(height):
        octopi[i,j] = int(list(data[i])[j])

def simulate(limit, octopi):
    days,flashes = 0,0
    while True:
        for x in range(width):
            for y in range(height):
                octopi[x,y] += 1
        while len([value for value in octopi.values() if value > 9]) > 0:
            for x in range(width):
                for y in range(height):
                    if octopi[x,y] > 9:
                        octopi[x,y] = 0
                        flashes += 1
                        for dx in [-1,0,1]:
                            for dy in [-1,0,1]:
                                if x+dx in range(width):
                                    if y+dy in range(height):
                                        if octopi[x+dx,y+dy] != 0:
                                            octopi[x+dx,y+dy] += 1
        days += 1
        if days == limit:
            print(flashes)
        if sum(octopi.values()) == 0:
            print(days)
            break

simulate(100, octopi)