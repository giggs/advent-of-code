data = open('input.txt').read().splitlines()

right_cucumbers = set()
down_cucumbers = set()
for x in range(len(data)):
    for y in range(len(data[0])): 
        if data[x][y] == '>':
            right_cucumbers.add((x,y))
        elif data[x][y] == 'v':
            down_cucumbers.add((x,y))

i = 0
while True:
    i += 1
    new_right_cucumbers = set()
    new_down_cucumbers = set()
    for position in right_cucumbers:
        dest = (position[0],(position[1]+1)%len(data[0]))
        if dest not in right_cucumbers and dest not in down_cucumbers:
            new_right_cucumbers.add(dest)
        else:
            new_right_cucumbers.add(position)
    for position in down_cucumbers:
        dest = ((position[0]+1)%len(data),position[1])
        if dest not in down_cucumbers and dest not in new_right_cucumbers:
            new_down_cucumbers.add(dest)
        else:
            new_down_cucumbers.add(position)
    if new_right_cucumbers == right_cucumbers and new_down_cucumbers == down_cucumbers:
        break
    else:
        right_cucumbers = new_right_cucumbers.copy()
        down_cucumbers = new_down_cucumbers.copy()

print(i)