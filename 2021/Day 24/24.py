# The input is the same code snippet 14 times with 3 variables changed each time

# inp w
# mul x 0
# add x z
# mod x 26
# div z (a)
# add x (b)
# eql x w
# eql x 0
# mul y 0
# add y 25
# mul y x
# add y 1
# mul z y
# mul y 0
# add y w
# add y (c)
# mul y x
# add z y

# if a == 1, then the whole snippet is just z = z*26 + w + c
# if a == 26, x = w+c from the previous snippet. if (w+c from latest snippet where a == 1) = w+b from this snippet, the snippet is z = z // 26
# since we want z == 1 at the end and we have 7 instances of a == 1 and 7 instances of a == 26, for all a == 26 we must have z = z // 26
# The code is essentially a stack, where you push a value (w+c) if a == 1 and you pop a value if a == 26
# pop input = pop push + c push + b pop

# For this input:
# input[13] = input[0] + 15 - 11
# input[12] = input[1] + 8 - 1
# input[11] = input[4] + 13 - 10
# input[10] = input[5] + 4 - 12
# input[9] = input[8] + 5 - 7
# input[7] = input[6] + 1 - 5
# input[3] = input[2] + 2 - 9

# p1 = 52926995971999
# p2 = 11811951311485