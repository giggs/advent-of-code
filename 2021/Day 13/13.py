from collections import defaultdict

dots, folds = open('input.txt').read().split('\n\n')
grid = defaultdict(lambda:'.')
code = []
p1,width,height = 0,100000,100000

for dot in dots.splitlines():
    x,y = dot.split(',')
    x,y = int(x), int(y)
    grid[(x,y)] = '#'

for fold in folds.splitlines():
    axis,value = fold.split('=')
    axis = axis[-1]
    value = int(value)
    new = []
    for key in grid.keys():
        if axis == 'x':
            if key[0] > value:
                new.append(((value-(key[0]-value),key[1])))
                width = value       
        else:
            if key[1] > value:
                new.append((key[0],value-(key[1]-value)))
                height = value
    for dot in new:
        grid[dot] = '#'
    if fold == folds.splitlines()[0]:
        print(len([key for key in grid.keys() if key[0] < width and key[1] < height]))

for y in range(height+1):
    for x in range(width+1):
        code.append(grid[(x,y)])
    code.append('\n')
print(''.join(code))