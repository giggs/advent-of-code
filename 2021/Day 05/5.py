from typing import DefaultDict

input = open('input.txt').read().splitlines()
lines = DefaultDict(lambda: 0)
diagonals = DefaultDict(lambda: 0)
part1,part2 = 0,0

for vent in input:
    vent = [int(x) for x in vent.replace(' -> ',',').split(',')]
    x1,x2,y1,y2 = vent[0],vent[2],vent[1],vent[3]
    if x1 == x2:
        y,end = max(y1,y2)+1,min(y1,y2)
        while y != end:
            y -= 1
            lines[x1,y] += 1
    elif y1 == y2:
        x,end = max(x1,x2)+1,min(x1,x2)
        while x > end:
            x -= 1
            lines[x,y1] += 1
    else:
        if x1 > x2:
            x,dx = x1+1,-1
        else:
            x,dx = x1-1,1
        if y1 > y2:
            y,dy = y1+1,-1
        else:
            y,dy = y1-1,1
        while x != x2:
            x += dx
            y += dy
            diagonals[x,y] += 1

for key in lines:
    if lines[key] > 1:
        part1 += 1
print(part1)

for key in set(lines) | set(diagonals):
    if lines[key] + diagonals[key] > 1:
        part2 += 1
print(part2)