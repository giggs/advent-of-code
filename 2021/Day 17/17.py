import re
# Stole this parsing from u/4HbQ because I want to remember that I can use regex like this

x1,x2,y1,y2 = map(int, re.findall(r'-?\d+', open('input.txt').read()))
valid_x, valid_y, valid_tuples = [],[],[]
initial_x, initial_y = 0, -y1
# If possible, the highest point will be reached on a trajectory that finishes straight down. You will necessarily be at y=0 on the second to last step with your velocity being -initial_y.
# The last step will send you on the last row so your speed will be -y1 and your initial_y was -y1-1, which is the upper-bound for initial_y. For most (all?) input this is going to be the 
# maximum y speed, but I still checked to see if that was the case here

def launch_probe(start_x,start_y,x1,x2,y1,y2):
    xs = start_x
    ys = start_y
    xx = 0
    yy = 0
    while xx <= x2 and yy >= y1:
        xx += xs
        yy += ys
        xs -= 1
        ys -= 1
        if xs < 0:
            xs = 0
        if x1 <= xx <= x2 and y1 <= yy <= y2:
            return True
    return False

while initial_x < x2:
    initial_x += 1
    x_speed = initial_x
    x = 0
    while x_speed > 0:
        x += x_speed
        x_speed -= 1
        if x1 <= x <= x2:
            valid_x.append(initial_x)
            break
        elif x > x2:
            break

for x in valid_x:
    initial_y = -y1
    while initial_y >= y1:
        initial_y -= 1
        if launch_probe(x,initial_y,x1,x2,y1,y2):
            valid_tuples.append((x,initial_y))
            valid_y.append(initial_y)

print((max(valid_y)*(max(valid_y)+1)//2))
print(len(valid_tuples))