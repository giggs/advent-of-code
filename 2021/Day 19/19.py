import timeit

def run():
    data = open('input.txt').read().strip().split('\n\n')
    matches = len(data) - 1 # number of pair of scanners to find
    actual_beacons = set()

    def t1(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((-x,z,y))
        return sorted(rotated_scanner)

    def t2(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((x,z,-y))
        return sorted(rotated_scanner)

    def t3(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((-x,-y,z))
        return sorted(rotated_scanner)
        
    def t4(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((x,-z,y))
        return sorted(rotated_scanner)

    def t5(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((-x,y,-z))
        return sorted(rotated_scanner)

    def t6(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((x,-y,-z))
        return sorted(rotated_scanner)

    def t7(coordinates):
        rotated_scanner = []
        for (x,y,z) in coordinates:
            rotated_scanner.append((-x,-z,-y))
        return sorted(rotated_scanner)

    def does_it_match(seta,setb):
        for location in seta:
            distances_a = set()
            for loc in seta:
                distances_a.add((location[0]-loc[0],location[1]-loc[1],location[2]-loc[2]))
            for point in setb:
                distances_b = set()
                for p in setb:
                    distances_b.add((point[0]-p[0],point[1]-p[1],point[2]-p[2]))
                common_distances = distances_a & distances_b
                if len(common_distances) > 11:
                    return True, location, point, list(common_distances)
        return False, 0, 0, 0

    origin = data.pop(0).splitlines()
    origin_a = (0,0,0)
    for beacon in origin[1:]:
        coordinates = [int(x) for x in beacon.split(',')]
        actual_beacons.add((coordinates[0],coordinates[1],coordinates[2]))

    matched = []
    origin = [origin_a] + list(actual_beacons)
    matched.append(origin)
    match = False

    while matches > 0:
        for scanner in matched:
            origin_a = scanner[0]
            beacons_a = scanner[1:]
            for scan in data:
                scan = scan.splitlines()
                template_beacons_b = []
                for beacon in scan[1:]:
                    coordinates = [int(x) for x in beacon.split(',')]
                    template_beacons_b.append((coordinates[0],coordinates[1],coordinates[2]))       
                beacons_b = template_beacons_b.copy()
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t1(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t2(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t3(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t4(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t5(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t6(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
                beacons_b = t7(template_beacons_b)
                count = 0
                while (not match) and count < 3:
                    count += 1
                    beacons_b = ([(y,z,x) for (x,y,z) in beacons_b])
                    match,reference,new,d = does_it_match(beacons_a,beacons_b)
                if match:
                    break
            if match:
                origin_a = (reference[0]-new[0],reference[1]-new[1],reference[2]-new[2])
                new_beacons_b = set()
                for beacon in beacons_b:
                    new_beacons_b.add((beacon[0]+origin_a[0],beacon[1]+origin_a[1],beacon[2]+origin_a[2]))
                actual_beacons = actual_beacons| new_beacons_b
                beacons_a = list(new_beacons_b)
                matched.append([origin_a]+beacons_a)
                data.remove('\n'.join(scan))
                matches -= 1
                match = False
                continue

    print(len(actual_beacons))
    p2 = 0
    while len(matched) > 0:
        origin = matched.pop(0)[0]
        for scanner in matched:
            base = scanner[0]
            distance = abs(origin[0] - base[0]) + abs(origin[1] - base[1]) + abs(origin[2] - base[2])
            p2 = max(p2, distance)
    print(p2)
            
print(timeit.timeit('run()',number=1,globals=globals()))