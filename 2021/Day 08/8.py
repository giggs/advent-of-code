data = open('input.txt').read().splitlines()
p1,p2 = 0,0

for line in data:
    numbers = {}
    input,output = line.split('| ')    
    digits = [''.join(sorted(list(digit))) for digit in input.split(' ')]
    for digit in digits:
        if len(digit) == 2:
            numbers[digit] = 1
            numbers[1] = digit
        elif len(digit) == 3:
            numbers[digit] = 7
        elif len(digit) == 4:
            numbers[digit] = 4
            top_left_mid = list(digit)
        elif len(digit) == 7:
            numbers[digit] = 8
    top_left_mid.remove(numbers[1][0])
    top_left_mid.remove(numbers[1][1])
    for digit in digits:
        if len(digit) == 5:
            if numbers[1][0] in digit and numbers[1][1] in digit:
                numbers[digit] = 3
            elif top_left_mid[0] in digit and top_left_mid[1] in digit:
                numbers[digit] = 5
            else:
                numbers[digit] = 2
        elif len(digit) == 6:
            if not (top_left_mid[0] in digit and top_left_mid[1] in digit):
                numbers[digit] = 0
            elif numbers[1][0] in digit and numbers[1][1] in digit:
                numbers[digit] = 9
            else:
                numbers[digit] = 6
    out = []
    for digit in output.split(' '):
        out.append(str(numbers[''.join(sorted(list(digit)))]))
        if len(digit) in (2,3,4,7):
            p1 += 1
    p2 += int(''.join(out))

print(p1,p2)