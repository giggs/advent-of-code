## Clever idea from u/logiblocs that I wanted to save. Count the number of times each letter is used for all 10 digits. For the example, it's 
## f = 9, a = 8, c = 8, g = 7, d = 7, b = 6, e = 4
## Replace each letter used in a digit with it's count and then sort. It's unique for each digit.
## Since the left part of each line has each digit only once, you can use that as a hashmap for the digits!

data = open('input.txt').read().splitlines()
p1,p2 = 0,0
numbers = {'467889':'0', '89':'1', '47788':'2', '77889':'3', '6789':'4', '67789':'5', '467789':'6', '889':'7', '4677889':'8', '677889':'9'}

for line in data:
    input,output = line.split('| ')    
    for letter in set(input):
        if letter != ' ':
            output = output.replace(letter,str(input.count(letter)))
    out = []
    for digit in output.split(' '):
        out.append(numbers[''.join(sorted(list(digit)))])
        if len(digit) in (2,3,4,7):
            p1 += 1
    p2 += int(''.join(out))

print(p1,p2)