input = [int(x) for x in open('input.txt').read().splitlines()]
input.extend([0,0])
count,count2 = 0,0

for i in range(1,len(input)-2):
    if input[i] > input[i-1]:
        count +=1
    if input[i+2] > input[i-1]:
        count2 +=1

print(count,count2)