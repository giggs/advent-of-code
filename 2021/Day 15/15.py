import queue  ## First time using Dijkstra. Great article at https://www.redblobgames.com/pathfinding/a-star/introduction.html

data = open('input.txt').read().splitlines()
h,w = len(data), len(data[0])
grid = {}
for y in range(h):
    for x in range(w):
        grid[(y,x)] = int(data[y][x])

for i in range(1,5):
    for y in range(h):
        for x in range(w):
            if grid[(y,x+((i-1)*w))] < 9:
                grid[(y,x+(i*w))] = grid[(y,x+((i-1)*w))]+1
            else:
                grid[(y,x+(i*w))] = 1

for i in range(1,5):
    for y in range(h):
        for x in range(5*w):
            if grid[(y+((i-1)*w),x)] < 9:
                grid[(y+(i*h),x)] = grid[(y+((i-1)*w),x)]+1
            else:
                grid[(y+(i*h),x)] = 1

def Dijkstra(height,width):
    paths = queue.PriorityQueue()
    paths.put((0,0), grid[(0,0)])
    came_from = {}
    cost_so_far = {}
    came_from[(0,0)] = None
    cost_so_far[(0,0)] = 0
    goal = (height-1,width-1)

    while not paths.empty():
        current = paths.get()
        if current == goal:
            print(cost_so_far[goal])
            break
        neighbors = []
        x,y = current[0],current[1]
        for dx in [-1,0,1]:
            for dy in [-1,0,1]:
                if (x+dx) in range(width) and (y+dy) in range(height) and (abs(dx) + abs(dy)) != 2:
                    if x+dx != x or y+dy != y:
                        neighbors.append((x+dx,y+dy))
        for next in neighbors:
            new_cost = cost_so_far[current] + grid[next]
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost
                paths.put(next,priority)
                came_from[next] = current

Dijkstra(h,w)
Dijkstra(5*h,5*w)