data = list(open('input.txt').read().strip())
conversion = {}
for line in open('conversion.txt').read().splitlines(): ## I copied the conversion table from the webpage instead of typing it
    a,b = line.split(' = ')
    conversion[a] = b
for i in range(len(data)):
    data[i] = conversion[data[i]]
data = list(''.join(data))
expression = []
sum_versions = 0

def operations(op,nbs,liste):
    if op == 0:
        liste.append(sum(nbs))
    elif op == 1:
        n = 1
        for v in nbs:
            n *= v
        liste.append(n)
    elif op == 2:
        liste.append(min(nbs))
    elif op == 3:
        liste.append(max(nbs))
    elif op == 5:
        if nbs[0] > nbs[1]:
            liste.append(1)
        else:
            liste.append(0)
    elif op == 6:
        if nbs[0] < nbs[1]:
            liste.append(1)
        else:
            liste.append(0)
    elif op == 7:
        if nbs[0] == nbs[1]:
            liste.append(1)
        else:
            liste.append(0)
    return liste

def parse(packet,sum_versions,value,countdown,bool):
    while len(packet) > 1 :
        if bool:
            countdown -= 1
            if countdown < 0:
                break
        version = int(''.join(packet[:3]),2)
        sum_versions += version
        ID = int(''.join(packet[3:6]),2)
        packet = packet[6:]
        if ID == 4:
            number = ''
            while packet.pop(0) != '0':
                number += ''.join(packet[:4])
                packet = packet[4:]
            number += ''.join(packet[:4])
            number = int(number,2)
            packet = packet[4:]
            value.append(number)
            continue
        else:
            if packet.pop(0) == '0':
                length = int(''.join(packet[:15]),2)
                packet = packet[15:]
                packet,subpacket = packet[length:],packet[:length]
                subvalue = []
                subpacket,sum_versions,subvalue,countdown = parse(subpacket,sum_versions,subvalue,countdown,False)
                value = operations(ID,subvalue,value)
            else:
                number = int(''.join(packet[:11]),2)
                packet = packet[11:]
                subvalue = []
                packet,sum_versions,subvalue,discard = parse(packet,sum_versions,subvalue,number,True)
                value = operations(ID,subvalue,value)
    return packet,sum_versions,value,countdown

data,sum_versions,expression,countdown = parse(data,sum_versions,expression,0,False)
print(sum_versions,expression[0])