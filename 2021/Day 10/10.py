data = open('input.txt').read().splitlines()
trimmed_data = data.copy()
scores1 = {')': 3, ']': 57, '}':1197, '>': 25137}
scores2 = {')': 1, ']': 2, '}':3, '>': 4}
pairs = {'()': '', '[]':'', '{}':'', '<>': ''}
matches = {')': '(', ']': '[', '}': '{', '>': '<'}
reversed_matches = {'(': ')', '[': ']', '{': '}', '<': '>'}
p1 = 0
p2 = []

for line in data:
    workline = ''.join(list(line))
    while sum([workline.count(key) for key in pairs.keys()]) > 0:
        for key in pairs.keys():
            workline = workline.replace(key,pairs[key])
    workline = list(workline)
    i = 0
    while i < len(workline):
        if workline[i] in scores1.keys():
            if workline[i-1] != matches[workline[i]]:
                p1 += scores1[workline[i]]
                i = len(workline)
                trimmed_data.remove(line)                
        i += 1

for line in trimmed_data:
    line_score = 0
    counts = {')': 0, ']': 0, '}':0, '>': 0, '(': 0, '[': 0, '{': 0, '<': 0}
    for j in reversed(range(len(line))):
        counts[line[j]] += 1
        if line[j] in matches.values():
            if counts[line[j]] > counts[reversed_matches[line[j]]]:
                counts[reversed_matches[line[j]]] += 1
                line_score *= 5
                line_score += scores2[reversed_matches[line[j]]]
    p2.append(line_score)

print(p1)
print(sorted(p2)[(len(p2)-1)//2])