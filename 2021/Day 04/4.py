input = open('input.txt').read().split('\n\n')
numbers,boards = (input[0]).split(','),[board.replace('\n',' ').split(' ') for board in input[1:]]
for board in boards:
    while board.count('') != 0:
        board.remove('')

def play_bingo(announcer,players,bool):
    while bool:
        for player in players:
            for i in range(len(player)):
                if player[i] == announcer[0]:
                    player[i] = '*'
                    break
        for player in players:
            for i in range(0,5):
                if player[5*i:(5*i)+5].count('*') == 5 or [player[i+k] for k in range(0,len(player),5)].count('*') == 5:
                    winner = player.copy()
                    players.remove(player)
                    last_called = int(announcer[0])
                    bool = False
                    break
        del announcer[0]
    return last_called,winner,announcer,players

lucky_number,winning_board,numbers,boards = play_bingo(numbers,boards,True)
print(lucky_number*sum(int(x) for x in winning_board if x !='*'))

while len(boards) > 0:
    playing = True
    last_number,losing_board,numbers,boards = play_bingo(numbers,boards,True)
print(last_number*sum(int(x) for x in losing_board if x !='*'))