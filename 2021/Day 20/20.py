alg, img = open('input.txt').read().strip().split('\n\n')
img = img.splitlines()
algorithm = set(i for i in range(len(alg)) if alg[i] == '#')
image = set()
height = len(img)
width = len(img[0])
for x in range(height):
    for y in range(width):
        if img[x][y] == '#':
            image.add((x,y))

def enhance(file,iterations):
    for _ in range(iterations):
        newimage = set()
        # each enhancement should increase the original image size by 1 in each of the 4 cardinal directions
        # not having it as an infinite field means the outermost layer is incorrect after each enhancement
        # so having the field increased by 2*iterations seemed safe enough to have the correct picture in the set boundaries
        for x in range(-iterations*2,height+iterations*2):  
            for y in range(-iterations*2,width+iterations*2): 
                code = ''
                for dx in [-1,0,1]:
                    for dy in [-1,0,1]: 
                        if (x+dx,y+dy) in file:
                            code += '1'
                        else:
                            code += '0'
                if int(code,2) in algorithm:
                    newimage.add((x,y))
        file = newimage.copy()

    count = 0
    for x in range(-iterations,height+iterations):
        for y in range(-iterations,height+iterations):
            if (x,y) in file:
                count+=1
    return(count)

print(enhance(image,2))
print(enhance(image,50))