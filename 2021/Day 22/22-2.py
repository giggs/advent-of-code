import re

cubes = []
for line in open('input.txt').read().strip().splitlines():
    x1,x2,y1,y2,z1,z2 = map(int, re.findall(r'-?\d+', line))
    x2,y2,z2 = x2+1,y2+1,z2+1
    cube = [x1,x2,y1,y2,z1,z2]
    new_cubes = []
    
    for other_cube in cubes:
        i1,i2,j1,j2,k1,k2 = other_cube[0],other_cube[1],other_cube[2],other_cube[3],other_cube[4],other_cube[5]
        if (x1 < i2 and x2 > i1) and (y1 < j2 and y2 > j1) and (z1 < k2 and z2 > k1):
            if i1 < x1:
                new_cubes.append([i1,x1,j1,j2,k1,k2])
                i1 = x1
            if i2 > x2:
                new_cubes.append([x2,i2,j1,j2,k1,k2])
                i2 = x2
            if j1 < y1:
                new_cubes.append([i1,i2,j1,y1,k1,k2])
                j1 = y1
            if j2 > y2:
                new_cubes.append([i1,i2,y2,j2,k1,k2])
                j2 = y2
            if k1 < z1:
                new_cubes.append([i1,i2,j1,j2,k1,z1])
                k1 = z1
            if k2 > z2:
                new_cubes.append([i1,i2,j1,j2,z2,k2])
                k2 = z2
        else:
            new_cubes.append(other_cube)
    if line[:3] == "on ":
        new_cubes.append(cube)
    cubes = new_cubes.copy()

print(sum((cube[1]-cube[0])*(cube[3]-cube[2])*(cube[5]-cube[4]) for cube in cubes))

# This took hours because at first I tried to slice the cubes in a slightly different fashion, not updating the edges after each slice and generally obfuscating the process
# After realizing this I scratched my 300+ lines solution and started over. Turns out it can be quite clean