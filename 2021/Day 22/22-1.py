import re
## solution for part 1 was quick to make. I wanted to keep it
cubes = set()

for line in open('input.txt').read().strip().splitlines():
    x1,x2,y1,y2,z1,z2 = map(int, re.findall(r'-?\d+', line))
    if abs(x1) < 51:
        for x in range(x1,x2+1):
            for y in range(y1,y2+1):
                for z in range(z1,z2+1):
                    if line[:3] == "off" and (x,y,z) in cubes:
                        cubes.remove((x,y,z))
                    elif line[:3] == "on ":
                        cubes.add((x,y,z))

print(len(cubes))