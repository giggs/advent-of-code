p1_pos = 6
p2_pos = 4
p1,p2,die,i = 0,0,0,0

while True:
    i += 1
    forward = 0
    for _ in range(3):
        die += 1
        forward += die
    if i % 2 == 1:
        p1_pos = (forward + p1_pos) % 10
        if p1_pos == 0:
            p1_pos = 10
        p1 += p1_pos
        if p1 > 999:
            print(die*p2)
            break
    else:
        p2_pos += forward
        p2_pos = p2_pos % 10
        if p2_pos == 0:
            p2_pos = 10
        p2 += p2_pos
        if p2 > 999:
            print(die*p1)
            break

p1_pos = 6
p2_pos = 4
p1,p2 = 0,0
i = 1
universes = {}
universes[(p1,p2,p1_pos,p2_pos,i)] = 1
rolls = {3:1, 4:3, 5:6, 6:7, 7:6, 8:3, 9:1}

while len(universes) > 0:
    universe = universes.popitem()
    number = universe[1]
    universe = universe[0]
    i = universe[4]
    if i%2 == 1:
        i+=1
        for key in rolls.keys():
            p1_pos = universe[2]
            p1_pos += key
            p1_pos = p1_pos%10 if p1_pos > 10 else p1_pos
            if universe[0] + p1_pos > 20:
                p1 += number*rolls[key]
            else:
                universes[(universe[0] + p1_pos, universe[1],p1_pos,universe[3],i)] = number*rolls[key]
    else:
        i+=1
        for key in rolls.keys():
            p2_pos = universe[3]
            p2_pos += key
            p2_pos = p2_pos%10 if p2_pos > 10 else p2_pos
            if universe[1] + p2_pos > 20:
                p2 += number*rolls[key]
            else:
                universes[(universe[0], universe[1]+p2_pos,universe[2],p2_pos,i)] = number*rolls[key]

print(max(p1,p2))  