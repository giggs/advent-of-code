p1_pos = 6
p2_pos = 4
p1,p2,die,i = 0,0,0,0

while True:
    i += 1
    forward = 0
    for _ in range(3):
        die += 1
        forward += die
    if i % 2 == 1:
        p1_pos = (forward + p1_pos) % 10
        if p1_pos == 0:
            p1_pos = 10
        p1 += p1_pos
        if p1 > 999:
            print(die*p2)
            break
    else:
        p2_pos += forward
        p2_pos = p2_pos % 10
        if p2_pos == 0:
            p2_pos = 10
        p2 += p2_pos
        if p2 > 999:
            print(die*p1)
            break

p1_pos = 6
p2_pos = 4
p1,p2 = 0,0
universes = {}
rolls = {3:1, 4:3, 5:6, 6:7, 7:6, 8:3, 9:1}

def dirac_dice(p1,p2,p1_pos,p2_pos):
        if p2 > 20:
            return (0,1)
        if (p1,p2,p1_pos,p2_pos) in universes:
            return universes[(p1,p2,p1_pos,p2_pos)]
        wins = (0,0)
        for key in rolls.keys():
            new_p1_pos = p1_pos + key
            new_p1_pos = new_p1_pos%10 if new_p1_pos > 10 else new_p1_pos
            new_p1 = new_p1_pos + p1            
            win1,win2 = dirac_dice(p2,new_p1,p2_pos,new_p1_pos)
            wins = (wins[0]+rolls[key]*win2,wins[1]+rolls[key]*win1)
        universes[(p1,p2,p1_pos,p2_pos)] = wins
        return wins

print(max(dirac_dice(p1,p2,p1_pos,p2_pos)))  