from typing import DefaultDict
import string

data = open("input.txt").read().splitlines()
links = DefaultDict(lambda:[])
p1,p2 = [], []
start = ['start']

for line in data:
    source,dest = line.split('-')
    links[source].append(dest)
    links[dest].append(source)
links['end'] = []

def connect(path,total,part):
    if path[-1] == 'end':
        total.append(path)          
    next_node = links[path[-1]].copy()
    while len(next_node) > 0:
        add = True
        if part:
            if next_node[0][0] in string.ascii_lowercase:
                if next_node[0] in path:
                    add = False
        elif next_node[0] == 'start' :
            add = False
        else:
            if next_node[0][0] in string.ascii_lowercase:
                if path.count(next_node[0]) > 0:                        
                    for node in path:
                        if node[0] in string.ascii_lowercase:
                            if path.count(node) == 2:
                                add = False
        if add:
            new_path = path.copy()
            new_path.append(next_node[0])
            connect(new_path,total,part)
        if len(next_node) > 0:
            next_node.pop(0)

connect(start,p1,True)
print(len(p1))
connect(start,p2,False)
print(len(p2))