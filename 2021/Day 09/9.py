from typing import DefaultDict


data = open('input.txt').read().splitlines()
points = DefaultDict(lambda:9)
low_points = 0
basins = []

for i in range(len(data)):
    for j in range(len(list(data)[i])):
        points[i,j] = int(list(data[i])[j])

for i in range(len(data)):
    for j in range(len(list(data[0]))):
        if points[i,j] < points[i,j+1] and points[i,j] < points[i,j-1] and points[i,j] < points[i-1,j] and points[i,j] < points[i+1,j]:
            low_points += (points[i,j]+1)
            basin = [(i,j)]
            for (x,y) in basin:
                for dx in [-1,0,1]:
                    for dy in [-1,0,1]:
                        if abs(dx)+abs(dy) != 2:
                            if points[x+dx,y+dy] != 9 and (x+dx,y+dy) not in basin:
                                basin.append((x+dx,y+dy))
            basins.append(len(basin))

basins = sorted(basins)[-3:]
print(low_points,basins[0]*basins[1]*basins[2])