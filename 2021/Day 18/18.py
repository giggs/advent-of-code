import itertools

data = open('input.txt').read().splitlines()
permutations = [p for p in itertools.permutations(data,2)]
p2 = 0
number = list(data.pop(0))
for i in range(len(number)):
    if number[i] in '0123456789':
        number[i] = int(number[i])

def add(n,liste):
    for character in liste.pop(0):
        if character in '0123456789':
            n.append(int(character))
        else:
            n.append(character)
    return n

def reduce(liste):
    while True:
        depth = 0
        for start in range(len(liste)): # explode
            if liste[start] == '[':
                depth += 1
            elif liste[start] == ']':
                depth -= 1
            if depth > 4:
                left_number, right_number = liste[start+1], liste[start+3]
                for _ in range(start,start+5):
                    liste.pop(start)
                liste.insert(start,0)                
                k = start+1 # add right
                while k < len(liste) and not isinstance(liste[k],int):
                    k += 1
                if k != len(liste):
                    liste[k] = liste[k] + right_number                           
                j = start-1 # add left
                while j > 0 and not isinstance(liste[j],int):
                    j -= 1
                if j != 0:
                    liste[j] = liste[j] + left_number                
                break
        else:
            splitted = False
            for start in range(len(liste)): # split
                if isinstance(liste[start],int) and liste[start] > 9:
                    splitted = True
                    left_number = liste[start]//2
                    right_number = liste[start] - left_number
                    liste = liste[:start] + ['[',left_number,',',right_number,']'] + liste[start+1:]
                    break
            if not splitted:
                break    
    return liste

def evaluate(liste):
    for c in range(len(liste)):
        if isinstance(liste[c],int):
            liste[c] = str(liste[c])
    liste = ''.join(liste).replace('[','((3*').replace(',',')+(').replace(']','*2))')
    return eval(liste)

while len(data) > 0:
    number = ['['] + number + [',']
    number = add(number,data) + [']']
    number = reduce(number)
print(evaluate(number))

for p in permutations:
    p = [p[0],p[1]]
    pair = ['[']
    pair = add(pair,p) + [',']
    pair = add(pair,p) + [']']
    p2 = max(p2,evaluate(reduce(pair)))
print(p2)