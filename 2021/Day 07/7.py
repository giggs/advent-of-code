crabs = [int(x) for x in open('input.txt').read().split(',')]
part1 = max(crabs)**2
part2 = (max(crabs)**2)*len(crabs)

for position in range(0, max(crabs)):
    fuel1 = 0
    fuel2 = 0
    for crab in crabs:
        steps = abs(crab-position)
        fuel1 += steps
        fuel2 += (steps*(steps+1))//2
    if fuel1 < part1:
        part1 = fuel1
    if fuel2 < part2:
        part2 = fuel2

print(part1,part2)