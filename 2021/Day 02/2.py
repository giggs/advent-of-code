input = open("input.txt").read().splitlines()

x,y1,y2,aim = 0,0,0,0

for instruction in input:
    command,number = instruction.split(' ')
    if command == 'up':
        y1 -= int(number)
        aim -= int(number)
    elif command == 'down':
        y1 += int(number)
        aim += int(number)
    else:
        x += int(number)
        y2 += aim*int(number)

print(x*y1,x*y2)