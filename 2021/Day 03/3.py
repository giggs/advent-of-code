input = open('input.txt').read().splitlines()
oxygen,seeohtwo = input.copy(),input.copy()
length = len(input[0])
j,k = 0,0

def get_gamma_epsilon(min,max,data):
    g = []
    e = []
    for i in range(min,max):
        ones = 0
        for line in data :
            ones += int(line[i])
        if ones >= len(data)//2:
            g.append("1")
            e.append("0")
        else :
            g.append("0")
            e.append("1")
    return g,e

def get_life_support(bit,bool,data):
    while len(data) != 1:
        one,zero = get_gamma_epsilon(bit,bit+1,data)
        template = one if bool else zero
        n = 0
        while n < len(data):
            if data[n][bit] != template[0]:
                data.remove(data[n])
                n -= 1
            n += 1
        bit += 1
    return int(''.join(data),2)

gamma,epsilon = get_gamma_epsilon(0,length,input)
print(int(''.join(gamma),2)*int(''.join(epsilon),2))

print(get_life_support(j,True,oxygen)*get_life_support(k,False,seeohtwo))