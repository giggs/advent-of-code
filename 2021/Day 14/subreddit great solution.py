## This is solution by u/4HbQ is definitely something I should have looked into
## For every replacement, decrease the count of the original pair
## Increase the count of the 2 replacement pairs and of the new character

from collections import Counter

tpl, rules = open('input.txt').read().split('\n\n')
rules = dict(r.split(" -> ") for r in rules.splitlines())
pairs = Counter(map(str.__add__, tpl, tpl[1:]))
chars = Counter(tpl)

for _ in range(40):
    for (a,b), c in pairs.copy().items():
        x = rules[a+b]
        pairs[a+b] -= c
        pairs[a+x] += c
        pairs[x+b] += c
        chars[x] += c

print(max(chars.values())-min(chars.values()))

