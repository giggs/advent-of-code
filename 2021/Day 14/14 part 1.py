from collections import defaultdict
import string
import timeit

## First solution to part 1

def run():
    polymer, rules = open('input.txt').read().split('\n\n')
    polymer = list(polymer)
    rules = rules.splitlines()
    insertions = defaultdict(lambda:'')
    counts = {}
    min = 2**32
    max = 0

    for rule in rules:
        key,value = rule.split(" -> ")
        insertions[(key[0],key[1])] = value

    for _ in range(10):
        for i in range(0,(len(polymer)*2)-2,2):
            polymer.insert(i+1,insertions[(polymer[i],polymer[i+1])])


    for letter in string.ascii_uppercase:
        count = polymer.count(letter)
        if count:
            min = count if count < min else min
            max = count if count > max else max

    print(max-min)

print(timeit.timeit('run()',number=1,globals=globals()))