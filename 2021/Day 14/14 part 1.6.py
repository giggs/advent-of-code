import string
import timeit
import re

## Faster still, still way too slow. modified input included

def run():
    polymer, rules = open('input copy.txt').read().split('\n\n')
    rules = rules.splitlines()
    insertions = {}
    min = 2**64
    max = 0

    for rule in rules:
        key,value = rule.split(" -> ")
        value = ''.join([key[0],'.',value,'.',key[1]])
        insertions[(key)] = value

    for _ in range(10):
        for key in insertions.keys():
            polymer = re.sub(rf"{key}", rf"{insertions[key]}", polymer)
        polymer = polymer.replace('.','')
           
    for letter in string.ascii_uppercase:
        count = polymer.count(letter)
        if count:
            min = count if count < min else min
            max = count if count > max else max

    print(max-min)

print(timeit.timeit('run()',number=1,globals=globals()))