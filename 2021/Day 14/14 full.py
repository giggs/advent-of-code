from collections import defaultdict
import timeit
import re
import itertools

## By far my worst solution so far! I couldn't find a way to get a good DP algorithm in the time I had 
## and just split the work by generating everything possible pair to 20 iterations,
## then making the necessary replacements. I had to add a rule to the input to make it work.
## It's really bad but it works. Now I can go admire the good solutions

def run(limit):
    polymer, rules = open('input copy.txt').read().split('\n\n')
    polymer = list(polymer)
    setup = rules.replace(' -> ','').replace('\n','').replace('.','')
    setup = set(setup)
    rules = rules.splitlines()
    insertions = {}
    letters = defaultdict(lambda:0)
    cache = {}

    for rule in rules:
        key,value = rule.split(" -> ")
        value = ''.join([key[0],'.',value,'.',key[1]])
        insertions[(key)] = value

    for prod in itertools.product(setup, repeat = 2):
        extended_prod = ''.join((prod[0],prod[1]))
        for i in range(limit):
            for key in insertions.keys():
                extended_prod = re.sub(rf"{key}", rf"{insertions[key]}", extended_prod)
            extended_prod = extended_prod.replace('.','')
        cache[(''.join((prod[0],prod[1])))] = (extended_prod[:-1], [(letter,extended_prod[:-1].count(letter)) for letter in setup])
    
    new_polymer = []
    for i in range(len(polymer)-1):   
        new_polymer.append(cache[(''.join((polymer[i],polymer[i+1])))][0])
    new_polymer.append(polymer[-1])
    new_polymer = ''.join(new_polymer)

    new_polymer = list(new_polymer)

    for i in range(len(new_polymer)-1):
        pair = ''.join(new_polymer[i:i+2])
        for letter in cache[pair][1]:
            letters[letter[0]] += letter[1]
    
    letters[new_polymer[-1]] += 1   

    print(max(value for value in letters.values())-min(value for value in letters.values()))

print(timeit.timeit('run(5)',number=1,globals=globals()))
print(timeit.timeit('run(20)',number=1,globals=globals()))