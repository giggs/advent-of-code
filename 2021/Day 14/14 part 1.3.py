import string
import timeit

## Made it faster, still way too slow

def run():
    polymer, rules = open('input.txt').read().split('\n\n')
    polymer = {index: value for index,value in enumerate(polymer)}
    rules = rules.splitlines()
    insertions = {}
    min = 2**64
    max = 0
    
    for rule in rules:
        key,value = rule.split(" -> ")
        insertions[(key[0],key[1])] = value

    for _ in range(20):
        i = len(polymer)-1
        target = len(polymer)*2 - 1
        j = 1
        while i > 0:
            polymer[target-j] = polymer[i]
            polymer[target-j-1] = insertions[(polymer[i-1],polymer[i])]
            i -= 1
            j += 2 

    polymer = [letter for letter in polymer.values()]
    for letter in string.ascii_uppercase:
        count = polymer.count(letter)
        if count:
            min = count if count < min else min
            max = count if count > max else max

    print(max-min)

print(timeit.timeit('run()',number=1,globals=globals()))