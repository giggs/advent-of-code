input = [int(x) for x in open('input.txt').read().split(',')]
fish = [input.count(i) for i in range(9)]
days = 256

while days > 0:
    fish = fish[1:] + fish[:1]
    fish[6] += fish[-1]
    days -= 1
    if days == 256-80:
        print(sum(fish))

print(sum(fish))

## Modified with stuff I found on the subreddit that I liked