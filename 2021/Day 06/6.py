input = [int(x) for x in open('input.txt').read().split(',')]
fish = [0]*10
numbers = set(input)
days = 256

for number in numbers:
    fish[number] = input.count(number)

while days > 0:
    spawners = fish[0]
    for i in range(1,10):
        fish[i-1] = fish[i]
    fish[6] += spawners
    fish[8] += spawners
    days -= 1
    if days == 256-80:
        print(sum(fish))

print(sum(fish))