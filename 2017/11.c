#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))

int eleven()
{
	FILE* input = fopen("11.txt", "r");
	int c, i, x, y, max_x, max_y;
	i = x = y = max_x = max_y = 0;
	char direction[3] = { '\0' };

	while ((c = fgetc(input)) != EOF) {
		if (c == ',' || c == '\n') {
			direction[i] = '\0';
			i = 0;
			if (!strcompare(direction, "ne")) {
				x++;
				y--;
			}
			else if (!strcompare(direction, "n")) {
				y -= 2;
			}
			else if (!strcompare(direction, "nw")) {
				x--;
				y--;
			}
			else if (!strcompare(direction, "sw")) {
				x--;
				y++;
			}
			else if (!strcompare(direction, "s")) {
				y += 2;
			}
			else if (!strcompare(direction, "se")) {
				x++;
				y++;
			}
			max_x = MAX(ABS(x), max_x);
			max_y = MAX(ABS(y),max_y);
		}
		else {
			direction[i++] = c;
		}	
	}
	printf("%d %d ", MAX(ABS(x),ABS(y)), MAX(max_x,max_y));
}