#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))

static char grid[128][128] = { '\0' };

void expand(int y, int x)
{
	grid[y][x] = 0;
	for (int dy = -1; dy < 2; dy++)
		for (int dx = -1; dx < 2; dx++) {
			if (ABS(dx) + ABS(dy) == 2 ||
				y + dy > 127 || y + dy < 0 ||
				x + dx > 127 || x + dx < 0)
				continue;
			else {
				if (grid[y + dy][x + dx])
					expand(y + dy,x + dx);
			}
		}
}

void knot_hash(int* dense_hash, int* lengths, int j);

int fourteen()
{
	char puzzle[] = "vbqugkhl-";
	int squares = 0;
	int regions = 0;
	
	for (int digit = 0; digit < 128; digit++) {
		int lengths[100] = { 0 };
		int j = 0;
		int c;
		int k = -1;
		while ((c = puzzle[++k]) != '\0')
			lengths[j++] = c;
		char d[4];
		itoa(digit, d);
		int l = -1;
		while ((c = d[++l]) != '\0')
			lengths[j++] = c;
		
		int dense_hash[16] = { 0 };
		knot_hash(dense_hash, lengths, j);

		for (int n = 0; n < 16; n++){
			int p = 0;
			int number = dense_hash[n];
			while (number) {
				squares += number & 1;
				grid[digit][8 * (n + 1) - p++ -1] = number & 1;
				number >>= 1;
			}
		}
	}
	
	for (int y = 0; y < 128; y++) {
		for (int x = 0; x < 128; x++) {
			if (grid[y][x] == 1) {
				regions++;
				expand(y, x);
			}
		}
	}
	
	printf("%d %d ", squares, regions);
	return 0;
}
