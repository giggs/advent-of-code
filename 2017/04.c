#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int four()
{
	FILE* input = fopen("04.txt", "r");
	int answer1 = 0;
	int answer2 = 0;
	int c;

	while ((c = fgetc(input)) != EOF)
	{
		if (c == '\n')
			continue;
		char passphrase[200][20] = {'0'};
		int i = 0;
		int j = 0;
		passphrase[j][i++] = c;

		while ((c = fgetc(input)) != '\n')
		{
			if (c == ' ')
			{
				++j;
				i = 0;
			}
			else
			{
				passphrase[j][i++] = c;
			}
		}
		int is_valid1 = 1;
		int is_valid2 = 1;
		for (int k = 0; k <= j; ++k)
		{
			char s1[20];
			strcpy(s1, passphrase[k]);
			bubblesort(s1);
			for (int l = k+1; l <= j; ++l)
			{
				char s2[20];
				strcpy(s2, passphrase[l]);
				bubblesort(s2);
				int m = 0;
				while (s1[m] == s2[m])
				{
					if (s1[m] == '\0')
					{
						is_valid2 = 0;
						goto PART1;
					}
					++m;
				}
				if (is_valid2 == 0)
				{
PART1:				
					m = 0;
					while (passphrase[k][m] == passphrase[l][m])
					{
						if (passphrase[k][m] == '\0')
						{
							is_valid1 = 0;
							goto COUNT;
						}
						++m;
					}
				}
			}
		}
COUNT:	
		answer1 += is_valid1;
		answer2 += is_valid2;
	}
	printf("%d %d ", answer1, answer2);

	return 0;
}