#define _CRT_SECURE_NO_WARNINGS
#ifndef TABLE
#define TABLE 100
#endif
#include <stdio.h>
#define MAXLINES 100
#define MAXVAL 10000

long long registers[26] = { 0 };
long long registers_2[52] = { 0 };
char instructions[MAXLINES][TABLE];

int getline(char s[], int lim, FILE* file);
int strsplit(char s[], char t[][TABLE], char split[]);

int sp1 = 0;
int sp0 = 0;
long long val1[MAXVAL];
long long val0[MAXVAL];
long long* pv1 = &val1;
long long* pv0 = &val0;

void push1(long long f)
{
	if (sp1 < MAXVAL)
		val1[sp1++] = f;
}

void push0(long long f)
{
	if (sp0 < MAXVAL)
		val0[sp0++] = f;
}

long long pop1(void)
{
	if (*pv1)
		return *pv1++;
}

long long pop0(void)
{
	if (*pv0)
		return *pv0++;
}

int lines = -1;
int t0 = 0;
int t1 = 0;

int simulate(int program)
{
	char workstring[3][TABLE];
	char inst[4];

	int t = (program == 0) ? t0 : t1;
	int value = 0;
	int offset = (program == 0) ? 0 : 26;

	while (t >= 0 && t <= lines) {
		char d;
		int e = strsplit(instructions[t], workstring, " ");
		strcopy(inst, workstring[0]);
		char c = workstring[1][0];
		if (e > 1)
			d = workstring[2][0];

		if (!strcompare(inst, "snd")) {
			if (is_letter(c))
				value = registers_2[offset + c - 'a'];
			else
				value = atoi(workstring[1]);
			if (program == 0)
				push0(value);
			else {
				push1(value);
			}
		}
		else if (!strcompare(inst, "set")) {
			if (is_letter(d))
				registers_2[offset + c - 'a'] = registers_2[offset + d - 'a'];
			else
				registers_2[offset + c - 'a'] = atoi(workstring[2]);
		}
		else if (!strcompare(inst, "add")) {
			if (is_letter(d))
				registers_2[offset + c - 'a'] += registers_2[offset + d - 'a'];
			else
				registers_2[offset + c - 'a'] += atoi(workstring[2]);
		}
		else if (!strcompare(inst, "mul")) {
			if (is_letter(d))
				registers_2[offset + c - 'a'] *= registers_2[offset + d - 'a'];
			else
				registers_2[offset + c - 'a'] *= atoi(workstring[2]);
		}
		else if (!strcompare(inst, "mod")) {
			if (is_letter(d))
				registers_2[offset + c - 'a'] %= registers_2[offset + d - 'a'];
			else
				registers_2[offset + c - 'a'] %= atoi(workstring[2]);
		}
		else if (!strcompare(inst, "rcv")) {
			if (program == 0) {
				if (*pv1) 
					registers_2[c - 'a'] = pop1();
				else
					return t;
			}
			else {
				if (*pv0) 
					registers_2[offset + c - 'a'] = pop0();
				else
					return t;
			}
		}
		else { // jgz
			int trigger = 0;
			if (is_letter(c))
				trigger = registers_2[offset + c - 'a'];
			else
				trigger = atoi(workstring[1]);
			if (trigger > 0)
				if (is_letter(d))
					t += registers_2[offset + d - 'a']-1;
				else
					t += atoi(workstring[2]) - 1;
		}
		t++;
	}
	return t;
}

int eighteen()
{
	FILE* input = fopen("18.txt", "r");
	char s[TABLE];
	int t = 0;

	while (t = getline(s, MAXLINES, input) > 0) 
		strcopy(instructions[++lines], s);
	
	t = -1;
	char workstring[3][TABLE];
	int sound = 0;
	char inst[4];

	while (++t >= 0 && t <= lines) {
		char d;
		int e = strsplit(instructions[t], workstring, " ");
		strcopy(inst, workstring[0]);
		char c = workstring[1][0];
		if (e > 1)
			d = workstring[2][0];

		if (!strcompare(inst, "snd")) {
			if (is_letter(c))
				sound = registers[c - 'a'];
			else
				sound = atoi(workstring[1]);
		}
		else if (!strcompare(inst, "set")) {
			if (is_letter(d))
				registers[c - 'a'] = registers[d - 'a'];
			else
				registers[c - 'a'] = atoi(workstring[2]);
		}
		else if (!strcompare(inst, "add")) {
			if (is_letter(d))
				registers[c - 'a'] += registers[d - 'a'];
			else
				registers[c - 'a'] += atoi(workstring[2]);
		}
		else if (!strcompare(inst, "mul")) {
			if (is_letter(d))
				registers[c - 'a'] *= registers[d - 'a'];
			else
				registers[c - 'a'] *= atoi(workstring[2]);
		}
		else if (!strcompare(inst, "mod")) {
			if (is_letter(d))
				registers[c - 'a'] %= registers[d - 'a'];
			else
				registers[c - 'a'] %= atoi(workstring[2]);
		}
		else if (!strcompare(inst, "rcv") && registers[c - 'a'] > 0) {
				printf("%d ", sound);
				break;
		}
		else {
			int trigger = 0;
			if (is_letter(c))
				trigger = registers[c - 'a'];
			else
				trigger = atoi(workstring[1]);
			if (trigger > 0)
				t += atoi(workstring[2]) - 1;
		}
	}

	registers_2[26+'p' - 'a'] = 1;
	
	do{
		t0 = simulate(0);
		t1 = simulate(1);
	} while (*pv0 || *pv1);

	printf("%d ", sp1);
	return 0;
}