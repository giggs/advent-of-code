#define _CRT_SECURE_NO_WARNINGS
#ifndef TABLE
#define TABLE 100
#endif
#define MAXLINE 200
#include <stdio.h>

char programs[1500][TABLE] = { '\0' };
char temp[15][100] = { '\0' };
int weights[1500] = { 0 };
char children[1500][TABLE][TABLE] = {'\0'};

int get_weight(char s[])
{
	int index = find_index(s, programs);
	int w = weights[index];
	int m = 0;
	while (children[index][m][0] != '\0') {
		char child[30] = { '\0' };
		strcopy(child, children[index][m]);
		w += get_weight(child);
		m++;
	}
	return w;
}

int find_difference(int l)
{
	int root_weights[10] = { 0 };
	int m = 0;
	while (children[l][m][0] != '\0') {
		char child[20];
		strcopy(child, children[l][m]);
		root_weights[m] = get_weight(child);
		++m;
	}
	static int diff = 0;
	while (1) {
		int i = 1;
		while (root_weights[++i] != 0) {
			if (root_weights[0] != root_weights[i]) {
				if ((i > 1) || root_weights[0] == root_weights[2]) {
					diff = root_weights[i] - root_weights[i - 1];
					diff = (diff > 0) ? diff : -diff;
				}
				else {
					diff = root_weights[i] - root_weights[0];
					diff = (diff < 0) ? diff : -diff;
					i = 0;
				}
				find_difference(find_index(children[l][i], programs));
				return diff;
			}
			if (root_weights[++i] == 0) {
				diff = weights[l] - diff;
				return diff;
			}
		}
	}
}

int seven()
{
	FILE* input = fopen("07.txt", "r");
	char line[MAXLINE];
	int i = -1;
	char root[] = { '\0' };

	while (getline(line, MAXLINE, input) > 0) {
		++i;
		int splits = strsplit(line, programs[i], " (");
		splits = strsplit(programs[i + 1], temp[0], ")");
		weights[i] = atoi(temp[0]);
		if (temp[1][0]) {
			splits = strsplit(temp[1], temp[2], " -> ");
			splits = strsplit(temp[3], children[i], ", ");
			temp[1][0] = '\0';
		}		
	}
	int l = 0;
	for (l = 0; l <= i; ++l) {
		int found = 0;
		char name[30] = {'\0'};
		strcopy(name, programs[l]);
		for (int k = 0; k <= i; ++k) {
			if (k == l)
				continue;
			int m = 0;
			while (children[k][m][0] != '\0') {
				if (!strcompare(name, children[k][m++])) {
					found = 1;
					break;
				}
			}
			if (found)
				break;
			}
		if (!found) {
			printf("%s %d ", name, find_difference(l));
			break;
		}
	} 
	return 0;
}