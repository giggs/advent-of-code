#define _CRT_SECURE_NO_WARNINGS
#ifndef TABLE
#define TABLE 100
#endif
#define MAXLINE 200
#include <stdio.h>

int registers_8[17577] = { 0 };
int max = 0;
int part2 = 0;
int modified[1000] = { 0 };

/* Fun hash_8 function by polomi
int hash_8(char* s)
{
	int index = 0;
	for (int i = 0; i < 3 && s[i]; ++i) {
		index |= (s[i] - 'a') << (5 * i);
	}
	return index;
}*/

int hash_8(char* s)
{
	int output = 0;
	for (int i = 0; s[i] != '\0'; i++)
		output += (s[i] - 'a') * power(26, 2 - i);
	return output;
}

int eight()
{
	FILE* input = fopen("08.txt", "r");
	char line[MAXLINE];
	int i = 0;
	while (getline(line, MAXLINE, input) > 0) {
		char instruction[10][TABLE];
		int splits = strsplit(line, instruction, " ");
		int value = atoi(instruction[2]);
		int target = hash_8(instruction[0]);
		int reg_condition = hash_8(instruction[4]);
		int reg_value = atoi(instruction[6]);
		value = (instruction[1][0] == 'i') ? value : -value;
		if (!strcompare("<", instruction[5])) {
			if (registers_8[reg_condition] < reg_value)
				registers_8[target] += value;
		}
		else if (!strcompare("<=", instruction[5])) {
			if (registers_8[reg_condition] <= reg_value)
				registers_8[target] += value;
		}
		else if (!strcompare(">", instruction[5])) {
			if (registers_8[reg_condition] > reg_value)
				registers_8[target] += value;
		}
		else if (!strcompare(">=", instruction[5])) {
			if (registers_8[reg_condition] >= reg_value)
				registers_8[target] += value;
		}
		else if (!strcompare("==", instruction[5])) {
			if (registers_8[reg_condition] == reg_value)
				registers_8[target] += value;
		}
		else {
			if (registers_8[reg_condition] != reg_value)
				registers_8[target] += value;
		}
		part2 = (registers_8[target] > part2) ? registers_8[target] : part2;
		modified[i++] = target;
	}
	for (int j = 0; modified[j] > 0; j++)
		max = (registers_8[modified[j]] > max) ? registers_8[modified[j]] : max;
	printf("%d %d ", max,part2);
	return 0;
}