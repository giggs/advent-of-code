#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int blocks[16] = { 0 };
int sequences[5000][16] = {0};

int find_max(void)
{
	int j = 0;
	int max = blocks[0];
	for (int i = 1; i < 16; i++)
		if (blocks[i] > max)
		{
			j = i;
			max = blocks[i];
		}
	return j;
}

void distribute(int a)
{
	int pos = a;
	int banks = blocks[a];
	blocks[a] = 0;
	while (banks > 0)
	{
		pos = (pos + 1) % 16;
		blocks[pos] += 1;
		banks--;
	}
} 

int six()
{
	FILE* input = fopen("06.txt", "r");
	char s[2];
	int i = 0;
	int c;

	while ((c = fgetc(input)) != '\n')
	{
		if ((c == ' ') || (c == '\t'))
			continue;
		s[0] = c;
		s[1] = fgetc(input);
		blocks[i++] = atoi(s);
	}
	int loop = 0;
	int cycles = 0;
	int part2 = 0;
	
	do
	{
		for (int j = 0; j < 16; j++)
			sequences[cycles][j] = blocks[j];
		distribute(find_max());
		cycles++;
		for (int k = 0; k < cycles; k++)
		{
			int m = 0;
			while ((blocks[m] == sequences[k][m]) && m < 16)
				++m;
			if (m == 16)
			{
				part2 = cycles - k;
				loop = 1;
				break;
			}
		}
	} while (!loop);

	printf("%d %d ", cycles, part2);

	return 0;
}