#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void one()
{
	const char* file_name = "01.txt";
	FILE* input = fopen(file_name, "r");
	char numbers[10000];

	int c;
	int len = 0;

	while ((c = fgetc(input)) != EOF)
		if (c != '\n')
		{
			numbers[len] = (c - '0');
			++len;
		}

	int p1 = 1;
	int p2 = (len) / 2;
	int sum1, sum2;
	sum1 = sum2 = 0;

	for (int i = 0; i <= (len + 1); ++i)
	{
		c = numbers[i];
		if (c == numbers[(i + p1) % len])
			sum1 += c;
		if (c == numbers[(i + p2) % len])
			sum2 += c;
	}
	
	printf("%d %d ", sum1, sum2);

	return 0;
}