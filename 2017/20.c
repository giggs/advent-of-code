#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define MAXLINE 1005
#define HASHSIZE 1500
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))

int getline_20(char s[], int lim, FILE* file)
{
	int c, i;
	i = 0;
	while (--lim > 0 && (c = fgetc(file)) != EOF && c != '\n') {
		if (c != '=' && c != '<' && c != '>' && c != ' ' && c != 'p' && c != 'v' && c != 'a')
		s[i++] = c;
	}
	/*if (c == '\n')
		s[i++] = c;*/ // I most likely don't need the newline symbol
	s[i] = '\0';
	return i;
}

struct particle {
	int px;
	int py;
	int pz;
	int psum;
	int vx;
	int vy;
	int vz;
	int vsum;
	int ax;
	int ay;
	int az;
	int asum;
};

struct particle particles[1001];

void simulate_20(struct particle* p)
{
	p->vx += p->ax;
	p->vy += p->ay;
	p->vz += p->az;
	p->vsum = ABS(p->vx) + ABS(p->vy) + ABS(p->vz);
	p->px += p->vx;
	p->py += p->vy;
	p->pz += p->vz;
	p->psum = ABS(p->px) + ABS(p->py) + ABS(p->pz);
}

int hash(struct particle p)
{
	unsigned hashval;
	hashval = 17 * p.px + 37 * p.py + 53 * p.pz;
	return hashval % HASHSIZE;
}

int collision_check(int i, int j, int pos[][15])
{
	if (pos[j][0] == pos[j][1])
		return j;
	struct particle q = particles[i];
	struct particle p = particles[pos[j][0]];
	if ((q.px == p.px) && (q.py == p.py) && (q.pz = p.pz))
		return j;
	else
		return collision_check(i, (j + 1)%HASHSIZE, pos);
}

int twenty()
{
	FILE* input = fopen("20.txt", "r");
	char line[80];
	int i = 1;
	char split[9][TABLE];
	int closest = 1000;
	int part1 = 0;

	while (getline_20(line, MAXLINE, input) > 0) {
		strsplit(line, split, ",");
		struct particle* p = &particles[i];
		p->px = atoi(split[0]);
		p->py = atoi(split[1]);
		p->pz = atoi(split[2]);
		p->psum = ABS(p->px) + ABS(p->py) + ABS(p->pz);
		p->vx = atoi(split[3]);
		p->vy = atoi(split[4]);
		p->vz = atoi(split[5]);
		p->vsum = ABS(p->vx) + ABS(p->vy) + ABS(p->vz);
		p->ax = atoi(split[6]);
		p->ay = atoi(split[7]);
		p->az = atoi(split[8]);
		p->asum = ABS(p->ax) + ABS(p->ay) + ABS(p->az);
		if (p->asum < closest) {
			part1 = i;
			closest = p->asum;
		}
		else if (p->asum == closest) {
			if (p->vsum < particles[part1].vsum) {
				part1 = i-1;
				closest = p->asum;
			}
			else if (p->vsum == particles[part1].vsum) 
				if (p->psum < particles[part1].psum) {
					part1 = i;
					closest = p->asum;
				}
		}
		i++;
	}
	printf("%d ", part1);

	int deleted = 0;

	for (int a = 0; a < 100; a++) {
		int positions[HASHSIZE][15] = { 0 };
		int max_j = 0;
		for (i = 1; i < 1001; i++) {
			struct particle* p = &particles[i];
			if (p->asum == 0)
				continue;
			simulate_20(p);
			int j = hash(*p);
			j = collision_check(i, j, positions);
			max_j = MAX(max_j, j);
			int k = -1;
			while (positions[j][++k] != 0)
				;
			positions[j][k] = i;
		}
		for (i = 0; i < max_j; i++) {
			if (positions[i][0] == positions[i][1])
				continue;
			int j = 0;
			if (positions[i][1] != positions[i][2]) {
				while (positions[i][j] != positions[i][j + 1]) {
					struct particle* p = &particles[positions[i][j]];
					p->asum = 0;
					deleted++;
					j++;
				}
			}
		}
	}

	printf("%d ", 1000 - deleted);

	return 0;
}