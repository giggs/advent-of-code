#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int five()
{
	FILE* input = fopen("05.txt", "r");
	
	int c;
	int part1 = 0;
	int part2 = 0;
	int position = 0;
	int instructions[1500] = { 0 };
	int j = 0;
	while ((c = fgetc(input)) != EOF)
	{
		if (c == '\n')
			continue;
		char inst[10];
		int i = 0;
		do {
			inst[i++] = c;
		} while ((c = fgetc(input)) != '\n');
		inst[i] = '\0';
		instructions[j++] = atoi(inst);
	}
	int instructions_2[1500] = { 0 };
	for (int k = 0; k < j; k++)
		instructions_2[k] = instructions[k];

	while (position < j)
	{
		int temp_pos = position;
		position += instructions[temp_pos];
		instructions[temp_pos] += 1;
		part1++;
	}
	position = 0;
	while (position < j)
	{
		int temp_pos = position;
		position += instructions_2[temp_pos];
		if (instructions_2[temp_pos] > 2)
			instructions_2[temp_pos] -= 1;
		else
			instructions_2[temp_pos] += 1;
		part2++;
	}

	printf("%d %d ", part1, part2);
}