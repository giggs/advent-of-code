#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define SIZE 256
#define LOOPS 16

static int circular_list[SIZE] = { 0 };
void knot_hash(int* dense_hash, int* lengths, int j);

int ten()
{
	int current_pos = 0;
	int skip_size = 0;
	int lengths[LOOPS] = { 94,84,0,79,2,27,81,1,123,93,218,23,103,255,254,243 };
	
	int* index = lengths;

	for (int i = 0; i < SIZE; ++i)
		circular_list[i] = i;

	while (index < lengths + LOOPS) {
		int length = *index;
		++index;
		int_reverse(circular_list, current_pos, length);
		current_pos = (current_pos + length + skip_size) % SIZE;
		skip_size++;
	}
	printf("%d ", circular_list[0] * circular_list[1]);

	FILE* input = fopen("10.txt", "r");
	int p2lengths[100] = { 0 };
	int j = 0;
	int c;
	while ((c = fgetc(input)) != '\n')
		p2lengths[j++] = c;
	
	int dense_hash[16] = { 0 };
	knot_hash(dense_hash, p2lengths, j);
	
	for (int n = 0; n < 16; n++) {
		printf("%02x", dense_hash[n]);
	}
	return 0;
}