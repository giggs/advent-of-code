#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "my_functions.h"
#include <time.h>



int main()
{
	double total = 0.0;
	clock_t begin;
	clock_t end;
	double t;
	
	printf("Day 01: ");
	begin = clock();
	one();
	end = clock();
	
	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;
	
	printf("Day 02: ");
	begin = clock();
	two();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 03: ");
	begin = clock();
	three();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 04: ");
	begin = clock();
	four();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;


	printf("Day 05: ");
	begin = clock();
	five();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 06: ");
	begin = clock();
	six();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 07: ");
	begin = clock();
	seven();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 08: ");
	begin = clock();
	eight();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 09: ");
	begin = clock();
	nine();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 10: ");
	begin = clock();
	ten();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t Time: %f\n", t);
	total += t;

	printf("Day 11: ");
	begin = clock();
	eleven();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 12: ");
	begin = clock();
	twelve();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 13: ");
	begin = clock();
	thirteen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 14: ");
	begin = clock();
	fourteen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 15: ");
	begin = clock();
	fifteen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 16: ");
	begin = clock();
	sixteen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t Time: %f\n", t);
	total += t;

	printf("Day 17: ");
	begin = clock();
	seventeen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 18: ");
	begin = clock();
	eighteen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 19: ");
	begin = clock();
	nineteen();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 20: ");
	begin = clock();
	twenty();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 21: ");
	begin = clock();
	twentyone();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 22: ");
	begin = clock();
	twentytwo();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 23: ");
	begin = clock();
	twentythree();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 24: ");
	begin = clock();
	twentyfour();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("Day 25: ");
	begin = clock();
	twentyfive();
	end = clock();

	t = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("\t\t\t\t\t\t Time: %f\n", t);
	total += t;

	printf("2017 All Days \t\t\t\t\t   Total time: %f\n", total);

}
