#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

static char programs[] = "abcdefghijklmnop";
static char template_programs[] = "abcdefghijklmnop";
static char answers[200][17];

int sixteen()
{
	int z = 0;
	do {
		int c;
		FILE* input = fopen("16.txt", "r");

		while ((c = fgetc(input)) != '\n') {
			if (c == 's') {
				char sn[3];
				int i = 0;
				while ((c = fgetc(input)) != ',' && c != '\n') {
					sn[i++] = c;
				}
				sn[i] = '\0';
				int n = atoi(sn);
				char temp_programs[17] = { '\0' };
				char* p = &programs[16 - (n)];
				strcat(temp_programs, p);
				programs[16 - n] = '\0';
				strcat(temp_programs, programs);
				strcopy(programs, temp_programs);
				p++;
			}
			else if (c == 'x') {
				int k = 0;
				char x[3];
				char y[3];
				while ((c = fgetc(input)) != '/') {
					x[k++] = c;
				}
				x[k] = '\0';
				k = 0;
				while ((c = fgetc(input)) != ',' && c != '\n') {
					y[k++] = c;
				}
				y[k] = '\0';
				int i = atoi(x);
				int j = atoi(y);
				swap(programs, i, j);
			}
			else if (c == 'p') {
				char a = fgetc(input);
				char b = fgetc(input);
				b = fgetc(input);
				int i, j;
				for (i = 0; programs[i] != a; i++)
					;
				for (j = 0; programs[j] != b; j++)
					;
				swap(programs, i, j);
			}
		}
		fclose(input);
		strcopy(answers[z], programs);
		if (z++ == 0)
			printf("%s ", programs);
		
	} while (strcompare(programs, template_programs));

	int j = 1000000000 / z;	

	printf("%s ",answers[1000000000-(j*z)-1]);
	return 0;
}