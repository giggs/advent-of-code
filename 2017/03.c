#include <stdio.h>

int hash_3_table[3000] = { 0 };

int hash_3(int m, int n)
{
	return 100 * (m + 20) + n + 20;
}

int sum(int m, int n)
{
	int p = 0;
	for (int i = -1; i <= 1; ++i)
		for (int j = -1; j <= 1; ++j)
			p += hash_3_table[hash_3(m + i, n + j)];
	return p;
}

int three()
{
	int input = 312051;
	int p2 = 1;
	int i, j, max, min;
	i = j = max = min = 0;
	hash_3_table[hash_3(i, j)] = p2;

	while (p2 < input)
	{
		max++;
		min--;
		i++;
		p2 = sum(i, j);
		hash_3_table[hash_3(i, j)] = p2;
		while ((p2 < input) && (j < max))
		{
			++j;
			p2 = sum(i, j);
			hash_3_table[hash_3(i, j)] = p2;
		}
		while ((p2 < input) && (i > min))
		{
			--i;
			p2 = sum(i, j);
			hash_3_table[hash_3(i, j)] = p2;
		}
		while ((p2 < input) && (j > min))
		{
			--j;
			p2 = sum(i, j);
			hash_3_table[hash_3(i, j)] = p2;
		}
		while ((p2 < input) && (i < max))
		{
			++i;
			p2 = sum(i, j);
			hash_3_table[hash_3(i, j)] = p2;
		}
	}
	int spirals = 0;
	int number = 1;
	while (number*number <= input)
		number += 2;
	int x = (number-1)/2;
	int y = -((number-1)/2);
	int length = x * 2;
	number *= number;
	if (number == input)
	{
		printf("%d %d ", (x + ( - y)), p2);
		return 0;
	}
	number -= length;
	if (number < input)
	{
		x -= input - number;
		printf("%d %d ", ((x = (x < 0) ? -x : x) + (-y)), p2);
		return 0;
	}
	x = -x;
	number -= length;
	if (number < input)
	{
		y += input - number;
		printf("%d %d ", (x + (y = (y < 0) ? -y : y)), p2);
		return 0;
	}
	y = -y;
	number -= length;
	if (number < input)
	{
		x += input - number;
		printf("%d %d ", ((x = (x < 0) ? -x : x) + y), p2);
		return 0;
	}
	x = -x;
	number -= length;
	y -= input - number;
	printf("%d %d ", ((x = (x < 0) ? -x : x) + y), p2);
	return 0;	
	// Part 2 is lookable on OEIS but I wanted to do it for the challenge
}