#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

struct point {
	int y;
	int x;
	int value;
};

struct point maze[204][204];
char answer[27];
char* p = &answer;
int x;
int y;
struct point previous;
struct point temp;
struct point next_point;

struct point next()
{
	if (maze[y][x].value == '+') {
		if (previous.x == x && maze[y][x + 1].value != ' ')
			x++;
		else if (previous.x == x)
			x--;
		else if (previous.y == y && maze[y + 1][x].value != ' ')
			y++;
		else if (previous.y == y)
			y--;
	}
	else if (maze[y][x].value >= 'A' && maze[y][x].value <= 'Z') {
		*p++ = maze[y][x].value;
		if (previous.y == y)
			x += (previous.x < x) ? 1 : -1;
		else
			y += (previous.y < y) ? 1 : -1;
		}
	else{
		if (previous.y == y)
			x += (previous.x < x) ? 1 : -1;
		else
			y += (previous.y < y) ? 1 : -1;
	}
	return maze[y][x];
}

int nineteen()
{
	FILE* input = fopen("19.txt", "r");

	int line = 0;
	int i = 0;
	int c;
	int part2 = 1;

	while ((c = fgetc(input)) != EOF) {
		if (c == '\n') {
			++line;
			i = 0;
		}
		else {
			struct point* p = &maze[line][i];
			p->y = line;
			p->x = i++;
			p->value = c;
		}
	}
	while (maze[y][++x].value != '|')
		;
	previous = maze[y][x];
	temp.x = x;
	temp.y = ++y;
	temp.value = (maze[y][x].value);

	do {
		next_point = next();
		previous = temp;
		temp = next_point;
		++part2;
	}	while (next_point.value != ' ');
	
	printf("%s %d ", answer, part2);
}