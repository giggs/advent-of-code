#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define MAXLINE 10

int thirteen()
{
	FILE* input = fopen("13.txt", "r");
	char line[MAXLINE];
	int levels[100] = { 0 };
	int severity = 0;
	int time = 0;

	while (getline(line, MAXLINE, input) > 0) {
		char split[2][TABLE];
		strsplit(line, split[0], ": ");
		levels[atoi(split[0])] = atoi(split[1]);
	}
	while (time < 100) {
		int period = levels[time];
		if (period != 0) {
			if (time % ((period-1) * 2) == 0)
				severity += (time * period);
		}
		time++;
	}
	printf("%d ", severity);

	int delay = 1;

	while (1) {
		time = delay;
		severity = 0;
		while (time < delay+100) {
			int period = levels[time-delay];
			if (period != 0) {
				if (time % ((period - 1) * 2) == 0) {
					severity += (time * period);
					break;
				}
			}
			time++;
		}
		if (severity)
			delay++;
		else
			break;
	}
	printf("%d ", delay);
	return 0;
}