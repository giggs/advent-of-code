#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define hash_22SIZE 1000000
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))


typedef struct dir* Dirptr;

typedef struct dir {
    int x;
    int y;
    Dirptr left;
    Dirptr right;
}Dir;

Dir up;
Dir down;
Dir left;
Dir right;

Dirptr u = &up;
Dirptr d = &down;
Dirptr l = &left;
Dirptr r = &right;

struct nlist {
    int x;
    int y;
    unsigned int infected;
};

int part2_22 = 0;

static struct nlist hash_22tab[hash_22SIZE];

unsigned hash_22(unsigned int x, unsigned int y)
{
    //return ((53 + y) * 273 + x) % hash_22SIZE; //this one makes collisions
    return (1000 * y + x) % hash_22SIZE;
}

struct Flags {
    unsigned int part_2 : 1;
} flags;

void infect(int x, int y)
{
    struct nlist* np;
    unsigned hash_22val;
    np = &hash_22tab[hash_22(x, y)];
    while (np->infected != 0 && (np->x != x || np->y != y)) {
        np++;
        if (np == hash_22tab + hash_22SIZE)
            np = hash_22tab;
    }
    np->x = x;
    np->y = y;
    if (np->infected == -1)
        np->infected = 0;
    if (flags.part_2) {
        np->infected += 1;
        if (np->infected == 2)
            part2_22++; 
    }
    else {
        np->infected += 2;
    }  
}

int infected(int x, int y) {
    struct nlist* np = &hash_22tab[(hash_22(x, y))];
    while (np->infected != 0) {
        if ( (np->x == x) && (np->y == y)) {
            return np->infected;
        }
        np++;
        if (np == hash_22tab + hash_22SIZE)
            np = hash_22tab;
    }
    return 0;
}

void clean(int x, int y) 
{
    struct nlist* np = &hash_22tab[hash_22(x, y)];
    while (np->infected && ((np->x != x) || (np->y != y))) {
        np++;
        if (np == hash_22tab + hash_22SIZE)
            np = hash_22tab;
    } 
    np->infected = -1;
}

struct state {
    Dir dir;
    union position {
        int coord[2];
        struct {
            int x, y;
        };
    }pos;
}virus;

int twentytwo()
{
    up.x = 0;       up.y = -1;      up.left = l;        up.right = r;
    down.x = 0;     down.y = 1;     down.left = r;      down.right = l;
    left.x = -1;    left.y = 0;     left.left = d;      left.right = u;
    right.x = 1;    right.y = 0;    right.left = u;     right.right = d;
    virus.dir = up;
    
    int line = 1;
    int i = 1;
    int max_i = 0;
    int c;
    int part1 = 0;

    FILE* input = fopen("22.txt", "r");
    while ((c = fgetc(input)) != EOF) {
        if (c == '\n') {
            line++;
            max_i = MAX(max_i, i);
            i = 0;

        }
        else if (c == '#') {
            infect(i, line);
        }
        i++;
    }
    fclose(input);

    virus.pos.x = max_i / 2;
    virus.pos.y = max_i / 2;

    for (int i = 0; i < 10000; i++) {
        if (infected(virus.pos.x, virus.pos.y) > 0) {
            clean(virus.pos.x, virus.pos.y);
            virus.dir = *virus.dir.right;
        }
        else {
            infect(virus.pos.x, virus.pos.y);
            virus.dir = *virus.dir.left;
            part1++;
        }
        virus.pos.x += virus.dir.x;
        virus.pos.y += virus.dir.y;
    }
    printf("%d ", part1);

    struct nlist empty;
    empty.x = 0;
    empty.y = 0;
    empty.infected = 0;

    for (int i = 0; i < hash_22SIZE; i++)
        hash_22tab[i] = empty;

    line = 1;
    i = 1;
    max_i = 0;

    input = fopen("22.txt", "r");
    while ((c = fgetc(input)) != EOF) {
        if (c == '\n') {
            line++;
            max_i = MAX(max_i, i);
            i = 0;
            
        }
        else if (c == '#') {
            infect(i, line);
        }
        i++;
    }
    fclose(input);

    virus.pos.x = max_i / 2;
    virus.pos.y = max_i / 2;

    flags.part_2 = 1;

    for (int i = 0; i < 10000000; i++) {
        int choose = infected(virus.pos.x, virus.pos.y);

        switch (choose) {
        case 3:
            clean(virus.pos.x, virus.pos.y);
            virus.dir = *virus.dir.right;
            virus.dir = *virus.dir.right;
            break;
        case 2:
            virus.dir = *virus.dir.right;
            infect(virus.pos.x, virus.pos.y);
            break;
        case -1:
        case 0:
            virus.dir = *virus.dir.left;
            infect(virus.pos.x, virus.pos.y);
            break;
        default:
            infect(virus.pos.x, virus.pos.y);
        }
        virus.pos.x += virus.dir.x;
        virus.pos.y += virus.dir.y;

    }
    printf("%d ", part2_22);

    return 0;
}
