#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define LIMIT 20

void reset(char s[], char d[])
{
	for (int i = 0; i < LIMIT; ++i)
	{
		d[i] = s[i];
	}
}

int two()
{
	FILE* input = fopen("02.txt", "r");
	int checksum1 = 0;
	int checksum2 = 0;
	int template[LIMIT] = {0};
	int min = 9999;
	int max = 0;
	int num = 0;
	int c;
	int i = 0;

	int numbers[LIMIT] = {0};

	while ((c = fgetc(input)) != EOF)
	{
		if ((c == ' ') || (c == '\t'))
		{
			min = (num <= min) ? num : min;
			max = (num >= max) ? num : max;
			numbers[i] = num;
			++i;
			num = 0;

		}
		else if (c == '\n')
		{
			if (num != 0)
			{
				min = (num <= min) ? num : min;
				max = (num >= max) ? num : max;
				checksum1 += (max - min);
				numbers[i] = num;
				min = 9999;
				for (int j = 0; j <= i; ++j)
					for (int k = 0; k <= i; ++k)
						if ( j != k && (numbers[k] > numbers[j]) && (numbers[k] % numbers[j] == 0))
							checksum2 += numbers[k] / numbers[j];

				reset(template, numbers);
				max = num = i = 0;
			}
		}
		else
		{
			num *= 10;
			num += (c-'0');
		}

	}
	printf("%d %d ", checksum1, checksum2);
	return 0;
}