#include <stdio.h>
#define STEPS 344

int list[2018] = {0};

int seventeen()
{
	int t = 0;
	int next, temp;
	while (t < 2017) {
		next = list[t];
		for (int i = 0; i < STEPS; i++) {
			temp = next;
			next = list[next];
		}
		list[temp] = ++t;
		list[t] = next;
	}
	int part2 = 0;
	t = 0;
	for (int i = 1; i < 5000001; ++i) {
		t = (t+STEPS) % i + 1;
		if (t == 1) // just keep track of what is after 0
			part2 = i;
	}
	printf("%d %d ", next, part2);
}