#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

short tape[26000000];
int pos = 13000000;
int steps = 12861455;


int twentyfive()
{
	int answer = 0;
	char state = 'A';
	while (steps > 0) {
		switch (state) {
		case 'A':
			state = 'B';
			if (tape[pos] == 0) {
				tape[pos] = 1;
				answer++;
				pos++;
			}
			else {
				tape[pos] = 0;
				pos--;
				answer--;
			}
				break;
		case 'B':
			if (tape[pos] == 0) {
				tape[pos] = 1;
				pos--;
				answer++;
				state = 'C';
			}
			else {
				tape[pos] = 0;
				pos++;
				answer--;
				state = 'E';
			}
				break;
		case 'C':
			if (tape[pos] == 0) {
				tape[pos] = 1;
				pos++;
				answer++;
				state = 'E';
			}
			else {
				tape[pos] = 0;
				pos--;
				answer--;
				state = 'D';
			}
				break;
		case 'D':
			state = 'A';
			if (tape[pos] == 0) {
				tape[pos] = 1;
				answer++;
				pos--;
			}
			else {
				pos--;
			}
				break;
		case 'E':
			if (tape[pos] == 0) {
				pos++;
				state = 'A';
			}
			else {
				tape[pos] = 0;
				pos++;
				answer--;
				state = 'F';
			}
				break;
			
		case 'F':
			if (tape[pos] == 0) {
				tape[pos] = 1;
				pos++;
				answer++;
				state = 'E';
			}
			else {
				pos++;
				state = 'A';
			}
			break;
		}
		steps--;
	}
	printf("%d ", answer);
}