#if !defined (MYFUNCTIONS)
#define MYFUNCTIONS
#ifndef TABLE
#define TABLE 100
#endif
#ifndef SIZE
#define SIZE 256
#endif
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))

int is_digit(char c)
{
	return (c >= '0' && c <= '9');
}

int is_letter(char c)
{
	return (c >= 'a' && c <= 'z');
}

int getline(char s[], int lim, FILE* file)
{
	int c, i;
	i = 0;
	while (--lim > 0 && (c = fgetc(file)) != EOF && c != '\n')
		s[i++] = c;
	/*if (c == '\n')
		s[i++] = c;*/ // I most likely don't need the newline symbol
	s[i] = '\0';
	return i;
}

void strcopy(char *s, char *t) // copy t to s
{
	while (*s++ = *t++);
}

int atoi(char* s)
{
	int n, sign;
	n = 0;
	sign = (*s == '-') ? -1 : 1;
	if (sign == -1)
		++s;
	for (; *s >= '0' && *s <= '9'; s++)
		n = 10 * n + (*s - '0');

	return n * sign;
}


//int strlen(char s[])
//{
//	int i = 0;
//	while (s[i++] != '\0')
//		;
//	return i - 1;
//}

void bubblesort(char s[]) // for strings
{
	char temp;
	int n = strlen(s);

	for (int i = 0; i < n - 1; ++i)
		for (int j = i + 1; j < n; ++j)
			if (s[i] > s[j])
			{
				temp = s[i];
				s[i] = s[j];
				s[j] = temp;
			}
}

int strsplit(char s[], char t[][TABLE], char split[]) 
{
	int i = 0;
	int j = 0;
	int k = 0;
	int len = strlen(split);

	while ((s[i] != '\0'))
		if (s[i] != split[0])
			t[j][k++] = s[i++];
		else {
			int l = 0;
			while ((s[i + l] == split[l]))
				l++;
			if (l == len) {
				t[j++][k] = '\0';
				k = 0;
				i+= (l);
			}
			else {
				for (l; l > 0; --l) 
					t[j][k++] = s[i++];
			}
		}
	t[j][k] = '\0';
	return j;
}

void swap(char* s, int i, int j)
{
	char temp = s[i];
	s[i] = s[j];
	s[j] = temp;
}

int strcompare(char *s, char *t)
{
	for (; *s == *t; s++, t++)
		if (*s == '\0')
			return 0;
	return *s - *t;
}

int find_index(char s[], char t[][TABLE]) //find index of a string in an array of strings
{
	int i = -1;
	while (strcompare(s, t[++i])) //continue while not equal
		;
	return i;
}

int power(int base, int n)
{
	int p;
	for (p = 1; n > 0; --n)
		p = p * base;
	return p;
}

int int_is_in(int find, int list[], int len) //returns 1 if find is in list
{
	int i = -1;
	while (i < len) {
		if (list[++i] == find)
			return 1;
	}
	return 0;
}

//void strcat(char* s, char* t) // s buffer must be sufficient
//{
//	while (*s)
//		++s;
//	while (*s++ = *t++)
//		;
//}

void reverse_string(char s[])
{
	int c, i, j;
	for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
	{
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void itoa(int n, char s[])
{
	int i, sign;
	if ((sign = n) < 0)
		n = -n;
	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse_string(s);
}

void hex_itoa(int n, char s[])
{
	int i, sign;
	if ((sign = n) < 0)
		n = -n;
	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse_string(s);
}

void int_reverse(int* s, int pos, int len)
{
	for (int j = pos + len - 1, t = 0; t < len / 2; pos++, j--, t++) {
		pos = pos % SIZE;
		j = (j < 0) ? j + SIZE : j % SIZE;
		int c = s[pos];
		s[pos] = s[j];
		s[j] = c;
	}
}


void knot_hash(int* dense_hash, int* lengths, int j)
{
	int current_pos = 0, skip_size = 0;
	int circular_list[SIZE] = { 0 };

	for (int i = 0; i < SIZE; ++i)
		circular_list[i] = i;

	int sequence[5] = { 17, 31, 73, 47, 23 };
	for (int k = 0; k < 5; k++)
		lengths[j++] = sequence[k];

	for (int k = 0; k < 64; k++) {
		int* index = lengths;
		while (index < lengths + j) {
			int length = *index;
			++index;
			int_reverse(circular_list, current_pos, length);
			current_pos = (current_pos + length + skip_size) % SIZE;
			skip_size++;
		}
	}
	int hash;
	for (int m = 0; m < SIZE; m++) {
		if (m % 16 == 0)
			hash = circular_list[m];
		else {
			hash ^= circular_list[m];
			if (m % 16 == 15)
				dense_hash[m / 16] = hash;
		}
	}
	return dense_hash;
}

int int_sum(int s[], int len)
{
	int sum = 0;
	for (int i = 0; i < len; i++)
		sum += s[i];
	return sum;
}

void int_copy(int* s, int* t, int len)
{
	for (int i = 0; i < len; i++)
		*s++ = *t++;
}


#endif