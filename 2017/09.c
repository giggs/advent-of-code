#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int nine()
{
	FILE* input = fopen("09.txt", "r");

	int part1 = 0;
	int part2 = 0;
	int nest = 0;
	int c;

	while ((c = fgetc(input)) != '\n') {
		if (c == '{') {
			nest++;
			part1 += nest;
		}
		else if (c == '}') {
			nest--;
		}
		else if (c == '<')/* garbage begins */ {
			while ((c = fgetc(input)) != '>') {
				part2++;
				if (c == '!') {
					part2--;
					c = fgetc(input);
				}
			}
		}
	}
	printf("%d %d ", part1, part2);
}