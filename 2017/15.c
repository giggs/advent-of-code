#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <immintrin.h>

int fifteen()
{
	unsigned long long a = 618;
	unsigned long long b = 814;
	unsigned int a_factor = 16807;
	unsigned int b_factor = 48271;
	unsigned long long modulo = 2147483647;
	unsigned int judge_1 = 0;
	unsigned int judge_2 = 0;
	__m128i factors = _mm_set_epi64x(b_factor, a_factor);
	__m128i o_factors = _mm_set_epi64x(b_factor, a_factor);
	__m128i generators;
	unsigned long long* l = (unsigned long long*) & generators;
	generators = _mm_set_epi64x(b, a);
	__m128i mods = _mm_set_epi64x(modulo, modulo);
	__m128i shiftr;
	__m128i cmp;
	__m128i sub;
	__m128i mulmask;
	__m128i zeros = _mm_set_epi64x(0, 0);
	__m128i masks = _mm_set_epi64x(7, 3);
	__m128i mul;
	__m128i a_fac = _mm_set_epi64x(1, a_factor);
	__m128i b_fac = _mm_set_epi64x(b_factor, 1);

	for (int i = 0; i < 40000000; i++) {

		generators = _mm_mul_epu32(generators, factors);
		shiftr = _mm_srli_epi64(generators, 31);
		generators = _mm_and_si128(generators, mods);
		generators = _mm_add_epi64(generators, shiftr);
		cmp = _mm_cmpgt_epi64(generators, mods);
		sub = _mm_sub_epi64(generators, mods);
		generators = _mm_blendv_epi8(generators, sub, cmp);

		judge_1 += ((l[0] & 0xFFFF) == (l[1] & 0xFFFF));
	}
	a = 618;
	b = 814;
	
	//generators = _mm_set_epi64x(b, a);
	
	for (int i = 0; i < 5000000; i++) {
		/* SIMD version, slower though.
		generators = _mm_mul_epu32(generators, factors);
		shiftr = _mm_srli_epi64(generators, 31);
		generators = _mm_and_si128(generators, mods);
		generators = _mm_add_epi64(generators, shiftr);
		cmp = _mm_cmpgt_epi64(generators, mods);
		sub = _mm_sub_epi64(generators, mods);
		generators = _mm_blendv_epi8(generators, sub, cmp);

		while ((l[0]&3)&&(l[1]&7)) {
			// below is a mul mask to have a single while loop. It's slower than 3 while loops.
			mulmask = _mm_and_si128(generators, masks);
			mulmask = _mm_cmpeq_epi64(mulmask, zeros);
			mul = _mm_mul_epu32(generators, factors);
			generators = _mm_blendv_epi8(mul, generators, mulmask);
			generators = _mm_mul_epu32(generators, factors);
			shiftr = _mm_srli_epi64(generators, 31);
			generators = _mm_and_si128(generators, mods);
			generators = _mm_add_epi64(generators, shiftr);
			cmp = _mm_cmpgt_epi64(generators, mods);
			sub = _mm_sub_epi64(generators, mods);
			generators = _mm_blendv_epi8(generators, sub, cmp);
		} 
		while (l[0] & 3) {
			generators = _mm_mul_epu32(generators, a_fac);
			shiftr = _mm_srli_epi64(generators, 31);
			generators = _mm_and_si128(generators, mods);
			generators = _mm_add_epi64(generators, shiftr);
			cmp = _mm_cmpgt_epi64(generators, mods);
			sub = _mm_sub_epi64(generators, mods);
			generators = _mm_blendv_epi8(generators, sub, cmp);
		}

		while (l[1] & 7) {
			generators = _mm_mul_epu32(generators, b_fac);
			shiftr = _mm_srli_epi64(generators, 31);
			generators = _mm_and_si128(generators, mods);
			generators = _mm_add_epi64(generators, shiftr);
			cmp = _mm_cmpgt_epi64(generators, mods);
			sub = _mm_sub_epi64(generators, mods);
			generators = _mm_blendv_epi8(generators, sub, cmp);
		}
		
		judge_2 += ((l[0] & 0xFFFF) == (l[1] & 0xFFFF));
		*/
		do {
					a *= a_factor;
					a = (a & modulo) + (a >> 31);
					a = (a > modulo) ? (a - modulo) : a;
					b *= b_factor;
					b = (b & modulo) + (b >> 31);
					b = (b > modulo) ? (b - modulo) : b;

		} while ((a & 3) && (b & 7));

		while (a & 3) {
			a *= a_factor;
			a = (a & modulo) + (a >> 31);
			a = (a > modulo) ? (a - modulo) : a;
		}
		while (b & 7) {
			b *= b_factor;
			b = (b & modulo) + (b >> 31);
			b = (b > modulo) ? (b - modulo) : b;
		}

		judge_2 += ((a & 0xFFFF) == (b & 0xFFFF));
	}

	printf("%d %d ", judge_1, judge_2);
}