#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define TABLE 100
#define MAXLINE 100
#define PIPES 7
#define GROUPTREE 400
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))

int twelve()
{
	int programs[2001][PIPES] = { 2000 };
	for (int a = 0; a < 2001; a++)
		for (int b = 0; b < PIPES; b++)
			programs[a][b] = 2000;
	char line[MAXLINE];
	int c;
	char split[10][TABLE];
	int n = 0;
	int i = 0;

	FILE* input = fopen("12.txt", "r");

	while (getline(line, MAXLINE, input) > 0) {
		strsplit(line, split, " <-> ");
		n = strsplit(split[1], split, ", ");
		int j = 0;
		for (j ; j <= n; j++)
			programs[i][j] = atoi(split[j]);
		i++;
	}
	
	int prog = 0;
	int groups[300] = {2000};
	for (int a = 0; a < 100; a++)
		groups[a] = 2000;
	int prog_count = 0;
	int group_count = 0;
	
	while (prog_count < 2000) {
		
		int minimum = MIN(2000, prog);
		int prog_chain[GROUPTREE] = { prog };
		for (int a = 0; a < GROUPTREE; a++)
			prog_chain[a] = prog;
		int prog_index = 1;
		
		for (int k = 0; k < prog_index; k++) {
			int index = prog_chain[k];
			for (int l = 0; (l < PIPES) && (programs[index][l] != 2000); l++) {
				int child = programs[index][l];
				int exists = 0;
				for (int m = 0; m < prog_index; m++) {
					if (prog_chain[m] == child) {
						exists = 1;
						break;
					}
				}
				if (!exists) {
					prog_chain[prog_index++] = child;
					minimum = MIN(minimum, child);
				}
			}
		}
		if (prog == 0)
			printf("%d ", prog_index);

		if (!int_is_in(minimum, groups, group_count + 1)) {
			groups[group_count++] = minimum;
			prog_count += prog_index;
		}
		prog++;

	}

	printf("%d ", group_count);
	return 0;
}