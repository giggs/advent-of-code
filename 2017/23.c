#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define MAXlines_23 100
#define MAXVAL 10000

long long registers_23[8] = { 0 };
char instructions[MAXlines_23][TABLE];

int getline(char s[], int lim, FILE* file);
int strsplit(char s[], char t[][TABLE], char split[]);

int lines_23 = -1;

int twentythree()
{
	FILE* input = fopen("23.txt", "r");
	char s[TABLE];
	int t = 0;
	int part1 = 0;

	while (t = getline(s, MAXlines_23, input) > 0)
		strcopy(instructions[++lines_23], s);

	t = -1;
	char workstring[3][TABLE];
	char inst[4];
	int init;

	while (++t >= 0 && t <= lines_23) {
		char d;
		int e = strsplit(instructions[t], workstring, " ");
		strcopy(inst, workstring[0]);
		if (t == 0)
			init = atoi(workstring[2]);
		char c = workstring[1][0];
		if (e > 1)
			d = workstring[2][0];	
		if (!strcompare(inst, "set")) {
			if (is_letter(d))
				registers_23[c - 'a'] = registers_23[d - 'a'];
			else
				registers_23[c - 'a'] = atoi(workstring[2]);
		}
		else if (!strcompare(inst, "sub")) {
			if (is_letter(d))
				registers_23[c - 'a'] -= registers_23[d - 'a'];
			else
				registers_23[c - 'a'] -= atoi(workstring[2]);
		}
		else if (!strcompare(inst, "mul")) {
			part1++;
			if (is_letter(d))
				registers_23[c - 'a'] *= registers_23[d - 'a'];
			else
				registers_23[c - 'a'] *= atoi(workstring[2]);
		}
		else { // jnz
			int trigger = 0;
			if (is_letter(c))
				trigger = registers_23[c - 'a'];
			else
				trigger = atoi(workstring[1]);
			if (trigger != 0)
				t += atoi(workstring[2]) - 1;
		}
	}
	int a = 1;
	int b, c, d, e, f, g, h;
	b = c = d = e = f = g = h = 0;
	b = init;
	c = b;
	b *= 100;
	b += 100000;
	c = b + 17000;
 	do {
		f = 1;
		d = 2;
		for (int i = 2; i * i < b; i++)
			if (b % i == 0) {
				h++;
				break;
			}
		b += 17;
	} while (b-17 != c);

	printf("%d %d ", part1, h); return 0;
}