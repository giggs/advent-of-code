#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))
#define BRIDGES 114

static int bridges[BRIDGES];
char split[2][TABLE];
static int line = 0;
int part1 = 0;
int part2_24 = 0;
int bridge_length = 0;

void find_match(int*s , int* t, int depth)
{
	int found = 0;
	for (int i = 0; i < line; i++) {
		if (t[i] == s[1]) {
			found = 1;
			int bridges_copy[BRIDGES];
			int_copy(bridges_copy, t, sizeof(bridges_copy) / sizeof(int));
			int worklist[2];
			if (i % 2 == 0) {
				worklist[1] = t[i + 1];
				worklist[0] = s[0] + bridges_copy[i] + bridges_copy[i + 1];
				bridges_copy[i + 1] = -1;
			}
			else {
				worklist[1] = t[i-1];
				worklist[0] = s[0] + bridges_copy[i] + bridges_copy[i - 1];
				bridges_copy[i - 1] = -1;
			}
			bridges_copy[i] = -1;
			find_match(worklist, bridges_copy, depth+2);
		}
	}
	if (!found) {
		if (depth >= bridge_length) {
			bridge_length = depth;
			part2_24 = MAX(part2_24, s[0]);
		}
		part1 = MAX(part1, s[0]);
	}
}

int twentyfour()
{
	FILE* input = fopen("24.txt", "r");
	
	char bridge[6] = { '\0' };
	char split[2][TABLE];
	int start;

	while (getline(bridge, 1000, input) > 0) {
		strsplit(bridge, split, "/");
		bridges[line++] = atoi(split[0]);
		if (!bridges[line - 1])
			start = line - 1;
		bridges[line++] = atoi(split[1]);
		if (!bridges[line - 1])
			start = line - 1;

	}
		
	int bridges_copy[BRIDGES];
	int_copy(bridges_copy, bridges, sizeof(bridges_copy)/sizeof(int));
	int worklist[2];
	worklist[0] = bridges_copy[start];
		
	if (start % 2 == 0) {
		worklist[1] = bridges[start + 1];
		worklist[0] += bridges_copy[start + 1];
		bridges_copy[start+1] = -1;		
	}
	else {
		worklist[1] = bridges[start-1];
		worklist[0] += bridges[start - 1];
		bridges_copy[start - 1] = -1;
	}
	bridges_copy[start] = -1;
	find_match(worklist, bridges_copy, 2);

	printf("%d %d ", part1, part2_24);
	return 0;
}