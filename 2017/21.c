#define _CRT_SECURE_NO_WARNINGS
#define TABLE 100
#include <stdio.h>
#define SIZE 2200

#define MAX(A, B) ((A) > (B) ? (A) : (B)) 
#define MIN(A, B) ((A) < (B) ? (A) : (B))
#define ABS(A) ((A) > 0 ? (A) : -(A))

char enhance_even[20][10];
char enhance_odd[600][17];
int oddcounts[600];
int oddcounts_copy[600];
int matches[600][9];
char even[SIZE][SIZE];
char odd[SIZE][SIZE];
char fast_even_2[SIZE][SIZE];
char fast_even_1[SIZE][SIZE];

int getline_21(char s[], int lim, FILE* file)
{
	int c, i;
	i = 0;
	while (--lim > 0 && (c = fgetc(file)) != EOF && c != '\n') {
		if (c != '/')
			s[i++] = c;
	}
	/*if (c == '\n')
		s[i++] = c;*/ // I most likely don't need the newline symbol
	s[i] = '\0';
	return i;
}

void r_even(char *s) // (0,0)(0,1) (1,0)(1,1)
{
	char temp[5];
	temp[0] = s[2];
	temp[1] = s[0];
	temp[3] = s[1];
	temp[2] = s[3];
	temp[4] = '\0';
	strcopy(s, temp);
}

void r_odd(char *s)
{
	char temp[10];
	temp[0] = s[6];
	temp[1] = s[3];
	temp[2] = s[0];
	temp[3] = s[7];
	temp[4] = s[4];
	temp[5] = s[1];
	temp[6] = s[8];
	temp[7] = s[5];
	temp[8] = s[2];
	temp[9] = '\0';
	strcopy(s, temp);
}

void f_even(char* s) // (0,0)(0,1) (1,0)(1,1)
{
	swap(s, 0, 1);
	swap(s, 2, 3);
}

void f_odd(char* s) // (0,0)(0,1)(0,2) (1,0)(1,1)(1,2) (2,0)(2,1)(2,2)
{
	char temp[10];
	swap(s, 0, 6);
	swap(s, 1, 7);
	swap(s, 2, 8);
}

int even_to_i(char* s)
{
	int n = 0;
	for (int i = 0; i < 4; i++) {
		n <<= 1;
		if (s[i] == '#')
			n++;
	}
	return n;
}

int odd_to_i(char* s)
{
	int n = 0;
	for (int i = 0; i < 9; i++) {
		n <<= 1;
		if (s[i] == '#')
			n++;
	}
	return n;
}

void expand_even(char s[SIZE][SIZE], char t[SIZE][SIZE], int i, int j)
{
	char temp[5];
	temp[0] = s[j*2][i*2];
	temp[1] = s[j*2][i*2 + 1];
	temp[2] = s[j*2 + 1][i*2];
	temp[3] = s[j*2 + 1][i*2 + 1];
	temp[4] = '\0';
	char exp[10];
	strcopy(exp, enhance_even[even_to_i(temp)]);
	char one[4] = { exp[0], exp[1], exp[2] };
	char two[4] = { exp[3], exp[4], exp[5] };
	char three[4] = { exp[6], exp[7], exp[8] };
	if (i == 0) {
		t[j * 3][0] = '\0';
		t[j * 3 + 1][0] = '\0';
		t[j * 3 + 2][0] = '\0';
	}
	strcat(t[j * 3], one);
	strcat(t[j * 3 + 1], two);
	strcat(t[j * 3 + 2], three);
}

int second_even(char s[SIZE][SIZE], char t[SIZE][SIZE], int i, int j)
{
	char temp[5];
	temp[0] = s[j * 2][i * 2];
	temp[1] = s[j * 2][i * 2 + 1];
	temp[2] = s[j * 2 + 1][i * 2];
	temp[3] = s[j * 2 + 1][i * 2 + 1];
	temp[4] = '\0';
	char exp[10];
	strcopy(exp, enhance_even[even_to_i(temp)]);
	return odd_to_i(exp);
}

void expand_odd(char s[SIZE][SIZE], char t[SIZE][SIZE], int i, int j)
{
	char temp[10];
	temp[0] = s[j * 3][i * 3];
	temp[1] = s[j * 3][i * 3+1];
	temp[2] = s[j * 3][i * 3+2];
	temp[3] = s[j * 3+1][i * 3];
	temp[4] = s[j * 3+1][i * 3+1];
	temp[5] = s[j * 3+1][i * 3+2];
	temp[6] = s[j * 3+2][i * 3];
	temp[7] = s[j * 3+2][i * 3+1];
	temp[8] = s[j * 3+2][i * 3+2];
	temp[9] = '\0';
	char exp[17];
	strcopy(exp, enhance_odd[odd_to_i(temp)]);
	char one[5] = { exp[0], exp[1], exp[2], exp[3] };
	char two[5] = { exp[4], exp[5], exp[6], exp[7] };
	char three[5] = { exp[8], exp[9], exp[10], exp[11] };
	char four[5] = { exp[12], exp[13], exp[14], exp[15] };
	if (i == 0) {
		t[j * 4][0] = '\0';
		t[j * 4+1][0] = '\0';
		t[j * 4+2][0] = '\0';
		t[j * 4+3][0] = '\0';
	}
	strcat(t[j * 4], one);
	strcat(t[j * 4+1], two);
	strcat(t[j * 4+2], three);
	strcat(t[j * 4+3], four);
}

void fast_expand(int i, char t[SIZE][SIZE], char u[SIZE][SIZE], int* c, int j)
{
	char exp[17];
	strcopy(exp, enhance_odd[i]);
	char one[5] = { exp[0], exp[1], exp[2], exp[3] };
	char two[5] = { exp[4], exp[5], exp[6], exp[7] };
	char three[5] = { exp[8], exp[9], exp[10], exp[11] };
	char four[5] = { exp[12], exp[13], exp[14], exp[15] };
	t[0][0] = '\0';
	t[1][0] = '\0';
	t[2][0] = '\0';
	t[3][0] = '\0';
	strcat(t[0], one);
	strcat(t[1], two);
	strcat(t[2], three);
	strcat(t[3], four);
	int a;
	int* p = &matches[i];
	for (int y = 0; y < 2; y++)
		for (int x = 0; x < 2; x++)
			expand_even(t, u, x, y);
	for (int y = 0; y < 3; y++)
		for (int x = 0; x < 3; x++) {
			a = second_even(u, t, x, y);
			c[a] += j;
			*p++ = a;
		}
	j += 0;
}

int bitcount(int i)
{
	int c = 0;
	for (c; i; i >>= 1)
		c += i & 1;
	return c;
}


int twentyone()
{
	FILE* input = fopen("21.txt", "r");
	char line[40];
	char split[2][TABLE];

	while (getline_21(line, 120, input) > 0) {
		strsplit(line, split, " => ");
		if (line[17] == '\0') {  // even
			for (int i = 0; i < 4; i++) {
				strcopy(enhance_even[even_to_i(split[0])], split[1]);
				r_even(split[0]);
			}
			f_even(split[0]);
			for (int i = 0; i < 4; i++) {
				strcopy(enhance_even[even_to_i(split[0])], split[1]);
				r_even(split[0]);
			}
		}
		else {
			for (int i = 0; i < 4; i++) {
				strcopy(enhance_odd[odd_to_i(split[0])], split[1]);
				r_odd(split[0]);
			}
			f_odd(split[0]);
			for (int i = 0; i < 4; i++) {
				strcopy(enhance_odd[odd_to_i(split[0])], split[1]);
				r_odd(split[0]);
			}
		}
	}

	strcopy(odd[0], ".#.");
	strcopy(odd[1], "..#");
	strcopy(odd[2], "###");
	int i = 0;
	int part1 = 0;
	int part2 = 0;
	expand_odd(odd, even, 0, 0);
	for (int j = 0; j < 600; j++)
		matches[j][0] = -1;

	for (i = 2; i < 6; i++) {
		int j = MAX(strlen(odd[0]), strlen(even[0]));
		if (j % 2 == 0) {
			for (int y = 0; y < j / 2; ++y)
				for (int x = 0; x < j / 2; ++x) {
					if (i % 2 == 0)
						expand_even(even, odd, x, y);
					else
						expand_even(odd, even, x, y);
				}
		}
		else {
			for (int y = 0; y < j / 3; ++y)
				for (int x = 0; x < j / 3; ++x) {
					if (i % 2 == 0)
						expand_odd(even, odd, x, y);
					else
						expand_odd(odd, even, x, y);
				}
		}
		
	}
	
	for (int x = 0; x < 50; x++) {
		for (int y = 0; y < 50; y++)
			if (even[x][y] == '#')
				part1++;
	}
	
	int root = odd_to_i(".#...####");
	fast_expand(root, fast_even_1, fast_even_2, oddcounts, 1);

	for (i = 0; i < 5; i++) {
		int oddcounts_copy[600];
		for (int m = 0; m < 600; m++)
			oddcounts_copy[m] = 0;
		for (int j = 0; j < 600; j++) {
			if (oddcounts[j]) {
				if (matches[j][0] > -1) {
					for (int k = 0; k < 9; k++) {
						oddcounts_copy[matches[j][k]]+= oddcounts[j];
					}
				}
				else {
					fast_expand(j, fast_even_1, fast_even_2, oddcounts_copy, oddcounts[j]);
					
				}
			}
		}
		int_copy(oddcounts, oddcounts_copy, sizeof(oddcounts) / sizeof(int));
	}
	for (int j = 0; j < 600; j++)
		if (oddcounts[j]) {
			part2 +=  bitcount(j)*oddcounts[j];
		}

	printf("%d %d ", part1,part2);
	return 0;
}