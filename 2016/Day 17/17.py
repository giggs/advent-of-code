import hashlib 
frontier, paths, input_length = [(1,1,'awrkjxxr')], [], len('awrkjxxr')

while frontier:
    x, y, path = frontier.pop(0)
    if x == y == 4:
        paths.append(path[input_length:]) ; continue
    doors = hashlib.md5(path.encode()).hexdigest()[:4]
    moves = zip(doors,[(0,-1),(0,1),(-1,0),(1,0)],'UDLR')
    for door, (dx,dy), d in moves:
        if door in 'bcdef' and 1 <= x+dx <= 4 and 1 <= y+dy <= 4:
            frontier.append((x+dx,y+dy,path+d))
    
print(min(paths, key=len), len(max(paths, key=len)))