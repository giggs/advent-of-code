from collections import UserDict
import heapq, timeit

class MazeDict(UserDict):
    def __missing__(self,key):
        x,y = key[0],key[1]
        n = x*x + 3*x + 2*x*y + y + y*y + 1350
        n = bin(n)
        n = n.count('1')
        if n%2 == 1:
            self[key] = False
        else:
            self[key] = True
        return self[key] 

maze = MazeDict()

def a_star_search(maze,part):
    start = (1,1)
    cost_so_far = {}
    cost_so_far[start] = 0
    frontier = []
    heapq.heappush(frontier, (0, start))

    while frontier:
        current = heapq.heappop(frontier)[-1]
        if current == (31,39):
            return cost_so_far[current]
        
        neighbours = []
        for dx in [-1,0,1]:
            for dy in [-1,0,1]:
                nx = current[0] + dx
                ny = current[1] + dy
                if nx >= 0 and ny >= 0 and abs(dx)+abs(dy) != 2 and maze[(nx,ny)]:
                    neighbours.append((nx,ny))
        
        for next in neighbours:
            new_cost = cost_so_far[current] + 1
            if not part:
                if new_cost > 50: continue
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + (abs(next[0]-31) + abs(next[1]-39)) # manhattan distance heuristic
                heapq.heappush(frontier, (priority, next))
    
    return len(cost_so_far.keys())

print(a_star_search(maze,True))
print(a_star_search(maze,False))