instructions = open('input.txt').read().split(', ')

x,y,i = 0,0,0
direction = [1,1,-1,-1]
locations = set()
found = False

for element in instructions:
    if element[0] == 'R':
        i += 1
    else:
        i -= 1
    i = i%4
    steps = int(element[1:])
    if i == 0 or i == 2:
        if not found:
            for _ in range(steps):
                y += direction[i]
                if (x,y) in locations:
                    part2 = (abs(x) + abs(y)) 
                    found = True
                else:
                    locations.add((x,y))
        else:
            y += steps*direction[i]
    else:
        if not found:
            for _ in range(steps):
                x += direction[i]
                if (x,y) in locations:
                    part2 = (abs(x) + abs(y)) 
                    found = True
                else:
                    locations.add((x,y))
        else:
            x += steps*direction[i]

print(abs(x) + abs(y), part2)