instructions = open('input.txt').read().strip().splitlines()
p1 = list('abcdefgh')
p2 = list('fbgdceah')
rotations = {0:1, 1:2, 2:3, 3:4, 4:6, 5:7, 6:0, 7:1}
rev_rotations = {1:1, 2:6, 3:2, 4:7, 5:3, 6:0, 7:4, 0:1}

def swap(argument,i,j,pw,reverse = False):
    if argument == 'letter':
        i = pw.index(i)
        j = pw.index(j)
    i, j = int(i), int(j)
    pw[i], pw[j] = pw[j], pw[i]
    return pw

def rotate(argument,i,j,pw,reverse = False):
    if argument == 'based':
        if reverse:
            i = rev_rotations[pw.index(j)]
        else:
            i = rotations[pw.index(j)]    
    i = int(i)
    if (argument == 'left' and not reverse) or (argument != 'left' and reverse):
        for _ in range(i%8):
            pw.append(pw.pop(0))
    else:
        for _ in range(i%8):
            pw.insert(0,pw.pop(-1))
    return pw

def reverse(argument,i,j,pw,reverse = False):
    length = len(pw)
    i, j = int(i), int(j)
    pre, rev, suf = pw[:i], pw[i:j+1], pw[min(j+1,len(pw)-1):]
    pw = pre + rev[::-1] + suf
    return pw[:length]

def move(argument,i,j,pw,reverse = False):
    if reverse:
        i, j = j,i
    i, j = int(i), int(j)
    pw.insert(j,pw.pop(i))
    return pw

actions = {'swap': swap, 'rotate': rotate, 'reverse': reverse, 'move': move}

def solve(operations, pw, bool):
    if bool:
        operations = operations[::-1]
    for line in operations:
        line = line.split(' ')
        action, argument, i, j = line[0], line[1], line[2], line[-1]
        pw = actions[action](argument,i,j,pw,bool)
    return ''.join(pw)

print(solve(instructions,p1,False))
print(solve(instructions,p2,True))