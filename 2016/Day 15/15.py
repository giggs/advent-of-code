import re

input = open('input.txt').read().splitlines()
discs = {}
for i, disc in enumerate(input, start=1):
    _, position, _, start = re.findall('\d+',disc)
    discs[int(position)] = int(start)+i

x = 0
while any((x+start) % position for (position,start) in discs.items()) != 0:
    x += 1
print(x)

discs[11] = len(input) + 1

x = 0
while any((x+start) % position for (position,start) in discs.items()) != 0:
    x += 1
print(x)