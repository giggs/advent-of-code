IPs = open('input.txt').read().splitlines()
TLS_IPs = []
SSL_IPs = []

for IP in IPs:
    TLS = False
    SSL = False
    ABA = []
    IP = IP.replace('[',']').split(']')
    for i in range(0,len(IP),2):
        string = list(IP[i])        
        for j in range(0,len(string)-3):
            first = ''.join([string[j],string[j+1]])
            second = ''.join([string[j+3],string[j+2]])
            if first == second and string[j] != string[j+1]:
                TLS = True
            if string[j] == string[j+2] and string[j+1] != string[j]:
                ABA.append(string[j:j+3])
        if string[-3] == string[-1] and string[-1] != string[-2]:
            ABA.append(string[-3:])
    for i in range(1,len(IP),2):
        string = list(IP[i])        
        for j in range(0,len(string)-3):
            first = ''.join([string[j],string[j+1]])
            second = ''.join([string[j+3],string[j+2]])
            if first == second and string[j] != string[j+1]:
                TLS = False
        for aba in ABA:
            bab = ''.join([aba[1], aba[0], aba[1]])
            if bab in ''.join(string) and SSL == False:
                SSL_IPs.append(IP)
                SSL = True
    if TLS:
        TLS_IPs.append(IP)

print(len(TLS_IPs))
print(len(SSL_IPs))