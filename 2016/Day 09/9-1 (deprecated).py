input = list(open('input.txt').read().strip().replace('x','*'))
# First attempt at this problem, done after finishing AoC 2020 then 2015
# I got stuck on part2 and abandonned it for a year. This was my original solution
part1 = []
i = 1

while i < len(input)-1:
    j = i
    while input[j] != '*':
        j += 1
    length = int(''.join(input[i:j]))
    k = j + 1
    while input[k] != ')':
        k += 1
    repeats = int(''.join(input[j+1:k]))
    sequence = ''.join(input[k+1:k+length+1])
    for _ in range(repeats):
        part1.extend(list(sequence))
    substring = ''.join(input[i-1:k+length+1])

    i = k + length + 2

print(len(part1))