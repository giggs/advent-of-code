part1 = open('input.txt').read().strip()
part2 = list(part1)

def count(liste,part):
    answer = 0
    while len(liste) > 0:
        character = liste.pop(0)
        if character == '(':
            end = liste.index(')')
            length,copies = [int(x) for x in ''.join(liste[:end]).split('x')]
            liste = liste[end+1:]
            sublist, liste = liste[:length], liste[length:]
            answer += copies*count(sublist,part) if part else copies*length    
        else:
            answer +=1
    return answer
    
print(count(list(part1),False))
print(count(part2,True))

# Day 9 initially had me stumped for a while. After AoC 2021 I came back to it, understanding recursion a little better
# Turns out it wasn't too hard, I just sucked at recursion. I cleaned up the code a bit after browsing other solutions on reddit