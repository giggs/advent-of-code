messages = open('input.txt').read().splitlines()

part1 = []
part2 = []

for i in range(len(messages[0])):
    letters = []
    high_count = 0
    low_count = len(messages)
    for message in messages:
        letters.append(message[i])
    for l in set(letters):
        if letters.count(l) > high_count:
            high = l
            high_count = letters.count(l)
        if letters.count(l) < low_count:
            low = l
            low_count = letters.count(l)
    part1.append(high)
    part2.append(low)

print(''.join(part1))
print(''.join(part2))