import hashlib

key = 'ffykfhsq'
password1 = []
password2 = [0,0,0,0,0,0,0,0]
p2indexes = []
i = 0
count = 0

while count != 8:
    i += 1
    hash = []
    hash.append(key)
    hash.append(str(i))
    hash = "".join(hash)
    result = hashlib.md5(hash.encode())
    result = result.hexdigest()
    if result[0:5] == '00000':
        if len(password1) != 8:
            password1.append(result[5])
        if  result[5] in '01234567' and result[5] not in p2indexes:
            password2[int(result[5])] = result[6]
            p2indexes.append(result[5])
            count += 1

print(''.join(password1))
print(''.join(password2))