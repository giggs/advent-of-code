print(int(bin(3017957)[3:] + '1', 2)) # See solution to the Josephus problem
print(3017957 - max([3**n for n in range(15) if 3**n < 3017957])) # Similar pattern but with 3^n