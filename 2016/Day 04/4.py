import re

def shift_cypher(room):
    letters = list('abcdefghijklmnopqrstuvwxyz')
    real_name = []
    for letter in room:
        if letter not in letters:
            if letter == '-':
                real_name.append(' ')
        else:
            index = letters.index(letter)
            index += int(''.join(re.findall('\d',room)))
            index %= 26
            real_name.append(letters[index])
    return(''.join(real_name))

rooms = open('input.txt').read().splitlines()
answer = 0
real_rooms = []

for room in rooms:
    room, checksum = room.replace(']','').split('[')
    letters = set(re.findall('[a-z]',room))
    counts = []
    for letter in letters:
        counts.append((-room.count(letter),letter))
    actual_cheksum = ''.join(element[1] for element in sorted(counts)[0:5])
    if checksum == actual_cheksum:
        answer += int(''.join(re.findall('\d',room)))
        real_rooms.append(room)
        if 'north' in shift_cypher(room):
            print(int(''.join(re.findall('\d',room))))

print(answer)