# probably special-cased for my input, got lazy. 
# I actually did this one semi-manually, looking at what the code did and walking back up.
# A value is set to d (input+2555), then copied to a. Each loop is then basically a // 2
# If a is odd output 1, else output 0. So you need to figure out a d such that repeated // 2
# will alternate between odd and even. Start with 0,1,2,5,10,21 etc... till you reach 2730
# 2730 - 2555 = 175

data = open('input.txt').read().strip().splitlines()

answer = 0
found = False
while not found:
    output = []
    i = 0
    answer += 1
    registers = {}
    registers['a'] = answer #set to 7 for part 1
    registers['b'] = 0
    registers['c'] = 0
    registers['d'] = 0
    while i < len(data):
        if i == 5:
            registers['d'] += registers['b']
            registers['b'] = 0
            i += 1
            continue
        elif i == 7:
            registers['d'] += registers['c']*365
            registers['c'] = 0
            i+= 1
            continue
        line = data[i].split(' ')
        if line[0] == 'inc':
            registers[line[1]] += 1
        elif line[0] == 'dec':
            registers[line[1]] -= 1
        if line[1] in 'abcd':
            line[1] = registers[line[1]]
        else:
            line[1] = int(line[1])
        if line[0] == 'cpy':
            registers[line[2]] = line[1]
        elif line[0] == 'out':
            output.append(line[1])
            if len(output) > 8:
                if output == [0,1,0,1,0,1,0,1,0]:
                    print(answer)
                    found = True
                break
        elif line[0] == 'jnz':
            if line[2] in 'abcd':
                line[2] = registers[line[2]]
            if line[1] != 0:
                i += (int(line[2]))
                continue              
        i += 1