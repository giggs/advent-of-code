from collections import defaultdict


input = open('input.txt').read().splitlines()

registers = {}
registers['a'] = 0
registers['b'] = 0
registers['c'] = 1
registers['d'] = 0
i = 0

while i < len(input):
    line = input[i].split(' ')
    
    if line[0] == 'inc':
        registers[line[1]] += 1
    elif line[0] == 'dec':
        registers[line[1]] -= 1
    if line[1] in 'abcd':
        line[1] = registers[line[1]]
    else:
        line[1] = int(line[1])
    if line[0] == 'cpy':
        registers[line[2]] = line[1]
    elif line[0] == 'jnz':
        if line[1] != 0:
            i += (int(line[2]))
            continue              
    i += 1

print(registers['a'])