import re

ranges = sorted([(int(x),int(y)) for (x,y) in re.findall('(\d+)-(\d+)',open('input.txt').read())])
allowed, lowest, ceiling = 0, False, 0
for (lower, upper) in (ranges):
    if lower-1 > ceiling:
        allowed += lower-1-ceiling
        if not lowest: lowest = True, print(lower-1)
    ceiling = max(ceiling,upper)

print(allowed)