import re

input = open('input.txt').read().splitlines()
answer = 0
grid = []
for _ in range(6):
    grid.append([' ']*50)

for line in input:
    if "rect" in line:
        nums = [int(x) for x in re.findall('\d+', line)]
        for x in range(nums[1]):
            for y in range(nums[0]):
                grid[x][y] = '#'
    elif "row" in line:
        nums = [int(x) for x in re.findall('\d+', line)]
        row = nums[0]
        step = nums[1]
        for _ in range(step):
            grid[row].insert(0,grid[row].pop(-1))
    else:
        nums = [int(x) for x in re.findall('\d+', line)]
        column = nums[0]
        step = nums[1]
        temp = []
        for _ in range(6):
            temp.append(grid[_][column])
        for _ in range(step):
            temp.insert(0,temp.pop(-1))
        for _ in range(6):
            grid[_][column] = temp.pop(0)

for row in grid:
    answer += row.count('#')
print(answer)

for row in grid:
    print(''.join(row))