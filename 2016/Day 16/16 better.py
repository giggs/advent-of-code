def fill_data(length): # partially optimized solution based on u/p_tseng's comment
    a = '01111010110010011'
    switch = {'0':'1', '1':'0'}
    
    while len(a) < length:
        b = ''.join([switch[c] for c in a[::-1]])
        a = a + '0' + b

    chunk = length & ~(length - 1) #optimization from the subreddit

    checksum = []
    for i in range(0,length,chunk):
        checksum.append('1' if a[i:i+chunk].count('1') % 2 == 0 else '0')
    return(''.join(checksum))

print(fill_data(272))
print(fill_data(35651584))

## Checksum explanation: you will performe the checksum operation times 
## the largest power of 2 that divides the disk size (chunk)
## Each character in the final checksum corresponds to chunk characters on the disk
## The number of 1s in that chunk is equivalent to repeated XNOR operations
## There are faster ways still, noticing that each doubling will add len(input) 1s
## and a joiner whose parity is defined by the dragon curve. This way you don't 
## even need to make a long string, just compute the number of ones in advance.
## Left as an exercise to future me if I feel like it.