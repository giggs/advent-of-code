def fill_data(length): #first solution, runs in about 5 seconds
    a = '01111010110010011'
    switch = {'0':'1', '1':'0'}
    
    while len(a) < length:
        b = ''.join([switch[c] for c in a[::-1]])
        a = a + '0' + b

    a = a[:length]

    while len(a)%2 == 0:
        checksum = []
        for i in range(0,len(a)-1,2):
            if a[i] == a[i+1]:
                checksum.append('1')
            else:
                checksum.append('0')
        a = ''.join(checksum)

    return(''.join(a))

print(fill_data(272))
print(fill_data(35651584))