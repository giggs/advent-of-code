from collections import defaultdict

## This is most likely special-cased for my input. I was lazy
data = open('input.txt').read().strip().splitlines()

registers = {}
registers['a'] = 12 #set to 7 for part 1
registers['b'] = 0
registers['c'] = 0
registers['d'] = 0
i = 0

while i < len(data):
    line = data[i].split(' ')
    if i == 7:
        registers['a'] += abs(registers['c'])
        registers['c'] = 0
        i += 1
        continue
    elif i == 9:
        registers['a'] += abs(registers['b'])*abs(registers['d'])
        registers['c'] = 0
        registers['d'] = 0
        i+=1
        continue
    elif i == 15:
        registers['c'] += abs(registers['d'])
        registers['d'] = 0
        i+=1
        continue
    elif i == 23:
        registers['a'] += abs(registers['d'])
        registers['d'] = 0
        i += 1
        continue
    if line[0] == 'tgl':
        if line[1] in 'abcd':
            line[1] = registers[line[1]]
        else:
            line[1] = int(line[1])
        j = i + line[1]
        i += 1
        if j < 0 or j >= len(data):
            continue
        else:
            target_line = data[j].split(' ')
            if len(target_line) == 2:
                if target_line[0] == 'inc':
                    data[j] = ' '.join(['dec', target_line[1]])
                else:
                    data[j] = ' '.join(['inc', target_line[1]])
            else:
                if target_line[0] == 'jnz':
                    data[j] = ' '.join(['cpy', target_line[1], target_line[2]])
                else:
                    data[j] = ' '.join(['jnz', target_line[1], target_line[2]])
            continue
    if line[0] == 'inc':
        registers[line[1]] += 1
    elif line[0] == 'dec':
        registers[line[1]] -= 1
    if line[1] in 'abcd':
        line[1] = registers[line[1]]
    else:
        line[1] = int(line[1])
    if line[0] == 'cpy':
        registers[line[2]] = line[1]
    elif line[0] == 'jnz':
        if line[2] in 'abcd':
            line[2] = registers[line[2]]
        if line[1] != 0:
            i += (int(line[2]))
            continue              
    i += 1

print(registers['a'])
