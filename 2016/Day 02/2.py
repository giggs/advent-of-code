input = open('input.txt').read().splitlines()

password = []

x = 5
for digit in input:
    for i in range(len(digit)):
        d = digit[i]
        if d == 'U':
            x -= 3 if x > 3 else 0
        elif d == 'D':
            x += 3 if x < 7 else 0
        elif d == 'R' and x != 3 and x != 6 and x != 9:
            x += 1
        elif d == 'L' and x != 1 and x != 4 and x != 7:
            x -= 1
    password.append(str(x))

print("".join(password))

real_password = []
keys ='0123456789ABCD'
x = 5

for digit in input:
    for i in range(len(digit)):
        d = digit[i]
        if d == 'U':
            if x == 3 or x == 13:
                x -= 2
            elif 6 <= x <= 12 and x != 9:
                x -= 4
        elif d == 'D':
            if x == 1 or x == 11:
                x += 2
            elif 2 <= x <= 8 and x != 5:
                x += 4
        elif d == 'R' and x != 1 and x != 4 and x != 9 and x < 12:
            x += 1
        elif d == 'L' and x != 1 and x != 2 and x !=5 and x != 10 and x != 13:
            x -= 1
    real_password.append(keys[x])

print("".join(real_password))