import re, itertools, heapq, timeit

storage = open('input.txt').read().strip().splitlines()[2:]
nodes = {}
max_x, max_y, part1 = 0, 0, 0

for node in storage:
    x, y, used, avail = re.findall('x(\d+)-y(\d+)\s*\d+T\s*(\d+)T\s*(\d+)',node)[0]
    x, y, used, avail = map(int,(x, y, used, avail))
    max_x, max_y = max(x, max_x), max(y, max_y)
    if used == 0:
        start = (x,y)
        nodes[(x,y)] = [used, avail, '_']
    elif used > 100:
        nodes[(x,y)] = [used, avail, '#']
    else:
        nodes[(x,y)] = [used, avail, '.']
nodes[(max_x, 0)][2] = 'G'

for a,b in itertools.combinations(nodes,2):
    a_used, a_avail, _ = nodes[a]
    b_used, b_avail, _ = nodes[b]
    if a_used > 0 and a_used < b_avail:
        part1 += 1
    elif b_used > 0 and b_used < a_avail:
        part1 += 1
print(part1)

height, width = max_y+1, max_x+1
data = []
for y in range(height):
    for x in range(width):
        data.append(nodes[(x,y)][-1])
data = tuple(data)

def a_star_search(start,table):
    cost_so_far = {}
    cost_so_far[(table, start)] = 0
    frontier = []
    heapq.heappush(frontier, (0, (table,start)))

    while frontier:
        current = heapq.heappop(frontier)[-1]
        grid, (x,y) = current
        if grid[0] == 'G':
            return cost_so_far[current]
        
        neighbours = set()
        for dx in [-1,0,1]:
            for dy in [-1,0,1]:
                nx = x + dx
                ny = y + dy
                if nx >= 0 and ny >= 0 and nx <= max_x and ny <= max_y and abs(dx)+abs(dy) != 2:
                    if grid[(width)*ny+nx] != '#':
                        successor = list(grid)
                        successor[y*(width)+x], successor[ny*(width)+nx] = successor[ny*(width)+nx], successor[y*(width)+x]
                        neighbours.add((tuple(successor),(nx,ny)))
        
        for next in neighbours:
            state, (nx,ny) = next
            new_cost = cost_so_far[current] + 1
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + (abs(nx-0) + abs(ny-0)) # manhattan distance heuristic
                heapq.heappush(frontier, (priority, next))

print(a_star_search(start,data))