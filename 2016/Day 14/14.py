import hashlib
from collections import defaultdict

key, i = 'zpqevtbw', -1
indexes_1, indexes_2 = set(), set()
p1_triples, p2_triples = defaultdict(lambda:[]), defaultdict(lambda:[])

def find_repeats(string, indexes, triples):
    for c in set(string):
        if c*5 in string:
            for index in triples[c]:
                if index + 1000 >= i:
                    indexes.add(index)
    for c,char in enumerate(string[:-2]):
        if char == string[c+1] == string[c+2]:
            return triples[char].append(i)

while len(indexes_2) < 64:
    i += 1
    hash = hashlib.md5((key+str(i)).encode()).hexdigest()
    if len(indexes_1) < 64:
        find_repeats(hash, indexes_1, p1_triples)
    for _ in range(2016):
        hash = hashlib.md5(hash.encode()).hexdigest()
    find_repeats(hash, indexes_2, p2_triples)

print(sorted(list(indexes_1))[63], sorted(list(indexes_2))[63])