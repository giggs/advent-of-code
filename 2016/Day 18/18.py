from collections import defaultdict

line = open('input.txt').read() 
width = len(line)
grid = defaultdict(lambda:1)

for i,tile in enumerate(line):
    grid[(i,0)] = 0 if tile == "^" else 1

for y in range(1,400000):
    for x in range(width):
        prev_tiles = grid[(x-1,y-1)] + grid[(x,y-1)] + grid[(x+1,y-1)]
        grid[(x,y)] = 0 if (prev_tiles == 2 and grid[(x,y-1)] == 1) or (prev_tiles == 1 and grid[(x,y-1)] == 0) else 1
    if y == 39:
        print(sum(v for (x,y), v in grid.items() if x in range(width)))
print(sum(v for (x,y), v in grid.items() if x in range(width)))