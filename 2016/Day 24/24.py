import heapq

input = open('input.txt').read().strip().splitlines()

maze = {}
goal = set()
height, width = len(input)-1, len(input[0])-1
for y, line in enumerate(input):
    for x, char in enumerate(line):
        maze[(x,y)] = char
        if char != '#':
            goal.add(char)
        if char == '0':
            start = (x,y)

def a_star_search(start,maze,goal,part):
    seen = frozenset('0')
    cost_so_far = {}
    cost_so_far[(start,seen)] = 0
    frontier = []
    heapq.heappush(frontier, (0, (start,seen)))
    

    while frontier:
        state = heapq.heappop(frontier)[-1]
        (x,y), visited = state
        if not part and visited == goal:
            return cost_so_far[state]
        if part and visited == goal and (x,y) == start:
            return cost_so_far[state]
        
        neighbours = []
        for dx in [-1,0,1]:
            for dy in [-1,0,1]:
                nx = x + dx
                ny = y + dy
                if nx >= 0 and ny >= 0 and nx <= width and ny <= height and abs(dx)+abs(dy) != 2 and maze[(nx,ny)] != '#':
                    new_visited = set(visited)
                    new_visited.add(maze[(nx,ny)])
                    neighbours.append(((nx,ny),frozenset(new_visited)))
        
        for next in neighbours:
            new_cost = cost_so_far[state] + 1
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost 
                heapq.heappush(frontier, (priority, next))

print(a_star_search(start,maze,goal,False))
print(a_star_search(start,maze,goal,True))