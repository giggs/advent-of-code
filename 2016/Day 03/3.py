import re

input = re.findall('\d+', open('input.txt').read())
triangles = []
part2 = []

for i in range(0,len(input),3):
    sides = sorted([int(input[i]), int(input[i+1]), int(input[i+2])])
    if sides[0] + sides[1] > sides[2]:
        triangles.append(sides)

print(len(triangles))

for i in range(0,len(input),9):
    sides = sorted([int(input[i]), int(input[i+3]), int(input[i+6])])
    if sides[0] + sides[1] > sides[2]:
        part2.append(sides)
    i += 1
    sides = sorted([int(input[i]), int(input[i+3]), int(input[i+6])])
    if sides[0] + sides[1] > sides[2]:
        part2.append(sides)
    i += 1
    sides = sorted([int(input[i]), int(input[i+3]), int(input[i+6])])
    if sides[0] + sides[1] > sides[2]:
        part2.append(sides)

print(len(part2))