import re, heapq, itertools

buildings = open('input.txt').read().splitlines()
floors = []
elevator_start = []

for floor in buildings:
    items = []
    items.extend(re.findall('(\w+)\s(g)enerator',floor))
    items.extend(re.findall('(\w+)-compatible\s(m)icrochip',floor))
    elevator_start.append(frozenset(items))
part2 = elevator_start.copy()
part2[0] = elevator_start[0] | frozenset([('elerium', 'g'), ('elerium', 'm'), ('dilithium', 'm'), ('dilithium', 'g')])

items = frozenset([])
for floor in elevator_start:
    items |= floor
goal = (frozenset([]),frozenset([]),frozenset([]),items,4)
goal2 = (frozenset([]),frozenset([]),frozenset([]),items | part2[0],4)
elevator_start.append(1)
part2.append(1)
elevator_start = tuple(elevator_start)
part2 = tuple(part2)

def heuristic(state):
    # Put an excessive weight on states that have many items on the lower floors
    state = state[::-1]
    weight = 0
    for i in range(1,len(state)):
        weight += len(state[i])*(10**i)
    return weight
 
def a_star_search(start, successors, goal):
    frontier = []
    heapq.heappush(frontier, (0, start)) # Creates a priority queue
    cost_so_far = {}
    cost_so_far[start] = 0

    while frontier:
        current = heapq.heappop(frontier)[1]
        if current == goal:
            return cost_so_far[current]
        for next in successors(current).keys():
            new_cost = cost_so_far[current] + 1
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(next)
                heapq.heappush(frontier, (priority, next))

    return 'Fail'

def elevator_successors(state): # Initially, I used a dict to keep the actions. Turns out you don't need the actions, but it's not faster to use a list or a set so I left it as is.
    first, second, third, fourth, elevator = state
    result = {}
    for floor in state[:4]: # Cull states where a microchip is fried
        generators = [item[0] for item in floor if item[1] == 'g']
        if generators:
            chips = [item[0] for item in floor if item[1] == 'm']       
            for chip in chips:
                if chip not in generators:
                    return {}
    
    if elevator == 1:
        for x,y in itertools.combinations_with_replacement(first,2):
            if x[1] != y[1] and x[0] != y[0]: # Can't move unpaired generator and chip
                continue
            result[first - frozenset([x,y]), second | frozenset([x,y]), third, fourth, 2] = (x, y, 1-2)
    
    elif elevator == 2:
        for x,y in itertools.combinations_with_replacement(second,2):
            if x[1] != y[1] and x[0] != y[0]:
                continue
            if first: # if first floor is empty, moving back to it is useless
               result[first | frozenset([x,y]), second - frozenset([x,y]), third, fourth, 1] = (x, y, 2-1)
            result[first, second - frozenset([x,y]), third | frozenset([x,y]), fourth, 3] = (x, y, 2-3)
    
    elif elevator == 3:
        for x,y in itertools.combinations_with_replacement(third,2):
            if x[1] != y[1] and x[0] != y[0]:
                continue
            if first or second: # if the first two floors are empty, going back down is useless
                result[first, second | frozenset([x,y]), third - frozenset([x,y]), fourth, 2] = (x, y, 3-2)
            result[first, second, third - frozenset([x,y]), fourth | frozenset([x,y]), 4] = (x, y, 3-4)

    else:
        for x,y in itertools.combinations_with_replacement(fourth,2):
            if x[1] != y[1] and x[0] != y[0]:
                continue
            result[first, second, third | frozenset([x,y]), fourth - frozenset([x,y]), 3] = (x, y, 4-3)

    return result         

print(a_star_search(elevator_start,elevator_successors,goal))
print(a_star_search(part2,elevator_successors,goal2))