from typing import DefaultDict
import re

data = open('input.txt').read().replace('output ','1000').splitlines()
instructions = {}
bots = DefaultDict(lambda:[])
p2 = 1

for line in data:
    numbers = [int(x) for x in re.findall(r'-?\d+', line)]
    if len(numbers) == 2:
        bots[numbers[1]].append(numbers[0])
    else:
        instructions[numbers[0]] = (numbers[1],numbers[2])

binning = True
while binning:
    binning = False
    for bot,value in bots.items():
        if len(value) == 2:
            if value == [61,17] or value == [17,61]:
                print(bot) 
            low, high = instructions[bot][0], instructions[bot][1]
            bots[low].append(min(value))
            bots[high].append(max(value))
            bots[bot] = []; break
    for bot,value in bots.items():
        if bot < 1000 and len(value) > 0:
            binning = True; break

for bot,value in bots.items():
    if bot == 10000 or bot == 10001 or bot == 10002:
        for chip in value:
            p2 *= chip

print(p2)