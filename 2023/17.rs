use std::fs;
use std::time::Instant;
use std::collections::{HashMap, BinaryHeap};
use std::thread;
use std::sync::Arc;

const GRIDSIZE : usize = 141;

fn manhattan(row : i16, col : i16) -> i16 { // The heuristic, even heavily weighted, doesn't help much. I just wanted to save an example.
    let result = GRIDSIZE as i16-1-row + GRIDSIZE as i16-1-col;
    return -result;
}

fn djikstra( grid : &[[u8; GRIDSIZE]; GRIDSIZE], min : i16, max : i16) -> i16 {
    let mut queue = BinaryHeap::from_iter([(0_i16, 0_i16, 0_i16, 0_i16, -1_i16)]);
    let mut costs : HashMap< (i16, i16, i16), i16 > = HashMap::new();
    while let Some((_heuristic, cost, row, col, dir)) = queue.pop() {
        if (row, col) == ((GRIDSIZE-1) as i16, (GRIDSIZE-1) as i16) {
            return -cost;
        }
        for (i,new_dir) in (0_i16..).zip([ (1,0), (0,1), (-1,0), (0,-1) ]) {
            if dir == i as i16 || (i as i16 + 2) % 4 == dir { // only handle turns
                continue;
            }
            let mut move_cost : i16 = 0;
            for distance in 1..max + 1 {
                let new_row = row + new_dir.0 * distance;
                let new_col = col + new_dir.1 * distance;
                if new_row < GRIDSIZE as i16 && new_col < GRIDSIZE as i16 
                && new_row >= 0 && new_col >= 0 {
                    move_cost -= grid[new_row as usize][new_col as usize] as i16;
                    if distance < min {
                        continue;
                    }
                    let new_cost = cost + move_cost;
                    if new_cost > *costs.get(&(new_row, new_col, i)).unwrap_or(&i16::MIN) {
                        costs.insert((new_row, new_col, i), new_cost);
                        let heuristic = manhattan(new_row, new_col);
                        queue.push((new_cost + 3*heuristic, new_cost, new_row, new_col, i));
                    }
                }
            }
        }
    }
    return 0;
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut grid = [[0_u8;GRIDSIZE];GRIDSIZE];
    for (y,row) in data.lines().enumerate() {
        //assert!(y < GRIDSIZE);
        for (x,n) in row.chars().enumerate() {
            //assert!(x < GRIDSIZE);
            grid[y][x] = n as u8 - '0' as u8;
        }
    }    

    let grid_ptr = Arc::new(grid);
    let t2 = thread::spawn(move || djikstra(&grid_ptr, 4, 10));
    let t1 = thread::spawn(move || djikstra(&grid, 1, 3));
    let p1 = t1.join().unwrap();
    let p2 = t2.join().unwrap();
    println!("{}\n{}", p1, p2);
    
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}

// The idea to handle turns only was stolen from reddit, making this much cleaner
// My original solution in Python was a very standard step by step, uncleaned bas version here
// https://topaz.github.io/paste/#XQAAAQDxCQAAAAAAAAA0m0pnuFI8c9q4WY2st59WiNyOazRZUWZalmp50hs9qpT3u35XoD1dtCt5/BNh1oS4rL2JxaG9jXmYdXiy+yyulDqqTRcFXU8+72o/l77i7YcLZSb1e4qqQZvXpTUw53FlQsfDzG+0hxMxzU+gHjt4MgzW/I16iFwX+C0PviRB83/1AYLsUjLLbS1jhfSY2WDpyiIkxFjw3PmUqP2gkodZV+qDxcenhMXcAlMGpMA4hM8yPhYebjtdUma1dIh+H9VPdwq1sRnE8CfOgJDx0MtCe1iLLVmqKUfVHve3hZve6onxcbXXFAVTaggOFfKEiaADYLKlIHPVYiVesDPiRYfDezWogzdMu2EVTnofGaPFd0BUIuJk2IkzRe8N2AxTbnC5pyJF5Ab+MLJYkfxb0iE26GWFxXP3uF622CGhHPbVTMiaKVzEHKwdm6r6Wyldt0P4bsf7FLz0ZTq/lcNWWerWYEqejSSTxylzjWk57I2emijPwNiuGWJ2SSidqjRUSgGP3cTTTiRZwTrZ+8tKSGbybJ7l22+6eK94rfZucoEXIUEVCauEcUGC4BCrtjqEn9/20yqYZ6OOlGwqBO6hcv6TPvh2KlSyUtzVlnNx6gaHyZwUXoDUjfNWa12pHA0EZ2wvs67B1cvG34l9frE/sPlBUR+KqonooAiuN1YQK+66wTooDCUUF+BQZCs0E0ZoeRhHiOdFXOuIfmR7ZYgLYwPGSh1nvCN1tKt6kJuB4PAxMar8AzuEXmSJzNO3u4KQ4ArTo3CV7uKS0ax7t3fnse4udHuCREcFuP8Qc5hXo7h1zdtUdGWEFlE+bNyfcdgXUyWVIjFiYNvpJEzhnVEY105BBW3Kx+rgJEegAsqWWWp46ldm3TzbC/xln5MMJgUbdK65LyWiItfZn0wVQ1PEQWw2WDb0d00GGg9/doEV/y4FcwA=