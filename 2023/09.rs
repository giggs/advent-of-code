use std::fs;

fn check_if_done( diffs : & Vec<Vec<i32>> ) -> bool {
    let diff = diffs.last().unwrap();
    for n in diff {
        if *n != 0 {
            return false;
        }
    }
    return true;
}

fn main() {
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut p1 : i32 = 0;
    let mut p2 : i32 = 0;
    
    for line in data.lines() {
        let mut ints : Vec<i32> = Vec::new();
        let mut line_iterator = line.split_whitespace();
        while let Some(x) = line_iterator.next() {
            ints.push(x.parse::<i32>().unwrap());
        }
        let mut diffs : Vec<Vec<i32>> = Vec::new();
        diffs.push(ints);
        while check_if_done(&diffs) == false {
            let prev_line = diffs.last().unwrap();
            let mut new_line : Vec<i32> = Vec::new();
            for i in 0..(prev_line.len()-1) {
                new_line.push(prev_line[i+1] - prev_line[i]);
            }
            diffs.push(new_line);
        }
        for i in (1..(diffs.len()-1)).rev() {
            let a = *diffs[i].last().unwrap();
            let b = *diffs[i-1].last().unwrap();
            let c = diffs[i-1][0];
            let d = diffs[i][0];
            diffs[i-1].push(a + b);
            diffs[i-1].insert(0,c-d);
        }
        p1 += diffs[0].last().unwrap();
        p2 += diffs[0][0];
    }
    println!("{}\n{}", p1, p2);
} 