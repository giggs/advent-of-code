use std::fs;
use std::collections::HashMap;

fn gcd(a : usize, b : usize) -> usize {
    if b == 0 {
        return a;
    }
    return gcd(b, a % b);
}

fn lcm(nums: &[usize]) -> usize {
    if nums.len() == 1 {
        return nums[0];
    }
    let a = nums[0];
    let b = lcm(&nums[1..]);
    return a * b / gcd(a, b);
}

fn are_we_there_yet(cycles : &[usize], len : usize) -> bool {
    for i in 0..len {
        if cycles[i] == 1 {
            return false;
        }
    }
    return true;
}

fn main() {
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut data_iterator = data.split("\n\n");
    let dirs : Vec<char> = data_iterator.next().unwrap().chars().collect();
    let intersections = data_iterator.next().unwrap();
    let mut positions : Vec<String> = Vec::new();
    let mut map : HashMap<String, [String;2]> = HashMap::new();
    for temp in intersections.lines() {
        let intersection = temp.replace("(", "").replace(")","");
        let mut intersection_iterator = intersection.split(" = ");
        let key = String::from(intersection_iterator.next().unwrap());
        let mut value : [String; 2] = [String::new(),String::new()];
        let temp = intersection_iterator.next().unwrap();
        let mut temp_iterator = temp.split(", ");
        value[0] = String::from(temp_iterator.next().unwrap());
        value[1] = String::from(temp_iterator.next().unwrap());
        if key.ends_with('A') {
            positions.push(key.clone());
        }
        map.insert(key,value);
    }
    let len = positions.len();
    let mut cycles : [usize;10] = [1;10];
    
    let mut current = String::from("AAA");
    let mut p1 : usize = 0;
    let modulo = dirs.len();

    while current != "ZZZ" {
        let next = if dirs[p1%modulo] == 'L' {0} else {1};
        current = map[&current][next].clone();
        p1 += 1;
    }

    let mut p2 : usize = 0;
    while are_we_there_yet(&cycles, len) == false {
        let next = if dirs[p2%modulo] == 'L' {0} else {1};
        p2 += 1;
        for i in 0..len {
            let pos = positions[i].clone();
            positions[i] = map[&pos][next].clone();
            if positions[i].ends_with('Z') {
                if cycles[i] == 1 {
                    cycles[i] = p2;
                }
            }
        }
    }
    p2 = lcm(&cycles);
    println!("{}\n{}", p1, p2);
} 