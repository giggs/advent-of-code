use std::fs;
use std::time::Instant;

const GRIDSIZE : usize = 100;
// Lazy solution parsing the whole grid every time.
// Can go faster by only handling the rocks

fn north( grid : & mut Vec<Vec<char>> ) {
    for x in 0..GRIDSIZE {
        let mut destination = 0;
        for y in 0..GRIDSIZE {
            match grid[y][x] {
                'O' => {
                    grid[y][x] = '.';
                    grid[destination][x] = 'O';
                    destination += 1;
                }
                '#' => destination = y+1,
                _ => ()
            }
        }
    }
}

fn south( grid : & mut Vec<Vec<char>> ) {
    for x in 0..GRIDSIZE {
        let mut destination : i8 = ( GRIDSIZE-1 ) as i8;
        for y in (0..GRIDSIZE).rev() {
            match grid[y][x] {
                'O' => {
                    grid[y][x] = '.';
                    grid[destination as usize][x] = 'O';
                    destination -= 1;
                }
                '#' => destination = y as i8 - 1,
                _ => ()
            }
        }
    }
}

fn west( grid : & mut Vec<Vec<char>> ) {
    for y in 0..GRIDSIZE {
        let mut destination = 0;
        for x in 0..GRIDSIZE {
            match grid[y][x] {
                'O' => {
                    grid[y][x] = '.';
                    grid[y][destination] = 'O';
                    destination += 1;
                }
                '#' => destination = x+1,
                _ => ()
            }
        }
    }
}

fn east( grid : & mut Vec<Vec<char>> ) {
    for y in 0..GRIDSIZE {
        let mut destination : i8 = (GRIDSIZE-1) as i8;
        for x in (0..GRIDSIZE).rev() {
            match grid[y][x] {
                'O' => {
                    grid[y][x] = '.';
                    grid[y][destination as usize] = 'O';
                    destination -= 1;
                }
                '#' => destination = x as i8 -1,
                _ => ()
            }
        }
    }
}

fn cycle( grid : & mut Vec<Vec<char>> ) {
    north(grid); 
    west(grid);
    south(grid);
    east(grid);
}

fn count( grid : & Vec<Vec<char>> ) -> usize {
    let mut result = 0;
    for y in 0..GRIDSIZE {
        result += grid[y].iter().filter(|x| **x == 'O').count()
                * ( GRIDSIZE - y );
    }
    result
}
fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut grid : Vec<Vec<char>> = Vec::new();
    for line in data.lines() {
        assert_eq!(line.len(), GRIDSIZE);
        grid.push(line.chars().collect());
    }
    let mut cache : Vec<Vec<Vec<char>>> = Vec::new();
    cache.push(grid.clone());
    north(& mut grid);
    let p1 = count(&grid);
    west(& mut grid);
    south(& mut grid);
    east(& mut grid);

    let mut cycles = 1;
    let mut test;
    loop {
        test = cache.iter().position(|x| *x == grid);
        if test.is_some() {
            break;
        }
        cache.push(grid.clone());
        cycle(& mut grid);
        cycles += 1;
    }

    let cycle_length = cycles - test.unwrap();

    println!("{}\n{}", p1, count(&cache[(1000000000 - test.unwrap()) % cycle_length + test.unwrap()]));
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
} 