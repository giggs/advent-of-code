use std::fs;
use std::time::Instant;

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut galaxies : Vec< ( i32, i32, i32, i32 ) > = Vec::new();
    let mut n : i32 = 0;
    for ( y, row ) in data.lines().enumerate() { // Expand empty rows while parsing
        if galaxies.last().is_some_and( |g| y as i32 - ( g.1 - n ) > 1 ) {
            n += 1;
        }
        for ( x, col ) in row.chars().enumerate() {
            if col == '#' {
                galaxies.push( ( x as i32, y as i32 + n, x as i32, y as i32 + n * 999999 ) );
            }
        }
    }
    galaxies.sort();

    n = 0;
    for i in 1..galaxies.len() { // Expand empty columns in a single pass through the galaxies list
        let b = galaxies[ i ];
        let a = & mut galaxies[ i - 1 ];
        let test = ( b.0 - a.0 ) > 1;
        a.0 += n;
        a.2 += n * 999999;
        n += test as i32;
    }
    let last = galaxies.last_mut().unwrap();
    last.0 += n;
    last.2 += n * 999999;

    let mut p1 : u64 = 0;
    let mut p2 : u64 = 0;
    for i in 0..galaxies.len() {
        for j in ( i + 1 )..galaxies.len() {
            p1 += ( galaxies[ i ].0 - galaxies[ j ].0 ).abs() as u64 + ( galaxies[ i ].1 - galaxies[ j ].1 ).abs() as u64;
            p2 += ( galaxies[ i ].2 - galaxies[ j ].2 ).abs() as u64 + ( galaxies[ i ].3 - galaxies[ j ].3 ).abs() as u64;
        }
    }
    println!("{}\n{}", p1, p2 );
    let elapsed = now.elapsed();
    println!("{}", elapsed.as_micros());
} 