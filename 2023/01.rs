use std::fs;

fn main() {
    let mut data = fs::read_to_string("./in.txt").expect("Unable to read file");
    let mut sum : u32 = 0;
    let mut sum2 : u32 = 0;
    let mut first : u32 = 0;
    let mut second : u32 = 0;
    for c in data.chars() {
        if c.is_digit(10) {
            let d : u32 = c.to_digit(10).expect("Unable to convert to digit");
            if first == 0 {
                first = d * 10;
            }
            second = d;
        }
        if c == '\n' {
            sum += first+second;
            first = 0;
            second = 0
        }
    }

    data = data.replace("one","o1e");
    data = data.replace("two", "t2o");
    data = data.replace("three", "t3e");
    data = data.replace("four", "f4r");
    data = data.replace("five", "f5e");
    data = data.replace("six", "s6x");
    data = data.replace("seven", "s7n");
    data = data.replace("eight", "e8t");
    data = data.replace("nine", "n9e");

    for c in data.chars() {
        if c.is_digit(10) {
            let d : u32 = c.to_digit(10).expect("Unable to convert to digit");
            if first == 0 {
                first = d * 10;
            }
            second = d;
        }
        if c == '\n' {
            sum2 += first+second;
            first = 0;
            second = 0
        }
    }

    println!("{}\n{}", sum, sum2);
}
