use std::fs;
use std::collections::HashMap;
use std::cmp;

const P1_CARDS : & str = "23456789TJQKA";
const P2_CARDS : & str = "J23456789TQKA";

fn compute_tiebreak( hand : & str, part1 : bool ) -> usize {
    let mut result : usize = 0;
    for card in hand.chars() {
        result <<= 4;
        if part1 == true {
            result |= P1_CARDS.find( card ).unwrap();
        } else {
            result |= P2_CARDS.find( card ).unwrap();
        }
    }
    return result;
}

fn compute_strength( hand : & str ) -> usize {
    let mut strength : usize = 0;
    for card in hand.chars() {
        strength += hand.matches( card ).count();
    }
    return strength << 21;
}

fn sort_key( hand : &str, part1 : bool ) -> usize {
    let mut value : usize = 0;
    if part1 == true {
        value = compute_strength( hand );
    } else {
        for sub in "23456789TQKA".chars() {
            let new_hand : & str = &hand.replace( "J", & sub.to_string() );
            value = cmp::max( value, compute_strength( new_hand ) );
        }
    }
    return value | compute_tiebreak( hand, part1 );
}

fn compute_winnings( hands : & mut Vec< & str >, bids : & HashMap< & str, u64 >, part1 : bool ) -> u64 {
    hands.sort_by_cached_key( |x| sort_key( x, part1 ) );
    let mut result : u64 = 0;
    for ( i, hand ) in hands.iter().enumerate() {
        result += ( i as u64+1 )*bids[ hand ];
    }
    return result;
}
fn main() {
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut bids : HashMap< & str, u64 > = HashMap::new();
    let mut hands : Vec< & str > = Vec::new();
    for hand_bid in data.lines() {
        let mut hand_iterator = hand_bid.split_whitespace();
        let hand : &str = hand_iterator.next().unwrap();
        hands.push( hand );
        let bid = hand_iterator.next().unwrap().parse::< u64 >().unwrap();
        bids.insert( hand, bid );
    }
    
    println!( "{}\n{}", 
            compute_winnings( & mut hands, &bids, true ),
            compute_winnings( & mut hands, &bids, false ) );
} 