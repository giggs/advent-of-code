use std::fs;
use std::collections::HashMap;
use std::time::Instant;

fn solve( springs : &[char], damaged : &[usize], 
        pos : usize, damaged_index : usize, current_length : usize, cache : & mut HashMap< ( usize, usize, usize ), usize >) -> usize {
    if let Some(&value) = cache.get(&(pos, damaged_index, current_length)) {
        value
    } else {
        if pos == springs.len() {
            if damaged_index == damaged.len() && current_length == 0 {
                return 1
            } else {
                return 0
            }
        }
        let mut total : usize = 0;
        for c in ['.','#']{
            if springs[pos] == c || springs[pos] == '?' {
                if c == '.' {
                    if current_length == 0 {
                        total += solve(springs, damaged, pos+1, damaged_index, 0, cache);
                    } else if damaged_index < damaged.len() && damaged[damaged_index] == current_length {
                        total += solve(springs,damaged,pos+1,damaged_index+1,0,cache);
                    }
                } else {
                    total += solve(springs,damaged,pos+1,damaged_index, current_length+1,cache);
                }
            }
        }
        cache.insert((pos, damaged_index, current_length), total);
        total
    }
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut p1 : usize = 0;
    let mut p2 : usize = 0;
    let mut cache : HashMap< ( usize, usize, usize ), usize > = HashMap::new();
    for line in data.lines() {
        let mut line_iterator = line.split_whitespace();
        let mut springs : Vec<char> = line_iterator.next().unwrap().chars().collect();
        let mut springs_2 : Vec<char> = springs.clone();
        let mut damaged : Vec< usize > = Vec::new();
        let mut group_iterator = line_iterator.next().unwrap().split(',');
        while let Some(x) = group_iterator.next() {
            damaged.push(x.parse::< usize >().unwrap());
        }
        let mut damaged_2 : Vec<usize> = damaged.clone();
        for _i in 0..4 {
            springs_2.push('?');
            springs_2.extend(springs.clone());
            damaged_2.extend(damaged.clone());
        }
        springs_2.push('.');
        springs.push('.');
        p1 += solve(&springs, &damaged, 0, 0, 0,& mut cache);
        cache.clear();
        p2 += solve(&springs_2, &damaged_2, 0, 0, 0, & mut cache);
        cache.clear();
    }
    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{}", elapsed.as_micros());
} 