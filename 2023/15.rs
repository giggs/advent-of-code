use std::fs;
use std::time::Instant;

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut data_iterator = data.split( ',' );
    let mut p1 = 0;
    let mut boxes : Vec< Vec< ( & str, usize ) > > = vec![Vec::new();256];
    while let Some( step ) = data_iterator.next() {
        let mut hash : usize = 0;
        let mut box_hash : usize = 0;
        let mut i = 0;
        for ( j, c ) in step.chars().enumerate() {
            if c == '=' || c == '-' {
                box_hash = hash;
                i = j;
            }
            hash += c as usize;
            hash += hash << 4;
            hash &= 0xFF;
        }
        p1 += hash;
        
        let label : &str = &step[ ..i ];
        let item = & mut boxes[ box_hash ];
        let mut j = 0;
        let mut found = false;
        while j < item.len() {
            if item[ j ].0 == label {
                found = true;
                break;
            } else {
                j += 1;
            }
        }
        if  & step[ i..i+1 ] == "=" {
            let focal = step[ i+1.. ].parse::< usize >().unwrap();
            if found {
                item[ j ].1 = focal;
            } else {
                item.push( ( label, focal ) );
            }
        } else if found {
            item.remove( j );
        }
    }

    let mut p2 = 0;
    for (i,item) in boxes.iter().enumerate() {
        for (j, lens) in item.iter().enumerate() {
            p2 += ( i + 1 ) * ( j + 1 ) * lens.1;
        }
    }

    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
} 