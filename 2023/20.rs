use std::fs;
use std::time::Instant;
use std::collections::{HashMap, VecDeque};

const LOW : bool = false;
const HIGH : bool = true;

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut types : HashMap< &str, char > = HashMap::new();
    let mut flip_flops : HashMap< &str, bool > = HashMap::new();
    let mut conjunctions : HashMap< &str, HashMap< &str, bool > > = HashMap::new();
    let mut destinations : HashMap< &str, Vec< &str > > = HashMap::new();
    let mut reverse : HashMap< &str, Vec< &str > > = HashMap::new();

    for line in data.lines() {
        let (sender_and_type, receivers_raw) : (&str, &str) = line.split_once(" -> ").unwrap();
        let module_type : char = sender_and_type.chars().nth(0).unwrap();
        let sender : &str = &sender_and_type[1..];
        let mut receivers : Vec<&str> = Vec::new();
        let mut receiver_iterator = receivers_raw.split(", ");
        while let Some(receiver) = receiver_iterator.next() {
            receivers.push(receiver);
        }
        for r in &receivers {
            if let Some(parents) = reverse.get_mut(r) {
                parents.push(sender)
            } else {
                let mut parents : Vec<&str> = Vec::new();
                parents.push(sender);
                reverse.insert(r, parents);
            }
        }
        destinations.insert(sender, receivers);
        types.insert(sender, module_type);
        if module_type == '%' {
            flip_flops.insert(sender, LOW);
        }
    }

    for (key,value) in &types {
        if *value == '&' {
            for module in reverse.get_mut(key).unwrap() {
                if let Some(connections) = conjunctions.get_mut(key) {
                    connections.insert(module, LOW);
                } else {
                    let mut connections : HashMap< &str, bool > = HashMap::new();
                    connections.insert(module, LOW);
                    conjunctions.insert(key,connections);
                }
            }
        }
    }

    let mut targets : HashMap<&str, usize> = HashMap::new();
    let last_conv = reverse.get("rx").unwrap()[0];
    for target in reverse.get(last_conv).unwrap() {
        targets.insert(target, 0);
    }

    let mut counter = 0;
    let mut low_count = 0;
    let mut high_count = 0;
    let mut p1 = 0;
    let mut p2;
    'simulate:
    loop {
        counter += 1;
        let mut queue : VecDeque< (&str, bool) > = VecDeque::new();
        queue.push_back(("roadcaster", LOW));
        'process_queue:
        while let Some(current) = queue.pop_front() {
            let (module, pulse) = current;
            if pulse == HIGH {
                high_count += 1;
            } else {
                low_count += 1;
            }
            let mut next_pulse = LOW;
            if let Some(module_type) = types.get(module) {
                match *module_type {
                    'b' => next_pulse = LOW,
                    '%' => if pulse { 
                        continue 'process_queue; 
                    } else {
                        let state = flip_flops.get_mut(module).unwrap();
                        *state = !*state;
                        next_pulse = *state;
                    }
                    '&' | _ => {
                        next_pulse = LOW;
                        let memory = conjunctions.get(module).unwrap();
                        for (_module, last_pulse) in memory {
                            if *last_pulse == LOW {
                                next_pulse = HIGH;
                                break;
                            }
                        }
                    }
                }
            }
            if let Some(dests) = destinations.get(module) {
                for dest in dests {
                    if targets.get(dest).is_some_and(|_x| next_pulse == LOW) {
                        targets.insert(dest, counter);
                        p2 = 1;
                        for (_module, cycle) in &targets {
                            p2 *= *cycle;
                        }
                        if p2 != 0 {
                            break 'simulate;
                        }
                    }
                    let memory = conjunctions.get_mut(dest);
                    if memory.is_some() {
                        memory.unwrap().insert(module, next_pulse);
                    }
                    queue.push_back((dest, next_pulse));
                }
            }
        }
        if counter == 1000 {
            p1 = low_count*high_count;
        }
    }
    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}