use std::fs;
use std::collections::HashMap;
const DIRS : [i32;3] = [-1,0,1];

fn find_number( row : &Vec<char>, col : & mut usize ) -> u32 {
    let mut result : u32 = 0;
    while *col < row.len() && row[*col].is_digit(10) {
        result = result*10 + row[*col] as u32 - '0' as u32;
        *col += 1;
    }
    return result;
}

fn is_engine_part( y : usize, x : usize, number : u32, grid : &Vec<&str>, in_out_gears : & mut HashMap< (usize, usize), (u32, u32) > ) -> bool {
    for dy in DIRS {
        let ny : i32 = y as i32 + dy;
        for dx in DIRS {
            let nx : i32 = x as i32 + dx;
            if ny >= 0 && ny < grid.len() as i32 && nx >= 0 && nx < grid[ny as usize].len() as i32 {
                let new_row_as_chars : Vec<char> = grid[ny as usize].chars().collect();
                let nc : char = new_row_as_chars[nx as usize];
                if nc != '.' && !nc.is_digit(10) {
                    if nc == '*' {
                        let key : (usize, usize) = (ny as usize, nx as usize);
                        if in_out_gears.contains_key(&key) {
                            let value = in_out_gears.get_mut(&key).expect("something");
                            value.0 += 1;
                            value.1 *= number;
                        }
                        else {
                            in_out_gears.insert(key, (1,number));
                        }
                    }
                    return true;    
                }
            }
        }
    }
    return false;
}

fn main() {
    let data = fs::read_to_string("./in.txt").expect("Unable to read file");
    let grid : Vec<&str> = data.split("\r\n").collect();
    let mut p1 = 0;
    let mut p2 = 0;
    let mut gears : HashMap< (usize, usize), (u32, u32) > = HashMap::new();
    for (y,row) in grid.iter().enumerate() {
        let mut x : usize = 0;
        let row_as_chars : Vec<char> = grid[y as usize].chars().collect();
        while x < row.len() {
            let c = row_as_chars[x];
            let mut end = x;
            if c.is_digit(10) {
                let result : u32  = find_number(&row_as_chars, & mut end);
                for cx in x..end{
                    if is_engine_part(y, cx, result, &grid, & mut gears) {
                        p1 += result;
                        break;
                    } 
                }
                x = end;
            } else {
                x += 1;
            }   
        }
    }
    for numbers in gears.into_values() {
        if numbers.0 == 2 {
            p2 += numbers.1;
        }
    }
    println!("{}\n{}", p1, p2);
}