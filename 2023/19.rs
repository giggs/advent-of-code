use std::fs;
use std::time::Instant;
use std::collections::{HashMap, VecDeque};
use std::cmp;

fn update_within_ranges( rating : usize, less_than : bool, value : i64, ranges : [i64;8]) -> [i64;8] {
    let mut res = ranges.clone();
    if less_than {
        res[rating*2+1] = cmp::min(res[rating*2+1], value-1);
    } else {
        res[rating*2] = cmp::max(res[rating*2], value+1);
    }
    res
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut data_iterator = data.split("\n\n");
    let raw_rules = data_iterator.next().unwrap();
    let parts = data_iterator.next().unwrap();
    let mut rules : HashMap< &str, Vec<(usize, bool, i64, &str)>> = HashMap::new();
    for rule in raw_rules.lines() {
        let mut first_iterator = rule.split('{');
        let key = first_iterator.next().unwrap();
        let rest = first_iterator.next().unwrap();
        let mut conds = rest[0..rest.len()-1].split(',');
        let mut value : Vec<(usize, bool, i64, &str)> = Vec::new();
        while let Some(cond) = conds.next() {
            let mut cond_iterator = cond.split(':');
            let test = cond_iterator.next().unwrap();
            if let Some(dest) = cond_iterator.next() {
                let rating = "xmas".find(test.chars().nth(0).unwrap()).unwrap();
                value.push((rating, test.chars().nth(1).unwrap() == '<', test[2..].parse::<i64>().unwrap(), dest));
            } else {
                value.push((usize::MAX, false, 0, test))
            }
        }
        rules.insert(key, value);
    }
    let mut p1 = 0;
    for ex_part in parts.lines() {
        let part = &ex_part[1..ex_part.len()-1];
        let mut ratings : [i64;4] = [0;4];
        for (i,r) in part.split(',').into_iter().enumerate() {
            ratings[i] = r[2..].parse::<i64>().unwrap();
        }
        let mut workflow : &str = "in";
        while workflow != "A" && workflow != "R" {
            let w = rules.get(workflow).unwrap();
            for ww in w {
                if (ww.0 != usize::MAX && (ww.1 ^ (ratings[ww.0] > ww.2))) || (ww.0 == usize::MAX){
                    workflow = ww.3;
                    break;
                }
            }
        }
        if workflow == "A" {
            let sum : i64 = ratings.iter().sum();
            p1 += sum;
        }
    }

    let mut queue : VecDeque<(&str, [i64;8])> = VecDeque::new();
    queue.push_back(("in",[1,4000,1,4000,1,4000,1,4000]));
    let mut p2 = 0;
   
    while !queue.is_empty() {
        let current = queue.pop_front().unwrap();
        let workflow = current.0;
        let mut ranges = current.1;
        match workflow {
            "R" => continue,
            "A" => {
                let mut score = 1;
                for i in 0..4 {
                    score *= ranges[i*2+1]-ranges[i*2]+1;
                }
                p2 += score;
                continue;
            }
            _ => {
                let w = rules.get(workflow).unwrap();
                for ww in w {
                    if ww.0 == usize::MAX {
                        queue.push_back((ww.3, ranges));
                    } else {
                        queue.push_back((ww.3, update_within_ranges(ww.0, ww.1, ww.2, ranges)));
                        ranges = update_within_ranges(ww.0, !ww.1, if ww.1 {ww.2-1} else {ww.2+1}, ranges);
                    }
                }
            }
        }
    }

    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}