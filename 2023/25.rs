use std::fs;
use std::time::Instant;
use std::collections::HashMap;

fn bfs< 'a >( start : &'a str, mut end: &'a str, graph : & HashMap< &'a str, Vec< &'a str > > ) -> Vec< &'a str > {
    let mut reachable_components = Vec::new();
    let mut previous : HashMap< & str, & str > = HashMap::new();
    previous.insert( start,"" );
    reachable_components.push( start );
    let mut index = 0;
    while index < reachable_components.len() { // Do not use a queue and remove from it, you want to keep all reachables nodes for the final step
        let current = reachable_components[ index ];
        index += 1;
        if current == end {
            break;
        }
        for dest in graph.get( current ).unwrap() {
            if previous.get( dest ).is_none() {
                previous.insert( dest, current );
                reachable_components.push( dest );
            }
        }
    }
    if end != "" && previous.get( end ).is_none() {
        return reachable_components
    }
    end = *reachable_components.last().unwrap(); 
    let mut path = Vec::new();
    while end != "" {
        path.push( end );
        end = previous.get( end ).unwrap();
    }
    path
}


fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut graph : HashMap< & str, Vec< & str > > = HashMap::new();

    for line in data.lines() {
        let mut dest_iterator = line[ 5.. ].split( ' ' );
        let src = & line[ ..3 ];
        while let Some( dest ) = dest_iterator.next() {
            let mut vec = graph.entry( src ).or_insert( Vec::new() );
            vec.push( dest );
            vec = graph.entry( dest ).or_insert( Vec::new() );
            vec.push( src );
        }
    }    

    let total_components = graph.len();
    let mut start = "";
    let mut end = "";
    for ( k,_v ) in & graph {
        start = *k;
        break;
    }

    // Find the longest of the shortest paths between any pair of nodes. Remove all edges used in this path.
    // Repeat twice and you should have cut all bridges
    for _ in 0..3 {
        let path = bfs( start, end, & graph );
        end = path[ 0 ];
        let mut previous = end;
        for i in 1..path.len() {
            let current = path[ i ];
            let mut vec = graph.get_mut( current ).unwrap();
            vec.swap_remove(vec.iter().position( |x| *x == previous ).unwrap() );
            vec = graph.get_mut(previous).unwrap();
            vec.swap_remove(vec.iter().position( |x| *x == current ).unwrap() );
            previous = current;
        }
    }
    let group_count = bfs( start, end, & graph ).len();
    println!( "{}", ( total_components - group_count ) * group_count );
    let elapsed = now.elapsed();
    println!( "{} µs", elapsed.as_micros() );
} 

// Golfed python solution for kicks
/*
import math, networkx as nx
print(math.prod([len(subgraph) for subgraph in nx.spectral_bisection(nx.from_dict_of_lists({vertex:edges.split() for vertex,edges in [line.split(': ') for line in open('in.txt')]}))]))
*/