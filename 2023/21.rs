use std::fs;
use std::time::Instant;
use std::collections::{HashSet, VecDeque};
use num::Complex;

const DIRS : [Complex<isize>;4] = [Complex::new(1,0), Complex::new(-1,0), Complex::new(0,1), Complex::new(0,-1)]; 
const GRIDSIZE : isize = 131;
const STEPS_1 : isize = 64;
const STEPS_2 : isize = 26501365;

fn step( visited : HashSet< Complex<isize> >, walls : & HashSet< Complex<isize>> ) -> HashSet< Complex<isize>>{
    let mut queue : VecDeque<Complex<isize>> = VecDeque::from_iter(visited.iter().cloned());
    let mut new_visited = HashSet::new();
    while !queue.is_empty() {
        let current = queue.pop_front().unwrap();
        for dir in DIRS {
            let next = current+dir;
            let test = walls.get(&Complex::new(next.re.rem_euclid(GRIDSIZE), next.im.rem_euclid(GRIDSIZE)));
            if test.is_none() { 
                new_visited.insert(next);
            }
        }
    }
    new_visited
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut walls : HashSet< Complex<isize> > = HashSet::new();
    let mut visited : HashSet< Complex<isize> > = HashSet::new(); 
    for (y,line) in data.lines().enumerate() {
        for (x,col) in line.chars().enumerate() {
            if col == '#' {
                walls.insert(Complex::new(x as isize,y as isize));
            } else if col == 'S' {
                visited.insert(Complex::new(x as isize,y as isize));
            }
        }
    }
    
    let mut magic_values : Vec<isize> = Vec::new();
    let mut steps = 1;
    let mut p1 = 0;
    loop {
        visited = step(visited, &walls);
        if steps == STEPS_1 {
            p1 = visited.len();
        }
        if (steps % GRIDSIZE) == (STEPS_2 % GRIDSIZE) {
            magic_values.push(visited.len() as isize);
        }
        steps += 1;
        if magic_values.len() == 3 {
            break;
        }
    }
    // Each grid copy eventually reaches a "steady-state" where it alternates between 2 values. 
    // There are no walls on the column/row of the starting point, so cycles are based on the grid size
    // I tried a polynomial fit on wolfram alpha with values at GRIDSIZE*n % STEPS_2 and got a quadratic function
    // Manual solve:
    
    let cycles = STEPS_2 / GRIDSIZE;
    let c : isize = magic_values[0];
    let b : isize = (4*(magic_values[1]-magic_values[0]) + c - magic_values[2]) / 2;
    let a : isize = magic_values[1] - c - b;
    let p2 = a * cycles * cycles + b * cycles + c;
    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}