use std::fs;
use std::time::Instant;
use num::Complex;
use std::collections::{HashSet, VecDeque};
use std::cmp::max;
use std::thread;
use std::sync::Arc;

const GRIDSIZE : usize = 110;

fn energize( grid : &Vec<Vec<char>>, start : (Complex< i8 >, Complex< i8 >)) -> usize {
    let mut seen : HashSet< ( Complex< i8 >, Complex< i8 > ) > = HashSet::new();
    let mut queue : VecDeque< (Complex< i8 >, Complex< i8 >) > = VecDeque::new();
    queue.push_back(start);

    while let Some( mut current ) = queue.pop_front() {
        if current.0.re < 0 || current.0.re >= GRIDSIZE as i8
        || current.0.im < 0 || current.0.im >= GRIDSIZE as i8 {
            continue;
        }
        seen.insert(current);
        let c =  grid[current.0.im as usize][current.0.re as usize];
        match c {
            '/' | '\\' =>  
            {
                if ( c == '/' ) ^ ( current.1.im == 0 ) {
                    current.1 *= Complex::new(0,1);
                } else {
                    current.1 *= Complex::new(0,-1);
                }
                current.0 += current.1;
                if seen.get(&current).is_none() {
                    queue.push_back(current);
                }
            }
            '|' | '-' => 
                if ( c == '|' ) ^ ( current.1.re == 0 ) { // Splitting the beam
                    let children : [(Complex< i8 >, Complex< i8 >);2] = 
                    [ ( current.0, current.1*Complex::new(0,1) ), ( current.0, current.1*Complex::new(0,-1) )];
                    for mut child in children {
                        child.0 += child.1;
                        if seen.get(&child).is_none() {
                            queue.push_back(child);
                        }
                    }
                } else {
                    current.0 += current.1;
                    if seen.get(&current).is_none() {
                        queue.push_back(current);
                    }
                }
            _ => 
            {
                current.0 += current.1;
                if seen.get(&current).is_none() {
                    queue.push_back(current);
                }
            }
        }
    }
    let mut tiles : HashSet< Complex< i8 > > = HashSet::new();
    for ray in seen {
        tiles.insert(ray.0);
    }
    return tiles.len();
}

fn row_bruteforce( grid : &Vec<Vec<char>>, row_start : i8, step : i8) -> usize {
    let mut p2 : usize = 0;
    for i in 0..GRIDSIZE {
        p2 = max(p2, energize(&grid, (Complex::new(row_start, i as i8), Complex::new(step,0))));
    }
    return p2;
}

fn column_bruteforce( grid : &Vec<Vec<char>>, column_start : i8, step : i8) -> usize {
    let mut p2 : usize = 0;
    for i in 0..GRIDSIZE {
        p2 = max(p2, energize(&grid, (Complex::new(i as i8, column_start), Complex::new(0,step))));
    }
    return p2;
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut grid : Vec<Vec<char>> = Vec::new();
    for line in data.lines() {
        grid.push(line.chars().collect());
    }
    assert_eq!(GRIDSIZE, grid.len());
    let p1 = energize(&grid, (Complex::new(0,0), Complex::new(1,0)));
    let grid0 = Arc::new(grid);
    let grid1 = Arc::clone(&grid0);
    let grid2 = Arc::clone(&grid0);
    let grid3 = Arc::clone(&grid0);
    let h_0 = thread::spawn(move || row_bruteforce(&grid0.to_vec(), 0, 1));
    let h_1 = thread::spawn(move || row_bruteforce(&grid1.to_vec(), (GRIDSIZE-1) as i8, -1));
    let h_2 = thread::spawn(move || column_bruteforce(&grid2.to_vec(), 0, 1));
    let h_3 = thread::spawn(move || column_bruteforce(&grid3.to_vec(), (GRIDSIZE-1) as i8, -1));
    let mut p2 = max(h_0.join().unwrap(), h_1.join().unwrap());
    p2 = max(p2, h_2.join().unwrap());
    p2 = max(p2, h_3.join().unwrap());

    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
} 
