use std::fs;
use std::collections::{HashMap, HashSet};
use num::Complex;

const NEIGHBOURS : [Complex< i16 >;4] = [Complex::new(0,1),
                                         Complex::new(0,-1),
                                         Complex::new(1,0),
                                         Complex::new(0,-1)];

fn main() {
    let pipes : HashMap< char, [Complex< i16 >;2]> = HashMap::from([
        ('|', [Complex::new(0,1), Complex::new(0,-1)]),
        ('-', [Complex::new(1,0), Complex::new(-1,0)]),
        ('L', [Complex::new(0,-1), Complex::new(1,0)]),
        ('J', [Complex::new(-1,0), Complex::new(0,-1)]),
        ('7', [Complex::new(0,1), Complex::new(-1,0)]),
        ('F', [Complex::new(1,0), Complex::new(0,1)]),
    ]);
    
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut maze_loop : HashSet< Complex< i16 > > = HashSet::new();
    let mut start : Complex< i16 > = Complex::new(0,0);
    let mut grid : Vec< Vec < char > > = Vec::new();
    for line in data.lines() {
        grid.push(line.chars().collect());
    }
    for (y,row) in grid.iter().enumerate() {
        for (x,col) in row.iter().enumerate() {
            let coord = Complex::new(x as i16,y as i16);
            if *col == 'S' {
                maze_loop.insert(coord);
                start = coord;
            }
        }
    }

    assert_ne!(start, Complex::new(0,0));
    let mut current = start;
    let mut next = current;
    let mut found : bool = false;

    for neighbour in NEIGHBOURS { // find the next pipe from starting point
        if found {
            break;
        }
        next = current + neighbour;
        let pipe = grid[next.im as usize][next.re as usize];
        if  pipe == '.' {
            continue;
        }
        for dir in pipes[&pipe] {
            if next + dir == current {
                maze_loop.insert(next);
                found = true;
                break;
            }
        }
    }

    let start = current;
    let mut prev = start;
    current = next;

    'search: loop { // Follow the path until you loop
        for dir in pipes[&grid[current.im as usize][current.re as usize]] {
            if (current + dir) != prev {
                prev = current;
                current += dir;
                if maze_loop.get(&current).is_some() {
                    break 'search;
                } else {
                    maze_loop.insert(current);
                    break;
                }
            }
        }      
    }
    let mut p2 : i16 = 0;
    
    for (y,row) in grid.iter().enumerate() {
        for (x,_col) in row.iter().enumerate() {
            let mut point : Complex< i16 > = Complex::new(x as i16,y as i16);
            if maze_loop.get(&point).is_none() {
                let mut hits : i16 = 0;
                while point.re < grid[0].len() as i16 {
                    point += 1; // point inside polygon raycast algorithm, we raycast right
                    if maze_loop.get(&point).is_some() {
                        match grid[point.im as usize][point.re as usize] {
                            '|' | 'F' | '7' => hits += 1, // ignoring L/J allows "squeezing" between pipes
                            _ => (),
                        } // Note that this assumes a certain shape for S ¯\_(ツ)_/¯
                    }
                }
                p2 += hits & 1;
            }
        }
    }
    println!("{}\n{}", maze_loop.len()/2, p2);
} 
