use std::fs;
use std::time::Instant;
use std::collections::{HashSet, HashMap};
use std::cmp;

fn chain_reaction( supports : & HashMap<i16, HashSet<i16>>, supported_by : & mut HashMap<i16, HashSet<i16>>, id : i16) -> isize {
    let mut count = 0;
    let mut stack : Vec<i16> = Vec::new();
    stack.push(id);
    while !stack.is_empty() {
        let current = stack.pop().unwrap();
        if let Some(supported_bricks) = supports.get(&current) { 
            for supported_brick in supported_bricks {
                let supporting_bricks = supported_by.get_mut(supported_brick).unwrap();
                supporting_bricks.remove(&current);
                if supporting_bricks.len() == 0 {
                    stack.push(*supported_brick);
                    count += 1;
                }
            }
        }
    }
    count
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" ).replace("~", ",");
    let mut supported_by : HashMap< i16, HashSet<i16>> = HashMap::new();
    let mut supports : HashMap<i16, HashSet<i16>> = HashMap::new();
    let mut height_map : HashMap< (i16,i16), (i16,i16) > = HashMap::new();
    let mut bricks : Vec< [i16;7] > = Vec::new();
    let mut brickset : HashSet< i16 > = HashSet::new();
    let mut unsafe_bricks : HashSet<i16> = HashSet::new();
    let mut p2 = 0;
    for (i,line) in data.lines().enumerate() {
        let mut line_iterator = line.split(',');
        let mut brick : [i16;7] = [0;7];
        let mut j = 0;
        while let Some(x) = line_iterator.next() {
            brick[j] = x.parse::<i16>().unwrap();
            j += 1;
        }
        brick[j] = i as i16;
        assert!(brick[0] <= brick[3]);
        assert!(brick[1] <= brick[4]);
        assert!(brick[2] <= brick[5]);
        bricks.push(brick);
        brickset.insert(i as i16);
    }
    bricks.sort_by_key(|x| x[2]);

    for brick in &bricks {
        let mut falls_to : i16 = 0;
        for x in brick[0]..brick[3]+1 {
            for y in brick[1]..brick[4]+1 {
                falls_to = cmp::max(falls_to, height_map.entry((x,y)).or_insert((0,-1)).0); //height
            }
        }
        let brick_height = brick[5] - brick[2] + 1;
        for x in brick[0]..brick[3]+1 {
            for y in brick[1]..brick[4]+1 {
                let previous_height = height_map.get_mut(&(x,y)).unwrap();
                if previous_height.0 == falls_to {
                    let supporting_bricks = supported_by.entry(brick[6]).or_insert(HashSet::new());
                    supporting_bricks.insert(previous_height.1); // id
                    let supported_bricks = supports.entry(previous_height.1).or_insert(HashSet::new());
                    supported_bricks.insert(brick[6]);
                }
                *previous_height = (falls_to+brick_height, brick[6]);
            }
        }
    }
  
    for (_supported_brick,supporting_bricks) in &supported_by {
        if supporting_bricks.len() == 1 && brickset.get(supporting_bricks.iter().next().unwrap()).is_some() {
            unsafe_bricks.extend(supporting_bricks);
        }
    }
    
    for unsafe_brick in &unsafe_bricks {
        p2 += chain_reaction(& supports, &mut supported_by.clone(), *unsafe_brick);
    }

    println!("{}\n{}", bricks.len() - unsafe_bricks.len(), p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}