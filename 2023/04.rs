use std::fs;
use std::collections::HashSet;
fn main() {
    let data = fs::read_to_string("./in.txt").expect("Unable to read file");
    let draws : Vec<&str> = data.split("\n").collect();
    let mut counts : Vec<i32> = Vec::with_capacity(draws.len());
    counts.resize(draws.len(), 1);
    let mut p1 : i32 = 0;
    let mut p2 : i32 = 0;
    for (i,draw) in draws.iter().enumerate() {
        let relevant_info : &str = draw.split(": ").collect::<Vec<_>>()[1];
        let cards : Vec<_>= relevant_info.split(" | ").collect();
        let mut winning_set : HashSet<i32> = HashSet::new();
        let mut hand_set : HashSet<i32> = HashSet::new();
        for card in cards[0].split(' ') {
        let test = card.parse::<i32>();
            if test.is_ok() {
                winning_set.insert(test.unwrap());
            }
        }
        for card in cards[1].split(' ') {
            let test = card.parse::<i32>();
            if test.is_ok() {
                hand_set.insert(test.unwrap());
            }
        }
        let matches : u32 = winning_set.intersection(&hand_set).count() as u32;
        if matches > 0 {
            let base : i32 = 2;
            p1 += base.pow(matches - 1);
            for j in 1..matches+1 {
                counts[i + j as usize] += counts[i];
            }
        }
    }
    for count in counts {
        p2 += count;
    }
    println!("{}\n{}", p1, p2);
}