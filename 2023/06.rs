const TIMES : [ f64; 4 ] = [ 51., 92., 68., 90. ];
const DISTANCES : [ f64; 4 ] = [ 222., 2031., 1126., 1225. ];
const P2_TIME : f64 = 51926890.;
const P2_DISTANCE : f64 = 222203111261225.;

fn get_number_of_winning_plays( time: f64, distance: f64 ) -> f64 {
    let delta : f64 = time*time - 4.*( distance + 1. );
    let x1 = (time + f64::sqrt(delta))/2.;
    let x2 = (time - f64::sqrt(delta))/2.;
    return x1.ceil() - x2.floor() - 1.;
}
fn main() {
    let mut p1 : f64 = 1.;
    for i in 0..4 {
        p1 *= get_number_of_winning_plays(TIMES[i], DISTANCES[i]);
    }
    println!("{}\n{}", p1, get_number_of_winning_plays(P2_TIME, P2_DISTANCE));
}