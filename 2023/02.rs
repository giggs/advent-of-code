use std::fs;
use std::collections::HashMap;
use std::cmp::max;

fn main() {
    let data = fs::read_to_string("./in.txt").expect("Unable to read file");
    let lines = data.split("\r\n");
    let games : Vec<&str> = lines.collect();
    let mut p1 : u32 = 0;
    let mut p2 : u32 = 0;
    let limits : HashMap<String, u8> = HashMap::from([
        (" red".to_string(), 12 as u8),
        (" green".to_string(), 13 as u8),
        (" blue".to_string(), 14 as u8),
    ]);

    for (game_id,game) in games.iter().enumerate() {
        let discard : Vec<&str> = game
            .split(": ")
            .collect();
        let draws : Vec<&str> = discard[1]
            .split("; ")
            .collect();
        let mut red : u8 = 0;
        let mut green : u8 = 0;
        let mut blue : u8 = 0;
        let mut possible : bool = true;
        for draw in draws.iter() {
            let cubes : Vec<&str> = draw
                .split(", ")
                .collect();
                for cube in cubes.iter() {
                    let (n, color) = cube.split_at(cube.find(' ').unwrap());
                    let number = n.parse::<u8>().unwrap();
                    if number > limits[color] {
                        possible = false;
                    }
                    match color {
                        " red" => red = max(red, number),
                        " green" => green = max(green, number),
                        " blue" => blue = max(blue, number),
                        _ => println!("Error, expected one of the three listed colors")
                    }
                }

        }
        if possible == true {
            p1 += (game_id+1) as u32;
        }
        p2 += red as u32*green as u32 *blue as u32;
    }
    println!("{}\n{}", p1, p2);
}
