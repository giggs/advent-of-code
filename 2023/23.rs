use std::fs;
use std::time::Instant;
use std::collections::{HashMap, HashSet, VecDeque};
use std::cmp::max;

const DIRS : [(i16, i16);4] = [(1,0), (-1,0), (0,1), (0,-1)];
const GRIDSIZE : usize = 141;

fn main() {
    let now = Instant::now();
    let wrong_ways : HashMap< char, (i16, i16) > = [('>', (0,-1)), ('v', (-1,0))].iter().cloned().collect();
    let mut nodes : HashMap< (i16, i16), HashSet<((i16,i16),usize)>> = HashMap::new();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut grid : Vec<Vec<char>> = Vec::new();
    let mut start : (i16, i16) = (0,0);
    let mut goal : (i16, i16) = (0,0);
    for (y,row) in data.lines().enumerate() {
        grid.push(row.chars().collect());
        for (x,col) in row.chars().enumerate() {
            if y == 0 && col == '.' {
                start = (y as i16,x as i16);
            } else if y == GRIDSIZE-1 && col == '.' {
                goal = (y as i16,x as i16);
            }
        }
    }
    let mut p1 = 0;
    let mut p2 = 0;
    let mut queue : VecDeque<((i16,i16), (i16,i16), usize, (i16,i16), usize)> = VecDeque::new();
    queue.push_back((start, start, 0, start, 0));

    while !queue.is_empty() {
        let (tile, prev, steps, prev_junction, prev_steps) = queue.pop_front().unwrap();
        if tile == goal {
            p1 = max(p1, steps);
            let mut set = nodes.entry(tile).or_insert(HashSet::new());
            set.insert((prev_junction, steps - prev_steps));
            set = nodes.entry(prev_junction).or_insert(HashSet::new());
            set.insert((tile, steps - prev_steps));
            continue;
        }
        let mut turns : HashSet<(i16,i16)> = HashSet::new();
        let mut new_queue : VecDeque<((i16,i16), (i16,i16), usize, (i16,i16), usize)> = VecDeque::new();
        for dir in DIRS {
            let mut next = (tile.0 + dir.0, tile.1 + dir.1);
            if next.0 < 0 || next.0 == GRIDSIZE as i16 {
                continue;
            }
            let dest : char = grid[next.0 as usize][next.1 as usize];
            if dest == '#' {
                continue;
            }
            turns.insert(next);
            if next == prev {
                continue;
            }
            let mut new_steps = steps + 1;
            if let Some(x) = wrong_ways.get(&dest) {
                if *x == dir {
                    continue;
                } else {
                    next.0 -= x.0;
                    next.1 -= x.1;
                    new_steps += 1;
                }
            }
            new_queue.push_back((next, tile, new_steps, prev_junction, prev_steps));
        }
        if turns.len() != 2 {
            if tile != prev_junction {
                let mut set = nodes.entry(prev_junction).or_insert(HashSet::new());
                set.insert((tile, steps-prev_steps));
                set = nodes.entry(tile).or_insert(HashSet::new());
                set.insert((prev_junction, steps - prev_steps));
            }
            for mut n in new_queue {
                n.3 = tile;
                n.4 = steps;
                queue.push_back(n);
            }
        } else {
            for n in new_queue {
                queue.push_back(n);
            }
        }
    }


    let junctions : Vec<(i16,i16)> = Vec::from_iter(nodes.keys().cloned());
    let mut stack : Vec<((i16,i16),usize,u64)> = Vec::new();
    stack.push((start, 0, 0_u64));

    // A possible optimization would be to cancel a path when the sum of the 
    // remaining edges can't beat the current best? 
    while !stack.is_empty() {
        let (tile, steps, mut seen) = stack.pop().unwrap();
        if tile == goal {
            p2 = max(steps, p2);
            continue;
        }
        seen |= 1 << junctions.iter().position(|x| *x == tile).unwrap();
        for (next_tile, inc_steps) in nodes.get(&tile).unwrap().iter() {
            if (seen & (1 << junctions.iter().position(|x| *x == *next_tile).unwrap())) == 0{
                stack.push((*next_tile, steps + *inc_steps, seen));
            }
        }
    }

    println!("{}\n{}", p1, p2);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}
