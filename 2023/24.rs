use std::fs;
use std::time::Instant;
use num::abs;

const AREA_MIN : i128 = 200000000000000;
const AREA_MAX : i128 = 400000000000000;

fn intersect(x1 : i128, x2 : i128, x3 : i128, x4 : i128,
            y1 : i128, y2 : i128, y3 : i128, y4 : i128) -> (i128, i128) {
    let denominator = ((x1-x2)*(y3-y4)) - ((y1-y2)*(x3-x4));
    let mut px = 0;
    let mut py = 0;
    if denominator != 0 {
        px = ((x1*y2 - y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
        py = ((x1*y2 - y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
    }
    (px, py)
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" ).replace(" @", ",");
    let mut hailstones : Vec<[i128;6]> = Vec::new();
    for line in data.lines() {
        let mut line_iterator = line.split(", ");
        let mut hailstone : [i128;6] = [0;6];
        let mut i = 0;
        while let Some(x) = line_iterator.next() {
            hailstone[i] = x.parse::<i128>().unwrap();
            i += 1;
        }
        hailstones.push(hailstone);
    }
    let mut p1 = 0;
    let mut twins : Vec<Vec<(i128,i128)>> = Vec::new();
    for _ in 0..3 {
        twins.push(Vec::new());
    }
    for (i,a) in hailstones.iter().enumerate() {
        for j in i+1..hailstones.len() {
            let b = hailstones[j];
            let x1 = a[0];
            let x2 = x1 + a[3];
            let x3 = b[0];
            let x4 = x3 + b[3];
            let y1 = a[1];
            let y2 = y1 + a[4];
            let y3 = b[1];
            let y4 = y3 + b[4];
            let (px, py) = intersect(x1, x2, x3, x4, y1, y2, y3, y4);
            if px != 0 && py != 0 {
                if (x1 < px) == (x1 < x2) && (x3 < px) == (x3 < x4) {
                if AREA_MIN <= px && AREA_MIN<=py && px<=AREA_MAX && py<=AREA_MAX {
                        p1 += 1
                    }
                }
            }
            if a[3] == b[3] {
                twins[0].push((abs(x1-x3), a[3]));
            } else if a[4] == b[4] {
                twins[1].push((abs(y1-y3), a[4]));
            } else if a[5] == b[5] {
                twins[2].push((abs(a[2]-b[2]), a[5]));
            }
        }
    }
    let mut rock_speed : [i128;3] = [0;3];
    // Some stones have the same velocity on a given axis.
    // The rock can only hit it if distance % (rockspeed-stonespeed) == 0
    // Bruteforce the rock speed to find values that match on each axis
    for i in 0..3 {
        'next_velocity:
        for value in 0..i128::MAX {
            for speed in [-value, value] {
                let mut count = 0;
                for test in twins[i].iter() {
                    let diff = speed-test.1;
                    if diff != 0 && test.0.rem_euclid(diff) == 0 {
                        count += 1;
                    }
                }
                if count == twins[i].len() {
                    rock_speed[i] = speed;
                    break 'next_velocity;
                }
            }
        }
    }
    // From the rock's point of view, every hailstone comes to it
    // Plot the paths of 2 stones as seen by the rock
    // The intersection of those paths must be the rock's position
    let a = hailstones[0];
    let b = hailstones[1];
    let dvxa = a[3]-rock_speed[0];
    let dvxb = b[3]-rock_speed[0];
    let dvya = a[4]-rock_speed[1];
    let dvyb = b[4]-rock_speed[1];
    let dvza = a[5]-rock_speed[2];
    let dvzb = b[5]-rock_speed[2];
    let x1 = a[0]+dvxa;
    let x2 = x1+dvxa;
    let x3 = b[0]+dvxb;
    let x4 = x3+dvxb;
    let y1 = a[1]+dvya;
    let y2 = y1+dvya;
    let y3 = b[1]+dvyb;
    let y4 = y3+dvyb;
    let z1 = a[2]+dvza;
    let z2 = z1+dvza;
    let z3 = b[2]+dvzb;
    let z4 = z3+dvzb;

    let (rx,ry) = intersect(x1, x2, x3, x4, y1, y2, y3, y4);
    let (check, rz) = intersect(y1, y2, y3, y4, z1, z2, z3, z4);
    assert_eq!(check, ry);

    println!("{}\n{}", p1, rx+ry+rz);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}