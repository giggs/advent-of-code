use std::process::exit;
use std::{fs, str::SplitWhitespace};
use std::iter::Skip;

fn get_numbers_from_line(line_iterator: & mut Skip<SplitWhitespace<'_>>, vector: & mut Vec<u64>) {
    while let Some(x) = line_iterator.next() {
        let test = x.parse::<u64>();
        if test.is_ok() {
            vector.push(test.unwrap());
        }
    }
}

fn process_seeds(seeds: & mut Vec<u64>, ranges: & Vec<Vec<u64>>, reverse : bool) {
    for i in 0..seeds.len() {
        let mut seed : u64 = seeds[i];
        for range in ranges {
            let mut destination : u64 = 0;
            let mut source : u64 = 0;
            if reverse == false {
                destination = range[0];
                source = range[1];
            } else {
                source = range[0];
                destination = range[1];
            }
            let length : u64 = range[2];
            if seed >= source && seed < (source+length) {
                seed = destination + seed - source;
                seeds[i] = seed;
                break;
            }
        }
    }
}

fn main() {
    let input = fs::read_to_string("./in.txt").expect("Unable to read file");
    let categories : Vec<&str> = input.split("\n\n").collect();
    let mut seed_string = categories[0].split_whitespace().skip(1);
    let mut seeds : Vec<u64> = Vec::new();
    get_numbers_from_line(&mut seed_string, & mut seeds);
    seed_string = categories[0].split_whitespace().skip(1);
    let mut p2_ranges : Vec<u64> = Vec::new();
    get_numbers_from_line(&mut seed_string, &mut p2_ranges);
    let transition_count : usize = categories.len();
    let mut transitions : Vec<Vec<Vec<u64>>> = Vec::new();
    for transition in 1..transition_count {
        let mut range_iterator = categories[transition].lines().skip(1);
        let mut ranges : Vec<Vec<u64>> = Vec::new();
        while let Some(x) = range_iterator.next() {
            let mut range : Vec<u64> = Vec::new();
            get_numbers_from_line(& mut x.split_whitespace().skip(0), & mut range);
            ranges.push(range);
        }
        process_seeds(&mut seeds, &ranges, false);
        transitions.push(ranges);
    }
    let p1 : &u64 = seeds.iter().min().unwrap();
    println!("{}", p1);
    // I really didn't feel like doing interval math
    use std::time::Instant;
    let now = Instant::now();
    for seed in 0..*p1 {
        let mut dummy_vec : Vec<u64> = vec![seed];
        for transition in transitions.iter().rev() {
            process_seeds(& mut dummy_vec, &transition, true);
        }
        let mut i : usize = 0;
        while i < p2_ranges.len() {
            if dummy_vec[0] > p2_ranges[i] && dummy_vec[0] < (p2_ranges[i] + p2_ranges[i+1]-1 ) {
                println!("{}", seed);
                let elapsed = now.elapsed();
                println!("{:.2?}", elapsed);
                return;
            }
            i += 2;
        }
    }
}
