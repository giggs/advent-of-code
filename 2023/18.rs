use std::fs;
use std::time::Instant;
use std::collections::HashMap;
use num::Complex;

fn main() {
    let now = Instant::now();
    let dirs : HashMap< char, Complex<f64> > = HashMap::from([ 
        ('R', Complex::new(1.,0.)), ('D', Complex::new(0.,1.)), ('L', Complex::new(-1.,0.)), ('U', Complex::new(0.,-1.)),
        ('0', Complex::new(1.,0.)), ('1', Complex::new(0.,1.)), ('2', Complex::new(-1.,0.)), ('3', Complex::new(0.,-1.))   
    ]);
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut p1 = 1.;
    let mut p2 = 1.;
    let mut pos1 : f64 = 0.;
    let mut pos2 : f64 = 0.;
    for line in data.lines() {
        let mut line_iterator = line[2..].split(' ');
        let d1 = line.as_bytes()[0];
        let n1 = line_iterator.next().unwrap().parse::<f64>().unwrap();
        let color = line_iterator.next().unwrap();
        let d2 = color.as_bytes()[color.len()-2];
        let n2 = i64::from_str_radix(&color[2..color.len()-2], 16).unwrap() as f64;
        let step1 = dirs.get(&(d1 as char)).unwrap() * n1;
        let step2 = dirs.get(&(d2 as char)).unwrap() * n2;
        pos1 += step1.re;
        pos2 += step2.re;
        p1 += pos1*step1.im + n1/2.; // Simplified shoelace formula -> trapezoid formula
        p2 += pos2*step2.im + n2/2.; 
    }
    println!("{}\n{}", p1 as i64, p2 as i64);
    let elapsed = now.elapsed();
    println!("{} µs", elapsed.as_micros());
}