use std::fs;
use std::time::Instant;

fn parse(grids : &Vec<Vec<Vec<u8>>>) -> (i32,i32) {
    let mut p1 : i32 = 0;
    let mut p2 : i32 = 0;
    for pattern in grids.iter() {
        for i in 1..pattern.len() as i32 {
            let mut smudges = 0;
            let mut up = i as usize;
            let mut down : i32 = i-1;
            while up < pattern.len() && down >= 0 {
                for j in 0..pattern[up].len() {
                    smudges += (pattern[up][j] != pattern[down as usize][j]) as i32;
                }
                if smudges > 1 {
                    break;
                }
                up += 1;
                down -= 1;
            }
            match smudges {
                1 => p2 += i*100,
                0 => p1 += i*100,
                _ => ()
            }
        }
        for j in 1..pattern[0].len() as i32 {
            let mut smudges = 0;
            let mut right = j as usize;
            let mut left = j-1;
            while right < pattern[0].len() && left >= 0 {
                for i in 0..pattern.len() {
                    smudges += (pattern[i][left as usize] != pattern[i][right]) as i32;
                }
                if smudges > 1 {
                    break;
                }
                right += 1;
                left -= 1;
            }
            match smudges {
                1 => p2 += j,
                0 => p1 += j,
                _ => ()
            }
        }
    }
    (p1,p2)
}

fn main() {
    let now = Instant::now();
    let data = fs::read_to_string( "./in.txt" ).expect( "Unable to read file" );
    let mut data_iterator = data.split("\n\n");
    let mut grids : Vec<Vec<Vec<u8>>> = Vec::new();
    while let Some(pattern) = data_iterator.next() {
        let mut grid : Vec<Vec<u8>> = Vec::new();
        for line in pattern.lines() {
            grid.push(line.as_bytes().to_vec());
        }
        grids.push(grid);
    }
    let result = parse(&grids);
    println!("{}\n{}", result.0, result.1);
    let elapsed = now.elapsed();
    println!("{}", elapsed.as_micros());
} 