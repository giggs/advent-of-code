#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

#define SIZE 10

enum type_flags
{
	radiation =	  1,
	fire =	      1 << 1,
	slashing =    1 << 2,
	cold =        1 << 3,
	bludgeoning = 1 << 4
};

enum faction
{
	infection,
	immune_system,
};

struct group
{
	int units;
	int hit_points;
	int attack_damage;
	int effective_power;
	int initiative;
	unsigned int attack_type;
	unsigned int weak;
	unsigned int immune;
	faction faction;
	group* target;
};

group immune_system_units[SIZE] = {};
group infection_units[SIZE] = {};
group* attack_order[2* SIZE] = {};

type_flags type_parse(const std::string& s)
{
	if (s == "radiation")
	{
		return radiation;
	}
	else if (s == "fire")
	{
		return fire;
	}
	else if (s == "slashing")
	{
		return slashing;
	}
	else if (s == "cold")
	{
		return cold;
	}
	else
	{
		return bludgeoning;
	}
}

bool operator<(const group a, const group b)
{
	if (a.effective_power != b.effective_power)
	{
		return a.effective_power > b.effective_power;
	}
	else
	{
		return a.initiative > b.initiative;
	}
}

void parse_faction(std::ifstream& in)
{
	std::string line;
	getline(in, line);
	faction faction = infection;
	group* groups = infection_units;
	if (line == "Immune System:")
	{
		faction = immune_system;
		groups = immune_system_units;
	}
	getline(in, line);
	
	for (int i = 0; line != ""; getline(in, line), i++)
	{
		std::istringstream sline(line);
		std::string temp;
		group & current_group = groups[i];
		current_group.faction = faction;
		sline >> current_group.units >> temp >> temp >> temp >> current_group.hit_points
			>> temp >> temp >> temp;
		if (temp.front() == '(')
		{
			temp = temp.substr(1, temp.size());
			if (temp == "weak")
			{
				while (temp.back() != ';' && temp.back() != ')')
				{
					sline >> temp;
					if (temp.back() == ',')
					{
						temp.pop_back();
						current_group.weak |= type_parse(temp);
					}
				}
				if (temp.back() == ';')
				{
					temp.pop_back();
					current_group.weak |= type_parse(temp);
					sline >> temp;
					while (temp.back() != ')')
					{
						sline >> temp;
						if (temp.back() == ',')
						{
							temp.pop_back();
							current_group.immune |= type_parse(temp);
						}
					}
					temp.pop_back();
					current_group.immune |= type_parse(temp);
				}
				else
				{
					temp.pop_back();
					current_group.weak |= type_parse(temp);
				}
				sline >> temp;
			}
			else
			{
				while (temp.back() != ';' && temp.back() != ')')
				{
					sline >> temp;
					if (temp.back() == ',')
					{
						temp.pop_back();
						current_group.immune |= type_parse(temp);
					}
				}
				if (temp.back() == ';')
				{
					temp.pop_back();
					current_group.immune |= type_parse(temp);
					sline >> temp;
					while (temp.back() != ')')
					{
						sline >> temp;
						if (temp.back() == ',')
						{
							temp.pop_back();
							current_group.weak |= type_parse(temp);
						}
					}
					temp.pop_back();
					current_group.weak |= type_parse(temp);
				}
				else
				{
					temp.pop_back();
					current_group.immune |= type_parse(temp);
				}
				sline >> temp;
			}
		}
		
		sline >> temp >> temp >> temp >> temp;
		sline >> current_group.attack_damage >> temp;
		current_group.attack_type = type_parse(temp);
		sline >> temp >> temp >> temp >> current_group.initiative;
		current_group.effective_power = current_group.units * current_group.attack_damage;
		current_group.target = nullptr;
	}
}

void target_selection(group (&attackers)[SIZE], group (&defenders)[SIZE], bool (&targeted_indexes)[SIZE])
{
	for (group& attacker : attackers)
	{
		if (attacker.units == 0)
		{
			continue;
		}
		int potential_damage = 0;
		int potential_target = 11;
		
		for (int i = 0; i < SIZE; i++)
		{
			group defender = defenders[i];
			if (defender.units == 0 || targeted_indexes[i] == true)
			{
				continue;
			}
			int group_damage;
			if (attacker.attack_type & defender.immune)
			{
				group_damage = 0;
			}
			else if (attacker.attack_type & defender.weak)
			{
				group_damage = attacker.effective_power * 2;
			}
			else
			{
				group_damage = attacker.effective_power;
			}
			if (group_damage > potential_damage)
			{
				potential_damage = group_damage;
				potential_target = i;
				attacker.target = &defenders[i];
			}
			else if (group_damage == potential_damage && group_damage != 0)
			{
				if (defenders[i].effective_power > defenders[potential_target].effective_power)
				{
					potential_target = i;
					attacker.target = &defenders[i];
				}
				else if (defenders[i].effective_power == defenders[potential_target].effective_power
					&& defenders[i].initiative > defenders[potential_target].initiative)
				{
					potential_target = i;
					attacker.target = &defenders[i];
				}
			}
		}
		if (potential_target != 11)
		{
			targeted_indexes[potential_target] = true;
		}
	}

}

struct outcome
{
	bool victory;
	int units;
};

outcome simulate_combat(int boost, group(&infection_units)[SIZE], group(&immune_system_units)[SIZE], group* (&attack_order)[2 * SIZE])
{
	bool fighting = true;
	bool stalemate = true;

	while (fighting)
	{
		stalemate = true;
		std::sort(std::begin(immune_system_units), std::end(immune_system_units));
		std::sort(std::begin(infection_units), std::end(infection_units));

		bool targeted_immune_groups[SIZE] = {};
		bool targeted_infection_groups[SIZE] = {};

		target_selection(infection_units, immune_system_units, targeted_immune_groups);
		target_selection(immune_system_units, infection_units, targeted_infection_groups);

		for (group& g : infection_units)
		{
			attack_order[-(g.initiative - 2 * SIZE)] = &g;
		}

		for (group& g : immune_system_units)
		{
			attack_order[-(g.initiative - 2 * SIZE)] = &g;
		}

		for (group* attacker : attack_order)
		{
			if (attacker->units < 1)
			{
				continue;
			}
			if (attacker->target == nullptr)
			{
				continue;
			}
			else
			{
				group* defender = attacker->target;
				int effective_damage = attacker->units * attacker->attack_damage;
				if (attacker->attack_type & defender->weak)
				{
					effective_damage *= 2;
				}
				if (effective_damage >= defender->hit_points)
				{
					stalemate = false;
					defender->units -= effective_damage / defender->hit_points;
				}
				if (defender->units < 0)
				{
					defender->units = 0;
				}
			}
		}
		
		if (stalemate)
		{
			fighting = false;
			break;
		}

		int alive = 0;
		for (group& group : infection_units)
		{
			if (group.units == 0)
			{
				continue;
			}
			group.target = nullptr;
			group.effective_power = group.units * group.attack_damage;
			alive++;
		}

		if (alive == 0)
		{
			break;
		}

		fighting = false;
		for (group& group : immune_system_units)
		{
			if (group.units == 0)
			{
				continue;
			}
			group.target = nullptr;
			group.effective_power = group.units * group.attack_damage;
			fighting = true;
		}
	}
	outcome outcome = {};
	outcome.victory = false;
	if (!stalemate)
	{
		for (group* g : attack_order)
		{
			if (g->units > 0)
			{
				if (g->faction == immune_system)
				{
					outcome.victory = true;
				}
				outcome.units += g->units;
			}
		}
	}
	return outcome;
}

int main()
{
	std::ifstream file;
	file.open("Day 24/24.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file";
		return -1;
	}

	parse_faction(file);
	parse_faction(file);

	int boost = 0;
	outcome outcome = {};
	while (!outcome.victory)
	{
		group dup_immune_system_units[SIZE] = {};
		group dup_infection_units[SIZE] = {};
		group* dup_attack_order[2 * SIZE] = {};

		for (int i = 0; i < SIZE; i++)
		{
			dup_immune_system_units[i] = immune_system_units[i];
			dup_immune_system_units[i].attack_damage += boost;
			dup_immune_system_units[i].effective_power = 
				dup_immune_system_units[i].attack_damage * dup_immune_system_units[i].units;
			dup_infection_units[i] = infection_units[i];
		}

		outcome = simulate_combat(boost, dup_infection_units, dup_immune_system_units, dup_attack_order);
		if (boost == 0)
		{
			std::cout << outcome.units << " ";
		}
		boost++;
	}

	std::cout << outcome.units;
	
	return 0;
}