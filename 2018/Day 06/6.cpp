#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

typedef struct point {
	int x;
	int y;
	int count;
};

typedef struct center {
	point* center;
	int distance;
};

int points = 0;
int min_x = 1000;
int max_x = 0;
int min_y = 1000;
int max_y = 0;
std::vector<point> areas;

int find_closest(int x, int y, center& closest)
{
	int distance_sum = 0;
	for (int i = 0; i < points; i++)
	{			
		int dx = areas[i].x - x;
		int dy = areas[i].y - y;
		dx = dx < 0 ? -dx : dx;
		dy = dy < 0 ? -dy : dy;
		int manhattan = dx + dy;
		distance_sum += manhattan;
		if (manhattan < closest.distance)
		{
			closest.distance = manhattan;
			closest.center = &areas[i];
		}
		else if (manhattan == closest.distance)
		{
			closest.distance = manhattan;
			closest.center = NULL;
		}
	}
	return distance_sum < 10000;
}

int main()
{
	std::ifstream file;
	file.open("Day 06/6.txt", std::ifstream::in);
	
	
	int p2 = 0;

	for (std::string line; std::getline(file, line);)
	{
		if (line == "")
		{
			break;
		}
		std::string delimiter = ", ";
		size_t pos = 0;
		std::string number;
		point Point;
		pos = line.find(delimiter);
		number = line.substr(0, pos);
		Point.x = stoi(number);
		min_x = Point.x < min_x ? Point.x : min_x;
		max_x = Point.x > max_x ? Point.x : max_x;
		line.erase(0, pos + delimiter.length());
		Point.y = stoi(line);
		min_y = Point.y < min_y ? Point.y : min_y;
		max_y = Point.y > max_y ? Point.y : max_y;
		Point.count = 0;
		areas.push_back(Point);
		++points;
	}

	for (int x = min_x; x <= max_x; x++)
	{
		for (int y = min_y; y <= max_y; y++)
		{
			center closest;
			closest.distance = 1000;
			closest.center = NULL;
			
			p2 += find_closest(x, y, closest);
			
			if (closest.center)
			{
				closest.center->count++;
			}

		}
	}

	int _;
	center corner;
	corner.distance = 1000;
	corner.center = NULL;
	_ = find_closest(min_x - 10, min_y - 10, corner);
	corner.center->count = 0;
	
	corner.distance = 1000;
	corner.center = NULL;
	_ = find_closest(min_x - 10, max_y + 10, corner);
	corner.center->count = 0;
	
	corner.distance = 1000;
	corner.center = NULL;
	_ = find_closest(max_x + 10, min_y - 10, corner);
	corner.center->count = 0;
	
	corner.distance = 1000;
	corner.center = NULL;
	_ = find_closest(max_x + 10, max_y + 10, corner);
	corner.center->count = 0;

	int p1 = 0;
	for (int i = 0; i < points; i++)
	{
		if ((areas[i].x == min_x) || (areas[i].x == max_x)
			|| (areas[i].y == min_y) || (areas[i].y == max_y))
		{
			continue;
		}
		p1 = areas[i].count > p1 ? areas[i].count : p1;
	}
	printf("%d %d", p1, p2);

	return 0;
}