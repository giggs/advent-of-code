import re

infile = open("10.txt").read().strip().splitlines()

lights = []
for line in infile:
    a,b,c,d = map(int,re.findall("-?\d+",line))
    lights.append([a+c*10000,b+d*10000,c,d])

def min_y():
    return min(lights, key=lambda x:x[1])[1]
def max_y():
    return max(lights, key=lambda x:x[1])[1]

def min_x():
    return min(lights, key=lambda x:x[0])[0]
def max_x():
    return max(lights, key=lambda x:x[0])[0]

previous = max_y() - min_y()
for i,l in enumerate(lights):
        lights[i] = [l[0]+l[2], l[1]+l[3], l[2], l[3]]

p2 = 10000

while (next := max_y() - min_y()) < previous:
    for i,l in enumerate(lights):
        lights[i] = [l[0]+l[2], l[1]+l[3], l[2], l[3]]
    previous = next
    p2 += 1

for i,l in enumerate(lights):
    lights[i] = [l[0]-l[2], l[1]-l[3], l[2], l[3]]

x1 = min_x()
x2 = max_x()
y1 = min_y()
y2 = max_y()

coords = set([(a,b) for (a,b,c,d) in lights])
print(len(coords))
ans = ''

for y in range(y1,y2+1):
    for x in range(x1,x2+1):
        ans += '\u2588' if (x,y) in coords else ' '
    ans += '\n'

print(ans)
print(p2) #10633 too low