#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <set>
#include <unordered_set>
#include <iterator>
#include <utility>
#include <vector>

int points[334][4] = {};
int hashed_points[334] = {};

typedef struct bounds
{
	int min_x;
	int max_x;
	int min_y;
	int max_y;
};

bounds find_bounds()
{
	bounds bounds;
	bounds.min_x = INT_MAX;
	bounds.min_y = INT_MAX;
	bounds.max_x = INT_MIN;
	bounds.max_y = INT_MIN;
	
	for (int i = 0; i < 334; i++)
	{
		bounds.min_x = points[i][0] < bounds.min_x ? points[i][0] : bounds.min_x;
		bounds.min_y = points[i][1] < bounds.min_y ? points[i][1] : bounds.min_y;
		bounds.max_x = points[i][0] > bounds.max_x ? points[i][0] : bounds.max_x;
		bounds.max_y = points[i][1] > bounds.max_y ? points[i][1] : bounds.max_y;
	}
	return bounds;
}

int main()
{
	std::ifstream file;
	file.open("Day 10/10.txt", std::ifstream::in);
	std::string line;
	int p2 = 10000;
	int i = 0;

	for (std::string line; std::getline(file, line);)
	{
		if (line == "")
		{
			break;
		}
		std::regex numbers("-?[0-9]+");
		auto numbers_begin = std::sregex_iterator(line.begin(), line.end(), numbers);
		auto numbers_end = std::sregex_iterator();

		for (int j = 0; numbers_begin != numbers_end; numbers_begin++, j++)
		{
			std::smatch match = *numbers_begin;
			points[i][j] = stoi(match.str());
		}
		points[i][0] += (points[i][2]) * 10000;
		points[i][1] += (points[i][3]) * 10000;
		i++;
	}
	bounds previous;
	bounds next;
	do 
	{
		previous = find_bounds();
		for (int i = 0; i < 334; i++)
		{
			points[i][0] += points[i][2];
			points[i][1] += points[i][3];
		}
		next = find_bounds();
		p2++;
	} while ((next.max_y - next.min_y) < (previous.max_y - previous.min_y));
	
	for (int i = 0; i < 334; i++)
	{
		points[i][0] -= points[i][2];
		points[i][1] -= points[i][3];
	}
	p2--;

	std::vector<std::pair<int, int>> coords;
	
	for (int i = 0; i < 334; i++)
	{
		coords.push_back(std::make_pair(points[i][1], points[i][0]));
	}

	std::set<std::pair<int, int>> set(std::begin(coords), std::end(coords));

	for (int y = previous.min_y; y <= previous.max_y; y++)
	{
		for (int x = previous.min_x; x <= previous.max_x; x++)
		{
			auto find = set.find(std::make_pair(y, x));
			if (find != set.end())
			{
				std::cout << "\u2588";
			}
			else
			{
				std::cout << " ";
			}
		}
		std::cout << std::endl;
	}

	std::cout << p2 << std::endl;

}