#include <utility>
#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>

#define DEPTH 3879
#define TARGET_Y 713
#define TARGET_X 8

/* NOTE(giggs): Since the lack of collision with the custom
* hashes is guaranteed, this would most likely be much faster
* with an array instead of unordered maps
*/

typedef std::pair<int, int> coords;

std::vector<coords> dirs = {std::make_pair(-1, 0),
							std::make_pair(1,0),
							std::make_pair(0,1),
							std::make_pair(0,-1) };

int manhattan_distance_to_target(int y, int x)
{
	return ((y > TARGET_Y ? y - TARGET_Y : TARGET_Y - y)
		+ (x > TARGET_X ? x - TARGET_X : TARGET_X - x));
}

int distance_cutoff = manhattan_distance_to_target(0, 0);

enum gear
{
	neither,
	climbing_gear,
	torch
};

enum region
{
	rocky,
	wet, 
	narrow
};

struct state
{
	int time;
	int y;
	int x;
	region type;
	gear gear;
	bool operator< (const state & rhs)  const 
	{
		return time > rhs.time;
		/*
		return (manhattan_distance_to_target(y, x) + time)
			> 
			(manhattan_distance_to_target(rhs.y, rhs.x)
			+ rhs.time); 
			// Manhattan distance heuristic, didn't help much more than culling the queue 
		*/
	}
};

struct substate
{
	int y;
	int x;
	gear gear;

	bool operator==(const substate& other) const
	{
		return (y == other.y
			&& x == other.x
			&& gear == other.gear);
	}
};

template<>
struct std::hash<substate>
{
	std::size_t operator()(substate const& s) const noexcept
	{
		return (s.y * 10000 + s.x * 10 + (int)s.gear);
	}
};

template<>
struct std::hash<coords>
{
	std::size_t operator()(coords const& c) const noexcept
	{
		return (c.first * 1000 + c.second);
	}
};

struct region_info
{
	int erosion_level;
	int region_type;
};

std::unordered_map<coords, region_info> region_map;
std::unordered_map<substate, int> cost;

region_info compute_erosion_and_fill_maps(int y, int x)
{
	int geologic_index;
	int erosion_level;
	coords current = std::make_pair(y, x);
	region_info current_info;

	if (y == TARGET_Y && x == TARGET_X)
	{
		geologic_index = 0;
	}
	else if (y == 0)
	{
		geologic_index = x * 16807;
	}
	else if (x == 0)
	{
		geologic_index = y * 48271;
	}
	else
	{
		int erosion_left;
		int erosion_up;
		coords left = std::make_pair(y, x - 1);
		coords up = std::make_pair(y - 1, x);
		auto search_left = region_map.find(left);
		auto search_up = region_map.find(up);
		if (search_left == region_map.end())
		{
			erosion_left = compute_erosion_and_fill_maps(y, x - 1).erosion_level;
			search_left = region_map.find(left);
		}

		else
		{
			erosion_left = search_left->second.erosion_level;
		}

		if (search_up == region_map.end())
		{
			erosion_up = compute_erosion_and_fill_maps(y - 1, x).erosion_level;
			search_up = region_map.find(up);
		}

		else
		{
			erosion_up = search_up->second.erosion_level;
		}

		geologic_index = (erosion_left * erosion_up);
	}

	current_info.erosion_level = (geologic_index + DEPTH) % 20183;
	current_info.region_type = current_info.erosion_level % 3;
	region_map.insert({ current, current_info });
	return current_info;
}

std::vector<state> successors(state & current)
{
	std::vector<state> next;
	for (coords dir : dirs)
	{
		int y = current.y + dir.first;
		int x = current.x + dir.second;
		int type;
		if ((x >= 0) && (y >= 0))
		{
			state new_state;
			new_state.y = y;
			new_state.x = x;
			new_state.type = current.type;
			new_state.gear = current.gear;
			new_state.time = current.time;
			auto search = region_map.find(std::make_pair(y, x));
			if (search == region_map.end())
			{
				type = compute_erosion_and_fill_maps(y, x).region_type;
			}
			else
			{
				type = search->second.region_type;
			}
			if (type == rocky) 
			{
				new_state.type = rocky;
				if ((new_state.gear == climbing_gear) || (new_state.gear == torch))
				{
					new_state.time++;
					next.push_back(new_state);
				}
				else // neither equipped
				{
					new_state.time += 8;
					if (current.type == 0 || current.type == 1)
					{
						new_state.gear = climbing_gear;
						next.push_back(new_state);
					}
					else if (current.type == 0 || current.type == 2)
					{
						new_state.gear = torch;
						next.push_back(new_state);
					}
				}
			}
			else if (type == wet) 
			{
				new_state.type = wet;
				if ((new_state.gear == climbing_gear) || (new_state.gear == neither))
				{
					new_state.time++;
					next.push_back(new_state);
				}
				else // torch equipped
				{
					new_state.time += 8;
					if (current.type == 0 || current.type == 1)
					{
						new_state.gear = climbing_gear;
						next.push_back(new_state);
					}
					else if (current.type == 1 || current.type == 2)
					{
						new_state.gear = neither;
						next.push_back(new_state);
					}
				}
			}
			else 				{
				new_state.type = narrow;
				if ((new_state.gear == torch) || (new_state.gear == neither))
				{
					new_state.time++;
					next.push_back(new_state);
				}
				else // climbing_gear equipped
				{
					new_state.time += 8;
					if (current.type == 0 || current.type == 2)
					{
						new_state.gear = torch;
						next.push_back(new_state);
					}
					else if (current.type == 1 || current.type == 2)
					{
						new_state.gear = neither;
						next.push_back(new_state);
					}
				}
			}
		}
	}
	return next;
}

int main()
{
	int p1 = 0;

	for (int y = 0; y <= TARGET_Y; y++)
	{
		for (int x = 0; x <= TARGET_X; x++)
		{
			int type = compute_erosion_and_fill_maps(y, x).region_type;
			p1 += type;
		}
	}
	std::cout << p1 << " ";

	std::priority_queue<state> queue;
	queue.push({ 0, 0, 0, rocky, torch });
	cost.insert({ { 0, 0, torch }, 0 });

	while (!queue.empty())
	{
		state current = queue.top();
		queue.pop();
		if ((current.y == TARGET_Y) && (current.x == TARGET_X))
		{
			if (current.gear == torch)
			{
				std::cout << current.time << std::endl;
				return 0;
			}
			else
			{
				current.gear = torch;
				current.time += 7;
				queue.push(current);
				continue;
			}
		}
		std::vector<state> next_states = successors(current);
		for (state next : next_states)
		{
			substate sub = { next.y, next.x, next.gear };
			int distance = manhattan_distance_to_target(next.y, next.x);
			if (distance < distance_cutoff)
			{
				distance_cutoff = distance;
			}
			// reducing search space by removing points too far away.
			else if (distance > distance_cutoff + 70) 
			{
				continue;
			}
			auto search = cost.find(sub);
			if (search == cost.end())
			{
				cost.insert({ sub, next.time });
				queue.push(next);
			}
			else
			{
				if (next.time < search->second)
				{
					cost.erase(sub);
					cost.insert({ sub, next.time });
					queue.push(next);
				}
			}
		}
	}
	std::cout << "Empty queue, something must be wrong" << std::endl;
}