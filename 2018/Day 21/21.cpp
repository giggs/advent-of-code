#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <vector>
#include <sstream>

std::vector<std::string> instructions;
uint64_t registers[6] = { 0,0,0,0,0,0 };
std::set<uint64_t> stopping_values;

int main()
{
	std::ifstream file;
	file.open("Day 21/21.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file" << std::endl;
	}
	std::string line;
	std::getline(file, line);
	uint64_t* ip = &registers[atoi(&line.back())];

	while (getline(file, line) && line != "")
	{
		instructions.push_back(line);
	}

	/* NOTE(giggs):
	The only instruction where register 0 is accessed is #28 (eqrr 3 0 1)
	If you run the program (use day 19's code), when you reach it after the setup loop the value in 
	register 3 is 6132825, so this is the answer for part1.
	For part 2, the problem statement implies the program will loop after a while.
	I setup a simulation that kept running while I figured out the assembly code and 
	it hadn't finished after half an hour so I think understanding the code was required
	(over 6 thousands loops, slowly counting to up to slightly below 0x00FFFFFF).
	The loops workings are simulated below
	*/

	line = instructions[7];
	std::stringstream sline(line);
	std::string temp;
	uint64_t setup_number;
	int p2 = 0;
	bool outer_loop = true;
	sline >> temp >> setup_number;

	while (outer_loop)
	{
		registers[2] = registers[3] | 65536; // bori 3 65536 2
		registers[3] = setup_number; // seti 14070682 0 3

		while (true)
		{
			/*
			bani 2 255 1
			addr 3 1 3
			bani 3 16777215 3
			muli 3 65899 3
			bani 3 16777215 3
			*/
			registers[3] = (((registers[3] + (registers[2] & 255)) & 0x00FFFFFF) * 65899) & 0x00FFFFFF;

			// gtir 256 2 1 ; sets up the pointer value to go to eqrr 3 0 1
			if (256 > registers[2])
			{
				if (stopping_values.empty())
				{
					std::cout << registers[3] << " ";
					stopping_values.insert(registers[3]);
				}
				else
				{
					auto search = stopping_values.find(registers[3]);
					if (search == stopping_values.end())
					{
						stopping_values.insert(registers[3]);
						p2 = registers[3];
					}
					else
					{
						outer_loop = false;
					}
					break;
				}
			}
			else
			{
				/*
				loop from addi 1 1 4 to addi 1 1 1
				Increment registers[1] until it *256 > registers[2], then exit and copy
				registers[1] to registers[2]. Effectively division by 256.
				*/
				registers[2] /= 256;
			}
		}
	}
	std::cout << p2 << std::endl;
}