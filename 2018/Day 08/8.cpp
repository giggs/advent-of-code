#include <iostream>
#include <fstream>

int nodes[6000];
int metadata[6000];

typedef struct Node 
{
	int value;
	int meta;
	int children[6000];
};

int compute_value(std::ifstream& in, Node node)
{
	int limit;
	in >> limit;
	in >> node.meta;
	if (limit)
	{ 
		for (int i = 0; i < limit; i++)
		{
			Node child = {};
			node.children[i] = compute_value(in, child);
		} 
		for (int i = 0; i < node.meta; i++)
		{
			int index;
			in >> index;
			if (index <= limit)
			{
				node.value += node.children[index - 1];
			}
		}
		return node.value;
	}
	else
	{
		for (int i = 0; i < node.meta; i++)
		{
			int entry;
			in >> entry;
			node.value += entry;
		}
		return node.value;
	}
	
}

int main()
{
	std::ifstream file;
	file.open("Day 08/8.txt", std::ifstream::in);
	int packet;
	int p1 = 0;
	int* pnodes = &nodes[0];
	int* pmeta = &metadata[0];

	while (file >> packet)
	{
		*pnodes = packet;
		if (*pnodes)
		{
			file >> packet;
			*pmeta++ = packet;
			pnodes++;
			continue;
		}
		else
		{
			int entries;
			for (file >> entries; entries > 0; entries--)
			{
				int entry;
				file >> entry;
				p1 += entry;
			}
			// (*--pnodes)--; too clever
			--pnodes;
			*pnodes -= 1;

			while (!*pnodes) //no more child nodes
			{
				for (int entries = *--pmeta; entries > 0; entries--) 
				{
					int entry;
					file >> entry;
					p1 += entry;
				}
				if (pnodes == &nodes[0])
				{
					break;
				}
				// (*--pnodes)--; too clever
				--pnodes;
				*pnodes -= 1;
			}
			pnodes++;
		}
	}

	file.close();
	file.open("Day 08/8.txt", std::ifstream::in);

	int p2;

	Node root = {};
	
	root.value = compute_value(file, root);

	std::cout << p1 << " " << root.value << std::endl;

	return 0;
}