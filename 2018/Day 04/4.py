import re
from collections import defaultdict

input = open('4.txt').read().strip().splitlines()
input.sort()
guards = defaultdict(lambda: [0]*60)
start = 0

for line in input:
    if '#' in line:
        guard = int(re.findall('.*#(\d+)', line)[0])
    else:
        time, _ = line.split('] ')
        if 'fa' in line:
            start = int(time[-2:])
        else:
            end = int(time[-2:])
            for i in range(start, end+1):
                guards[guard][i] += 1

most_asleep = [0, 0, 0]
most_frequently_asleep = [0, 0, 0]

for guard in guards:
    sleep = sum(guards[guard])
    max_sleep = max(guards[guard])
    if sleep > most_asleep[0]:
        most_asleep = [sleep, guard, guards[guard].index(max_sleep)]
    if max_sleep > most_frequently_asleep[0]:
        most_frequently_asleep = [max_sleep, guard, guards[guard].index(max_sleep)] 

print(most_asleep[1]*most_asleep[2], most_frequently_asleep[1]*most_frequently_asleep[2])