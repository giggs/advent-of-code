#include <iostream>
#include <fstream>
#include <string>

#define PLAYERS 446
#define LAST 71522*100

typedef struct Marble
{
	int value;
	Marble *prev;
	Marble *next;
};

Marble marbles[(LAST+1)] = {};
unsigned int players[PLAYERS] = {};
int limit = LAST;

int main()
{
	int p1 = 0;
	int round = 2;
	clock_t begin = clock();
	Marble* current = &marbles[0];
	current->value = 0;
	current->prev = &marbles[1];
	current->next = &marbles[1];
	current++;
	current->value = 1;
	current->prev = &marbles[0];
	current->next = &marbles[0];

	while (round <= limit)
	{
		Marble* add = &marbles[round];
		add->value = round;
		if (round % 23 == 0)
		{
			players[round % PLAYERS] += round;
			for (int i = 0; i < 7; i++)
			{
				current = current->prev;
			}
			players[round % PLAYERS] += current->value;
			current->prev->next = current->next;
			current->next->prev = current->prev;
			current = current->next;
		}
		else
		{
			add->next = current->next->next;
			add->prev = current->next;
			current->next->next->prev = add;
			current->next->next = add;	
			current = add;
		}

		++round;

		if (round == LAST / 100)
		{
			for (int i = 0; i < PLAYERS; i++)
			{
				if (players[i] > p1)
				{
					p1 = players[i];
				}
			}
		}
	}

	unsigned int p2 = 0;

	for (int i = 0; i < PLAYERS; i++)
	{
		if (players[i] > p2)
		{
			p2 = players[i];
		}
	}
	clock_t end = clock();
	double t = double(end - begin) / CLOCKS_PER_SEC;
	std::cout << p1 << " " << p2 << " " << t << std::endl;

	return 0;

}