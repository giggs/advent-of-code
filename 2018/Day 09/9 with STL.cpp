// Tried using the STL after using my own doubly linked list for practice
// This is 100 times slower in optimized build 
// It also uses about 1.6x the memory


#include <iostream>
#include <fstream>
#include <list>
#include <time.h>

#define PLAYERS 446
#define LAST 71522*100

std::list<long long> marbles;
long long  players[PLAYERS] = {};
int limit = LAST;

int main()
{
	clock_t begin = clock();
	
	int p1 = 0;
	int round = 1;
	
	marbles.push_front(0);
	auto current = marbles.begin();

	while (round <= limit)
	{
		if (round % 23 == 0)
		{
			players[round % PLAYERS] += round;
			for (int i = 0; i < 7; i++)
			{
				current = (current == marbles.begin()) ? marbles.end() : current;
				current--;
			}
			auto del = current;
			players[round % PLAYERS] += *del;
			current++;
			marbles.erase(del);
			current = (current == marbles.end()) ? marbles.begin() : current;
		}
		else
		{
			current++;
			current = (current == marbles.end()) ? marbles.begin() : current;
			current++;
			current = (current == marbles.end()) ? marbles.begin() : current;
			marbles.insert(current, round);
			current--;
		}

		++round;

		if (round == LAST / 100)
		{
			for (int i = 0; i < PLAYERS; i++)
			{
				if (players[i] > p1)
				{
					p1 = players[i];
				}
			}
		}
	}

	long long p2 = 0;

	for (int i = 0; i < PLAYERS; i++)
	{
		if (players[i] > p2)
		{
			p2 = players[i];
		}
	}
	clock_t end = clock();
	double t = double(end - begin) / CLOCKS_PER_SEC;
	std::cout << p1 << " " << p2 << " " << t << std::endl;

	return 0;

}