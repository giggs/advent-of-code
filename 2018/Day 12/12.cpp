#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <assert.h>

char plants[2000] = {};
char next[2000] = {};
int generations[1000] = {};

int main()
{
	
	for (int i = 0; i < 2000; i++)
	{
		plants[i] = '.';
		next[i] = '.';
	}
	std::ifstream file;
	file.open("Day 12/12.txt", std::ifstream::in);
	char c;
	char* p = &plants[0];
	p += 500;

	while ((c = file.get()) != '\n')
	{
		if ((c == '#') || (c == '.'))
		{
			*p++ = c;
		}
	}

	std::string line;
	std::map<std::string, std::string> rules;

	for (std::string line; std::getline(file, line);)
	{
		if (line == "")
		{
			continue;
		}
		std::string delimiter = " => ";
		size_t pos = 0;
		std::string pattern;
		pos = line.find(delimiter);
		pattern = line.substr(0, pos);
		line.erase(0, pos + delimiter.length());
		rules.insert({ pattern, line });
	}

	for (int a = 0; a < 1000; a++)
	{
		for (int i = 500-a; i < 1998; i++)
		{
			std::string original(&plants[i - 2], 5);
			auto search = rules.find(original);
			if (search == rules.end())
			{
				next[i] == '.';
			}
			else {
				next[i] = (search->second)[0];
			}
		}
		std::copy(std::begin(next), std::end(next), std::begin(plants));

		int p1 = 0;

		for (int i = 500-a; i < 2000; i++)
		{
			if (plants[i] == '#')
			{
				p1 += i - 500;
			}
		}
		if (a == 19)
		{
			std::cout << p1 << " ";
		}
		if ((a > 10) && 
			((p1 - generations[a-10]) == ((generations[a-1] - generations[a - 2])*10)))
		{
			int increment = generations[a-1] - generations[a - 2];
			unsigned long long remaining = 50'000'000'000 - a;
			unsigned long long p2 = remaining * increment + generations[a-1];
			std::cout << p2 << std::endl;
			break;
		}
		generations[a] = p1;
	}

	return 0;
	
}