#include <iostream>
#include <fstream>
#include <string>

char array1[50][50] = {};
char array2[50][50] = {};

char change_open(int y, int x, char arr[50][50])
{
	int tree_count = 0;
	for (int dy = -1; dy < 2; dy++)
	{
		for (int dx = -1; dx < 2; dx++)
		{
			if (!(dx == 0 && dy == 0) 
				&& ((y + dy) >= 0) && ((y+dy) < 50)
				&& ((x+dx) >= 0) && ((x+dx) < 50))
			{
				if (arr[y + dy][x + dx] == '|')
				{
					tree_count++;
				}
			}
		}
	}

	if (tree_count > 2)
	{
		return '|';
	}
	else
	{
		return '.';
	}
}

char change_tree(int y, int x, char arr[50][50])
{
	int lumber_count = 0;
	for (int dy = -1; dy < 2; dy++)
	{
		for (int dx = -1; dx < 2; dx++)
		{
			if (!(dx == 0 && dy == 0)
				&& ((y + dy) >= 0) && ((y + dy) < 50)
				&& ((x + dx) >= 0) && ((x + dx) < 50))
			{
				if (arr[y + dy][x + dx] == '#')
				{
					lumber_count++;
				}
			}
		}
	}

	if (lumber_count > 2)
	{
		return '#';
	}
	else
	{
		return '|';
	}
}

char change_lumber(int y, int x, char arr[50][50])
{
	int lumber_count = 0;
	int tree_count = 0;
	for (int dy = -1; dy < 2; dy++)
	{
		for (int dx = -1; dx < 2; dx++)
		{
			if (!(dx == 0 && dy == 0)
				&& ((y + dy) >= 0) && ((y + dy) < 50)
				&& ((x + dx) >= 0) && ((x + dx) < 50))
			{
				if (arr[y + dy][x + dx] == '#')
				{
					lumber_count++;
				}
				else if (arr[y + dy][x + dx] == '|')
				{
					tree_count++;
				}
			}
		}
	}

	if ((lumber_count > 0) && (tree_count > 0))
	{
		return '#';
	}
	else
	{
		return '.';
	}
}

void advance_time(char before[50][50], char after[50][50])
{
	for (int y = 0; y < 50; y++)
	{
		for (int x = 0; x < 50; x++)
		{
			char c = before[y][x];
			switch (c)
			{
			case '.':
			{
				after[y][x] = change_open(y, x, before);
			} break;
			case '|':
			{
				after[y][x] = change_tree(y, x, before);
			} break;
			default:
			{
				after[y][x] = change_lumber(y, x, before);
			}
			}
		}
	}
}

int compute_resources(char (&arr)[50][50])
{
	int trees = 0;
	int lumberyards = 0;
	for (auto& row : arr)
	{
		for (char c : row)
		{
			switch (c)
			{
			case '|':
			{
				trees++;
			} break;
			case '#':
			{
				lumberyards++;
			} break;
			default:
			{
				;
			}
			}
		}
	}
	return trees * lumberyards;
}

int main()
{
	std::ifstream file;
	file.open("Day 18/18.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file" << std::endl;
		return 1;
	}

	std::string line;
	int y = 0;
	int x = 0;
	std::getline(file, line);
	for (; line != ""; std::getline(file,line), y++)
	{
		x = 0;
		for (char c : line)
		{
			array1[y][x++] = c;
		}
	}

	int resources_array[1001];

	// If you want to speed it up, check for duplicates as you compute
	// most likely with a map({resources, time})

	for (long long minutes = 0; minutes < 1000; minutes+= 2)
	{
		advance_time(array1, array2); 
		resources_array[minutes+1] = compute_resources(array2);
		advance_time(array2, array1);
		resources_array[minutes+2] = compute_resources(array1);
	}

	int i = 1000;
	int j = i-1;

	while (resources_array[j] != resources_array[i])
	{
		j--;
	}

	int period = i - j;

	while ((j % period) != (1000000000 % period))
	{
		j++;
	}

	std::cout << resources_array[10] << " " << resources_array[j];

	return 0;
}