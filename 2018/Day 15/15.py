victory = False
p2 = 0 
attack_power = 3

def solve(infile, cheat = 0):
    caves = open(infile).read().splitlines()
    walls = set()
    goblins = []
    goblins_HP = dict()
    elves = []
    elves_HP = dict()
    elves_attack = cheat if cheat else 3
    for y,row in enumerate(caves):
        for x,char in enumerate(row):
            if char == '#':
                walls.add((y,x))
            elif char == 'G':
                goblins.append((y,x))
                goblins_HP[(y,x)] = 200
            elif char == 'E':
                elves.append((y,x))
                elves_HP[(y,x)] = 200

    def BFS (start, goal, successors, forbidden):
        frontier = [[start]]
        explored = set()
        shortest = 0
        result = []
        while frontier:
            path = frontier.pop(0)
            if (shortest > 0) and len(path) >= shortest:
                continue
            current = path[-1]
            if path[0][0] == 26:
                a = 0
            for next in successors(current, forbidden):
                if next not in explored:
                    new = path+[next]
                    if in_range(next, goal):
                        if shortest == 0:
                            shortest = len(new)
                        if len(new) > shortest:
                            continue
                        result.append(new)
                        
                    frontier.append(new)
                    explored.add(next)
        result = sorted(result, key= lambda x:x[-1])
        return result[0] if result else None


    def move(square, forbidden):
        y, x = square
        result = []
        for dy in [-1,0,1]:
            for dx in [-1,0,1]:
                if (abs(dx) + abs(dy)) != 2:
                    nx = x+dx
                    ny = y+dy
                    if (ny, nx) not in forbidden:
                        result.append((ny,nx))

        return sorted(result)

    def in_range(unit, enemies):
        result = []
        y,x = unit
        if (y-1, x) in enemies:
            result.append((y-1, x))
        if (y,x-1) in enemies:
            result.append((y, x-1))
        if (y, x+1) in enemies:
            result.append((y, x+1))
        if (y+1, x) in enemies:
            result.append((y+1, x))
        return result if result else False

    rounds = 0

    while elves and goblins:
        armies = sorted(elves+goblins)
        for unit in armies:
            if unit in elves: # doesn't count elves killed before they moved
                if not goblins:
                    break
                if enemy := in_range(unit, goblins): 
                    enemy = sorted(enemy, key = lambda x:goblins_HP[x])[0]
                    goblins_HP[enemy] -= elves_attack
                    if goblins_HP[enemy] < 1:
                        del goblins_HP[enemy]
                        goblins.remove(enemy)
                else:
                    if new := BFS(unit, goblins, move, walls | set(goblins) | set(elves)):
                        new = new[1]
                        elves.remove(unit)
                        elves.append(new)
                        elves_HP[new] = elves_HP[unit]
                        del elves_HP[unit]
                        if enemy := in_range(new, goblins):
                            enemy = sorted(enemy, key = lambda x:goblins_HP[x])[0]
                            goblins_HP[enemy] -= elves_attack
                            if goblins_HP[enemy] < 1:
                                del goblins_HP[enemy]
                                goblins.remove(enemy)
            
            elif unit in goblins: # doesn't count goblins killed before they moved
                if not elves:
                    break
                if enemy := in_range(unit, elves):
                    enemy = sorted(enemy, key = lambda x:elves_HP[x])[0]
                    elves_HP[enemy] -= 3
                    if elves_HP[enemy] < 1:
                        if cheat:
                            return (False, 0)
                        del elves_HP[enemy]
                        elves.remove(enemy)
                else:
                    if new := BFS(unit, elves, move, walls | set(goblins) | set(elves)):
                        new = new[1]
                        goblins.remove(unit)
                        goblins.append(new)
                        goblins_HP[new] = goblins_HP[unit]
                        del goblins_HP[unit]
                        if enemy := in_range(new, elves):
                            enemy = sorted(enemy, key = lambda x:elves_HP[x])[0]
                            elves_HP[enemy] -= 3
                            if elves_HP[enemy] < 1:
                                if cheat:
                                    return (False, 0)
                                del elves_HP[enemy]
                                elves.remove(enemy)
        else:
            rounds += 1

    p1 = rounds*(sum(v for v in goblins_HP.values())+sum(v for v in elves_HP.values()))
    return(cheat, p1)

p1 = solve('15.txt')[1]
while not victory:
    attack_power += 1
    victory, p2  = solve('15.txt', attack_power) 
    
print(p1, p2)
input()