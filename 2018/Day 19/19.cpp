#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

vector<string> instructions;

int registers[6] = {0,0,0,0,0,0};


int main()
{
	bool p2 = false;
	ifstream file;
	file.open("Day 19/19.txt", ifstream::in);
	if (!file.good())
	{
		cout << "Error opening file" << endl;
	}
	string line;
	getline(file, line);
	int* ip = &registers[atoi(&line.back())];

	while (getline(file, line) && line != "")
	{
		instructions.push_back(line);
	}



	while (*ip >= 0 && *ip < instructions.size())
	{
		line = instructions[*ip];
		stringstream sline(line);
		string temp;
		int reg;
		int A;
		int B;
		sline >> temp >> A >> B >> reg;
		
		// Analyzing the assembly code, there's a setup loop then 2 nested loops
		// The setup loop sets the value in register[4] and 1 in r[1]
		// The outer loop is from ip 2 to ip 15 and the inner loop from ip 3 to ip 11
		// The inner loop adds r[1] to r[0] if r[1]*r[3]==r[4] and increments r[3] 
		// by 1 until r[3] > r[4]. 
		// The outer loop increments r[1] until r[1] > r[4]
		// So the code adds all divisors of r[4] after the setup.

		if (*ip == 3)
		{ 
			int ans = 0;
			for (int i = 1; i*i < registers[4]; i++)
			{
				if (registers[4] % i == 0)
				{
					ans = ans + i + registers[4] / i;
				}
			}
			std::cout << ans << " ";
			if (p2)
			{
				break;
			}
			else
			{
				p2 = true;
				*ip = 0;
				for (auto& reg : registers)
				{
					reg = 0;
				}
				registers[0] = 1;
				continue;
			}
		}
		
		if (temp == "addr")
		{
			registers[reg] = registers[A] + registers[B];;
		}
		else if (temp == "addi")
		{
			registers[reg] = registers[A] + B;
		}
		else if (temp == "mulr")
		{
			registers[reg] = registers[A] * registers[B];
		}
		else if (temp == "muli")
		{
			registers[reg] = registers[A] * B;
		}
		else if (temp == "banr")
		{
			registers[reg] = registers[A] & registers[B];
		}
		else if (temp == "bani")
		{
			registers[reg] = registers[A] & B;
		}
		else if (temp == "borr")
		{
			registers[reg] = registers[A] | registers[B];
		}
		else if (temp == "bori")
		{
			registers[reg] = registers[A] | B;
		}
		else if (temp == "setr")
		{
			registers[reg] = registers[A];
		}
		else if (temp == "seti")
		{
			registers[reg] = A;
		}
		else if (temp == "gtir")
		{
			registers[reg] = (A > registers[B]);
		}
		else if (temp == "gtri")
		{
			registers[reg] = (registers[A] > B);
		}
		else if (temp == "gtrr")
		{
			registers[reg] = (registers[A] > registers[B]);
		}
		else if (temp == "eqir")
		{
			registers[reg] = (A == registers[B]);
		}
		else if (temp == "eqri")
		{
			registers[reg] = (registers[A] == B);
		}
		else //(temp == "eqrr")
		{
			registers[reg] = (registers[A] == registers[B]);
		}

		(*ip)++;
	}
}