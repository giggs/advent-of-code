// Made this to try out multithreading

#include <iostream>
#include <assert.h>
#include <thread>
#include <time.h>

int serial = 1788;

typedef struct CellSquare
{
	int PowerLevel;
	int x;
	int y;
	int size;
};


int Grid[300][300] = {};

int CellPowerLevel(int x, int y, int serial)
{
	int RackID = x + 10;
	int PowerLevel = RackID * y;
	PowerLevel += serial;
	PowerLevel *= RackID;
	PowerLevel /= 100;
	PowerLevel %= 10;
	return PowerLevel - 5;
}

void MaxSquarePowerLevel(int size, CellSquare* square)
{
	for (int y = size - 1; y < 300; y++)
	{
		for (int x = size - 1; x < 300; x++)
		{
			int PowerLevel = 0;
			for (int j = y - (size - 1); j <= y; j++)
			{
				for (int i = x - (size - 1); i <= x; i++)
				{
					PowerLevel += Grid[j][i];
				}
			}
			if (PowerLevel > square->PowerLevel)
			{
				square->PowerLevel = PowerLevel;
				square->x = x - (size-1);
				square->y = y - (size-1);
				square->size = size;
			}
		}
	}
}

void threaded_search(int min, int max, CellSquare* psquare)
{

	for (int i = min; i <= max; i++)
	{
		MaxSquarePowerLevel(i,psquare);
	}
}

int main()
{
	
	assert(CellPowerLevel(3, 5, 8) == 4);
	assert(CellPowerLevel(122, 79, 57) == -5);
	
	CellSquare p1 = {};
	CellSquare p2 = {};
	
	for (int y = 0; y < 300; y++)
	{
		for (int x = 0; x < 300; x++)
		{
			
			Grid[y][x] = CellPowerLevel(x, y, serial);
			if ((x > 1) && (y > 1))
			{
				int PowerLevel = 0;
				for (int j = y - 2; j <= y; j++)
				{
					for (int i = x - 2; i <= x; i++)
					{
						PowerLevel += Grid[j][i];
					}
				}
				if (PowerLevel > p1.PowerLevel)
				{
					p1.PowerLevel = PowerLevel;
					p1.x = x - 2;
					p1.y = y - 2;
				}
			}
		}
	}
	std::cout << p1.x << "," << p1.y << " ";

	CellSquare threads[12] = {};

	clock_t begin = clock();

	std::thread t0(threaded_search, 4, 55, &threads[0]);
	std::thread t8(threaded_search, 55, 75, &threads[8]);
	std::thread t9(threaded_search, 75, 90, &threads[9]);
	std::thread t1(threaded_search, 90, 105, &threads[1]);
	std::thread t10(threaded_search, 105, 125, &threads[10]);
	std::thread t2(threaded_search, 125, 145, &threads[2]);
	std::thread t3(threaded_search, 145, 165, &threads[3]);
	std::thread t4(threaded_search, 165, 185, &threads[4]);
	std::thread t5(threaded_search, 185, 205, &threads[5]);
	std::thread t6(threaded_search, 205, 225, &threads[6]);
	std::thread t7(threaded_search, 225, 245, &threads[7]);
	std::thread t11(threaded_search, 245, 300, &threads[11]);

	t0.join();
	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();
	t8.join();
	t9.join();
	t10.join();
	t11.join();
	
	clock_t end = clock();

	double time = double(end - begin) / CLOCKS_PER_SEC;

	for (int i = 0; i < 12; i++)
	{
		if (threads[i].PowerLevel > p2.PowerLevel)
			p2 = threads[i];
	}

	std::cout << p2.x << "," << p2.y << "," << p2.size << "\n" << time << " seconds" << std::endl;

}
