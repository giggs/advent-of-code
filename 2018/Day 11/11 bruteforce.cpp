// My original solution bruteforcing the calculations. A few seconds on optimized build.
// I did a multithreaded version that takes about a second.
// The clever way uses Summed-area table
// https://en.wikipedia.org/wiki/Summed-area_table

#include <iostream>
#include <assert.h>

int serial = 1788;

typedef struct CellSquare
{
	int PowerLevel;
	int x;
	int y;
	int size;
};

CellSquare Largest = {};

int Grid[300][300] = {};

int CellPowerLevel(int x, int y, int serial)
{
	int RackID = x + 10;
	int PowerLevel = RackID * y;
	PowerLevel += serial;
	PowerLevel *= RackID;
	PowerLevel /= 100;
	PowerLevel %= 10;
	return PowerLevel - 5;
}

void MaxSquarePowerLevel(int size)
{
	for (int y = size - 1; y < 300; y++)
	{
		for (int x = size - 1; x < 300; x++)
		{
			int PowerLevel = 0;
			for (int j = y - (size - 1); j <= y; j++)
			{
				for (int i = x - (size - 1); i <= x; i++)
				{
					PowerLevel += Grid[j][i];
				}
			}
			if (PowerLevel > Largest.PowerLevel)
			{
				Largest.PowerLevel = PowerLevel;
				Largest.x = x - (size-1);
				Largest.y = y - (size-1);
				Largest.size = size;
			}
		}
	}
}

int main()
{
	
	assert(CellPowerLevel(3, 5, 8) == 4);
	assert(CellPowerLevel(122, 79, 57) == -5);
	
	for (int y = 0; y < 300; y++)
	{
		for (int x = 0; x < 300; x++)
		{
			
			Grid[y][x] = CellPowerLevel(x, y, serial);
			if ((x > 1) && (y > 1))
			{
				int PowerLevel = 0;
				for (int j = y - 2; j <= y; j++)
				{
					for (int i = x - 2; i <= x; i++)
					{
						PowerLevel += Grid[j][i];
					}
				}
				if (PowerLevel > Largest.PowerLevel)
				{
					Largest.PowerLevel = PowerLevel;
					Largest.x = x - 2;
					Largest.y = y - 2;
				}
			}
		}
	}
	std::cout << Largest.x << "," << Largest.y << " ";

	for (int i = 4; i <= 300; i++)
	{
		MaxSquarePowerLevel(i);
	}

	std::cout << Largest.x << "," << Largest.y << "," << Largest.size << std::endl;
}
