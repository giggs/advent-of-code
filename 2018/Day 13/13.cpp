#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <utility>
#include <vector>
#include <algorithm>

typedef struct cart
{
	int y;
	int x;
	int dy;
	int dx;
	int turn;
	int crashed;
};

void turn_left(cart& cart)
{
	if (cart.dx == 0)
	{
		cart.dx = cart.dy;
		cart.dy = 0;
	}
	else
	{
		cart.dy = -cart.dx;
		cart.dx = 0;
	}
}

void turn_right(cart& cart)
{
	if (cart.dx == 0)
	{
		cart.dx = -cart.dy;
		cart.dy = 0;
	}
	else
	{
		cart.dy = cart.dx;
		cart.dx = 0;
	}
}

bool cart_compare(cart a, cart b)
{
	if (a.y != b.y)
	{
		return a.y < b.y;
	}
	else
	{
		return a.x < b.x; 
	}
}

int main()
{
	std::ifstream file("Day 13/13.txt", std::ifstream::in);
	char c;
	int x = -1;
	int y = 0;
	int limit = 0;
	std::map<std::pair<int, int>, char> Turns;
	std::set<std::pair<int, int>> cartcoordinates;
	std::vector<cart> carts;

	while ((c = file.get()) && (c != EOF))
	{
		cart cart;
		++x;
		switch (c)
		{
		case '\n':
		{
			y++;
			x = -1;
			continue;
		} break;

		case '+':
		case '/':
		case '\\':
		{
			auto p = std::make_pair(y, x);
			Turns.insert({p, c});
		} break;

		case '|':
		case '-':
		case ' ':
		{
			continue;
		} break;

		default:
		{
			cart.x = x;
			cart.y = y;
			cart.turn = 0;
			if (c == '^')
			{
				cart.dx = 0;
				cart.dy = -1;
			}
			else if (c == 'v')
			{
				cart.dx = 0;
				cart.dy = 1;
			}
			else if (c == '<')
			{
				cart.dx = -1;
				cart.dy = 0;
			}
			else
			{
				cart.dx = 1;
				cart.dy = 0;
			}
			cart.crashed = 0;
			++limit; 
			carts.insert(carts.begin(), cart);
			cartcoordinates.insert(std::make_pair(y, x));
		} 
		}
	}

	bool rolling = true;
	int a = 0;
	bool p1 = false;
	int crashes = 0;

	while ((crashes+1) < limit)
	{
		std::sort(carts.begin(), carts.end(), cart_compare);
		a++;
		std::set<std::pair<int, int>> crashed;
		for (auto it = carts.begin(); it != carts.end(); it++)
		{
			cart& current = *it;
			if (current.crashed == 1)
			{
				continue;
			}
			auto p = std::make_pair(current.y, current.x);
			cartcoordinates.erase(p);
			current.x += current.dx;
			current.y += current.dy;
			p = std::make_pair(current.y, current.x);
			auto searchcart = cartcoordinates.find(p);
			if (searchcart != cartcoordinates.end())
			{
				if (!p1)
				{
					std::cout << current.x << ',' << current.y << " ";
					p1 = true;
				}
				crashed.insert(p);
				current.crashed = 1;
				crashes += 2;
				cartcoordinates.erase(p);
				for (auto it2 = carts.begin(); it != carts.end(); it2++)
				{
					if ((it2->x == current.x) && (it2->y == current.y) && (it2->crashed == 0))
					{
						it2->crashed = 1;
						break;
					}
				
				}
				continue;
			}
			else
			{
				cartcoordinates.insert(p);
				auto searchturn = Turns.find(p);
				if (searchturn != Turns.end())
				{
					char value = searchturn->second;
					switch (value)
					{
					case '/':
					{
						if (current.dx == 0)
						{
							turn_right(current);
						}
						else
						{
							turn_left(current);
						}
					} break;
					
					case '\\':
					{
						if (current.dx == 0)
						{
							turn_left(current);

						}
						else
						{
							turn_right(current);
						}
					} break;

					default: // case '+'
					{
						if (current.turn == 0)
						{
							turn_left(current);
						}
						else if (current.turn == 2)
						{
							turn_right(current);
						}
						current.turn = (current.turn + 1) % 3;
					}

					}
				}
			}
		}
	}

	for (auto it = carts.begin(); it != carts.end(); it++)
	{
		if (it->crashed == 0)
		{
			std::cout << it->x << "," << it->y << std::endl;
		}
	}

}