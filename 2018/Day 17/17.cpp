#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <deque>
#include <utility>

void splitstring(std::string& s1, std::string& s2, std::string delimiter) //split s2 into s1 and s2
{
	size_t pos = 0;
	pos = s2.find(delimiter);
	s1 = s2.substr(0, pos);
	s2.erase(0, pos + delimiter.length());
}

std::map<std::pair<int, int>, char> grid;
std::deque<std::pair<int, int>> queue;

int depth = 0;
int min_y = INT_MAX;


int main()
{
	std::ifstream file;
	file.open("Day 17/17.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file" << std::endl;
	}

	// input parsing, keeping track of min and max y values.

	for (std::string line; std::getline(file, line) && line != "";) 
	{
		std::string delimiter = ", ";
		size_t pos = 0;
		std::string substring= "";
		char c;
		int x;
		int y;
		int lower;
		int upper;

		splitstring(substring, line, ", ");

		c = substring[0];
		substring.erase(0, 2);

		if (c == 'x')
		{
			x = stoi(substring);
		}
		else
		{
			y = stoi(substring);
			depth = y > depth ? y : depth;
			min_y = y < min_y ? y : min_y;
		}

		splitstring(substring, line, "..");

		c = substring[0];
		substring.erase(0, 2);
		lower = stoi(substring);
		upper = stoi(line);

		if (c == 'y')
		{
			depth = upper > depth ? upper : depth;
			min_y = lower < min_y ? lower : min_y;

			for (int y = lower; y <= upper; y++)
			{
				grid.insert({ std::make_pair(y,x), '#' });
			}
		}
		else
		{
			for (int x = lower; x <= upper; x++)
			{
				grid.insert({ std::make_pair(y,x), '#' });
			}
		}
	}

	queue.push_back(std::make_pair(1, 500));

	while (queue.size() != 0)
	{
		std::pair<int, int> current = queue.front();
		queue.pop_front();

		if (current.first > depth)
		{
			continue;
		}

		auto search_current = grid.find(current);
		if (search_current == grid.end())
		{
			grid.insert({ current, '|' });
		}
		

		std::pair<int, int> below = std::make_pair(current.first + 1, current.second);

		auto search_below = grid.find(below);
		if (search_below == grid.end())
		{
			queue.push_back(below);
		}
		else if ((search_below->second == '#') || (search_below->second == '~'))
		{
			
			// Flow left and right if you can
			
			std::pair<int, int> left = std::make_pair(current.first, current.second - 1);
			std::pair<int, int> right = std::make_pair(current.first, current.second + 1);
			auto search_left = grid.find(left);
			auto search_right = grid.find(right);
			if (search_left == grid.end())
			{
				queue.push_back(left);
			}
			if (search_right == grid.end())
			{
				queue.push_back(right);
			}

			// So you've hit a wall! Check both either side to see if you can flow down

			if ((search_left != grid.end()) && (search_right != grid.end())
				&& (search_left->second != '~') && (search_right->second != '~'))
			{
				int temp_x = current.second;
				auto search_temp = grid.find(std::make_pair(current.first, temp_x + 1));
				while(search_temp != grid.end() 
					&& ((search_temp->second == '|') || (search_temp->second == '~')))
				{
					temp_x++;
					search_temp = grid.find(std::make_pair(current.first, temp_x + 1));
				}
				if (search_temp == grid.end() || search_temp->second != '#')
				{
					continue; //No reservoir, keep falling
				}
				temp_x = current.second;
				search_temp = grid.find(std::make_pair(current.first, temp_x - 1));
				while (search_temp != grid.end()
					&& ((search_temp->second == '|') || (search_temp->second == '~')))
				{
					temp_x--;
					search_temp = grid.find(std::make_pair(current.first, temp_x - 1));
				}
				if (search_temp == grid.end() || search_temp->second != '#')
				{
					continue; //No reservoir, keep falling
				}

				// Water is stuck, mark it as such and queue the places above it that have seen water

				temp_x = current.second;
				search_temp = grid.find(std::make_pair(current.first, temp_x));
				search_temp->second = '~';
				search_temp = grid.find(std::make_pair(current.first - 1, temp_x));
				if ((search_temp != grid.end()) && (search_temp->second == '|'))
				{
					queue.push_back(std::make_pair(current.first - 1, temp_x));
				}
				
				search_temp = grid.find(std::make_pair(current.first, temp_x + 1));
				while (search_temp != grid.end()
					&& ((search_temp->second == '|') || (search_temp->second == '~')))
				{
					search_temp->second = '~';
					temp_x++;
					search_temp = grid.find(std::make_pair(current.first - 1, temp_x));
					if ((search_temp != grid.end()) && (search_temp->second == '|'))
					{
						queue.push_back(std::make_pair(current.first - 1, temp_x));
					}
					search_temp = grid.find(std::make_pair(current.first, temp_x + 1));
				}

				temp_x = current.second;
				search_temp = grid.find(std::make_pair(current.first, temp_x - 1));
				while (search_temp != grid.end()
					&& ((search_temp->second == '|') || (search_temp->second == '~')))
				{
					search_temp->second = '~';
					temp_x--;
					search_temp = grid.find(std::make_pair(current.first - 1, temp_x));
					if ((search_temp != grid.end()) && (search_temp->second == '|'))
					{
						queue.push_back(std::make_pair(current.first - 1, temp_x));
					}
					search_temp = grid.find(std::make_pair(current.first, temp_x - 1));
				}
			}
		}

	}

	int p1 = 0;
	int p2 = 0;

	for (auto it = grid.begin(); it != grid.end(); it++)
	{
		if (it->first.first >= min_y)
		{
			if (it->second == '~')
			{
				p1++;
				p2++;
			}
			else if (it->second == '|')
			{
				p1++;
			}
		}
	}

	std::cout << p1 << " " << p2;

	return 0;
}
