#include <string>
#include <iostream>
#include <fstream>

#define DELAY 60
#define NB 5
#define LETTERS 26

static bool gates_1[LETTERS][LETTERS];
static bool gates_2[LETTERS][LETTERS];
static int blocked_1[LETTERS];
static int blocked_2[LETTERS];
std::string p1;
int done = LETTERS;
int p2 = 0;

typedef struct Worker {
	uint8_t sleep = 0;
	bool open[LETTERS] = {};
};
Worker team[NB];


int main()
{
	std::ifstream file;
	file.open("Day 07/7.txt", std::ifstream::in);
	//file.open("Day 07/test.txt", std::ifstream::in);

	for (std::string line; std::getline(file, line);)
	{
		if (line == "")
		{
			break;
		}

		uint8_t gate = line[5] - 'A';
		uint8_t next = line[36] - 'A';
		blocked_1[next]++;
		blocked_2[next]++;
		gates_1[gate][next] = 1;
		gates_2[gate][next] = 1;
	}
	
	while (done)
	{
		for (int i = 0; i < LETTERS; i++)
		{
			if (!blocked_1[i])
			{
				blocked_1[i]--;
				p1 += ('A' + i);
				done--;

				for (int j = 0; j < LETTERS; j++)
				{
					if (gates_1[i][j])
					{
						blocked_1[j]--;
					}
				}
				break;
			}
		}
	}

	done = LETTERS;

	while (done)
	{
		for (int i = 0; i < LETTERS; i++)
		{
			if (!blocked_2[i])
			{
				for (int j = 0; j < NB; j++)
				{
					if (team[j].sleep == 0)
					{
						blocked_2[i]--;
						team[j].sleep = DELAY + i + 1;
						for (int k = 0; k < LETTERS; k++)
						{
							if (gates_2[i][k])
							{
								team[j].open[k] = true;
							}
						}
						break;
					}
				}
			}
		}
		
		for (int j = 0; j < NB; j++)
		{
			team[j];
			if (team[j].sleep)
			{
				team[j].sleep--;
				if (team[j].sleep == 0)
				{
					for (int k = 0; k < LETTERS; k++)
					{
						if (team[j].open[k])
						{
							blocked_2[k]--;
							team[j].open[k] = false;
						}
					}
					done--;
				}
			}
		}
		p2++;
	}

	std::cout << p1 << " " << p2 << std::endl;
	return 0;

}