input = open('2.txt').read().strip().splitlines()

doubles = 0
triples = 0

for line in input:
    line_set = set(line)
    if len(line_set) == len(line):
        continue
    double = False
    triple = False
    for letter in line_set:
        if (not double) and line.count(letter) == 2:
            double = True
        elif (not triple) and line.count(letter) == 3:
            triple = True
    doubles += double
    triples += triple

print(doubles*triples)

for i,template in enumerate(input):
    l = len(template)
    for line in input[i+1:]:
        diffs = [template[j] != line[j] for j in range(l)]
        if sum(diffs) == 1:
            template = list(template)
            del template[diffs.index(1)]
            break
    if len(template) != l:
        break

print(''.join(template))