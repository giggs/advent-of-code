#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <utility>

typedef std::pair<int, int> coords;

typedef struct state
{
	int y;
	int x;
	int steps;
}state;

int main()
{
	std::ifstream file;
	file.open("Day 20/20.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file";
		return -1;
	}

	std::map<coords, int> room_coordinates;
	std::vector<state> state_stack;
	state position = {};
	char c;
	room_coordinates.insert({ std::make_pair(position.y, position.x), position.steps });

	while ((c = file.get()) != EOF)
	{
		switch (c)
		{
		case '^':
		case '$':
		{
			continue;
		} break;
		
		case '(':
		{
			state_stack.push_back(position);
		} break;
		
		case '|':
		{
			position = state_stack.back();
		} break;
		
		case ')':
		{
			position = state_stack.back();
			state_stack.pop_back();
		} break;
		
		default: // EWSN
		{
			if (c == 'E')
			{
				position.x++;
			}
			else if (c == 'W')
			{
				position.x--;
			}
			else if (c == 'S')
			{
				position.y--;
			}
			else
			{
				position.y++;
			}
			position.steps++;
			coords pair = std::make_pair(position.y, position.x);
			auto search = room_coordinates.find(pair);
			if (search == room_coordinates.end())
			{
				room_coordinates.insert({ pair, position.steps });
			}
			else
			{
				if (position.steps < search->second)
				{
					room_coordinates.insert({ pair, position.steps });
				}
			}
		} break;
		}
	}

	int p1 = 0;
	int p2 = 0;

	for (auto it = room_coordinates.begin(); it != room_coordinates.end(); ++it)
	{
		if (it->second > p1)
		{
			p1 = it->second;
		}
		if (it->second >= 1000)
		{
			p2++;
		}
	}
	std::cout << p1 << ' ' << p2;
}