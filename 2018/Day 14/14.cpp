#include <iostream>
#include <vector>

std::vector<int> recipes {3, 7};
int rounds = 47801;

int main()
{
	int elf_1 = 0;
	int elf_2 = 1;
	rounds += 10;
	int p2 = 0;
	
	auto size = recipes.size();

	bool searching = true;

	while (searching)
	{
		int sum = recipes[elf_1] + recipes[elf_2];
		if (sum > 9)
		{
			recipes.push_back(sum / 10);
			recipes.push_back(sum % 10);
		}
		else
		{
			recipes.push_back(sum);
		}
		size = recipes.size();
		elf_1 += (recipes[elf_1]+1);
		elf_2 += (recipes[elf_2]+1);
		elf_1 %= size;
		elf_2 %= size;

		if (size > 5)
		{
			if (recipes[size - 1] == 1 && recipes[size - 2] == 0 && recipes[size - 3] == 8
				&& recipes[size - 4] == 7 && recipes[size - 5] == 4 && recipes[size - 6] == 0)
			{
				p2 = size - 6;
				searching = false;
			}
			else if (recipes[size - 2] == 1 && recipes[size - 3] == 0 && recipes[size - 4] == 8
					&& recipes[size - 5] == 7 && recipes[size - 6] == 4 && recipes[size - 7] == 0)
			{
				p2 = size - 7;
				searching = false;
			}
		}
	}

	for (int i = 0; i < 10; i++)
	{
		std::cout << recipes[rounds - 10 + i];
	}

	std::cout << " " << p2 << std::endl;

	return 0;

}