input = [int(x) for x in open('1.txt').read().strip().replace('+','').splitlines()]

freq = 0
freqs = set()
part1, part2 = 'a', 'a'

while part2 == 'a':
    for line in input:
        freq += line
        if freq in freqs:
            part2 = freq; break
        else:
            freqs.add(freq)
    if part1 == 'a':
        part1 = freq
print(part1, part2)