input = open('5.txt').read().strip()
prev = len(input)
minimum = prev
new = 0

while prev != new:
    prev = new
    for letter in 'abcdefghijklmnopqrstuvwxyz':
        ul = letter.upper()+letter
        lu = letter+letter.upper()
        input = input.replace(ul,'').replace(lu,'')
    new = len(input)

print(new)

input = open('5.txt').read().strip()

for letter in 'abcdefghijklmnopqrstuvwxyz':
    input = open('5.txt').read().strip().replace(letter,'').replace(letter.upper(),'')
    prev = len(input)
    new = 0

    while prev != new:
        prev = new
        for letter in 'abcdefghijklmnopqrstuvwxyz':
            ul = letter.upper()+letter
            lu = letter+letter.upper()
            input = input.replace(ul,'').replace(lu,'')
        new = len(input)
    
    minimum = min(minimum, new)

print(minimum)