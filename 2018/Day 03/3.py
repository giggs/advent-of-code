from collections import defaultdict

input = open('3.txt').read().strip().replace(':','').splitlines()
squares = defaultdict(lambda: [])
claims = set(range(1, len(input)+1))
part1 = 0

for i,line in enumerate(input,1):
    _, _, topleft, dims = line.split(' ')
    left, top = topleft.split(',')
    width, height = dims.split('x')
    x = int(left)
    y = int(top)
    for dx in range(int(width)):
        for dy in range(int(height)):
            pos = (x+dx, y+dy)
            squares[pos].append(i)
            if len(squares[pos]) > 1:
                if len(squares[pos]) == 2:
                    part1 += 1
                for overlap in squares[pos]:
                    if overlap in claims:
                        claims.remove(overlap)

print(part1, (list(claims))[0])