#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

int registers[4] = { 0, 0, 0 ,0 };
int operation[4] = { 0, 0, 0 ,0 };
int temp_reg[4] = { 0, 0, 0 ,0 };
int after_registers[4] = { 0, 0, 0 ,0 };
opcodes* inst_pointers[16] = {};
typedef int (*opcodes) (int a, int b);

int addr(int A, int B)
{
	return registers[A] + registers[B];
}

int addi(int A, int B)
{
	return registers[A] + B;
}

int mulr(int A, int B)
{
	return registers[A] * registers[B];
}

int muli(int A, int B)
{
	return registers[A] * B;
}

int banr(int A, int B)
{
	return registers[A] & registers[B];
}

int bani(int A, int B)
{
	return registers[A] & B;
}

int borr(int A, int B)
{
	return registers[A] | registers[B];
}

int bori(int A, int B)
{
	return registers[A] | B;
}

int setr(int A, int B)
{
	return registers[A];
}

int seti(int A, int B)
{
	return A;
}

int gtir(int A, int B)
{
	return A > registers[B];
}

int gtri(int A, int B)
{
	return registers[A] > B;
}

int gtrr(int A, int B)
{
	return registers[A] > registers[B];
}

int eqir(int A, int B)
{
	return A == registers[B];
}

int eqri(int A, int B)
{
	return registers[A] == B;
}

int eqrr(int A, int B)
{
	return registers[A] == registers[B];
}

// The array of function pointers is basically the same as virtual functions
// If performance is important, use a switch statement instead.

opcodes instructions[] =
{
	addr, addi, mulr, muli, banr, bani, borr, bori,
	setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr
};

int opcode_numbers[16][16] = {};

void find_integers(std::string string, int* values)
{
	int n = string.size();
	std::string temp;

	for (int i = 0; i <= n; i++)
	{
		if (std::isdigit(string[i]))
		{
			temp += string[i];
		}
		else
		{
			if (temp.size() > 0)
			{
				*values++ = stoi(temp);
				temp = "";
			}
		}
	}
}


int main()
{
	std::ifstream file;
	file.open("Day 16/16.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error reading file" << std::endl;
		return 1;
	}

	std::string line;
	std::getline(file, line);

	int p1 = 0;

	for (auto & row : opcode_numbers)
	{
		for (auto & value : row)
		{
			value = -1;
		}
	}

	while (line != "")
	{
		int current = 0;
		
		find_integers(line, registers);
		std::getline(file, line);
		find_integers(line, operation);
		std::getline(file, line);
		find_integers(line, after_registers);
		std::getline(file, line);
		std::getline(file, line);

		for (int i = 0; i < 16; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				temp_reg[j] = registers[j];
			}
			temp_reg[operation[3]] = instructions[i](operation[1], operation[2]);
			int valid = 0;
			for (int k = 0; k < 4; k++)
			{
				if (temp_reg[k] == after_registers[k])
					++valid;
			}
			if (valid == 4)
			{
				current++;
				if (opcode_numbers[operation[0]][i] == -1)
				{
					opcode_numbers[operation[0]][i] = 1;
				}
			}
			else
			{
				opcode_numbers[operation[0]][i] = 0;
			}
		}
		if (current > 2)
		{
			p1++;
		}

	}

	int sum = 256;
	while (sum > 16)
	{
		sum = 0;
		for (int i = 0; i < 16; i++)
		{
			int row_sum = 0;
			for (int value : opcode_numbers[i])
			{
				row_sum += value;
			}
			if (row_sum == 1)
			{
				int index = 17;
				for (int j = 0; j < 16; j++)
				{
					if (opcode_numbers[i][j] == 1)
					{
						index = j;
						break;
					}
				}
				inst_pointers[i] = &instructions[index];
				for (int k = 0; k < 16; k++)
				{
					if (k != i)
					{
						opcode_numbers[k][index] = 0;
					}
				}
			}
			sum += row_sum;
		}

	}

	std::getline(file, line);
	std::getline(file, line);
	
	for (auto& value : registers)
	{
		value = 0;
	}

	while (line != "")
	{
		find_integers(line, operation);
		registers[operation[3]] = (*inst_pointers[operation[0]])(operation[1], operation[2]);
		std::getline(file, line);

	}
	std::cout << p1 << " " << registers[0];

}

// 258 too low
// 590 too high