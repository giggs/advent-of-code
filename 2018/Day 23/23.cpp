#include <iostream>
#include <fstream>
#include <string>
#include <queue>

#define ABS(A) ((A) > 0 ? (A) : -(A))

//split s2 at the first occurence of delimiter
//result: s1 (delimiter) s2
void splitstring(std::string& s1, std::string& s2, std::string delimiter) 
{
	size_t pos = 0;
	pos = s2.find(delimiter);
	s1 = s2.substr(0, pos);
	s2.erase(0, pos + delimiter.length());
}

struct robot
{
	int x;
	int y;
	int z;
	int radius;
};

robot robots[1000] = {};

inline bool robot_in_range(robot& r1, robot& r2) // r2 in range of r1
{
	return (ABS(r1.x - r2.x) + ABS(r1.y - r2.y) + ABS(r1.z - r2.z)) <= r1.radius;
}

struct cube
{
	int in_range;
	int min_x;
	int max_x;
	int min_y;
	int max_y;
	int min_z;
	int max_z;	
};

inline int distance_to_origin(const cube& c)
{
	return ABS((c.max_x + c.min_x) / 2) + ABS((c.max_y + c.min_y) / 2) + ABS((c.max_z + c.min_z) / 2);
}

bool operator<(const cube& a, const cube& b)
{
	//sorting by most connexions
	if (a.in_range != b.in_range)
	{
		return a.in_range < b.in_range;
	}

	//then by bigger bounding box
	else if ((a.max_x - a.min_x) != (b.max_x - b.min_x))
	{
		return (a.max_x - a.min_x) < (b.max_x - b.min_x);
	}

	//then by closest center to origin
	else
	{
		return distance_to_origin(a) > distance_to_origin(b);
	}

}

inline bool cube_in_range(cube& c, robot& r)
{
	// Check the distance from the robot to the edge of the bounding box
	int dist = 0;
	if (r.x < c.min_x)
	{
		dist += c.min_x - r.x;
	}
	else if (r.x > c.max_x)
	{
		dist += r.x - c.max_x;
	}
	if (r.y < c.min_y)
	{
		dist += c.min_y - r.y;
	}
	else if (r.y > c.max_y)
	{
		dist += r.y - c.max_y;
	}
	if (r.z < c.min_z)
	{
		dist += c.min_z - r.z;
	}
	else if (r.z > c.max_z)
	{
		dist += r.z - c.max_z;
	}
	return dist <= r.radius;
}



int main()
{
	std::ifstream file;
	file.open("Day 23/23.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file";
		return -1;
	}

	std::string line;
	getline(file, line);
	int radius = 0;
	int index = 0;
	int max = 0;
	int p1 = 0;

	for (int i = 0; line != ""; getline(file, line), i++)
	{
		robot robot;
		std::string temp;
		splitstring(temp, line, "pos=<");
		splitstring(temp, line, ",");
		
		robot.x = stoi(temp);
		max = ABS(robot.x) > max ? ABS(robot.x) : max;
		
		splitstring(temp, line, ",");
		
		robot.y = stoi(temp);
		max = ABS(robot.y) > max ? ABS(robot.y) : max;	
		
		splitstring(temp, line, ">, r=");
		
		robot.z = stoi(temp);
		max = ABS(robot.z) > max ? ABS(robot.z) : max;
		
		robot.radius = stoi(line);
		robots[i] = robot;
		if (robot.radius > radius)
		{
			radius = robot.radius;
			index = i;
		}
	}

	for (robot robot : robots)
	{
		p1 += (int)robot_in_range(robots[index], robot);
	}
	std::cout << p1 << " ";

	/* 
	Create a bounding box for all robots with a side
	the size of a power of 2. This way you can cleanly divide
	for every next step of the octree
	*/

	int temp = 1;
	while (temp < max)
	{
		temp <<= 1; 
	}
	max = temp; 
	
	std::priority_queue<cube> Groovy3DRangeRager; // Variable name thanks to ChatGPT
	cube start = {0, -max, max, -max, max, -max, max}; //No need to compute the robots in range
	Groovy3DRangeRager.push(start);
	while (!Groovy3DRangeRager.empty())
	{
		cube current = Groovy3DRangeRager.top();
		Groovy3DRangeRager.pop();
		if (current.min_x == current.max_x)
		{
			std::cout << distance_to_origin(current);
			break;
		}
		for (int z : {current.min_z, current.max_z})
		{
			for (int y : {current.min_y, current.max_y})
			{
				for (int x : {current.min_x, current.max_x})
				{
					cube next = {};
					if ((current.max_x - current.min_x) > 1)
					{
						if (x == current.min_x)
						{
							next.min_x = x;
							next.max_x = (current.min_x + current.max_x) / 2;
						}
						else
						{
							next.max_x = x;
							next.min_x = (current.min_x + current.max_x) / 2;
						}
						if (y == current.min_y)
						{
							next.min_y = y;
							next.max_y = (current.min_y + current.max_y) / 2;
						}
						else
						{
							next.max_y = y;
							next.min_y = (current.min_y + current.max_y) / 2;
						}
						if (z == current.min_z)
						{
							next.min_z = z;
							next.max_z = (current.min_z + current.max_z) / 2;
						}
						else
						{
							next.max_z = z;
							next.min_z = (current.min_z + current.max_z) / 2;
						}
					}
					else
					{
						next.min_x = next.max_x = x;
						next.min_y = next.max_y = y;
						next.min_z = next.max_z = z;
					}
					for (robot robot : robots)
					{
						next.in_range += cube_in_range(next, robot);
					}
					Groovy3DRangeRager.push(next);
				}
			}
		}
	}
} 
