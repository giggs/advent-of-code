#include <iostream>
#include <fstream>
#include <string>
#include <array>
#include <set>
#include <vector>
#include <map>

#define ABS(A) ((A) > 0 ? (A) : -(A))
#define SIZE 1264

/*
This is a naive approach. The solutions thread on reddit has several mentions of Union-find
https://en.wikipedia.org/wiki/Disjoint-set_data_structure
Should be much faster than this
*/

//split s2 at the first occurence of delimiter
//result: s1 (delimiter) s2
void splitstring(std::string& s1, std::string& s2, std::string delimiter)
{
	size_t pos = 0;
	pos = s2.find(delimiter);
	s1 = s2.substr(0, pos);
	s2.erase(0, pos + delimiter.length());
}

typedef std::array<int, 4> Vector4;
Vector4 fixed_points[SIZE];
std::map<Vector4, std::vector<Vector4>> connections;
std::set<Vector4> unconnected_points;

inline bool is_connected(Vector4 a, Vector4 b)
{
	return (ABS(a[0] - b[0]) + ABS(a[1] - b[1]) + ABS(a[2] - b[2]) + ABS(a[3] - b[3])) <= 3;
}

void remove_connected_points(Vector4 point)
{
	unconnected_points.erase(point);
	auto search = connections.find(point);
	if (search == connections.end())
	{
		std::cout << "No connections";
		return;
	}
	auto links = search->second;
	for (auto p : links)
	{
		auto search_p = unconnected_points.find(p);
		if (search_p != unconnected_points.end())
		{
			auto node = *search_p;
			remove_connected_points(node);
		}
	}
}

int main()
{
	std::ifstream file;
	file.open("Day 25/25.txt", std::ifstream::in);
	if (!file.good())
	{
		std::cout << "Error opening file";
		return -1;
	}

	char c;
	std::string line;
	std::string temp;
	getline(file, line);

	for (int i = 0; line != ""; getline(file, line), i++)
	{
		Vector4 point;
		splitstring(temp, line, ",");
		fixed_points[i][0] = stoi(temp);
		splitstring(temp, line, ",");
		fixed_points[i][1] = stoi(temp);
		splitstring(temp, line, ",");
		fixed_points[i][2] = stoi(temp);
		fixed_points[i][3] = stoi(line);
		unconnected_points.insert(fixed_points[i]);
	}

	for (Vector4 a : fixed_points)
	{
		std::vector<Vector4> connected;
		for (Vector4 b : fixed_points)
		{
			if (a != b && is_connected(a, b))
			{
				connected.push_back(b);
			}
		}
		connections.insert({ a, connected });
	}

	int constellations = 0;
	while (!unconnected_points.empty())
	{
		Vector4 base = *unconnected_points.begin();
		remove_connected_points(base);
		constellations++;
	}

	std::cout << constellations;

	return 0;

}

// 318 too low