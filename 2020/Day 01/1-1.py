with open('input.txt') as f:
    input = f.read().splitlines()

input = [int(x) for x in input]

if input.count(1010) == 1:
    input.remove(1010)

answer = 0

for x in input:
    for y in input:
        if x+y == 2020:
            answer = x*y


print(answer)