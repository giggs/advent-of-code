with open('input.txt') as f:
    input = f.read().splitlines()

input = [int(x) for x in input]

for x in input:
    for y in input:
        for z in input:
            if x+y+z == 2020:
                answer = x*y*z
                break

print(answer)