deck1, deck2 = open('input.txt').read().replace('Player 1:\n','').replace('Player 2:\n','').split('\n\n')

deck1 = [int(x) for x in deck1.splitlines()]
deck2 = [int(x) for x in deck2.splitlines()]
recursive_deck1 = deck1.copy()
recursive_deck2 = deck2.copy()

def play_Combat(p1,p2):
    while len(p1) != 0 and len(p2) != 0:
        card1 = p1.pop(0)
        card2 = p2.pop(0)
        if card1 > card2:
            p1.extend([card1, card2])
        else:
            p2.extend([card2, card1])
    return (sum(card*i for i, card in enumerate((deck1 + deck2)[::-1], 1)))

def play_Recursive_Combat(p1,p2,):
    previous1 = []
    previous2 = []
    while len(p1) != 0 and len(p2) != 0:
        if p1 in previous1 or p2 in previous2:
            return 1
        pp1, pp2 = p1.copy(), p2.copy() 
        previous1.append(pp1)
        previous2.append(pp2)
        card1 = p1.pop(0)
        card2 = p2.pop(0)
        if len(p1) >= card1 and len(p2) >= card2:
            copy1, copy2 = p1[:card1], p2[:card2]
            winner = play_Recursive_Combat(copy1,copy2)
        else:
            winner = 1 if card1 > card2 else 2
        if winner == 1:
            p1.extend([card1, card2])
        else:
            p2.extend([card2, card1])
    return 1 if len(p2) == 0 else 2

print(play_Combat(deck1, deck2))
if play_Recursive_Combat(recursive_deck1, recursive_deck2) == 1:
    print(sum(card*i for i, card in enumerate(recursive_deck1[::-1], 1)))
else:
    print(sum(card*i for i, card in enumerate(recursive_deck2[::-1], 1)))