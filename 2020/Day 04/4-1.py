with open('input.txt') as f:
    input = f.read()

input = input.replace('\n\n','$')
input = input.split('$')

fields = ['byr:', 'iyr:', 'eyr:', 'hgt:', 'hcl:', 'ecl:', 'pid:', 'cid:']

valid_passports = 0

for passport in input:
    nb_of_fields = 0
    for field in fields[:-1]:
        if field in passport:
            nb_of_fields += 1
    if nb_of_fields == 7:
        valid_passports += 1

print(valid_passports)

