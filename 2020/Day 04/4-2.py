with open('input.txt') as f:
    input = f.read()

input = input.replace('\n\n','$')
input = input.replace('\n',' ')
input = input.replace(':',' ')
input = input.split('$')

fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid', 'cid']

seven_fields = []

valid_passports = 0

for passport in input:
    nb_of_fields = 0
    for field in fields[:-1]:
        if field in passport:
            nb_of_fields += 1
    if nb_of_fields == 7:
        seven_fields.append(passport.split())

hair = '0123456789abcdef'
eye = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

for passport in seven_fields:
    checks = 0
    for i in range (0, len(passport), 2):
        field = passport[i]  
        detail = passport[i+1] 
        if field == 'byr' and 1920 <= int(detail) <= 2002:
            checks += 1
        if field == 'iyr' and 2010 <= int(detail) <= 2020:
            checks += 1
        if field == 'eyr' and 2020 <= int(detail) <= 2030:
            checks += 1
        if field == 'hgt':
            if detail[-2:] == 'in' and 59 <= int(detail[:-2]) <= 76:
                checks += 1
            elif detail[-2:] == 'cm' and 150 <= int(detail[:-2]) <= 193:
                checks += 1
        if field == 'hcl':
            if detail[0] == '#' and len(detail) == 7:
                hcl_check = 0
                for character in detail[1:]:
                    if character in hair:
                        hcl_check += 1
                if hcl_check == 6:
                    checks += 1
        if field == 'ecl':
            if len(detail) == 3 and detail in eye:
                checks += 1
        if field == 'pid' and len(detail) == 9 and detail.isnumeric:
            checks += 1
    if checks == 7:
        valid_passports += 1

print(valid_passports)