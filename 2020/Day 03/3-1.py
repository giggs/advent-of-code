with open('input.txt') as f:
    input = f.read().splitlines()

input = [list(x) for x in input]
del input[0]

nb_of_trees = 0

for line in input:
    row = input.index(line) + 1
    column = (3*row) % 31 
    if line[column] == '#':
        nb_of_trees += 1

print(nb_of_trees)