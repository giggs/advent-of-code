with open('input.txt') as f:
    input = f.read().splitlines()

def slide_down_slope(right, down, terrain):
    trees = 0
    step = 1
    for i in range(down, len(terrain), down):
        row = terrain[i]
        column = (right*step) % len(row) 
        if line[column] == '#':
            trees += 1
        step += 1

    return(trees)

parameters = [(1,1), (3,1), (5,1), (7,1), (1,2)]
nb_of_trees = []
answer = 1

for (x,y) in parameters:
    nb_of_trees.append(slide_down_slope(x,y,input))

for element in nb_of_trees:
    answer *= element

print(answer)