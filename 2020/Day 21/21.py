from collections import defaultdict

data = open('input.txt').read().splitlines()

ingredients = []
allergen_ingredient = defaultdict(lambda: set())
canonical_danegours_ingredient_list = []

for line in data:
    l_ingredients, allergens = line.split(' (contains ')
    allergens = allergens.replace(')','').replace(' ','').split(',')
    l_ingredients = l_ingredients.split(' ')
    ingredients.extend(l_ingredients)
    for allergen in allergens:
        if len(allergen_ingredient[allergen]) == 0:
            allergen_ingredient[allergen] = set(l_ingredients)
        else:
            allergen_ingredient[allergen] = set(l_ingredients) & allergen_ingredient[allergen]
        
while sum([len(value) for value in allergen_ingredient.values()]) != len(allergen_ingredient):
    for key, value in allergen_ingredient.items():
        if len(value) == 1:
            for k,v in allergen_ingredient.items():
                if k != key and list(value)[0] in v:
                    v = v.remove(list(value)[0])

for key,value in allergen_ingredient.items():
    ingredients = [i for i in ingredients if i != list(value)[0]]

for allergen in sorted(allergen_ingredient):
    canonical_danegours_ingredient_list.append(list(allergen_ingredient[allergen])[0])

print(len(ingredients))
print(','.join(canonical_danegours_ingredient_list))