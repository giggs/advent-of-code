input = [int(x) for x in '872495136']

cups = {}

for i in range(len(input)):
    cups[input[i]] = input[i+1] if i < len(input)-1 else input[0]

def play_cups(n,cups,ceiling):
    current = input[0]
    for move in range(n):
        one = cups[current]
        two = cups[one]
        three = cups[two]
        dest = current
        found = False
        while not found:
            dest = dest - 1 if dest > 1 else ceiling
            if dest != three and dest != two and dest != one:
                found = True
        link = cups[three]
        cups[current] = link
        otherlink = cups[dest] 
        cups[dest] = one
        cups[three] = otherlink
        current = link
    return cups

cups = play_cups(100,cups,9)
p1 = []
source = cups[1]
while cups[source] != 1:
    p1.append(source)
    source = cups[source]
p1.append(source)
p1 = ''.join([str(x) for x in p1])
print(p1)

cups = {}
for i in range(len(input)):
    cups[input[i]] = input[i+1] if i < len(input)-1 else 10
for x in range(10,1000000):
    cups[x] = x+1
cups[1000000] = input[0]

cups = play_cups(10000000,cups,1000000)
p2 = cups[1]*cups[cups[1]]
print(p2)