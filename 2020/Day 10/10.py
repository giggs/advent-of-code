with open('input.txt') as f:
    input = sorted(int(x) for x in f.read().splitlines())

input.insert(0,0)
input.append(input[-1]+3)
differences = []

for i in range(1, len(input)):
    differences.append(input[i]-input[i-1])

print(differences.count(1)*differences.count(3))

combinations = [1, 1, 1, 2, 4]

for i in range(5, len(input)):
    combinations.append(combinations[i-1]+combinations[i-2]+combinations[i-3])

def how_many_cont(n,data):
    count = 1
    while True:
        n += 1
        if data[n] - data[n-1] == 1:
            count +=1
        else:
            break
        
    return count

arrangements = 1

j = 0

while j < len(input)-1:
    arrangements *= combinations[how_many_cont(j,input)]
    j += how_many_cont(j,input)

print(arrangements)