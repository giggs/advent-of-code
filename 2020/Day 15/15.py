input = [2,20,0,4,1,17]

game = {input[i]: -(i+1) for i in range(len(input))}

j = len(input)
last_number = input[-1]
   
while j < 30000000:
    j += 1
    if game[last_number] < 0:
        if game[0] < 0: 
            game[0] = (-game[0])
        last_number = 0
    else:  
        diff = (j-1) - game[last_number]
        if diff not in game:
            game[diff] = (-j)
        elif game[diff] < 0:
            game[diff] = (-game[diff])
        game[last_number] = j-1
        last_number = diff
    if j == 2020:
        print(last_number)
print(last_number)


## The code below was written by /u/shamrin. It's the most elegant I found
## I'm keeping a copy for inspiration

# steps, ns = 30000000, [16,11,15,0,1,7]
# last, c = ns[-1], {n: i for i, n in enumerate(ns)}
# for i in range(len(ns) - 1, steps - 1):
#     c[last], last = i, i - c.get(last, i)
# print(last)