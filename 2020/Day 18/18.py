input = [eq for eq in open('input.txt').read().splitlines()]

def part_1(string):
    string = string.replace(' ',')').replace('*)','*').replace('+)','+')
    count = string.count(')')
    temp = []
    for _ in range(count+1):
        temp.append('(')
    temp.extend([string,')'])
    string = ''.join(temp)
    return eval(string)

def part_2(string):
    string = string.split(' ')
    string.append('#')
    i = 0
    while string[i] != '#':
        if not string[i] == '+':
            i += 1
        else:
            j = i+1
            if string[j] == '(':
                while string[j] != ')':
                    j += 1
            string.insert(j+1,')')
            j = i-1
            if string[j] == ')':
                while string[j] != '(':
                    j -= 1             
            string.insert(j,'(')
            i += 2
    del string[-1]
    string = ''.join(string)
    return eval(string)  
    
def evaluate(data,part):
    part_function = part_1 if part else part_2
    answer = 0
    for line in input:
        line = line
        while line.count('(') != 0:
            indices = [i for i, x in enumerate(line) if x == '('] 
            for i in reversed(indices):
                if line[i] == '(':
                    j = i
                    while line[j] != ')':
                        j += 1
                    substring = ''.join(line[i:j+1])
                    workstring = ''.join(substring[1:-1])
                    result = str(part_function(workstring))
                    line = line.replace(substring,result)                   
        answer += (part_function(line))
    return answer

print(evaluate(input,True))
print(evaluate(input,False))