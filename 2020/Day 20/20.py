from copy import deepcopy

data = open('input.txt').read().split('\n\n')
edges_lookup = {}
tile_lookup = {}
edges = []
corner_tiles = []
edge_tiles = []
tile_matches = {}

for tile in data:
    tile = tile.split('\n')
    ID = tile[0][5:9]
    top = ''.join(tile[1])
    bottom = ''.join(tile[-1])
    left = ''.join([line[0] for line in tile[1:]])
    right = ''.join([line[-1] for line in tile[1:]])
    edges_lookup[ID] = [top,right,bottom,left]
    actual_tile = [list(line) for line in tile[1:]]
    tile_lookup[ID] = actual_tile
    edges.extend([ID,top,right,bottom,left])

for tile in edges_lookup:
    matches = []
    for side in edges_lookup[tile]:
        reversed_side = side[::-1]
        if edges.count(side) == 2: 
            indexes = [index for index,string in enumerate(edges) if string == side]
            ID_1 = edges[indexes[0] - (indexes[0] % 5)]
            if ID_1 != tile:
                matches.append(ID_1)
            else:
                ID_2 = edges[indexes[1] - (indexes[1] % 5)]
                matches.append(ID_2)
        elif edges.count(reversed_side) == 1:
            ID_1 = edges[edges.index(reversed_side) - (edges.index(reversed_side) % 5)]
            matches.append(ID_1)
    if len(matches) == 2:
        corner_tiles.append(tile)
    elif len(matches) == 3:
        edge_tiles.append(tile)
    tile_matches[tile] = matches

part1 = 1
for x in corner_tiles:
    part1 *= int(x)
print(part1)

image = []
row = [0]*12
for _ in range(12):
    temp = row.copy()
    image.append(temp)

image[0][0] = corner_tiles[0]
matches = tile_matches[corner_tiles[0]]
image[0][1] = matches[0]
other_matches = tile_matches[matches[0]]
other_matches.remove(corner_tiles[0])
tile_matches[matches[0]] = other_matches
del matches[0]
tile_matches[corner_tiles[0]] = matches

for i in range(1,11):
    matches = tile_matches[image[0][i]]
    for match in matches:
        if match in edge_tiles or match in corner_tiles:
            image[0][i+1] = match
            other_matches = tile_matches[match]
            other_matches.remove(image[0][i])
            tile_matches[match] = other_matches
            matches.remove(match)
            tile_matches[image[0][i]] = matches

for i in range(11):
    for j in range(12):
        upper_tile = image[i][j]
        new_tile = tile_matches[upper_tile][0]
        image[i+1][j] = new_tile
        new_matches = tile_matches[new_tile]
        new_matches.remove(upper_tile)
        tile_matches[upper_tile] = new_matches
        if j > 0:
            left_tile = image[i+1][j-1]
            new_matches.remove(left_tile)
            left_matches = tile_matches[left_tile]
            left_matches.remove(new_tile)
            tile_matches[left_tile] = left_matches

true_image = deepcopy(image)

def find_matching_edge_on_right(left,right):
    left_tile = edges_lookup[left]
    right_tile = edges_lookup[right]
    found = False
    i = -1
    while not found:
        i += 1
        rotate = 0
        edge = left_tile[i]
        for j in range(len(right_tile)):
            if edge == right_tile[j] or edge == right_tile[j][::-1]:
                if i == 0: rotate = 1
                elif i == 2: rotate = 3
                elif i == 3: rotate = 2
                found = True
                break
    return rotate

def find_matching_edge_on_left(left,right):
    edge = edges_lookup[left][1] #right edge
    right_tile = edges_lookup[right]
    rotate = 0
    for j in range(len(right_tile)): 
        if edge == right_tile[j] or edge == right_tile[j][::-1]:
            if j == 0: rotate = 3
            elif j == 1: rotate = 2
            elif j == 2: rotate = 1
    return rotate

def find_matching_edge_on_top(top,bottom):
    edge = edges_lookup[top][2] #bottom edge
    bottom_tile = edges_lookup[bottom]
    rotate = 0
    for j in range(len(bottom_tile)):
        if edge == bottom_tile[j] or edge == bottom_tile[j][::-1]:
            if j == 3: rotate = 1
            elif j == 2: rotate = 2
            elif j == 1: rotate = 3
    return rotate

def rotate_cw(number,source):
    new_lookup = deepcopy(source)
    while number > 0: 
        j = len(source)-1
        for row in source:
            for i in range(len(row)):
                new_lookup[i][j] = row[i]
            j -= 1
        number -= 1
        source = deepcopy(new_lookup)
    return new_lookup

def change_edges_lookup(ID):
    tile = tile_lookup[ID]
    top = ''.join(tile[0])
    bottom = ''.join(tile[-1])
    left = ''.join([line[0] for line in tile])
    right = ''.join([line[-1] for line in tile])
    edges_lookup[ID] = [top,right,bottom,left]
    return edges_lookup[ID]

def do_you_v_flip(left,right):
    left_tile = ''.join([line[-1] for line in tile_lookup[left]])
    right_tile = ''.join([line[0] for line in tile_lookup[right]])
    return not left_tile == right_tile

def v_flip(tile):
    new = []
    for j in reversed(range(len(tile))):
        new.append(tile[j])
    return new

def complete_line(j):
    for i in range (1,12):
        left_image = image[j][i-1]
        right_image = image[j][i]
        rotate = find_matching_edge_on_left(left_image,right_image)
        tile_lookup[right_image] = rotate_cw(rotate,tile_lookup[right_image])
        if do_you_v_flip(left_image,right_image):
            tile_lookup[right_image] = v_flip(tile_lookup[right_image])
        change_edges_lookup(right_image)
        true_image[j][i] = tile_lookup[right_image]

def sea_monster_coordinates(r,c):
    coordinates = [(r,c), (r+1,c+1), (r+1,c+4), (r,c+5), (r,c+6), (r+1,c+7), (r+1,c+10), (r,c+11), (r,c+12), (r+1,c+13), (r+1, c+16), (r, c+17), (r, c+18), (r-1,c+18), (r,c+19)]
    return coordinates

def look_for_snakes(data):
    found = False
    for m in range(1, len(data)-1):
        for n in range(0, len(data[0])-19):
            coordinates = sea_monster_coordinates(m,n)
            count = 0
            for (x,y) in coordinates:
                if data[x][y] == '#':
                    count += 1
            if count == len(coordinates):
                found = True
                for (x,y) in coordinates:
                    data[x][y] = 'O'
    return data, found

rotate = find_matching_edge_on_right(image[0][0],image[0][1])
tile_lookup[image[0][0]] = rotate_cw(rotate,tile_lookup[image[0][0]])
change_edges_lookup(image[0][0])
true_image[0][0] = tile_lookup[image[0][0]]
complete_line(0)

for k in range(1,12):
    top_image = image[k-1][0]
    bottom_image = image[k][0]
    rotate = find_matching_edge_on_top(top_image,bottom_image)
    tile_lookup[bottom_image] = rotate_cw(rotate,tile_lookup[bottom_image])
    if ''.join(tile_lookup[top_image][-1]) != ''.join(tile_lookup[bottom_image][0]):
        tile_lookup[bottom_image] = v_flip(tile_lookup[bottom_image])
        tile_lookup[bottom_image] = rotate_cw(2,tile_lookup[bottom_image])
    change_edges_lookup(bottom_image)
    true_image[k][0] = tile_lookup[bottom_image]
    complete_line(k)

for row in true_image:
    for tile in row:
        del tile[0],tile[-1]
        for line in tile:
            del line[0],line[-1]

actual_true_image = []
for test in true_image:
    for i in range(len(test[0])):
        row = []
        for tile in test:
            row.extend(tile[i])
        actual_true_image.append(row)        

for _ in range(2):
    for __ in range(4):
        actual_true_image,found = look_for_snakes(actual_true_image)
        if found:
            final_map = []
            for row in actual_true_image:
                final_map.append(''.join(row))
            final_map = '\n'.join(final_map)
            print(final_map)
            print(final_map.count('#'))
            break
        actual_true_image = rotate_cw(1,actual_true_image)
    actual_true_image = v_flip(actual_true_image)