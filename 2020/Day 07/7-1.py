with open('input.txt') as f:
    input = f.read()

input = input.replace('bags','')
input = input.replace('bag','')
input = input.replace('.\n','$')
input = input.replace('contain','$')
for letter in '123456789 ':
    input = input.replace(letter,'')
input = input.split('$')

bags = ['shinygold']

i = 0

while i < len(bags):
    bag = bags[i]
    for j in range(1,len(input), 2):
        content = input[j]
        if bag in content:
            bags.append(input[j-1])
    i += 1

print(len(set(bags))-1)