with open('input.txt') as f:
    input = f.read()

input = input.replace('bags','')
input = input.replace('bag','')
input = input.replace('.\n','$')
input = input.replace('contain','$')
input = input.replace('1','1,')
input = input.replace('2','2,')
input = input.replace('3','3,')
input = input.replace('4','4,')
input = input.replace('5','5,')
input = input.replace('6','6,')
input = input.replace('7','7,')
input = input.replace('8','8,')
input = input.replace('9','9,')
input = input.replace(' ','')
input = input.split('$')

bags = [(1,'shinygold')]
i = 0

while i < len(bags):
    bag = bags[i]
    for j in range(0, len(input), 2):
        container = input[j]
        content = input[j+1].split(',')
        if content == 'noother':
            break
        if bag[1] == container:
            for k in range(0, len(content)-1, 2):
                nb = int((content[k]))*int(bag[0])
                bags.append((nb,content[k+1]))
            break
    i += 1

print(sum([bag[0] for bag in bags])-1)