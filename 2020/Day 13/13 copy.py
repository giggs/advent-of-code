my_arrival = int(open('input.txt').read().splitlines()[0])
schedule = [int(x) for x in open('input.txt').read().splitlines()[1].replace('x','1').split(',')]
bus_IDs = [x for x in schedule if x != 1]
departures = [ID * (my_arrival // ID + 1) for ID in bus_IDs]

print(bus_IDs[departures.index(min(departures))]*(min(departures)-my_arrival)) # part 1

time, increment = 1, 1

for i in range(len(bus_IDs)):
    while (time + schedule.index(bus_IDs[i])) % bus_IDs[i] != 0:
        time += increment
    increment *= bus_IDs[i]

print(time) # part 2