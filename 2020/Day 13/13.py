with open('input.txt') as f:
    input = f.read()
# part 1
input = input.replace('x,','')
input = input.replace('\n',',')
input = input.split(',')
input = [int(x) for x in input]

times = []
earliest = input[0]
bus_IDs = input[1:]

for bus in bus_IDs:
    x = earliest
    while x % bus != 0:
        x += 1
    times.append(x-earliest)

bus = input[times.index(min(times)) + 1]

print(bus*min(times))

with open('input.txt') as f:
    input = f.read().splitlines()
# part 2
start = int(input[0])
bus_schedule = input[1].replace('x','1')
bus_schedule = bus_schedule.split(',')
bus_schedule = [int(x) for x in bus_schedule]

differences = []
d = 1

for bus in bus_schedule[1:]:
    if bus == 1:
        d += 1
    else:
        differences.append(d)
        d = 1

print(bus_IDs)
print(differences)

# Notice that bus 29 is 29 minutes away from 409. Likewise, 17, 19 and 23 are the corresponding
# number of minutes away. 
# Therefore, the timestamp is a multiple of lcm(17,19,23,29,409) - 29.
# Luckily, these are all prime.

lcm = 17*19*23*29*409

i = lcm
search = True

while search:
        
    if (i-6)%37 == 0:
        if (i-29)%29 == 0:
            if (i+17)%17 == 0:
                if (i+18)%13 == 0:
                    if (i+19)%19 == 0:
                        if (i+23)%23 == 0:
                            if (i+31)%353 == 0:
                                if (i+72)%41 == 0:
                                    print(i-29)
                                    search = False
                                else:
                                    i += lcm
                            else:
                                i += lcm
                        else:
                            i += lcm
                    else:
                        i += lcm
                else:
                    i += lcm
            else:
                i += lcm
        else:
            i += lcm                        
    else:
        i += lcm