from collections import defaultdict

input = [list(line) for line in open('input.txt').read().replace('.','0').replace('#','1').splitlines()]
for row in input:
    for i in range(len(row)):
        row[i] = int(row[i])

pocket_d1 = defaultdict(lambda: 0)
column = [-1, len(input[0])+1]
row = [-1, len(input)+1]
depth, whatever = [-1, 2], [-1, 2]

for y in range(len(input)):
    for x in range(len(input[0])):
        z, w = 0, 0
        pocket_d1[(x,y,z,w)] = input[y][x]

pocket_d2 = pocket_d1.copy()
count = 0

while count < 6:
    for x in range(column[0], column[1]):
        for y in range(row[0], row[1]):
            for z in range(depth[0], depth[1]):
                for w in range(whatever[0], whatever[1]):
                    neighbours = 0
                    for dx in [-1,0,1]:
                        for dy in [-1,0,1]:
                            for dz in [-1,0,1]:
                                for dw in [-1,0,1]:
                                    if not (dx == dy == dz == dw == 0):
                                        if pocket_d1[x+dx,y+dy,z+dz,w+dw] == 1:
                                            neighbours += 1
                    if pocket_d1[x,y,z,w] == 1:
                        if 2 <= neighbours <= 3:
                            pocket_d2[x,y,z,w] = 1
                        else: 
                            pocket_d2[x,y,z,w] = 0                       
                    elif pocket_d1[x,y,z,w] == 0:
                        if neighbours == 3:
                            pocket_d2[x,y,z,w] = 1
                        else:
                            pocket_d2[x,y,z,w] = 0
    pocket_d1 = pocket_d2.copy()
    count += 1
    column[0], row[0], depth[0], whatever[0] = column[0]-1, row[0]-1, depth[0]-1, whatever[0]-1
    column[1], row[1], depth[1], whatever[1] = column[1]+1, row[1]+1, depth[1]+1, whatever[1]+1

print(sum(pocket_d1.values())) # Remove everything related to the 4th dimension for part 1