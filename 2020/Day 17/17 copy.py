input = [list(line) for line in open('input.txt').read().splitlines()]

pocket = set()
column = [-1, len(input[0])+1]
row = [-1, len(input)+1]
depth, whatever = [-1, 2], [-1, 2]

for y in range(len(input)):
    for x in range(len(input[0])):
        if input[y][x] == '#':
            pocket.add((x,y,0,0))

for _ in range(6):
    new_pocket = set()
    for x in range(column[0], column[1]):
        for y in range(row[0], row[1]):
            for z in range(depth[0], depth[1]):
                for w in range(whatever[0], whatever[1]):
                    neighbours = 0
                    for dx in [-1,0,1]:
                        for dy in [-1,0,1]:
                            for dz in [-1,0,1]:
                                for dw in [-1,0,1]:
                                    if not (dx == dy == dz == dw == 0):
                                        if (x+dx,y+dy,z+dz,w+dw) in pocket:
                                            neighbours += 1
                    if neighbours == 3:
                        new_pocket.add((x,y,z,w))                    
                    elif (x,y,z,w) in pocket and neighbours == 2:
                        new_pocket.add((x,y,z,w))                                  
    pocket = new_pocket
    column[0], row[0], depth[0], whatever[0] = column[0]-1, row[0]-1, depth[0]-1, whatever[0]-1
    column[1], row[1], depth[1], whatever[1] = column[1]+1, row[1]+1, depth[1]+1, whatever[1]+1

print(len(pocket)) # Remove everything related to the 4th dimension for part 1