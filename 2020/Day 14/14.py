import itertools

input = open('input.txt').read().replace('\n',' = ')
input = input.replace('mem[','')
input = input.replace(']','')
input = input.split(' = ')

memory_1 = {}
memory_2 = {}
mask = ''

for i in range(0, len(input), 2):
    if input[i] == 'mask':
        mask = input[i+1]
    else:
        memory_1[input[i]] = (int(input[i+1]) | int(mask.replace('X','0'), 2)) & int(mask.replace('X', '1'), 2)

for i in range(0, len(input), 2):
    if input[i] == 'mask':
        mask = input[i+1]
    else:
        result = list((bin(int(input[i]) | int(mask.replace('X','0'), 2)))[2:])
        while len(result) < 36:
            result.insert(0,'0')
        for combination in itertools.product('01', repeat = mask.count('X')):
            m = 0
            for l in range(len(mask)):
                if mask[l] == 'X':
                    result[l] = combination[m]
                    m += 1
            memory_2[int(''.join(result), 2)] = int(input[i+1])

print(sum(memory_1.values()))
print(sum(memory_2.values()))


## The code below was written by viliampucik (https://github.com/viliampucik/)
## It successfully implements ideas I wasn't able to get to work. I'm keeping it for inspiration

# import fileinput


# def write(memory, mask, address, value):
#     if "X" in mask:
#         i = mask.index("X")
#         write(memory, mask[:i] + "0" + mask[i+1:], address, value)
#         write(memory, mask[:i] + "1" + mask[i+1:], address, value)
#     else:
#         memory[int(mask, 2) | address] = value


# m1 = {}
# m2 = {}
# mask = and_mask = or_mask = not_mask = None

# for line in fileinput.input():
#     key, value = line.strip().split(" = ")
#     if key == "mask":
#         mask = value
#         and_mask = int(mask.replace("1", "0").replace("X", "1"), 2)
#         or_mask = int(mask.replace("X", "0"), 2)
#         not_mask = ~and_mask
#     else:
#         address = int(key[4:-1])
#         value = int(value)
#         m1[address] = value & and_mask | or_mask
#         write(m2, mask, address & not_mask, value)

# print(sum(m1.values()))
# print(sum(m2.values()))