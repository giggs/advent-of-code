import re

with open('input.txt') as f:
   input = f.read()

input = re.split('[-: \n]',input)
answer = 0

for i in range(2, len(input), 5):
    
    letter = input[i]
    min = int(input[i-2])
    max = int(input[i-1])+1
    password = input[i+2]
    
    if password.count(letter) in range(min, max):
        answer += 1

print(answer)