with open('input.txt') as f:
   input = f.read()

input = input.replace('-',' ')
input = input.replace(':','')
input = input.split()

answer = 0

for i in range(2, len(input), 4):
    if input[i+1].count(input[i]) in range(int(input[i-2]), int(input[i-1])+1):
        answer += 1

print(answer)