import re

with open('input.txt') as f:
   input = f.read()

input = re.split('[-: \n]',input)
answer = 0

for i in range(2, len(input), 5):
    
    letter = input[i]
    position_1 = input[i+2][int(input[i-2])-1]
    position_2 = input[i+2][int(input[i-1])-1]
    
    letter_in_p1 = position_1 == letter
    letter_in_p2 = position_2 == letter
        
    if letter_in_p1 ^ letter_in_p2:
        answer += 1

print(answer)