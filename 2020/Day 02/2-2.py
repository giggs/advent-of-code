with open('input.txt') as f:
   input = f.read()

input = input.replace('-',' ')
input = input.replace(':','')
input = input.split()

answer = 0

for i in range(2, len(input), 4):
    if input[i+1][int(input[i-2])-1] == input[i] and input[i+1][int(input[i-1])-1] != input[i] or input[i+1][int(input[i-1])-1] == input[i] and input[i+1][int(input[i-2])-1] != input[i]:
        answer += 1

print(answer)