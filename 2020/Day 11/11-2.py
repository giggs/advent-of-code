## Work in progress

from copy import deepcopy

seats = [list(seat) for seat in open('input.txt').read().splitlines()]

rows = len(seats)
columns = len(seats[0])

def get_adjacent_seats(r,c,data):
    
    adjacent_seats = []
    x = r
    y = c
    
    while len(adjacent_seats) == 0:
        if x == 0 :
            adjacent_seats.append((0,8))
        else:
            x -= 1
            while x > 0 and data[x][y] == '.':
                x -= 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 1:
        if x == rows-1 :
            adjacent_seats.append((0,8))
        else:
            x += 1
            while x < rows-1 and data[x][y] == '.':
                x += 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 2:
        if y == 0 :
            adjacent_seats.append((0,8))
        else:
            y -= 1
            while y > 0 and data[x][y] == '.':
                y -= 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 3:
        if y == columns-1 :
            adjacent_seats.append((0,8))
        else:
            y += 1
            while y < columns-1 and data[x][y] == '.':
                y += 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 4:
        if x == 0 or y == 0:
            adjacent_seats.append((0,8))
        else:
            x -= 1
            y -= 1
            while x > 0 and y > 0 and data[x][y] == '.':
                x -= 1
                y -= 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 5:
        if x == rows-1 or y == columns-1:
            adjacent_seats.append((0,8))
        else:
            x += 1
            y += 1
            while x < rows-1 and y < columns-1 and data[x][y] == '.':
                x += 1
                y += 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 6:
        if x == 0 or y == columns-1:
            adjacent_seats.append((0,8))
        else:
            x -= 1
            y += 1
            while x > 0 and y < columns-1 and data[x][y] == '.':
                x -= 1
                y += 1
            adjacent_seats.append((x,y))
    x = r
    y = c
    while len(adjacent_seats) == 7:
        if x == rows-1 or y == 0:
            adjacent_seats.append((0,8))
        else:
            x += 1
            y -= 1
            while x < rows-1 and y > 0 and data[x][y] == '.':
                x += 1
                y -= 1
            adjacent_seats.append((x,y))

    return adjacent_seats

def run_automata(data):
    worklist = deepcopy(data)
    for i in range(0,rows):
        for j in range(0,columns):
            adjacent = get_adjacent_seats(i,j,data)
            adjacent = [data[x][y] for (x,y) in adjacent]
            if data[i][j] == 'L' and adjacent.count('#') == 0:
                worklist[i][j] = '#'
            elif data[i][j] == '#' and adjacent.count('#') > 4:
                worklist[i][j] = 'L'
    return worklist

new_seats = deepcopy(seats)
new_seats = run_automata(new_seats)

while new_seats != seats:
    seats = deepcopy(new_seats)
    new_seats = run_automata(new_seats)

print(sum(row.count('#') for row in seats))