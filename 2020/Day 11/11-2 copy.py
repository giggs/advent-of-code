from copy import deepcopy

seats = [list(seat) for seat in open('input.txt').read().splitlines()]

rows,columns = len(seats),len(seats[0])

def get_adjacent_seats(r,c,data):
    
    adjacent_seats = []
 
    x,y = r,c
    if x > 0:
        x -= 1
        while x > 0 and data[x][y] == '.':
            x -= 1
        adjacent_seats.append((x,y))

    x,y = r,c
    if x < rows-1:
        x += 1
        while x < rows-1 and data[x][y] == '.':
            x += 1
        adjacent_seats.append((x,y))

    x,y = r,c
    if y > 0:
        y -= 1
        while y > 0 and data[x][y] == '.':
            y -= 1
        adjacent_seats.append((x,y))

    x,y = r,c
    if y < columns-1:
        y += 1
        while y < columns-1 and data[x][y] == '.':
            y += 1
        adjacent_seats.append((x,y))

    x,y = r,c
    if x > 0 and y > 0:
        x -= 1
        y -= 1
        while x > 0 and y > 0 and data[x][y] == '.':
            x -= 1
            y -= 1
        adjacent_seats.append((x,y))

    x,y = r,c
    if x < rows-1 and y < columns-1:
        x += 1
        y += 1
        while x < rows-1 and y < columns-1 and data[x][y] == '.':
            x += 1
            y += 1
        adjacent_seats.append((x,y))
 
    x,y = r,c
    if x > 0 and y < columns-1:
        x -= 1
        y += 1
        while x > 0 and y < columns-1 and data[x][y] == '.':
            x -= 1
            y += 1
        adjacent_seats.append((x,y))
 
    x,y = r,c
    if x < rows-1 and y > 0:
        x += 1
        y -= 1
        while x < rows-1 and y > 0 and data[x][y] == '.':
            x += 1
            y -= 1
        adjacent_seats.append((x,y))

    return adjacent_seats

def run_automata(source,dest):
    
    for i in range(rows):
        for j in range(columns):
            if source[i][j] == '.':
                continue
            adjacent = [source[x][y] for (x,y) in get_adjacent_seats(i,j,source)]
            if source[i][j] == 'L' and adjacent.count('#') == 0:
                dest[i][j] = '#'
            elif source[i][j] == '#' and adjacent.count('#') > 4:
                dest[i][j] = 'L'
            else:
                dest[i][j] = source[i][j]

worklist = deepcopy(seats)
run_automata(worklist,seats)

while worklist != seats:
    run_automata(seats,worklist)
    if worklist != seats:
        run_automata(worklist,seats)

print(sum(row.count('#') for row in seats))