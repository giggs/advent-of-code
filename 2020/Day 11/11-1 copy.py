from copy import deepcopy

seats = [list(seat) for seat in open('input.txt').read().splitlines()]

rows = len(seats)
columns = len(seats[0])

def get_adjacent_seats(r,c):
    
    adjacent_seats = [(r-1,c-1),(r-1,c),(r-1,c+1),(r,c-1),(r,c+1),(r+1,c-1),(r+1,c),(r+1,c+1)]

    for i,(x,y) in enumerate(adjacent_seats):
        if x < 0 or x > rows-1 or y < 0 or y > columns-1:
            adjacent_seats[i] = (0,8)
 
    return adjacent_seats

def run_automata(source,dest):
    
    for i in range(rows):
        for j in range(columns):
            if source[i][j] == '.':
                continue
            adjacent = [source[x][y] for (x,y) in get_adjacent_seats(i,j)]
            if source[i][j] == 'L' and adjacent.count('#') == 0:
                dest[i][j] = '#'
            elif source[i][j] == '#' and adjacent.count('#') > 3:
                dest[i][j] = 'L'
            else:
                dest[i][j] = source[i][j]

worklist = deepcopy(seats)
run_automata(worklist,seats)

while worklist != seats:
    run_automata(seats,worklist)
    if not worklist == seats:
        run_automata(worklist,seats)

print(sum(row.count('#') for row in seats))