from copy import deepcopy

seats = [list(seat) for seat in open('input.txt').read().splitlines()]

rows = len(seats)
columns = len(seats[0])

def get_adjacent_seats(r,c):
    
    adjacent_seats = [(r-1,c-1),(r-1,c),(r-1,c+1),(r,c-1),(r,c+1),(r+1,c-1),(r+1,c),(r+1,c+1)]

    for i in range(0,len(adjacent_seats)):
        seat = adjacent_seats[i]
        x = seat[0]
        y = seat[1]
        if x < 0 or x > rows-1 or y < 0 or y > columns-1:
            adjacent_seats[i] = (0,8)
 
    return adjacent_seats

def run_automata(data):
    worklist = deepcopy(data)
    for i in range(0,rows):
        for j in range(0,columns):
            adjacent = get_adjacent_seats(i,j)
            adjacent = [data[x][y] for (x,y) in adjacent]
            if data[i][j] == 'L' and adjacent.count('#') == 0:
                worklist[i][j] = '#'
            elif data[i][j] == '#' and adjacent.count('#') > 3:
                worklist[i][j] = 'L'
    return worklist

new_seats = deepcopy(seats)
new_seats = run_automata(new_seats)

while new_seats != seats:
    seats = deepcopy(new_seats)
    new_seats = run_automata(new_seats)

print(sum(row.count('#') for row in seats))