with open('input.txt') as f:
    input = f.read().replace('\n',' ').split()

acc = 0

positions = []
pointer = 0

loop = False

while loop == False:
    if pointer in positions:
        loop = True
    else: 
        positions.append(pointer)
        if input[pointer] == 'jmp':
            pointer += 2*(int(input[pointer + 1]))
        elif input[pointer] == 'acc':
            acc += int(input[pointer + 1])
            pointer += 2
        else:
            pointer += 2

print(acc)