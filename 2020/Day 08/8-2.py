with open('input.txt') as f:
    input = f.read().replace('\n',' ').split()

def run_program(program):

    positions = []
    loop = False
    pointer = 0
    acc = 0

    while not loop and pointer < len(program):
        if pointer in positions:
            loop = True
        else: 
            positions.append(pointer)
            if program[pointer] == 'jmp':
                pointer += 2*(int(program[pointer + 1]))
            elif program[pointer] == 'acc':
                acc += int(program[pointer + 1])
                pointer += 2
            else:
                pointer += 2

    return(loop,acc)

# Part 1 result
print(run_program(input)[1])

# Part 2

for i in range(0,len(input), 2):
    workfile = input.copy()
    instruction = workfile[i]
    if instruction == 'nop':
        workfile[i] = 'jmp'
    elif instruction == 'jmp':
        workfile[i] = 'nop'
  
    result = run_program(workfile)
    if not result[0]:
        print(result[1])
        break