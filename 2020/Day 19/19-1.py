import re

instructions, messages = open('input.txt').read().split('\n\n')
instructions, list_of_rules = instructions.replace('\n',' \n'), instructions.splitlines()
rules = {}

for rule in list_of_rules:
    rule = rule.split(':')
    rules[rule[0]] = rule[1]

for _ in range(8): #This is the minimum number of substitution cycles to go through everything
    for key in rules:
        instructions = re.sub(rf" \b{key}\b ", rf" ({rules[key]} ) ", instructions)

final = sorted(instructions.splitlines())
discard, the_one_rule = final[0].split(':')
the_one_rule = the_one_rule.replace(' ','').replace('"','').replace('(a)','a').replace('(b)','b')

print(len(re.findall(rf"\b{the_one_rule}\b",messages)))