import re
# input2.txt has the new rules for 8 and 11
instructions, messages = open('input2.txt').read().split('\n\n')
instructions, list_of_rules = instructions.replace('\n',' \n'), instructions.splitlines()
rules = {}

for rule in list_of_rules:
    rule = rule.split(':')
    rules[rule[0]] = rule[1]
# After 12 cycles any string change happens after the maximum message length and is therefore irrelevant
for _ in range(12): 
    for key in rules:
        instructions = re.sub(rf" \b{key}\b ", rf" ({rules[key]} ) ", instructions)

final = sorted(instructions.splitlines())
discard, the_one_rule = final[0].split(':')
the_one_rule = the_one_rule.replace(' ','').replace('"','').replace('(a)','a').replace('(b)','b')

print(len(re.findall(rf"\b{the_one_rule}\b",messages)))