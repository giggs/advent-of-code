with open('input.txt') as f:
    input = f.read().splitlines()

def binary_split(a_list):
    lower = a_list[:len(a_list)//2]
    upper = a_list[len(a_list)//2:]
    return lower, upper

rows = list(range(0,128))
columns = list(range(0,8))
seat_IDs = []

for ID in input:
    
    possible_rows = rows
    
    for character in ID[:7]:
        lower, upper = binary_split(possible_rows)
        if character == 'F':
            possible_rows = lower
        else:
            possible_rows = upper
    
    possible_columns = columns
    
    for character in ID[7:]:
        lower, upper = binary_split(possible_columns)
        if character == 'L':
            possible_columns = lower
        else:
            possible_columns = upper
    seat_IDs.append(possible_rows[0]*8+possible_columns[0])

print(sorted(seat_IDs)[-1])

## part 2

seats = list(range(0,len(rows)*len(columns)))

nb_missing_back_seats = seats[-1] - sorted(seat_IDs)[-1]
nb_missing_front_seats = len(seats) - len(seat_IDs) - nb_missing_back_seats

aircraft_seats = seats[nb_missing_front_seats:-nb_missing_back_seats]

for seat in aircraft_seats:
    if seat not in seat_IDs:
        print(seat)

