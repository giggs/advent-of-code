## Part 1: Find the highest seat ID

with open('input.txt') as f:
    input = f.read().splitlines()

seat_IDs = []

def boarding_pass_to_seat_ID(string):
    string = string.replace('F', '0')
    string = string.replace('L', '0')
    string = string.replace('B', '1')
    string = string.replace('R', '1')
    return int(string, 2)

for ID in input:
    seat_IDs.append(boarding_pass_to_seat_ID(ID))

print(sorted(seat_IDs)[-1])

## Part 2: Find your seat ID

seats = list(range(0,2**(len(input[0]))))

nb_missing_back_seats = seats[-1] - sorted(seat_IDs)[-1]
nb_missing_front_seats = len(seats) - len(seat_IDs) - nb_missing_back_seats

aircraft_seats = seats[nb_missing_front_seats:-nb_missing_back_seats]

for seat in aircraft_seats:
    if seat not in seat_IDs:
        my_seat = seat
        print(my_seat)

## Part 3: Generate your boarding pass

bin_seat = str(bin(my_seat))
bin_seat = bin_seat[2:]
letters = []
for character in bin_seat[:7]:
    if character == '0':
        letters.append('F')
    else:
        letters.append('B')
for character in bin_seat[7:]:
    if character == '0':
        letters.append('L')
    else:
        letters.append('R')

my_boarding_pass = ""

print(my_boarding_pass.join(letters))