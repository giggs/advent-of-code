directions = [list(line) for line in open('input.txt').read().splitlines()]

tiles = set()
moves = {'e': (1,0), 'ne': (1,-1), 'se': (0,1), 'w': (-1,0), 'nw': (0,-1), 'sw': (-1,1)}

for line in directions:
    q = 0
    r = 0
    while len(line) != 0:
        instruction = line.pop(0)
        if instruction == 'n' or instruction == 's':
            instruction = ''.join([instruction, line.pop(0)])
        q += moves[instruction][0]
        r += moves[instruction][1]
    if (q,r) in tiles:
        tiles.remove((q,r))
    else:
        tiles.add((q,r))

print(len(tiles))    

for _ in range(100):
    new_tiles = set()
    for tile in tiles:
        neighbours = [(tile[0] + moves[move][0],tile[1] + moves[move][1]) for move in moves]
        for neighbour in neighbours:
            ncount = 0
            for move in moves:
                nnq = neighbour[0] + moves[move][0]
                nnr = neighbour[1] + moves[move][1]
                if (nnq,nnr) in tiles:
                    ncount += 1
            if neighbour not in tiles and ncount == 2:
                new_tiles.add(neighbour)
            elif neighbour in tiles and (ncount == 1 or ncount == 2):
                new_tiles.add(neighbour)
    tiles = new_tiles.copy()

print(len(tiles))