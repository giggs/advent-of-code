input = [(x[0], int(x[1:])) for x in open('input.txt').read().splitlines()]

ship = 0+0j
direction = 1+0j

for (x,y) in input:
    if x == 'N': ship += y * 1j
    elif x == 'S': ship -= y * 1j
    elif x == 'E': ship += y
    elif x == 'W': ship -= y
    elif x == 'L': direction *= 1j ** (y / 90)
    elif x == 'R': direction *= (-1j) ** (y / 90)
    elif x == 'F': ship += direction * y 

print(int(abs(ship.real))+int(abs(ship.imag)))

ship = 0+0j
waypoint = 10+1j

for (x,y) in input:
    if x == 'N': waypoint += y * 1j
    elif x == 'S': waypoint -= y * 1j
    elif x == 'E': waypoint += y
    elif x == 'W': waypoint -= y  
    elif x == 'L': waypoint *= 1j ** (y / 90)
    elif x == 'R': waypoint *= (-1j) ** (y / 90)
    elif x == 'F': ship += waypoint * y  

print(int(abs(ship.real))+int(abs(ship.imag)))