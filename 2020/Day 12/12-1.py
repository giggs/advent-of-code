## Speedrun solution. Finished in 15min40s for part 1 and 25min48s for part 2 from opening the problem.
## Took 5 mins to notice that I had typed 'w' instead of 'W'

with open('input.txt') as f:
    input = f.read().splitlines()

worklist = []

for x in input: 
    i = x[0]
    j = int(x[1:])
    worklist.append((i,j))

x = 0
y = 0
f = 0

for (i,j) in worklist:
    if i == 'N':
        y += j
    elif i == 'S':
        y -= j
    elif i == 'E':
        x += j
    elif i == 'W':
        x -= j
    elif i == 'L':
        f -= j
        f = f%360
    elif i == 'R':
        f += j
        f = f%360
    elif i == 'F':
        if f == 0:
            x += j
        elif f == 90:
            y -= j
        elif f == 180:
            x -= j
        elif f == 270:
            y += j

print(abs(x)+abs(y))