## Speedrun solution. Finished in 15min40s for part 1 and 25min48s for part 2 from opening the problem.
## Took 5 mins to notice that I had typed 'w' instead of 'W'

with open('input.txt') as f:
    input = f.read().splitlines()

worklist = []

for x in input: 
    i = x[0]
    j = int(x[1:])
    worklist.append((i,j))

x = 0
y = 0
f = 0
waypoint =[10, 1]

for (i,j) in worklist:
    if i == 'N':
        waypoint[1] += j
    elif i == 'S':
        waypoint[1] -= j
    elif i == 'E':
        waypoint[0] += j
    elif i == 'W':
       waypoint[0] -= j
    elif i == 'L':
        if j == 90:
            tx = -waypoint[1]
            ty = waypoint[0]
            waypoint[0] = tx
            waypoint[1] = ty
        if j == 180:
            waypoint[0] = -waypoint[0]
            waypoint[1] *= -1
        if j == 270:
            tx = waypoint[1]
            ty = -waypoint[0]
            waypoint[0] = tx
            waypoint[1] = ty
    elif i == 'R':
        if j == 270:
            tx = -waypoint[1]
            ty = waypoint[0]
            waypoint[0] = tx
            waypoint[1] = ty
        if j == 180:
            waypoint[0] = -waypoint[0]
            waypoint[1] *= -1
        if j == 90:
            tx = waypoint[1]
            ty = -waypoint[0]
            waypoint[0] = tx
            waypoint[1] = ty
    elif i == 'F':
        x += j*waypoint[0]
        y += j*waypoint[1]

print(abs(x)+abs(y))