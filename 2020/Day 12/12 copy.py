input = [(x[0], int(x[1:])) for x in open('input.txt').read().splitlines()]

def read_instructions(ship,waypoint,move_waypoint): #Thank you Oswald
    coordinates = waypoint if move_waypoint else ship
    for (x,y) in input:
        if x == 'N': coordinates[1] += y
        elif x == 'S': coordinates[1] -= y
        elif x == 'E': coordinates[0] += y
        elif x == 'W': coordinates[0] -= y
        elif x in 'LR': 
            for _ in reversed(range(0, y if x == 'L' else 360 - y, 90)): # axial symmetry
                waypoint[0], waypoint[1] = -waypoint[1], waypoint[0] 
        elif x == 'F': 
            ship[0] += y * waypoint[0]
            ship[1] += y * waypoint[1]
    return abs(ship[0]) + abs(ship[1])

print(read_instructions([0,0],[1,0],False))
print(read_instructions([0,0],[10,1],True)) 