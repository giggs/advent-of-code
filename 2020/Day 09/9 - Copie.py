import timeit

with open('9.txt') as f:
    input = [int(x) for x in f.read().splitlines()]

for z in range(25, len(input)):
    number = input[z]
    has_property = False
    for x in range(z-25,z):
        for y in range(z-25,z):
            if input[x] + input[y] == number and input[x] != input[y]:
                has_property = True
          
    if has_property == False:
        print(number)
        break

def just_run(input,z):
    cont_range = [input[z-1], input[z-2]]
    z -= 2
    s= input[z]+input[z+1]
    while s != number:
        if s < number:
            z -= 1
            cont_range.append(input[z])
            s += input[z]
        elif s > number:
            s -= cont_range[0]
            del(cont_range[0])
            


    print(min(cont_range) + max(cont_range))

print(timeit.timeit('just_run(input,z)', number=1, globals=globals()))