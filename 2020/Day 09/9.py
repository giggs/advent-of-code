with open('input.txt') as f:
    input = [int(x) for x in f.read().splitlines()]

for z in range(25, len(input)):
    number = input[z]
    has_property = False
    for x in range(z-25,z):
        for y in range(z-25,z):
            if input[x] + input[y] == number and input[x] != input[y]:
                has_property = True
          
    if has_property == False:
        print(number)
        break

cont_range = []

while sum(cont_range) != number:
    if sum(cont_range) < number:
        z -= 1
        cont_range.append(input[z])
    elif sum(cont_range) > number:
        del(cont_range[0])

print(min(cont_range) + max(cont_range))