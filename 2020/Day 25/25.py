card_pk = 1614360
door_pk = 7734663

def get_loop(subject,target,loop):
    value = 1
    while value != target:
        loop += 1
        value *= subject
        value = value % 20201227
    return loop

def get_key(subject,loop):
    value = 1
    while loop > 0:
        value *= subject
        value = value % 20201227
        loop -= 1
    return value

card_loop = (get_loop(7,card_pk,0))
print(get_key(door_pk,card_loop))