import string

with open('input.txt') as f:
    input = f.read().split('\n\n')

letters = set(string.ascii_lowercase)
count = 0

for group in input:
    
    letters_in_group = letters
    for person in group.split():
        letters_in_group = letters_in_group & set(person)
    count += len(letters_in_group)

    
print(count)