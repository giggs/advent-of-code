with open('input.txt') as f:
    input = f.read()

input = input.split('\n\n')

counts = []

for group in input:
    count = 0
    worklist = group.split()
    for character in worklist[0]:
        char_count = 0
        for element in worklist:
            if character in element:
                char_count += 1
        if char_count == len(worklist):
            count += 1

    counts.append(count)

print(sum(counts))