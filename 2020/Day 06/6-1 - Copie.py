with open('input.txt') as f:
    input = f.read().split('\n\n')

print(sum(len(set(group.replace('\n',''))) for group in input))