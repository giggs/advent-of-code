import string

with open('input.txt') as f:
    input = f.read().split('\n\n')

characters = [char for char in string.ascii_lowercase]

counts = []

for group in input:
    count = 0
    for char in characters:
        if char in group:
            count += 1
    counts.append(count)

print(sum(counts))