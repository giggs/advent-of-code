with open('input.txt') as f:
    input = f.read().split('\n\n')
    
print(sum([len(set('abcdefghijklmnopqrstuvwxyz').intersection(*[set(person) for person in group.split()])) for group in input]))