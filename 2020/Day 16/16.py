input = open('input.txt').read().split('\n\n')
fields = input[0].replace(' or ','\n').replace(': ','\n').splitlines()
my_ticket = input[1].splitlines()[1]
nearby_tickets = input[2].splitlines()[1:]
ranges = [fields[i].split('-') for i in range(0,len(fields)) if i % 3 != 0]
valid_any_field = set()
valid_tickets = []
definitive_fields = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
count = 0
error_rate = 0
answer = 1

for range_ in ranges:
    min, max = int(range_[0]), int(range_[1])
    for n in range(min, max+1):
        valid_any_field.add(n)

for ticket in nearby_tickets:
    valid = True
    for value in ticket.split(','):
        if int(value) not in valid_any_field:
            error_rate += int(value)
            valid = False
            break
    if valid:
        valid_tickets.append(ticket)

print(error_rate)

while count < 20:
    for i in range(len(valid_tickets[0].split(','))):
        possible_fields = fields[::3]
        for field in definitive_fields:
            if field != 0:
                possible_fields.remove(field)
        for ticket in valid_tickets:
            value = int(ticket.split(',')[i])
            for j in range(0, len(fields), 3):
                range1 = fields[j+1].split('-')
                range2 = fields[j+2].split('-')
                if not (int(range1[0]) <= value <= int(range1[1]) or int(range2[0]) <= value <= int(range2[1])):
                    if fields[j] in possible_fields:
                        possible_fields.remove(fields[j])
        if len(possible_fields) == 1:
            definitive_fields[i] = possible_fields[0]
            count += 1
            break

for i in range(len(definitive_fields)):
    if 'departure' in definitive_fields[i]:
        answer *= int(my_ticket.split(',')[i])

print(answer)